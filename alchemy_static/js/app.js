/*
function gmapsReady(data) {
    requirejs(["app/intelligent_search"], function (search) {
        search.gmapsReady(data);
    });
}
*/
requirejs.config({
    //By default load any module IDs from js/lib
    urlArgs : "bust="+new Date().getTime(),
    baseUrl: '/personal/assets/alchemy/js/lib',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        "utils":"../utils",
        "app": "../app",
        "lbg_charting": "/personal/assets/lib/charting-min130427",
        "gmaps": "https://maps.googleapis.com/maps/api/js?v=3.11&sensor=false&libraries=places&callback=gmapsReady"
    },
    config: {
        text: {
            useXhr: function(url, protocol, hostname, port) {
                // allow cross-domain requests
                // remote server allows CORS
                return true;
            },
            onXhr: function (xhr, url) {
                //Called after the XHR has been created and after the
                //xhr.open() call, but before the xhr.send() call.
                //Useful time to set headers.
                //xhr: the xhr object
                //url: the url that is being used with the xhr object.
            }
        }
    }
});

// Start the main app logic.
requirejs(["app/mm","app/aggregation","app/intelligent_search","app/responsive","app/push"], function (mm, agg, search, responsive, push) {
    //mm.init();
    agg.init();
    //search.init();
    //responsive.init();
    //push.init();
});