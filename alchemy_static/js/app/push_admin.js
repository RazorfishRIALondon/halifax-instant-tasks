/*
*	@name: push.js
*	@description:
*		Connecting the Pubcli site to Real Time Alerts that provide disturbance and prompt interaction
*/
define(["jquery","utils/options","utils/core","utils/sessionstorage","utils/sse","utils/event","utils/localstorage","utils/notifications","utils/pagevisibility"], function($,options,core,storage,sse,broadcast,localStorage,notifications,page){
    return {

        init: function() {
            var self = this;
            
            /*
            *   Check for Storage support
            */
            if(!storage.browserDoesStorage()) {
                core.log("No support for Storage on this Device with this Browser");                
            }

            var self = this;

            this.addEventListeners();
            // Add an event registration for our AccountStatus MessageEvent type
            broadcast.addListener("Devices", function(data) {
                self.displayDevices(data);
            });

            // Now connect a socket and wait for the callback
            // Include the LBG User ID
            
            self.adminConnection = sse.connect(options.urls.pushAdminSSE, ["Devices"]);
        },

        /*
        *   @name: addEventListeners
        *   @description: Add Form event submit listeners
        */
        addEventListeners: function() {
            var self = this;
            $("form").bind("submit", function(e) {
                var form = $(this);
                if(e && e.preventDefault) e.preventDefault();

                $.ajax({
                  type: "POST",
                  url: $(this).attr("action"),
                  data: $(this).serialize(),
                  success: function() {
                    //core.log("Successful form post")
                    form[0].reset();
                  },
                  failure: function(e) {
                    core.log(e);
                  }
                });
            });

            $("form").removeClass("hide");
        },

        /*
        *   @name: displayDevices
        *   @description:
        *       Display any connected Devices
        */
        displayDevices: function(MessageEvent) {
            var self = this,
            messageDataJSON,
            devices,
            deviceList = $("#connected-devices");

            try{
                if(MessageEvent.data != "") {
                    messageDataJSON = JSON.parse(MessageEvent.data);
                    
                    if(messageDataJSON.devices) {
                        devices = messageDataJSON.devices;
                        $.each(devices, function(key, item) {
                            if($("li[data-id='"+key+"']").length == 0) {
                                var userAgent = item,
                                listItem = "<li data-id='"+key+"'";

                                if(userAgent.dist) {
                                    if(userAgent.dist.name.toLowerCase() == 'iphone') {
                                    listItem += "class='iphone'";
                                    }
                                    else if(userAgent.dist.name.toLowerCase() == 'android') {
                                        listItem += "class='android'";
                                    }
                                    else if(userAgent.dist.name.toLowerCase() == 'blackberry') {
                                        listItem += "class='blackberry'";
                                    }
                                    else {
                                        listItem += "class='generic'";
                                    }
                                }
                                else {
                                    listItem += "class='generic'";
                                }
                                
                                listItem += "><div class='data'>";
                                
                                if(userAgent.dist) {
                                    listItem += userAgent.dist.name+"<br/>";
                                }
                                else if(userAgent.flavor) {
                                    listItem += userAgent.flavor.name+"<br/>";    
                                }
                                /*
                                if(userAgent.browser) {
                                    //listItem += userAgent.browser.name+", "+userAgent.browser.version;
                                }
                                */
                                listItem += "</div></li>";
                                deviceList.append(listItem);
                            }
                        });   

                        /*
                        *   Now remove disconnected devices
                        */
                        $("li[data-id]").each(function(index, item) {
                            var remove = true,
                            scope = $(this);
                            $.each(devices, function(key, item) {
                                if(scope.attr("data-id") == key) {
                                    remove = false;
                                    return false;
                                }
                            });

                            if(remove) {
                                scope.remove();
                            }
                        });
                    }
                    else {
                        //deviceList.empty();
                    }
                } else {
                    deviceList.empty();
                    core.log("No data");
                }
            } catch(e) {
                core.log(e.message);
            }          
            
        }
    }
});