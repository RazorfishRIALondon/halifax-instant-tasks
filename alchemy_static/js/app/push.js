/*
*	@name: push.js
*	@description:
*		Connecting the IB site to Real Time Alerts that provide disturbance and prompt interaction
*/
define(["utils/options","utils/core","utils/sessionstorage","utils/sse","utils/event","utils/localstorage","utils/notifications"], function(options,core,storage,sse,broadcast,localStorage,notifications){
    return {

        init: function() {
            var self = this;
            
            /*
            *   Check for Storage support
            */
            if(!storage.browserDoesStorage()) {
                core.log("No support for Storage on this Device with this Browser");                
            }

            /*
            *   Use LocalStorage Token
            */            
            var pushToken = localStorage.getLocal("push_token");
            
            if(pushToken && pushToken != "") {
                self.listen();
            } else {
                notifications.checkPermission(function() {
                    self.requestToken();
                });
            }
            
        },

        /*
        *   @description:
        *       Requedst a new Push Token
        */
        requestToken: function() {
            var self = this;

            $.ajax({
                type:"POST",
                url:options.urls.push.requestToken,
                contentType:options.contentType,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(data) {
                    // Save the Push Token in LocalStorage
                    localStorage.setLocal("push_token", data);
                    
                    // Start listening for Push Message
                    self.listen();
                },
                error: function(e) {
                    core.log("Failed to retrieve Push Token");
                    core.log(e);
                }
            });
        },

        /*
        *   @name: demo
        *   @description:
        *       Listen for MessageEvents
        */
        listen: function() {
            var self = this;

            broadcast.addListener("custom", function(data) {
                self.handleCustomMessage(data, "custom");
            });
            
            /*
            *   Use LocalStorage Token
            */
            var pushToken = localStorage.getLocal("push_token");

            self.connection = sse.connect(options.urls.push.sse+"?push_token="+pushToken, ["custom"]);
        },

        /*
        *   @name: handleCustomMessage
        *   @description:
        *       Handle custom content messages
        */
        handleCustomMessage: function(MessageEvent, messageId) {
            var self = this
            try{
                if(MessageEvent.type == messageId && MessageEvent.data != "") {
                    var data = JSON.parse(MessageEvent.data),
                    localNotifications = JSON.parse(localStorage.getLocal("notifications"));

                    /**
                    *   If the Notification ID already exists in LocalStorage
                    *   then we either need to ignore it, or remove it 
                    */                    
                    if(localNotifications && localNotifications[data.id]) {
                        // If the data is empty, remove it from LocalStorage and from the UI
                        if(data == "") {
                            // Delete the Notification from LocalStorage
                            delete localNotifications[data.id];
                            localStorage.setLocal("notifications", JSON.stringify(localNotifications));
                        }
                    } else {
                        notifications.create({
                            id:data.id,
                            iconUrl:data.iconUrl,
                            title:data.title,
                            body:data.body,
                            url:data.url,
                            onClickCallback: function() {                                
                                // Run any Click Callbacks
                                if(data.url) {
                                    var newWindow = window.open(data.url, "LTSB");
                                    newWindow.focus();
                                }                                
                            }
                        });
                    }
                }  
            } catch(e) {
                core.log(e);
            }            
        }
    }
});