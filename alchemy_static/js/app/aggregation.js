/*
*	@name: aggregation.js
*	@description:
*		Project script for connecting to an external Credit Card provider and aggregating Account Summary and 
*		Detail information within IB
*/
define(["utils/options","utils/core","utils/sessionstorage","utils/sse","utils/event","utils/mediastream"], function(options,core,storage,sse,broadcast,mediastream){
    return {

        init: function() {
            /*
            *   Check for CORS support
            */
            if(!core.browserDoesCORS()) {
                core.log("No support for CORS on this Device with this Browser");
                return
            }
            /*
            *   Check for Storage support
            */
            if(!storage.browserDoesStorage()) {
                core.log("No support for Storage on this Device with this Browser");
                return;
            }
            
            /*
            *   Enable jQuery CORS support
            */
            $.support.cors = true;

            /*
            *   Route the view
            */
            this.routeView();
        },

        /*
        *   @description:
        *       Simple task router based on current view
        */
        routeView: function() {
            var self = this,
            url = window.location.pathname;

            /*
            *   Account Overview Page
            */
            if(core.urlContains(url, options.urls.accountsHome)) {

                /*
                *   EXPERIMENTAL: Access User Media
                */
                mediastream.accessUserMedia();

                /*
                *   Load our project CSS
                */
                core.loadCss(options.urls.css.aggregation);

                var userID = storage.getLocal(options.storageKeys.user.id),
                localAccount = storage.getLocal(options.storageKeys.aggregation.amex);

                /*
                *   If we have an account in SessionStorage go ahead and display it
                */
                if(!!localAccount) {
                    self.displayAccount(localAccount);
                } 
                /*
                *   Else if we have a userID but no account in SessionStorage, lets look up the User
                */
                else if(!!userID) {
                    self.requestAccountInit(userID);
                }
                /*
                *   Else the User has no account persisted server-side, so let's provide the CTA
                */
                else {
                    self.createAccountCTA();    
                }    
            
            }

        },

        /*
        *   @description:
        *       Create and add a new CTA for Account Aggregation
        *
        */
        createAccountCTA: function() {
            var self = this;
            
            // Request CTA
            $.ajax({
                type:"POST",
                url:options.urls.gae.accountAggregationCTA,
                contentType:options.contentType,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(html) {
                    // Inject the CTA in the top panel
                    $(".transferSlideOuter").before(html);

                    // Bind a click event to the CTA
                    self.accountAggregationCTA = $(".alchemy-account-aggregation-cta button");
                    self.accountAggregationCTA.bind("click", function(e) {
                        if(e && e.preventDefault) e.preventDefault();
                        // Collect the LBG User ID from the Button
                        var userID = $(this).attr("data-user-id");
                        // Request the Login Panel
                        self.requestAccountInit(userID);    
                    });
                },
                error: function(e) {
                    core.log("Failed to load Aggregation CTA");
                    core.log(e);
                }
            });
        },

        /*
        *	@description:
        *		Request the Login Panel for the Account
        */
        requestAccountInit: function(userID) {
            var self = this;
            $.ajax({
                type:"POST",
                url:options.urls.gae.accountAggregationInit,
                contentType:options.contentType,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                data:{
                    lbg_user_id:userID
                },
                success: function(html) {
                    // Remove the CTA
                    if(self.accountAggregationCTA && self.accountAggregationCTA.length != 0) {
                        self.accountAggregationCTA.remove();    
                    }
                    
                    // Remove any Pending dialog
                    if(self.accountPending && self.accountPending.length != 0) {
                        self.accountPending.remove();
                    }

                    // Check for Account Summary or Login in the view HTML received,
                    // Checking the parent HTML Element in the snippet
                    if($(html).hasClass("alchemy-account-summary")) {
                        // Store the HTML in SessionStorage
                        storage.setLocal(options.storageKeys.aggregation.amex, html);

                        // Display the Account Info
                        self.displayAccount(html);

                        // Animate account
                        self.animateToAccount();
                    } 
                    // Else display the Login Form
                    else {    
                        self.displayLogin(html);    
                    }
                    
                },
                error: function(e) {
                    core.log("Failed to retrieve Account Login view");
                    core.log(e);
                }
            });
        },

        /*
        *   @description:
        *       Cancel the Login before submission.
        *       Close the Form Login dialog and reinstate the CTA
        */
        cancelLogin: function() {
            var self = this;

            // Remove the parent panel
            $("#alchemy-account-aggregation-login").remove();

            // Create the CTA again
            self.createAccountCTA();

        },

        /*
        *   @description:
        *       Make the Request for Account Connection
        */
        requestAccountConnection: function() {
            var self = this;

            $.ajax({
                type:"POST",
                url:self.accountAggregationForm.attr("action"),
                data:self.accountAggregationForm.serialize(),
                contentType:options.contentType,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(html) {
                    // Add an event registration for our AccountStatus MessageEvent type
                    broadcast.addListener("AccountStatus", self.checkEventSourceResponse.bind(self));

                    // Now connect a socket and wait for the callback
                    // Include the LBG User ID
                    var lbg_user_id = self.accountAggregationForm.find("#lbg_user_id").val();
                    self.polling = sse.connect(options.urls.sseOrigin+lbg_user_id, ["AccountStatus"]);
                  
                    // Remove Login Form and its Parent panel
                    self.accountAggregationForm.parent().remove();

                    // Show the waiting dialog
                    self.displayPending(html);
                },
                error: function(e) {
                    core.log("Failed to submit Account Connection");
                    core.log(e);
                }
            });
            
        },

        /*
        *   @description:
        *       Evaluate the response from SSE
        */
        checkEventSourceResponse: function(MessageEvent) {
            var self = this;

            // If the data is empty we're not ready, otherwise...
            if(MessageEvent.data == "") return;
                
            // Disconnect the SSE socket so we stop polling for status
            sse.close();

            // Request init again with the LBG User ID provided in MessageEvent.data, to collect the Account Summary
            self.requestAccountInit(MessageEvent.data);
        },

        /*
        *   @description:
        *       Inject a Login Form into the UI
        */
        displayLogin: function(htmlView) {
            var self  = this,
            body = $("body");

            
            body.append(htmlView);

            this.accountAggregationForm = $("#alchemy-account-aggregation-login form");
            
            this.accountAggregationForm.submit(function(e) {
                if(e && e.preventDefault) e.preventDefault();

                // Remove all events from the Form
                $(this).unbind();
                // Add a placeholder event listener for Submit. Temp workaround until we handle UI state properly
                $(this).submit(function(e){if(e && e.preventDefault) e.preventDefault();});
                // Request the Account Summary
                self.requestAccountConnection();
            });

            // Add Close button event listener
            $("#alchemy-account-aggregation-login .close").bind("click", function(e) {
                if(e && e.preventDefault) e.preventDefault();

                // Reinstate the CTA
                self.cancelLogin();
            });
        },

        /*
        *	@description:
        *		Inject our Account Summary into the Account Summary List
        */
        displayAccount: function(html) {
        	var self = this;

            // Select the Account Summary List, last LI element, as we'll inject our list item before this
        	this.accountSummaryList = $("ul.myAccounts li.clearfix:last");
            this.accountSummaryList.before(html);

            this.newAccount = $(".alchemy-account-summary");            

            /*
            *   Bind Data Error Dialog close, if it exists
            */
            $(".alchemy-account-summary .close").bind("click", function(e) {
                if(e && e.preventDefault) e.preventDefault();
                $(this).parent(".alchemy-dialog-outer").remove();
            });

            /*
            *   Bind Disconnect listeners
            */
            var disconnectButtons = this.newAccount.find("*[data-action='disconnect']");
            disconnectButtons.bind("click", function(e) {
                if(e && e.preventDefault) e.preventDefault();

                // Fire Disconnect request to server
                self.disconnectAccount();             
            });

            // Set the Portrait image on the Canvas
            setTimeout(function() {
                mediastream.setImage();
            },1000);
            // Show the Portrait image
            setTimeout(function() {
                mediastream.showImage();
            },4000);
            
        },

        /*
        *   @description:
        *       Display a waiting dialog
        */
        displayPending: function(htmlView) {
            var self  = this,
            body = $("body");

            body.append(htmlView);

            // Create a reference to the Pending dialog
            self.accountPending = $("#alchemy-account-aggregation-pending");

            // Bind close event to Pending Dialog
            $("#alchemy-account-aggregation-pending .close").bind("click", function(e) {
                if(e && e.preventDefault) e.preventDefault();

                // Remove the disconnect panel
                $(this).parent().remove();
            });
        },

        /*
        *   @description:
        *       Disconnect the User and their AMEX Account from the server
        *
        */
        disconnectAccount: function() {
            var self = this,
            body = $("body");

            $.ajax({
                type:"POST",
                url:options.urls.gae.accountAggregationDisconnect,
                contentType:options.contentType,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                data:{
                    lbg_user_id:"919668954"
                },
                success: function(html) {
                    // Disconnect any SSE socket that's open
                    sse.close();

                    // Append the Disconnected view
                    body.append(html);

                    // Add Close button event listener
                    $("#alchemy-account-aggregation-disconnected .close").bind("click", function(e) {
                        if(e && e.preventDefault) e.preventDefault();

                        // Remove the disconnect panel
                        $(this).parent().remove();
                    });
                    
                    // Trash SessionStorage
                    storage.removeLocal(options.storageKeys.aggregation.amex);

                    // Remove Account from list
                    self.newAccount.remove();

                    // Create the CTA again
                    self.createAccountCTA();
                },
                error: function(e) {
                    core.log("Failed to disconnect Account");
                    core.log(e);
                }
            });
        },

        /*
        *   @description:
        *       Animate to the newly added account summary
        */
        animateToAccount: function() {
            var self = this;

            this.newAccount.css({"visibility":"hidden"});
            
            $("html, body").animate({
                 scrollTop: self.newAccount.position().top
            }, 1000, function() {
                self.newAccount.css({"visibility":"visible"});
            });
        }
    }
});