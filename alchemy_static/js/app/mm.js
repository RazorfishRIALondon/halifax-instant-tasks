/*
*	@name: mm.js
*	@description:
*		Project script for collecting and inserting the Money Manager Pie Chart data into the Accounts Home page
*/
define(["utils/options","utils/core","utils/sessionstorage","utils/mediastream","utils/event"], function(options,core,storage,mediastream,broadcast){

    return {
        init: function() {
            var self = this;

            if(!storage.browserDoesStorage()) {
                core.log("No support for Storage on this Device with this Browser");
                return;
            }

            this.routeView();       
        },

        /*
        *   @description:
        *       Simple task router based on current view
        */
        routeView: function() {
            var self = this,
            url = window.location.pathname,
            chart;

            /*
            *   Account Overview Page
            */
            if(core.urlContains(url, options.urls.accountsHome)) {


				/*
				* 	EXPERIMENTAL: Access User Media
				*/
				//mediastream.accessUserMedia();

				/*
				*	Listen for the UserMedia to be ready
				*/
				/*
				broadcast.addListener("mediastream_ready", function() {
					setTimeout(function() {
						// Set the Portrait image on the Canvas
						mediastream.setImage();
						// Show the Portrait image
						mediastream.showImage();
					},3000);					
				});
				*/
				
            	/*
            	*	Load our project CSS
            	*/
            	core.loadCss(options.urls.css.moneymanager);

                /*
                *   If we already have Chart data in session display the Chart
                */
                chart = storage.getLocal(options.storageKeys.omm.pieChart);
                if(!!chart) {
                    //core.log("Loading Chart from SessionStorage");
                    self.displayChart();
                } 
                /*
                *   Else, asynchronously load it whilst on the Accounts Home page, and then display it
                */
                else {
                    //core.log("Requesting Chart from MM Dashboard");
                    self.loadMMDashboard();
                }
                
            } 

            /*
            *   Money Manager Dashboard
            */
            else if(core.urlContains(url, options.urls.moneyManager.dashboard)) {
                /*
                *   Get MM Charts
                */
                self.collectChart();
            }
            
            /*
            *   Money Manager Bookmark
            */
            else if(core.urlContains(url, options.urls.moneyManager.bookmark)) {
                /*
                *   Get MM Charts
                */
                self.collectChart();
            }

            /*
            *   Login
            */
            else if(core.urlContains(url, options.urls.login.unauth)) {
                self.onLogin();
            }
            
            /*
            *   Logoff (Unauth logged off page)
            */
            else if(core.urlContains(url, options.urls.logoff.unauth)) {
                self.onLogoff();
            }

        },

        /*
        *   @description:
        *       Fired on log out of the IB site
        *
        */
        onLogin: function() {
            /*
            *   Remove the OMM Pie Chart data from SessionStorage whilst the Browser is still open
            */
            storage.clearLocal();
        },

        /*
        *   @description:
        *       Fired on log out of the IB site
        *
        */
        onLogoff: function() {
            /*
            *   Remove the OMM Pie Chart data from SessionStorage whilst the Browser is still open
            */
            storage.clearLocal();
        },
    
		/*
		*	@description:
		*		Collect MM Chart HTML from a page and store it locally
		*/
		collectChart: function(htmlParent) {
			/*
			*	If we have an HTML Body parent, use this to find the Charts
			*	Else use the current page DOM context
			*/
			var charts = $(htmlParent||document).find(".ommChartWrap, .ommNoTransChartWrap"),
			chartParent,
			chartHTML;

			/*
			*	Collect only the No Transactions Chart or the Chart with type 'pie'
			*/
			$.each(charts, function(i, elem) {
				if(elem.className.indexOf("ommNoTransChartWrap") != -1 || elem.className.indexOf("{type:'pie'}") != -1) {
					/*
					*	We need the Chart wrapper DIV element rather than just its contents, so inspect up one level to get the parent
					*	and then clone it
					*/
					chartParent = $(elem).parent().clone();
					/*
					*	Strip out the Header, Loading message, Actions and Rendered Chart
					*/
					chartParent.find(".ommLoader, .ommChart, h3, .actions").remove();
					/*
					*	Convert to HTML string object
					*/
					chartHTML = chartParent.html();
					/*
					*	Set in Storage
					*/
					storage.setLocal(options.storageKeys.omm.pieChart, chartHTML);
				}
			});
		},

		/*
		*	@description:
		*		Displays a Money Manager Chart from Storage, within the Accounts Home page right hand panel
		*/
		displayChart: function() {
			var self = this,
			panel,
			chart;
			
			/*
			*	Collect the Chart from local storage
			*/
			chart = storage.getLocal(options.storageKeys.omm.pieChart);
			if(!!chart) {

				/*
				*	Add the Body CSS class
				*/
				$("body").addClass("hasRaphael");

				/*
				*	Create and add the RF Chart container	
				*/
				$(".secondary .panel").prepend("<div id='rf-omm-chart'>"+chart+"</div>");				
				
				/*
				*	Call the Raphael/LBG init functions
				*/				
				self.initializeChart();				
			}
			else {
				core.log("Chart is missing in SessionStorage under this domain");
			}
		},

		/*
		*	@description:
		*		Load the Money Manager Dashboard page using XHR.
		*		Keep it off DOM and then interrogate it for data.
		*/
		loadMMDashboard: function() {
			var self = this,
			pageHTML;
			$.ajax({
				url:options.urls.moneyManager.dashboard,
				success: function(html) {
					pageHTML = $(html);
					self.collectChart(pageHTML);
					self.displayChart();
				},
				error: function(e) {
					core.log("Failed to load MM Dashboard page asynchronously. No Chart data available");
					core.log(e);
				}
			});
		},

		/*
		*	@description:
		*		Initialize Chart Table Data in the page into a RaphaelJS SVG Pie Chart
		*/
		initializeChart: function() {
			if(typeof(LBG) !== "undefined") {
				/*
				*	Initialize Chart
				*/
				if(LBG.ommIdentRaphael) LBG.ommIdentRaphael();
				if(LBG.ommDetect) LBG.ommDetect.init();				
				
				/*
				*	Hide any contextual help on Chart render
				*/
				$("#rf-omm-chart").find(".cxtHelp").hide();

			}
		}
	}
});