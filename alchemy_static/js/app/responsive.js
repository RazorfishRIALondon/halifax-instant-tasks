/*
*	@name: responsive.js
*	@description:
*		Project script for making the existing IB site responsive
*/
define(["utils/options","utils/core","utils/sessionstorage","utils/event"], function(options,core,storage,broadcast){

    return {
        init: function() {
            var self = this;

            this.routeView();            
        },

        /*
        *   @description:
        *       Simple task router based on current view
        */
        routeView: function() {
            var self = this,
            url = window.location.pathname,
            chart;

            /*
            *   Account Overview Page
            */
            if(core.urlContains(url, options.urls.accountsHome)) {
            	/*
            	*	Load our project CSS
            	*/
            	core.loadCss(options.urls.css.responsive);
            } 

            if(core.urlContains(url, options.urls.login.unauth)) {
                /*
                *   Load our project CSS
                */
                core.loadCss(options.urls.css.responsive);
                /*
                *   Restyle UI
                */
                this.restyleLogin();
            }             
        },

        restyleLogin: function() {
            $(".loginActions input.submitAction").removeAttr("src");
            $(".loginActions input.submitAction").attr("type","submit");
        }
    }
    
});