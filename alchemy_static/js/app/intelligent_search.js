/*
*	@name: intelligent_Search.js
*	@description:
*		Provides Intelligent Search and Results features
*/
define(["utils/options","utils/core","utils/sessionstorage","utils/event"], function(options,core,storage,broadcast){

    return {
        init: function() {
            var self = this;
            
            if(!storage.browserDoesStorage()) {
                core.log("No support for Storage on this Device with this Browser");
                return;
            }

            this.queryTriggers = {
                query_change_address:["move","address","home","work","billing"],
                query_utility_power:["gas","electricity","elec","power","utility","british gas","npower","edf"],
                query_utility_telecomms:["internet","wi-fi","wifi","3g","4g","broadband","phone","mobile","telephone","o2","orange","vodafone","three","t-mobile","cell"],
                query_transfer:["transfer"],
                query_pay:["pay","buy","standing","order","debit","direct","send","wire"],
                query_account_balance:["balance","limit","credit","account","savings"]
            }

            /*
            1. Change my address
            2. Review my gas bill
            3. When to gat an ISA (advice on a financial product) - no info yet
            4. Pay the Cleaner (local)
            5. Pay back Rebecca (international payment)
            6. Check Balance
            7. Transfer money internally
            8. Mortgage, first-time buyer (could replace ISA)
            */

            this.routeView();
        },

        /*
        *   @description:
        *       Simple task router based on current view
        */
        routeView: function() {
            var self = this,
            url = window.location.pathname;

            /*
            *   Account Overview Page
            */
            if(core.urlContains(url, options.urls.accountsHome)) {
            	/*
            	*	Load our project CSS
            	*/
            	core.loadCss(options.urls.css.intelligentsearch);

            	/*
            	*	Add listener for IS view ready
            	*/
            	broadcast.addListener("intelligent_search_ready", function(data) {
                    self.intelligentSearchReady(data);
                });

                /*
                *	Load the Intelligent Search view
                */
                self.createIntelligentSearchView();
            } 
        },

        /*
        *	@description:
        *		Load and inject the Intelligent Search view
        */
        createIntelligentSearchView: function() {
            var self = this,
            searchView = storage.getLocal(options.storageKeys.search.view);
            
            // Get IS view from Session Storage
            if(!!searchView) {
                broadcast.fire("intelligent_search_ready", searchView);
            } 
            // Else request this from the server
            else {
                // Request Intelligent Search view
                $.ajax({
                    type:"POST",
                    url:options.urls.gae.intelligentSearchView,
                    contentType:options.contentType,
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function(html) {
                        // Store the HTML response in Session Storage
                        storage.setLocal(options.storageKeys.search.view, html);
                        // Broadcast IS ready
                        broadcast.fire("intelligent_search_ready", html);
                    },
                    error: function(e) {
                        core.log("Failed to load Intelligent Search");
                        core.log(e);
                    }
                });    
            }
            
        },

        /*
        *	@description:
        *		Callback for boradcast event listener for when IS view has been returned by the server
        */
        intelligentSearchReady: function(html) {
            core.log("intelligentSearchReady");
        	var self = this,
        	header = $("#header"),
            pageWrap = $(".pageWrap"),
            headerPosition = header.position();
        	// Check for page header
        	if(header.length != 0){
        		// Inject the HTML view after the Logo
        		pageWrap.append(html);

                // Set Query & Places Results elements position
                self.intelligentSearchContainer = $("#alchemy-intelligent-search");
                // Set default form event listener
                /*
                self.intelligentSearchContainer.find("form").submit(function(e) {
                    if(e && e.preventDefault) e.preventDefault();
                });
                */
                self.query = $("#query");
                self.placesResults = $("#places-results");
                //header.append(self.intelligentSearchContainer);
                

                self.searchMask = $("#mask");
                $("body").append(self.searchMask);

                self.placesResultsList = $("#places-results ul");
                
                // Set Intelligent Search Container position
                /*
                self.intelligentSearchContainer.css({
                    top:headerPosition.top+"px",
                    left:headerPosition.left+$("#logo").outerWidth()+"px"
                });
                */

                // Set Places Results position
                /*
                $("#query_utility_power, #query_utility_telecomms, #places-results, #query_transfer, #query_account_balance").css({
                    width:self.query.outerWidth()+"px", 
                    top:self.query.offset().top-10+"px",
                    left:self.query.offset().left+"px"
                });
                */
               
                $(".secondary .panel").prepend($("#query_change_address"));
                $("#query_change_address").find("form").submit(function(e) {
                    if(e && e.preventDefault) e.preventDefault();
                });

                // Start listening for Triggers
                self.listenForTrigger();

                /*
                *   Require Google Maps. It has its own callback, so we don't need a listener
                */
                require(["gmaps"], function(gmaps) {
                    // wait
                });

                /*
                *   Store Account Balances
                */
                self.storeAccountBalances();
                /*
                *   Style Quick Transfer
                */
                self.setQuickTransfer();
        	}
        },

        /*
        *   @description:
        *       Callback for GMaps ready
        */
        gmapsReady: function(data) {
            var self = this;
            self.placesAutocompleteService = new google.maps.places.AutocompleteService();
            self.mapUKBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(49.383639452689664, -17.39866406249996),
                new google.maps.LatLng(59.53530451232491, 8.968523437500039)
            )

            broadcast.fire("gmaps_ready", data);
        },

        /*
        *   @description:
        *       Listen for Triggers in the Query, either from STT (Specch To Text) or device input
        */
        listenForTrigger: function() {
            var self = this;

            /*
            *   Listen: Query - Change Address
            */
            broadcast.addListener("query_change_address", function(data) {
                self.resultChangeAddress(data);
            });

            /*
            *   Listen: Query - Utility Power
            */
            broadcast.addListener("query_utility_power", function(data) {
                self.resultUtilityPower(data);
            });

            /*
            *   Listen: Query - Utility Telecomms
            */
            broadcast.addListener("query_utility_telecomms", function(data) {
                self.resultUtilityTelecomms(data);
            });

            /*
            *   Listen: Query - Pay or Transfer
            */
            broadcast.addListener("query_transfer", function(data) {
                self.resultTransfer(data);
            });

            /*
            *   Listen: Query - Account Balance
            */
            broadcast.addListener("query_account_balance", function(data) {
                self.resultAccountBalance(data);
            });

            $(window).keyup(function(e) {
                if(e.which == 27) {
                    self.searchMask.hide();
                    self.intelligentSearchContainer.css("display","none");
                    self.query.blur();
                }
                else {
                    self.searchMask.show();
                    self.intelligentSearchContainer.css("display","block");
                    self.query.focus();    
                }
                
            })
            /*
            *   Bind keyup event
            */
            self.query.keyup(function(e) {
                if(e && e.preventDefault) e.preventDefault();
                var query = $(this).val();



                // Show/Hide the Search Mask
                if(query.length == 0) {
                    self.searchMask.hide();
                    self.disableActiveText();
                } else {
                    self.searchMask.show();
                }

                // Evaluate Query:Address
                if(query.containsAny(self.queryTriggers.query_change_address)) {
                    // Hide the mask for address change
                    self.searchMask.hide();
                    broadcast.fire("query_change_address", query);
                } else {
                    $("#query_change_address").hide();
                    self.placesResults.hide();
                    self.placesResultsList.empty();
                }

                // Evaluate Query:Utility Power
                if(query.containsAny(self.queryTriggers.query_utility_power)) {
                    broadcast.fire("query_utility_power", query);
                } else {
                    $("#query_utility_power").hide();
                    self.query.attr("data-utility-power","false");
                }

                // Evaluate Query:Utility Telecomms
                if(query.containsAny(self.queryTriggers.query_utility_telecomms)) {
                    broadcast.fire("query_utility_telecomms", query);
                } else {
                    $("#query_utility_telecomms").hide();
                    self.query.attr("data-utility-telecomms","false");
                }

                // Evaluate Query:Pay Transfer
                if(query.containsAny(self.queryTriggers.query_transfer)) {
                    broadcast.fire("query_transfer", query);
                } else {
                    self.queryTransfer.hide();
                    self.query.attr("data-pay-transfer","false");
                }

                // Evaluate Query:Balance
                if(query.containsAny(self.queryTriggers.query_account_balance)) {
                    broadcast.fire("query_account_balance", query);
                } else {
                    $("#query_account_balance").hide();
                    self.query.attr("data-account-balance","false");
                }
            });

            // Add place item event listeners
            // [ST]NOTE: Using .live(), now deprecated in later jQuery versions in favour of .on()
            $("#places-results li").live("click", function(e) {
                if(e && e.preventDefault) e.preventDefault();
                // Empty the Places results
                self.placesResultsList.empty();

                // Populate the Form address fields
                var addressList = $(this).text().split(",");
                if(addressList[0]) $("#first_line").val(addressList[0]);
                if(addressList[1]) $("#second_line").val(addressList[1]);
                if(addressList[2]) $("#city").val(addressList[2]);
                if(addressList[3]) $("#postcode").val(addressList[3]);

                self.searchMask.hide();
                    self.intelligentSearchContainer.css("display","none");
                    self.query.blur();
            });


        },

        /*
        *   @description:
        *       Make request to Places Autocomplete Service, and handle the response callback
        */
        resultChangeAddress: function(query) {
            var self = this;

            if(!self.placesAutocompleteService) return;
            if(!query) return;
            var substr = query.substring(query.indexOf("address")+7);
            if(!substr) return;

            self.searchMask.hide();

            $(".secondary .panel").prepend($("#query_change_address"));
            $("#query_change_address").show();

            self.placesAutocompleteService.getQueryPredictions({
                bounds:self.mapUKBounds,
                input:substr
            }, function(predictions, status) {
                if(status != google.maps.places.PlacesServiceStatus.OK) {
                    core.log(status);
                    return;
                }

                // Set Places Results position
                self.placesResults.css({
                    width:self.query.outerWidth()+"px"
                });
                self.placesResults.show();

                // Enable active text
                self.enableActiveText();

                // Clear existing results
                self.placesResultsList.empty();

                // Create new Place predictions
                for(var i = 0, prediction; prediction = predictions[i]; i++) {
                    self.placesResultsList.append("<li data-place='"+prediction.description+"'>" + prediction.description + "</li>");
                }
            });
        },

        /*
        *   @description:
        *       Present Gas or Electricity Utility bill and related information
        */
        resultUtilityPower: function(query) {
            core.log("resultUtilityPower");
            var self = this;
            $("#query_utility_power").show();
            $("#query_utility_power, #query_utility_telecomms, #places-results, #query_transfer, #query_account_balance").css({
                width:self.query.outerWidth()+"px"
            });
        },

        /*
        *   @description:
        *       Present Telecomms Utility bill and related information
        */
        resultUtilityTelecomms: function(query) {
            core.log("resultUtilityTelecomms");
            var self = this;
            $("#query_utility_telecomms").show();
            $("#query_utility_power, #query_utility_telecomms, #places-results, #query_transfer, #query_account_balance").css({
                width:self.query.outerWidth()+"px"
            });
        },

        /*
        *   @description:
        *       Present Transfer features and related information
        */
        resultTransfer: function(query) {
            var self = this,
            valueMatch = query.match(/[-+]?[0-9]*\.?[0-9]+/gi),
            accounts = JSON.parse(storage.getLocal("account_balance")),
            fromAndToAccounts = [];

            // If not already visble, show the Action panel
            if(self.queryTransfer.css("display") != "block") {
                self.queryTransfer.show();
            }

            $("#query_utility_power, #query_utility_telecomms, #places-results, #query_transfer, #query_account_balance").css({
                width:self.query.outerWidth()+"px"
            });

            // Extract value match
            if(valueMatch) {
                $("#query_transfer .value").val(valueMatch);
            }

            // Match any accounts in order, and set them in the Form
            if(accounts) {
                // Store matched accounts from Query
                $.each(Object.keys(accounts), function(index, item) {
                    if(query.toLowerCase().indexOf(item.toLowerCase()) != -1) {
                        // Sort the accounts entered into the Query by indexOf ordering, so that the first stated 
                        // account is first in the fromAndToAccounts Array
                        if(fromAndToAccounts.length == 1) {
                            if(query.toLowerCase().indexOf(item.toLowerCase()) < query.toLowerCase().indexOf(fromAndToAccounts[0].toLowerCase())) {
                                fromAndToAccounts.unshift(item.toLowerCase());
                            } else {
                                fromAndToAccounts.push(item.toLowerCase());
                            }                        
                        } else {
                            fromAndToAccounts.push(item.toLowerCase());
                        }
                        
                    }
                });

                /*
                *   If no accounts or only 1 account is matched hide the card art
                */
                if(fromAndToAccounts.length == 0) {
                    self.fromCardArt.attr("src", "#");
                    self.fromCardArt.hide();
                    self.toCardArt.attr("src", "#");
                    self.toCardArt.hide();
                }
                else if(fromAndToAccounts.length == 1) {
                    self.toCardArt.attr("src", "#");
                    self.toCardArt.hide();
                }

                core.log(fromAndToAccounts);
                
                // Set From Account
                if(fromAndToAccounts.length >= 1) {
                    self.fromAccountSelectList.find("option").each(function(index, item){
                        var option = $(item);
                        // Check that fromAndToAccounts is greater than or equal to 1 in length
                        if(option.text().toLowerCase().indexOf(fromAndToAccounts[0]) != -1) {
                            option.attr("selected","selected");
                            self.fromCardArt.attr("src", accounts[fromAndToAccounts[0]].cardArt);
                            self.fromCardArt.show();
                        } else {
                            option.attr("selected","");                        
                        }
                    });         
                }
                    
                // Set To Account
                if(fromAndToAccounts.length == 2) {
                    self.toCardArt.attr("src", "#");
                    self.toCardArt.hide();
                    self.toAccountSelectList.find("option").each(function(index, item) {
                        var option = $(item);
                        if(option.text().toLowerCase().indexOf(fromAndToAccounts[1]) != -1) {
                            option.attr("selected","selected");
                            self.toCardArt.attr("src", accounts[fromAndToAccounts[1]].cardArt);
                            self.toCardArt.show();
                        } else {
                            option.attr("selected","");                        
                        }
                    });    
                }
                
            }
        },

        /*
        *   @description:
        *       Present Account Balances and related information
        */
        resultAccountBalance: function(query) {
            var self = this,
            displayed = self.query.attr("data-account-balance");

            // Check to see whether the balance is currently displayed
            if(displayed != "true") {
                
                $("#query_account_balance").show();
                $("#query_utility_power, #query_utility_telecomms, #places-results, #query_transfer, #query_account_balance").css({
                    width:self.query.outerWidth()+"px"
                });

                self.enableActiveText();

                var accounts = JSON.parse(storage.getLocal("account_balance"));                

                // Set the Query data attr for account balance to true
                self.query.attr("data-account-balance","true");

                // Show Savings account
                if(self.query[0].value.containsAny(["savings"])) {
                    self.query[0].value += ' : '+accounts[Object.keys(accounts)[1]].balance+' ("'+Object.keys(accounts)[1]+'")';
                }
                // Show Credit Card account
                else if(self.query[0].value.containsAny(["credit"])) {
                    self.query[0].value += ' : '+accounts[Object.keys(accounts)[2]].balance+' ("'+Object.keys(accounts)[2]+'")';
                }
                // Show Current Account
                else {
                    self.query[0].value += ' : '+accounts[Object.keys(accounts)[0]].balance+' ("'+Object.keys(accounts)[0]+'")';
                }
                
            } 
            // Else do nothing, as the balance is already displayed
            else {
                return;
            }
        },

        /*
        *   @description:
        *       Enable active query text
        */
        enableActiveText: function() {
            var self = this;
            self.query.addClass("active");
        },

        /*
        *   @description:
        *       Disable active query text
        */
        disableActiveText: function() {
            var self = this;
            self.query.removeClass("active");  
        },

        /*
        *   @description:   
        *       Store Account Balances for use later
        */
        storeAccountBalances: function() {
            var self = this,
            accounts = new Object();
            // Get Account Balances
            $("ul.myAccounts li").each(function(index, item) {
                var li = $(item),
                accountName = li.find("h2").text(),
                balance = li.find(".balance").text().replace("Balance ",""),
                cardArt = li.find("h2 img").attr("src");
                if(accountName && balance) accounts[accountName.toLowerCase()] = {
                    "name":accountName, 
                    "balance":balance, 
                    "cardArt":cardArt
                };
            });

            // Store the Account Balances in Session Storage
            storage.setLocal("account_balance", JSON.stringify(accounts));
        },

        setQuickTransfer: function() {
            var self = this,
            quickTransfer = $(".quickTransfer"),
            submitButton = quickTransfer.find("input[type='image']"),
            value = quickTransfer.find(".value");


            value.bind("click", function(e) {
                if(e && e.preventDefault) e.preventDefault();
            });

            self.queryTransfer = $("#query_transfer");
            self.fromAccountSelectList = $(".quickTransfer .fromAccount");
            self.toAccountSelectList = $(".quickTransfer .toAccount");
            
            // Inject the Quick Transfer form into the query_transfer dialog
            self.queryTransfer.append(quickTransfer);
            // Inject the Arrow
            value.after("<img class='to' src='#' alt='' />");//<div class='arrow'></div>
            value.before("<img class='from' src='#' alt='' />");
            
            self.fromCardArt = $(".quickTransfer img.from");
            self.toCardArt = $(".quickTransfer img.to");
            // Update Submit button
            submitButton.attr("src","/personal/assets/alchemy/img/intelligent_search/query_transfer_button.png");
            
        }
    }
});