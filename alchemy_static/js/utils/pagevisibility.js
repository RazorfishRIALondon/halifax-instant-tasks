/*
*	@name: pagevisibility.js
*	@description:
*		Access Document visibilityChange and hidden properties
*/
define(["utils/core"], function(core) {
	return {
		browserDoesVisibility: function() {
			var hidden, state, visibilityChange; 
			if (typeof document.hidden !== "undefined") {
				this.hidden = "hidden";
				this.visibilityChange = "visibilitychange";
				this.state = "visibilityState";
			} else if (typeof document.mozHidden !== "undefined") {
				this.hidden = "mozHidden";
				this.visibilityChange = "mozvisibilitychange";
				this.state = "mozVisibilityState";
			} else if (typeof document.msHidden !== "undefined") {
				this.hidden = "msHidden";
				this.visibilityChange = "msvisibilitychange";
				this.state = "msVisibilityState";
			} else if (typeof document.webkitHidden !== "undefined") {
				this.hidden = "webkitHidden";
				this.visibilityChange = "webkitvisibilitychange";
				this.state = "webkitVisibilityState";
			} else {
				this.hidden = null;
				this.visibilityChange = function(){ return false };
				this.state = "";
			}

		},

		init: function() {
			var self = this;
			this.browserDoesVisibility();
			// Add a listener that constantly changes the title
			document.addEventListener(self.visibilityChange, function(e) {
				core.log(document[self.state]);
				if(document[self.state] == "hidden") {
					setTimeout(function() {
						alert("Hey!");
					}, 2000);
				}
			}, false);

		}		
	}
});