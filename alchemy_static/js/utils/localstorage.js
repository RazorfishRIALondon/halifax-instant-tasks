/*
*	@name: local-storage.js
*	@description:
*		Storage (LocalStorage) API utility methods
*	@TODO:
*		Polyfill available for IE<8
*		https://developer.mozilla.org/en-US/docs/DOM/Storage
*/
define(["utils/core"], function(core) {
	return {
	    /*
		*	@description:
		*		Get text object from Session Storage
		*/
		getLocal: function(key)	{
			return window.localStorage.getItem(key);
		},

		/*
		*	@description:
		*		Save text object to Session Storage
		*/
		setLocal: function(key, value) {
			return window.localStorage.setItem(key, value);
		},

		/*
		*	@description:
		*		Remove text object from Session Storage
		*/
		removeLocal: function(key) {
			return window.localStorage.removeItem(key);
		},

		/*
		*	@description:
		*		Remove all objects from SessionStorage
		*/
		clearLocal: function() {
			window.localStorage.clear();		
		},

		/*
		*	@description:
		*		Check for SessionStorage capability
		*/
		browserDoesStorage: function() {
			return typeof(Storage) !== "undefined";
		},

		/*
		*	@description:
		*		Storage change event handler
		*/
		storageChangeListener: function() {
			if(!document.addEventListener) return;
			document.addEventListener("storage", function(storageEvent) {
				core.log(storageEvent);
			},false);
		}
	}
});