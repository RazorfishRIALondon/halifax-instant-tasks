/*
*	@name: sse.js
*	@description:
*		EventSource API connectivity with Server-Sent Events (SSE)
*	@notes:
*		FF, Opera, Chrome and Safari should all support the EventSource API,
*		however we detect capability, so IE and anything else will degrade to XHR + long-polling
*/
define(["utils/core","utils/options","eventsource_polyfill","utils/event"], function(core, options, void_sse, broadcast) {
	return {
		/*
		*	@description:
		*		Open a connection and listen for messages
		*/
		connect: function(sourceUrl, listeners) {
			var self = this,
			listener;
			// Create an EventSource connection to a specific Origin URL
			self.source = new EventSource(sourceUrl, {withCredentials: true});

			// For each Array of Message Event types, create a listener
			for(listener in listeners){
				if(listeners.hasOwnProperty(listener)) {
					self.source.addEventListener(listeners[listener], self.onMessage.bind(self), false);
				}
			}
			
			self.source.addEventListener("open", function(data) {
				self.onOpen(data);
			}, false);
			self.source.addEventListener("error", function(data) {
				self.onError(data);
			}, false);
			self.source.addEventListener("message", function(data) {
				self.onMessage(data);
			}, false);
		},

		/*
		*	@description:
		*		OpenEvent handler
		*/
		onOpen: function(MessageEvent) {
			//core.log("SSE connection opened");
			//core.log(MessageEvent);
		},

		/*
		*	@description:
		*		MessageEvent handler
		*/
		onMessage: function(MessageEvent) {
			var self = this;
			//this.checkOrigin(MessageEvent);
			//core.log("SSE : onMessage");
			//core.log(MessageEvent);
			// Broadcast the event
			broadcast.fire(MessageEvent.type, MessageEvent);			
		},

		/*
		*	@description:
		*		ErrorEvent handler
		*/
		onError: function(MessageEvent) {
			if (MessageEvent.readyState == EventSource.CLOSED) {
		    	//core.log("EventSource Connection closed");
		  	} else{
		  		//core.log("EventSource Error");
		  		//core.log(MessageEvent);
		  	}
		},

		/*
		*	@description:
		*		Check origin handler
		*/
		checkOrigin: function(MessageEvent) {
			var sourceURL = (window.location.protocol+'//'+options.urls.sseOrigin);
			if (MessageEvent.origin != sourceURL) {
				core.log("EventSource Origin Error");
			}
		},

		/*
		*	@description:
		*		Helper method for closing the EventSource socket connection
		*/
		close: function() {
			var self = this;
			if(!!self.source) self.source.close();
		}
	}
});