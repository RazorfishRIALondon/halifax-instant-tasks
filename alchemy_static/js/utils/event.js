/*
*	@name: event.js
*	@description:
*		Event broadcast registration. Bind to an event with a listener
*/
define(["utils/core"], function(core) {
	return {
		stack:{},
		addListener: function(event, listener) {
			// Usage Note:
			// We have to alternate Events.addListener calls with the same event, as otherwise the unique GUID timestamp might be identical
			// and only the first event/listener will exist in the stack
			// [ST]TODO: Don't use an Object for stack, instead use an Array and push to it. Then you will avoid GUID conflicts
			var guid = "__"+ (new Date().getTime()), self = this;
			
			if(!this.stack[event]) {
				this.stack[event] = {};
			}
			this.stack[event][guid] = function(data){
				listener.apply(self, [data]);
			}
			
			return {

				start: function() {
					this.stack[event][guid] = function(data){
						listener.apply(self, [data]);
					}
				},
				
				stop: function() {
					delete this.stack[event][guid];
				}
			}
		},
		
		fire: function(event, data) {
			var i, listeners = this.stack[event];
			
			if(typeof listeners === "undefined") {
				return false;
			}

			for(i in listeners) (function() {
				listeners[i](data);
			})();		
			
		},

		debug: function() {
			var i;
			for(i in this.stack) {
				if(this.stack.hasOwnProperty(i)) {
					core.log(i);
					core.log(this.stack[i]);
				}
			}

		}
	}
});

/*
From Angular JS:

var namedListeners = this.$$listeners[name];
if (!namedListeners) {
	this.$$listeners[name] = namedListeners = [];
}
namedListeners.push(listener);

// Returns a De-Registration function for this listener
return function() {
	namedListeners[indexOf(namedListeners, listener)] = null;
};
*/