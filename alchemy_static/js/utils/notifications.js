/*
*	@name: notifications.js
*	@description:
*		Notifications API utility methods
*/
define(["utils/core","utils/localstorage"], function(core,localStorage) {
	return {
	    /*
		*	@description:
		*		Check for SessionStorage capability
		*/
		browserDoesNotifications: function() {
			return window.webkitNotifications || navigator.mozNotification;
		},

		/*
		*	@description:
		*		Initialize
		*/
		init: function() {
			var self = this,
			localNotifications,
			data;

			this.fallback = false;
			self.permissionGranted = false;

			// Set our initial local storage
			data = localStorage.getLocal("notifications");
			this.notifications = JSON.parse(data)||{};

			if(!this.notificationAPI) {
				this.notificationAPI = this.browserDoesNotifications();
			}
			
			// If no Notifications API is available, set fallback property to true and return
			if(!this.notificationAPI) {
				this.fallback = true;
				return;
			}

			if(this.notificationAPI.checkPermission() != 0) {
				$("body").prepend("<div id='push-notifications'><button id='allow-push-notifications' class='button'>Allow Push Notifications</button></div>");
				$("#allow-push-notifications").bind("click", function(e) {
					if(e && e.preventDefault) e.preventDefault();
					self.checkPermission();
				});
			}
		},

		checkPermission: function(callback) {
			var self = this;
			self.init();
			
			// If fallback is true, return immediately
			if(this.fallback) {
				return;
			}

			if (this.notificationAPI.checkPermission() == 0) {
				self.permissionGranted = true;
				if(callback) callback();
			} else {
			    self.notificationAPI.requestPermission(function(e) {
			    	core.log(e);

			    	// Execute any Callback?? Only if permission granted
			    	if(callback) callback();
			    });
			}
		},

		/*
		*	@description:
		*		create a new Notification
		*	@arguments:
		*		id, iconUrl, title, body, onDisplayCallback, onErrorCallback, onClickCallback, onCloseCallback
		*/
		create: function(data) {
			var self = this,
			onDisplayCallback,
			onErrorCallback,
			onClickCallback;

			self.checkPermission();

			if(this.notifications[data.id]) {
				core.log("This Notification exists in memory/storage : "+data.id);
				return;
			}
			
			if(this.fallback) {
				//core.log("Notifications API unavailable");
				this.fallbackNotification(data);
			} else {
				// Create the Notification object
				this.notifications[data.id] = this.notificationAPI.createNotification(
					data.iconUrl,
					data.title, 
					data.body
				);

				/*
				*	Add Default Callbacks
				*/
				// Add default display event
				this.notifications[data.id].addEventListener("display", function(e) {
					self.onDisplayCallback(data)
				}, false);
				// Add default click event
				this.notifications[data.id].addEventListener("click", self.onClickCallback, false);
				// Add default error event
				this.notifications[data.id].addEventListener("error", self.onErrorCallback, false);
				// Add default close event
				this.notifications[data.id].addEventListener("close", self.onCloseCallback, false);
				// Set the Notification tag
				this.notifications[data.id].tag = data.id;

				/*
				*	Add Notification Callbacks
				*/
				// Do we have an onDisplayCallback?
				onDisplayCallback = data.onDisplayCallback ? data.onDisplayCallback : null;

				// Do we have an onErrorCallback?
				onErrorCallback = data.onErrorCallback ? data.onErrorCallback : null;

				// Do we have an onClickCallback?
				onClickCallback = data.onClickCallback ? data.onClickCallback : null;

				// Add display callback
				if(onDisplayCallback) {
					this.notifications[data.id].addEventListener("display", function(e) {
						self.onDisplayCallback(data);
					}, false);	
				}
				

				// Add error callback
				this.notifications[data.id].addEventListener("error", onErrorCallback, false);

				// Add click callback
				this.notifications[data.id].addEventListener("click", onClickCallback, false);

				
				// Show the Notification
				this.notifications[data.id].show();
				
				return this.notifications[data.id];
			}
			
		},

		/*
		*	@description:
		*		Fallback Notification for Devices that do not support the Notifications API
		*/
		fallbackNotification: function(data) {
			var self = this;
			// Create a UI Notification
			$("#content").append("<div id='"+data.id+"' class='message'><h3><span class='notification warning'>!</span><span class='title'>"+data.title+"</span></h3><p><a href='"+data.url+"' target='_blank'>"+data.body+"</a></p></div>");
			data["tag"] = data.id;
			
			// Add the data to our notifications session object
			this.notifications[data.id] = data;

			// Fire the onDisplay Callback
			self.onDisplayCallback(data);
		},

		/*
		*	@description:
		*		Default display Callback
		*	@scope:
		*		self/this = Notification object
		*/
		onDisplayCallback: function(data) {
			core.log("onDisplayCallback");
			core.log(data);

			localNotifications = JSON.parse(localStorage.getLocal("notifications"))||{};

			// Set a timestamp from the Event.timeStamp
			this.timestamp = data.timeStamp || new Date().getTime();

			// Add the notification to the local storage object
			localNotifications[data.id] = JSON.stringify(data);

			// Add Notification to SessionStorage
			localStorage.setLocal("notifications", JSON.stringify(localNotifications));
		},

		/*
		*	@description:
		*		Default Error Callback
		*	@scope:
		*		self/this = Notification object
		*/
		onErrorCallback: function(e) {
			core.log("onErrorCallback");
			core.log(e);
		},

		/*
		*	@description:
		*		Default Click Callback
		*	@scope:
		*		self/this = Notification object
		*/
		onClickCallback: function(e) {
			core.log("onClickCallback");
			core.log(e);
			var self = this;
			// Focus on the required window
			window.focus();

			// Finally remove the Notification
			this.cancel();

		},

		/*
		*	@description:
		*		Default Close Callback
		*	@scope:
		*		self/this = Notification object
		*/
		onCloseCallback: function(e) {
			core.log("onCloseCallback");
			core.log(e);

			// Remove all event listeners before saving
			delete self.ondisplay;
			delete self.onclick;
			delete self.onerror;
			delete self.onshow;
			delete self.onclose;
		}
	}
});