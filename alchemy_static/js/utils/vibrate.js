/*
*	@name: vibrate.js
*	@description:
*		Vibrate API for mobiler devices that support this, and provide Navigator access
*/
define(["utils/core"], function(core) {
	return {
	    /*
		*	@description:
		*		Establish device capability
		*/
		browserDoesVibration: function(key)	{
			return ("vibrate" in navigator) || navigator.vibrate || navigator.mozVibrate || navigator.webkitVibrate;
		},

		/*
		*	@description
		*		Return the vibration interface; fails silently
		*/
		getVibration: function() {
			this.vibrate = function(p) { return false; };
			if(navigator.vibrate) {
				this.vibrate = function(p) { navigator.vibrate(p); };
			}

			if(navigator.mozVibrate) {
				this.vibrate = function(p) { navigator.vibrate(p); };
			}

			if(navigator.webkitVibrate) {
				this.vibrate = function(p) { navigator.webkitVibrate(p); };
			}

			return this.vibrate;
		}
	}
});