/*
*	@name: mediastream.js
*	@description:
*		Access Video or Audio
*/
define(["utils/core","utils/options","utils/event","utils/sessionstorage"], function(core,options,broadcast,storage) {
	return {
		/*
		*	@description:
		*		Detect support for Navigator.getUserMedia
		*/
		browserDoesMediaStream: function() {
			return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
		},

		/*
		*	@description:
		*		Access UserMedia Stream
		*/
		accessUserMedia: function() {
			var self = this;

			if(!this.browserDoesMediaStream) return;

			/*
			*	Create the Canvas element
			*/
			this.createCanvas();

			/*
        	*	Load our project CSS
        	*/
        	core.loadCss(options.urls.css.mediastream);

			// Native support
			if(navigator.getUserMedia) {
				navigator.getUserMedia("video", self.successCallback.bind(this), self.errorCallback.bind(this));
			} 
			
			// For Chrome Canary with WebRTC MediaStream enabled
			else if(navigator.webkitGetUserMedia) {
				this.is_webkit = true;
				navigator.webkitGetUserMedia({"video":true}, self.successCallback.bind(this), self.errorCallback.bind(this));
			}

			// For Firefox Nightly
			else if(navigator.mozGetUserMedia) {
				navigator.mozGetUserMedia({video:true}, self.successCallback.bind(this), self.errorCallback.bind(this));
			}
		},

		/*
		*	@description:
		*		Create the Video, Canvas and Portrait elements
		*/
		createCanvas: function() {
			var self = this;

			this.localMediaStream = null;

			$("body").append("<video id='alchemy-video-media' width='500' height='400' autoplay></video>");
			$("body").append("<canvas id='alchemy-video-canvas' width='500' height='375'></canvas>");
			$("body").append("<div class='alchemy-picture-frame'><img src='#' id='alchemy-video-portrait' width='500' height='375' /><div class='button-container'><button>Send me this!</button></div><a href='#' class='close'>x</a></div>");
			
			this.video = $("#alchemy-video-media");
			this.canvas = $("#alchemy-video-canvas");
			this.pictureFrame = $(".alchemy-picture-frame");
			this.portrait = $("#alchemy-video-portrait");
			// Bind the close event listener
			this.pictureFrame.find(".close").bind("click", function(e) {
				if(e && e.preventDefault) e.preventDefault();
				$(this).parent().hide();
			});
			this.ctx = this.canvas[0].getContext("2d");
		},

		/*
		*	@description:
		*		Success callback
		*/
		successCallback: function(stream) {
			var self = this;

			// Set autoplay to true, else we may just see the first frame of the video stream
			this.video.autoplay = true;

			if(!this.is_webkit) {
				this.localMediaStream = stream;
			} else {
				this.localMediaStream = window.webkitURL.createObjectURL(stream);
			}
			this.video.attr("src",this.localMediaStream);
			this.video.bind("click", self.setImage.bind(self));
			broadcast.fire("mediastream_ready");
		},

		/*
		*	@description:
		*		Error callback
		*/
		errorCallback: function(e) {
			core.log(e)
		},

		/*
		*	Snapshot on an interval
		*
		*/
		intervalSnapshot: function() {
			var self = this;
			setInterval(function() {
				self.ctx[0].drawImage(self.video[0], 0, 0, 500, 375);
				self.portrait.attr("src",self.canvas[0].toDataURL("image/webp"));
			}, 67);			
		},

		/*
		*	@description:
		*		Set the Portrait image
		*/
		setImage: function() {
			var self = this;
			// "image/webp" works in Chrome 18. In other browsers, this will fall back to image/png.			
			self.ctx.drawImage(self.video[0], 0, 0, 500, 375);
			var data = self.canvas[0].toDataURL("image/webp");
			self.portrait.attr("src",data);
			storage.setLocal("subject_"+new Date().getTime(), data);			
		},

		/*
		*	@description:
		*		Show the Portrait image
		*/
		showImage: function() {
			this.pictureFrame.css({"left":"50px"});
			this.portrait.css({"display":"block"});
		},

		/*
		*	@description:
		*		Take a snapshot of the Test User Subject
		*/
		captureUser: function() {
			this.video.fire("click");
		}

	}
});