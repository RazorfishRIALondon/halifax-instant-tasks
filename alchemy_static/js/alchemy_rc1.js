(function(window) {
	/*
	*	Check for jQuery (any version)
	*/
	if($ !== "undefined") {
		var h1 = $("h1:first"),
		h1Text = h1.text();

		if(h1Text == "Your accounts") {
			h1.after("<h2>Alchemy RC1 Test</h2>");
			if(typeof window.console != "undefined") {
		        console.log("Alchemy RC1 Test");
		    }
		}
	}
})(window);