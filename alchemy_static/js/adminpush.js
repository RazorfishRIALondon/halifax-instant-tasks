requirejs.config({
	paths: {
        "utils":"../utils",
        "app": "../app",
        "jquery": "jquery"
    }
});

// Start the main app logic.
requirejs(["app/push_admin"], function (pushAdmin) {
    pushAdmin.init();
});