/*
*	rf_core
*/
define({
	/*
	*	@description:
	*		Local logger, in case IE is watching...
	*/
	log: function(msg) {
		if(this.options.debug && (typeof window.console != "undefined")) {
			console.log(msg);
		}
	},
	
	/*
	*	@description:
	*		Get text object from Session Storage
	*/
	getLocal: function(key)	{
		return window.sessionStorage.getItem(key);
	},

	/*
	*	@description:
	*		Save text object to Session Storage
	*/
	setLocal: function(key, value) {
		return window.sessionStorage.setItem(key, value);
	},

	/*
	*	@description:
	*		Remove text object from Session Storage
	*/
	removeLocal: function(key) {
		return window.sessionStorage.removeItem(key);
	},

	/*
	*	@description:
	*		Remove all objects from SessionStorage
	*/
	clearLocal: function() {
		window.sessionStorage.clear();		
	},

	/*
	*	@description:
	*		Fired on log out of the IB site
	*
	*/
	onLogoff: function() {
		var self = this;

		/*
		*	Remove the OMM Pie Chart data from SessionStorage whilst the Browser is still open
		*/
		self.clearLocal();
	},

	/*
	*	@description:
	*		Check for SessionStorage capability
	*/
	decentBrowser: function() {
		return typeof window.sessionStorage != "undefined";
	}
});