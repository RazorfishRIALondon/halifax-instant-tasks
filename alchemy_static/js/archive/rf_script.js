var RF = {
	options:{
		debug:true,
		storageKeys:{
			omm:{
				pieChart:"chart"
			}
		},
		urls:{
			accountsHome:"/personal/a/account_overview_personal",
			moneyManager:{
				dashboard:"/personal/a/mm_dashboard",
				bookmark:"/personal/a/mm_bookmark"
			},
			loging:{

			},
			logoff:{
				auth:"/personal/a/logoff"
			}
		}
	},

	init: function() {
		var self = this;

		if(!self.decentBrowser()) {
			alert("Sorry\n\nThis browser does not support Storage");
			return;
		}

		this.routeView();		
	},

	/*
	*	@description:
	*		Simple task router based on current view
	*/
	routeView: function() {
		var self = this,
		url = window.location.pathname;

		/*
		*	Account Overview Page
		*/
		if(url.indexOf(self.options.urls.accountsHome) != -1) {
			/*
			*	If we already have Chart data in session display the Chart
			*/
			var chart = self.getLocal(self.options.storageKeys.omm.pieChart);
			if(!!chart) {
				self.log("Loading Chart from SessionStorage");
				self.displayChart();
			} 
			/*
			*	Else, asynchronously load it whilst on the Accounts Home page, and then display it
			*/
			else {
				self.log("Requesting Chart from MM Dashboard");
				self.loadMMDashboard();
			}
			
		} 

		/*
		*	Money Manager Dashboard
		*/
		else if(url.indexOf(self.options.urls.moneyManager.dashboard) != -1) {
			/*
			*	Get MM Charts
			*/
			self.collectChart();
		}
		
		/*
		*	Money Manager Bookmark
		*/
		else if(url.indexOf(self.options.urls.moneyManager.bookmark) != -1) {
			/*
			*	Get MM Charts
			*/
			self.collectChart();
		}

		/*
		*	Logoff (UAT Auth timed exit page only, not Unauth destination page at this point)
		*/
		else if(url.indexOf(self.options.urls.logoff.auth) != -1) {
			self.onLogoff();
		}

	},

	/*
	*	@description:
	*		Local logger, in case IE is watching...
	*/
	log: function(msg) {
		if(this.options.debug && (typeof window.console != "undefined")) {
			console.log(msg);
		}
	},
	
	/*
	*	@description:
	*		Get text object from Session Storage
	*/
	getLocal: function(key)	{
		return window.sessionStorage.getItem(key);
	},

	/*
	*	@description:
	*		Save text object to Session Storage
	*/
	setLocal: function(key, value) {
		return window.sessionStorage.setItem(key, value);
	},

	/*
	*	@description:
	*		Remove text object from Session Storage
	*/
	removeLocal: function(key) {
		return window.sessionStorage.removeItem(key);
	},

	/*
	*	@description:
	*		Remove all objects from SessionStorage
	*/
	clearLocal: function() {
		window.sessionStorage.clear();		
	},

	/*
	*	@description:
	*		Collect MM Chart HTML from a page and store it locally
	*/
	collectChart: function(htmlParent) {
		var self = this,
		charts;

		/*
		*	If we have an HTML Body parent, use this to find the Charts
		*	Else use the current page DOM context
		*/
		charts = $(htmlParent||document).find(".ommChartWrap, .ommNoTransChartWrap");

		/*
		*	Collect only the No Transactions Chart or the Chart with type 'pie'
		*/
		$.each(charts, function(i, elem) {
			if(elem.className.indexOf("ommNoTransChartWrap") != -1 || elem.className.indexOf("{type:'pie'}") != -1) {
				/*
				*	We need the Chart wrapper DIV element rather than just its contents, so inspect up one level to the 
				*	parent and collect its first DIV child element
				*/
				var chartHTML = $(elem).parent().html();
				self.setLocal(self.options.storageKeys.omm.pieChart, chartHTML);
			}
		});
	},

	/*
	*	@description:
	*		Displays a Money Manager Chart from Storage, within the Accounts Home page right hand panel
	*/
	displayChart: function() {
		var self = this,
		panel = $(".secondary .panel");
		
		/*
		*	Add the Body CSS class
		*/
		$("body").addClass("hasRaphael");

		/*
		*	Inject required CSS style adjustments into the HEAD
		*/
		$("head").append("<style type='text/css'>.secondary .inner {margin:0;} .hasRaphael .ommChart, .ommNoTransChart {float:none!important;} .hasRaphael .tableWrap {margin-left:0!important;}.noTransWrap {display:none;}.ommNoTransChartWrap {overflow:hidden;} #rf-omm-chart{min-height:300px;} #rf-omm-chart h3 {display:none;}</style>");

		/*
		*	Create and add the RF Chart container	
		*/
		panel.prepend("<div id='rf-omm-chart'></div>");
		
		/*
		*	Collect the Chart from local storage
		*/
		var chart = self.getLocal(self.options.storageKeys.omm.pieChart);
		if(!!chart) {
			/*
			*	Inject the chart into the page
			*/
			$("#rf-omm-chart").append(chart);
			
			/*
			*	Call the Raphael/LBG init functions
			*	NOTE: we don't need this if the charting JS has been injected after the HTML code, as it will call the init functions itself
			*/
			
			/*
			self.initializeLBG();
			*/			
		}
		else {
			self.log("Chart is missing in SessionStorage under this domain");
		}
	},

	loadMMDashboard: function() {
		var self = this;
		$.ajax({
			url:self.options.urls.moneyManager.dashboard,
			success: function(html) {
				var pageHTML = $(html);
				self.collectChart(pageHTML);
				self.displayChart();
			},
			error: function(e) {
				self.log("Failed to load MM Dashboard page asynchronously. No Chart data available");
				self.log(e);
			}
		});
	},

	/*
	*	@description:
	*		Fired on log out of the IB site
	*
	*/
	onLogoff: function() {
		var self = this;

		/*
		*	Remove the OMM Pie Chart data from SessionStorage whilst the Browser is still open
		*/
		self.clearLocal();
	},

	/*
	*	@description:
	*		Depending on the location of MM Chart data we inject and the load ordering, we may need to re-initialize the Raphael functions
	*		and other LBG specific init functions
	*/
	initializeLBG: function() {
		/*
		*	Needed to init RaphaleJS charts
		*/
		LBG.ommIdentRaphael();
		LBG.refreshReset.init();
		/*
		*	Additional init functions
		*/
		LBG.ommGoToAnotherTool.init();
		LBG.ommFilters.init();
		LBG.ommTransactionTaxonomy.init();
		/*
		*	LBG.ommDetect.init() - Needed to init RaphaleJS charts?
		*/
		LBG.ommDetect.init();
		LBG.ommBudgetCalendar.init();
		LBG.ommAddOrEditReminderToCalendar.init();
		LBG.createGoal.init();
		LBG.ommCharacterCount.init();
		LBG.ommPlaceHolder.init();
		LBG.ommBudgets.init();
		LBG.ommGoals.init();
		LBG.ommGoalAllocate.init();
	},

	/*
	*	@description:
	*		Check for SessionStorage capability
	*/
	decentBrowser: function() {
		return typeof window.sessionStorage != "undefined";
	}
}
RF.init();