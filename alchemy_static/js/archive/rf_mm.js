/*
*	rf_core
*/
define(["./rf_core"], function(rf_core){
	
	return {
		rf_core:rf_core,
		/*
		*	@description:
		*		Collect MM Chart HTML from a page and store it locally
		*/
		collectChart: function(htmlParent) {
			var self = this,
			charts;

			/*
			*	If we have an HTML Body parent, use this to find the Charts
			*	Else use the current page DOM context
			*/
			charts = $(htmlParent||document).find(".ommChartWrap, .ommNoTransChartWrap");

			/*
			*	Collect only the No Transactions Chart or the Chart with type 'pie'
			*/
			$.each(charts, function(i, elem) {
				if(elem.className.indexOf("ommNoTransChartWrap") != -1 || elem.className.indexOf("{type:'pie'}") != -1) {
					/*
					*	We need the Chart wrapper DIV element rather than just its contents, so inspect up one level to the 
					*	parent and collect its first DIV child element
					*/
					var chartHTML = $(elem).parent().html();
					self.rf_core.setLocal(self.options.storageKeys.omm.pieChart, chartHTML);
				}
			});
		},

		/*
		*	@description:
		*		Displays a Money Manager Chart from Storage, within the Accounts Home page right hand panel
		*/
		displayChart: function() {
			var self = this,
			panel = $(".secondary .panel");
			
			/*
			*	Add the Body CSS class
			*/
			$("body").addClass("hasRaphael");

			/*
			*	Inject required CSS style adjustments into the HEAD
			*/
			$("head").append("<style type='text/css'>.secondary .inner {margin:0;} .hasRaphael .ommChart, .ommNoTransChart {float:none!important;} .hasRaphael .tableWrap {margin-left:0!important;}.noTransWrap {display:none;}.ommNoTransChartWrap {overflow:hidden;} #rf-omm-chart{min-height:300px;} #rf-omm-chart h3 {display:none;}</style>");

			/*
			*	Create and add the RF Chart container	
			*/
			panel.prepend("<div id='rf-omm-chart'></div>");
			
			/*
			*	Collect the Chart from local storage
			*/
			var chart = self.rf_core.getLocal(self.options.storageKeys.omm.pieChart);
			if(!!chart) {
				/*
				*	Inject the chart into the page
				*/
				$("#rf-omm-chart").append(chart);
				
				/*
				*	Call the Raphael/LBG init functions
				*	NOTE: we don't need this if the charting JS has been injected after the HTML code, as it will call the init functions itself
				*/
				
				/*
				self.initializeLBG();
				*/			
			}
			else {
				self.rf_core.log("Chart is missing in SessionStorage under this domain");
			}
		},

		loadMMDashboard: function() {
			var self = this;
			$.ajax({
				url:self.rf_core.options.urls.moneyManager.dashboard,
				success: function(html) {
					var pageHTML = $(html);
					self.collectChart(pageHTML);
					self.displayChart();
				},
				error: function(e) {
					self.rf_core.log("Failed to load MM Dashboard page asynchronously. No Chart data available");
					self.rf_core.log(e);
				}
			});
		},

		/*
		*	@description:
		*		Depending on the location of MM Chart data we inject and the load ordering, we may need to re-initialize the Raphael functions
		*		and other LBG specific init functions
		*/
		initializeLBG: function() {
			/*
			*	Needed to init RaphaleJS charts
			*/
			LBG.ommIdentRaphael();
			LBG.refreshReset.init();
			/*
			*	Additional init functions
			*/
			LBG.ommGoToAnotherTool.init();
			LBG.ommFilters.init();
			LBG.ommTransactionTaxonomy.init();
			/*
			*	LBG.ommDetect.init() - Needed to init RaphaleJS charts?
			*/
			LBG.ommDetect.init();
			LBG.ommBudgetCalendar.init();
			LBG.ommAddOrEditReminderToCalendar.init();
			LBG.createGoal.init();
			LBG.ommCharacterCount.init();
			LBG.ommPlaceHolder.init();
			LBG.ommBudgets.init();
			LBG.ommGoals.init();
			LBG.ommGoalAllocate.init();
		}
	}
});