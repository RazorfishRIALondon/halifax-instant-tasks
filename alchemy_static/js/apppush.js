requirejs.config({
	paths: {
        "utils":"../utils",
        "app": "../app",
        "jquery": "jquery-1.10.1.min"
    }
});

// Start the main app logic.
requirejs(["app/push"], function (push) {
    push.init();
});