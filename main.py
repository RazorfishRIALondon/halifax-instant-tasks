#!/usr/bin/env python
import logging
import os
import webapp2

#Local scripts
from controllers import default_controller
from controllers import tasks
from controllers import cron
from controllers import alchemy_controller

# System URLs
requestWarmUp = '/_ah/warmup'
requestVoxeoManifest = '/voxeo-manifest'
requestVoxeoApplication = '/voxeo-application'
# User Request Endpoint URL
requestDefault = '/'
requestSaveUserTaskStep1 = '/actions/save-task-step1'
requestSaveUserTaskStep2 = '/actions/save-task-step2'
requestSaveUserTaskStep3 = '/actions/save-task-step3'
requestGetTaskList = '/actions/get-task-list'
requestGetTask = '/actions/edit-task'
requestSaveFacebookUserID = '/actions/fbconnect'

# Alchemy
requestAlchemyStatic = r'/alchemy/static/(.*)'
requestAccountAggregationInit = '/alchemy/account-aggregation-init'
requestAccountAggregationSubmit = '/alchemy/account-aggregation-submit'
requestAccountAggregationDisconnect = '/alchemy/account-aggregation-disconnect'
requestAccountAggregationCTA = '/alchemy/account-aggregation-cta'
requestAccountAggregationSSE = '/alchemy/sse'
requestIntelligentSearchView = '/alchemy/intelligent-search-view'
requestAlchemyPush = '/alchemy/push'
requestAlchemyPushSSE = '/alchemy/push/sse'
requestAlchemyPushToken = '/alchemy/push/request-token'
requestAlchemyPushAdmin = '/alchemy/push-admin'
requestAlchemyPushAdminSSE = '/alchemy/push-admin/sse'
requestAlchemyBackgroundProcess = '/alchemy/background'

requestAlchemyCreateGroup = '/alchemy/create-group'
requestAlchemyGetGroup = '/alchemy/get-group'
requestAlchemyAddMembersToGroup = '/alchemy/add-members-to-group'
requestAlchemyCreatePurse = '/alchemy/create-purse'
requestAlchemyUpdatePurse = '/alchemy/update-purse'
requestAlchemyDeletePurse = '/alchemy/delete-purse'
requestAlchemySplitPay = '/alchemy/split-pay'
requestAlchemyCreateMember = '/alchemy/create-member'

requestAlchemySendSMS = '/alchemy/send-sms'
requestAlchemySendEmail = '/alchemy/send-email'

requestAlchemyCVP1 = '/shared-spaces'
requestAlchemySharedSpacesIB = '/shared-spaces/ib'
requestAlchemyCVP1Slash = '/shared-spaces/'
requestAlchemyCVP1Email = '/shared-spaces/email'

requestAlchemyCVP1AddTransaction = '/shared-spaces/add-transaction'
requestAlchemyCVP1GetTransactions = '/shared-spaces/get-transactions'

requestAlchemyScreenshots = '/alchemy/screenshots'

# Tasks
taskAuthentication = '/tasks/authentication'
taskCurrentAccountBalance = '/tasks/currentaccountbalance'
taskGetCurrentAccountBalance = '/tasks/getcurrentaccountbalance'
taskGetAMEXCard = '/tasks/getamexcard'
taskAlchemySplitPay = '/tasks/alchemy/split-pay'
taskAlchemySendSMS = '/tasks/alchemy/send-sms'
taskAlchemySendEmail = '/tasks/alchemy/send-email'
taskBrowserstackWorker = '/tasks/browserstack-worker'
taskBrowserstackScreenshot = '/tasks/browserstack-screenshot'

# CONR Jobs
cronRunAllUserTasks = '/cron/run-all-user-tasks'

app = webapp2.WSGIApplication([
	(requestWarmUp, default_controller.WarmupHandler),
	(requestDefault, default_controller.DefaultHandler),
	(requestAlchemyStatic, alchemy_controller.AlchemyStaticHandler),
	(requestAccountAggregationSSE, alchemy_controller.AccountaggregationSSE),
	(requestAccountAggregationCTA, alchemy_controller.AccountAggregationCTA),
	(requestAccountAggregationInit, alchemy_controller.AccountAggregationInit),
	(requestAccountAggregationSubmit, alchemy_controller.AccountAggregationSubmit),
	(requestAccountAggregationDisconnect, alchemy_controller.AccountAggregationDisconnect),
	(requestIntelligentSearchView, alchemy_controller.IntelligntSearchView),
	(requestAlchemyPush, alchemy_controller.Push),
	(requestAlchemyPushSSE, alchemy_controller.PushSSE),
	(requestAlchemyPushToken, alchemy_controller.RequestPushToken),
	(requestAlchemyPushAdmin, alchemy_controller.PushAdmin),
	(requestAlchemyPushAdminSSE, alchemy_controller.PushAdminSSE),
	(requestAlchemyBackgroundProcess, alchemy_controller.Background),
	(requestAlchemyCreateGroup, alchemy_controller.CreateGroup),
	(requestAlchemyGetGroup, alchemy_controller.GetGroup),
	(requestAlchemyAddMembersToGroup, alchemy_controller.AddMembersToGroup),
	(requestAlchemyCreatePurse, alchemy_controller.CreatePurse),
	(requestAlchemyUpdatePurse, alchemy_controller.UpdatePurse),
	(requestAlchemyDeletePurse, alchemy_controller.DeletePurse),
	(requestAlchemySplitPay, alchemy_controller.SplitPay),
	(requestAlchemyCreateMember, alchemy_controller.CreateMember),
	(requestAlchemySendSMS, alchemy_controller.SendSMS),
	(requestAlchemySendEmail, alchemy_controller.SendEmail),
	(requestAlchemyCVP1, alchemy_controller.SharedSpaces),
	(requestAlchemyCVP1Slash, alchemy_controller.SharedSpaces),
	(requestAlchemySharedSpacesIB, alchemy_controller.SharedSpacesIB),
	(requestAlchemyCVP1Email, alchemy_controller.SharedSpacesEmail),
	(requestAlchemyCVP1AddTransaction, alchemy_controller.AddTransaction),
	(requestAlchemyCVP1GetTransactions, alchemy_controller.GetTransactions),
	(requestAlchemyScreenshots, alchemy_controller.ReceiveScreenshots),
	(requestVoxeoManifest, default_controller.VoxeoVoiceManifestHandler),
	(requestVoxeoApplication, default_controller.VoxeoVoiceApplicationHandler),
	(requestSaveUserTaskStep1, default_controller.SaveUserTaskStep1),
	(requestSaveUserTaskStep2, default_controller.SaveUserTaskStep2),
	(requestSaveUserTaskStep3, default_controller.SaveUserTaskStep3),
	(requestGetTaskList, default_controller.GetTaskList),
	(requestGetTask, default_controller.GetTask),
	(requestSaveFacebookUserID, default_controller.SaveUserFacebookID),
	(taskAuthentication, tasks.AuthenticationTaskHandler),
	(taskCurrentAccountBalance, tasks.CurrentAccountBalanceTaskHandler),
	(taskGetCurrentAccountBalance, tasks.GetCurrentAccountBalanceHandler),
	(taskGetAMEXCard, tasks.GetAMEXCardHandler),
	(taskAlchemySplitPay, tasks.AlchemySplitPay),
	(taskAlchemySendSMS, tasks.SendSMS),
	(taskAlchemySendEmail, tasks.SendEmail),
	(taskBrowserstackWorker, tasks.CreateBrowserstackWorker),
	(taskBrowserstackScreenshot, tasks.TakeScreenshot),
	(cronRunAllUserTasks, cron.RunAllUserTasksHandler)
	],debug=True)
