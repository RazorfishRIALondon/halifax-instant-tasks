var RF = {
	init: function() {
		var self = this;
		this.addEventListeners();
	},

	addEventListeners: function() {
		var self = this;
		$("#intro").live("pageshow", function(event, ui){		  	
		  	/* 
		  	[ST] REMOVED: Getting the task list at the start requires a User ID, and a new User may not have one
		  	for some reason. It should be the Google Consumer Account User ID
		  	*/
		  	self.getTaskList();
		  	
		  	$(this).find("li").bind("swipe", function(e) {
				console.log(e);
			});
		});

		$("#create-task-step1 .task-list li a").bind("tap", function(e) {
			$("#create-task-step1 .task-list li").removeClass("active");
			$(this).parent("li").addClass("active");
		});
		
		/*
		*	On Page Show
		*/
		$("#create-task-step1, #create-task-step2, #edit-task").live("pageshow", function(event, url) {
			$(this).find("form").bind("submit", function(e) {
				e.preventDefault();
				var url = $(this).attr("action");
				$.mobile.changePage(url, {
					type: "post",
					data: $(this).serialize()
				});				
			});

			var button = $("#fb-connect");
	        if(button) {
	            button.bind("click", function(e) {
	                e.preventDefault();
	                FB.login(function(response) {
	                    if (response.authResponse) {
	                        console.log("User installed app");
	                        $.post("/actions/fbconnect", {
                                "facebook_id":response.authResponse.userID,
                                "user_id":self.currentUser
                            }, function(data) {
                                console.log(data);
                            });
	                    } else {
	                        console.log('User cancelled login or did not fully authorize.');
	                    }
	                }, {
	                	redirect_uri:"{{ gae_environment }}#create-task-step2",
	                    scope: "",
	                    display:""
	                });    
	                          
	            });
	        }
		});

		/*
		*	On Page Hide
		*/
		$("#create-task-step1, #create-task-step2").live("pagehide", function(event, url) {
			$(this).find("form").unbind("submit");
		});
		
		/*
		*	Create Task Step 3
		*/
		$("#create-task-step3").live("pageshow", function(event, url) {
			var form = $(this).find("form");
			form.bind("submit", function(e) {
				e.preventDefault();
				$.mobile.showPageLoadingMsg();
				var url = form.attr("action");
				$.post(url, form.serialize(), function(data){
					$.mobile.hidePageLoadingMsg();
					$.mobile.changePage("#intro");
				}).error(function() { 
					$.mobile.hidePageLoadingMsg();
					$.mobile.changePage('#error', 'pop', true, true);
				})
			});
		});
		
		/*
		*	Edit Page
		*/
		$("#edit-task").live("pageshow", function(event, url) {
			$("#edit-task .task-list li a").bind("click touchstart", function(e) {
				$("#edit-task .task-list li").removeClass("active");
				$(this).parent("li").addClass("active");				
			});

			
		});
		
	},

	getTaskList: function() {
		var self = this,
		user_id = $("#intro").attr("data-user-id");
	
		if(!!user_id) {
			$.mobile.showPageLoadingMsg();
			$("#intro").load("/actions/get-task-list?user_id="+user_id, function(data) {
				if($("#intro .task-list").length != 0) {
					$("#intro").removeClass("new");
					$("#intro").addClass("manage");
					self.addTaskListEvents();
				}			
				$.mobile.hidePageLoadingMsg();
			});	
		}
		
	},

	addTaskListEvents: function() {
		$("#task-list a").bind("click", function(e) {
			if(e && e.preventDefault) e.preventDefault();
			if(typeof($(this).attr("data-id")) != "undefined" && $(this).attr("data-id") != "") {
				$.mobile.changePage("/actions/edit-task", {
					type:"get",
					data:"task_id="+$(this).attr("data-id")+"&user_id="+$(this).attr("data-user-id")
				});
			}
		});
	}
}

RF.init();