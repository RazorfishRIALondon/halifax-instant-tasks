var analyticsElementArray=[];
var pageAnalyticsElementArray=[];


/************* PAGE SPECIFIC ANALYTICS CONTENT START ***********************/
function PageAnalyticsElement(name, content){
   this.tagAttribute=name;
   this.tagValue = content;
}


pageAnalyticsElementArray['WT.ti'] =  new PageAnalyticsElement('WT.ti','DCFCapturecarddetail');

pageAnalyticsElementArray['WT.sp'] =  new PageAnalyticsElement('WT.sp','IB;CurrentAccounts;Savings');

pageAnalyticsElementArray['WT.cg_n'] =  new PageAnalyticsElement('WT.cg_n','debitcardfunding');

pageAnalyticsElementArray['WT.cg_s'] =  new PageAnalyticsElement('WT.cg_s','dcfcapturecarddetail');

pageAnalyticsElementArray['WT.si_x'] =  new PageAnalyticsElement('WT.si_x','2');

pageAnalyticsElementArray['WT.si_n'] =  new PageAnalyticsElement('WT.si_n','debitcardfunding');

pageAnalyticsElementArray['DCSext.pagename'] =  new PageAnalyticsElement('DCSext.pagename','dcfcapturecarddetail');

pageAnalyticsElementArray['DCS.dcsaut'] =  new PageAnalyticsElement('DCS.dcsaut','[txtUserName]');

pageAnalyticsElementArray['DCSext.Brand'] =  new PageAnalyticsElement('DCSext.Brand','[strBrandName]');

pageAnalyticsElementArray['DCSext.makepymtamt'] =  new PageAnalyticsElement('DCSext.makepymtamt','[stramount]');

pageAnalyticsElementArray['DCSext.pageid'] =  new PageAnalyticsElement('DCSext.pageid','P221.01');

pageAnalyticsElementArray['DCSext.userjourney'] =  new PageAnalyticsElement('DCSext.userjourney','UJ-221');
pageAnalyticsElementArray['DCSext.isbalancetransferapplied'] =  new PageAnalyticsElement('DCSext.isbalancetransferapplied','[yes/no]');

/************* EVENT SPECIFIC ANALYTICS CONTENT START ***********************/
function AnalyticsElement(element, action, tagvalue){
    this.id=element;
    this.event=action;
    this.metatag=tagvalue;
}


