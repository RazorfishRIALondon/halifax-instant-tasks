var analyticsElementArray=[];
var pageAnalyticsElementArray=[];

// ************* PAGE SPECIFIC ANALYTICS CONTENT START ***********************//

function PageAnalyticsElement(name, content){
   this.tagAttribute=name;
   this.tagValue = content;
}

pageAnalyticsElementArray['WT.ti'] =  new PageAnalyticsElement('WT.ti','Account statement (Current Account)');
pageAnalyticsElementArray['WT.sp'] =  new PageAnalyticsElement('WT.sp','IB;Account Statement');
pageAnalyticsElementArray['WT.cg_n'] =  new PageAnalyticsElement('WT.cg_n','Account Statements');
pageAnalyticsElementArray['WT.cg_s'] =  new PageAnalyticsElement('WT.cg_s','Current Account Statement');
pageAnalyticsElementArray['DCS.dcsaut'] =  new PageAnalyticsElement('DCS.dcsaut','[txtUserName]');
// AVA PHASE 2 CODE STARTS.
pageAnalyticsElementArray['DCSext.Brand'] =  new PageAnalyticsElement('DCSext.Brand','[strBrandName]');
pageAnalyticsElementArray['DCSext.AccountType'] =  new PageAnalyticsElement('DCSext.AccountType','[straccountype]');
// AVA PHASE 2 CODE ENDS.

// ************* EVENT SPECIFIC ANALYTICS CONTENT START ***********************//

function AnalyticsElement(element, action, tagvalue){
    this.id=element;
    this.event=action;
    this.metatag=tagvalue;
}

analyticsElementArray['lkDelayedTrans'] =  new AnalyticsElement('lkDelayedTrans','click', "'WT.ac','common/properties.lnk0009a,common/properties.lnk0009b,View pending request'");
analyticsElementArray['lstAccFuncs:0:accountFunctionButtons:lkAccFuncs'] =  new AnalyticsElement('lstAccFuncs:0:lkAccFuncs','click', "'WT.ac','common/properties.btn0015a,common/properties.btn0015b,Make a payment'");
analyticsElementArray['lstAccFuncs:1:accountFunctionButtons:lkAccFuncs'] =  new AnalyticsElement('lstAccFuncs:1:lkAccFuncs','click', "'WT.ac','common/properties.btn0016a,common/properties.btn0016b,Make a transfer'");

analyticsElementArray['lnkRenametestNotGia'] =  new AnalyticsElement('lnkRenametestNotGia','click', "'WT.ac','common/properties.lnk0027b,common/properties.lnk0027b,Rename account'");
analyticsElementArray['lkDisplayOverdraftInfo'] =  new AnalyticsElement('lkDisplayOverdraftInfo','click', "'WT.ac','common/properties.lnk00043a,common/properties.lnk00043a,Change my overdraft limit'");
analyticsElementArray['lnktransactionsabroad'] =  new AnalyticsElement('lnktransactionsabroad','click', "'WT.ac','common/properties.lnk0087a,Find out more about transactions abroad'");

analyticsElementArray['lkM21txt001'] =  new AnalyticsElement('lkM21txt001','click', "'WT.ac','modules/m21_apply_online/m21htm301,Borrowing'");
analyticsElementArray['lkM21txt002'] =  new AnalyticsElement('lkM21txt002','click', "'WT.ac','modules/m21_apply_online/m21htm301,Credit cards','WT.si_x','Step 0 ','WT.si_n','Credit Card Application'");
analyticsElementArray['lkM21txt003'] =  new AnalyticsElement('lkM21txt003','click', "'WT.ac','modules/m21_apply_online/m21htm301,Personal Loans','WT.si_x','Step 0 ','WT.si_n','Loan Application'");
analyticsElementArray['lkM21txt004'] =  new AnalyticsElement('lkM21txt004','click', "'WT.ac','modules/m21_apply_online/m21htm301,Overdraft'");
analyticsElementArray['lkM21txt005'] =  new AnalyticsElement('lkM21txt005','click', "'WT.ac','modules/m21_apply_online/m21htm301,Mortages'");
analyticsElementArray['lkM21txt006'] =  new AnalyticsElement('lkM21txt006','click', "'WT.ac','modules/m21_apply_online/m21htm301,Savings and Investments'");
analyticsElementArray['lkM21txt008'] =  new AnalyticsElement('lkM21txt008','click', "'WT.ac','modules/m21_apply_online/m21htm301,eSavings','WT.si_x','Step 0 ','WT.si_n','Savings Product Application'");
analyticsElementArray['lkM21txt022'] =  new AnalyticsElement('lkM21txt022','click', "'WT.ac','modules/m21_apply_online/m21htm301,Easy Saver','WT.si_x','Step 0 ','WT.si_n','Savings Product Application'");
analyticsElementArray['lkM21txt010'] =  new AnalyticsElement('lkM21txt010','click', "'WT.ac','modules/m21_apply_online/m21htm301,Fixed Rate ISA','WT.si_x','Step 0 ','WT.si_n','ISA Application'");
analyticsElementArray['lkM21txt009'] =  new AnalyticsElement('lkM21txt009','click', "'WT.ac','modules/m21_apply_online/m21htm301,Cash ISA ','WT.si_x','Step 0 ','WT.si_n','ISA Application'");
analyticsElementArray['lkM21txt011'] =  new AnalyticsElement('lkM21txt011','click', "'WT.ac','modules/m21_apply_online/m21htm301,Pensions'");
analyticsElementArray['lkM21txt012'] =  new AnalyticsElement('lkM21txt012','click', "'WT.ac','modules/m21_apply_online/m21htm301,Share Dealing'");
analyticsElementArray['lkM21txt007'] =  new AnalyticsElement('lkM21txt007','click', "'WT.ac','modules/m21_apply_online/m21htm301,Incentive Saver'");
analyticsElementArray['m21txt25'] =  new AnalyticsElement('m21txt25','click', "'WT.ac','modules/m21_apply_online/m21htm301,Term Deposit','WT.si_x','Step 0','WT.si_n','Apply for Term Deposit'");
analyticsElementArray['lkm21txt3001'] =  new AnalyticsElement('lkm21txt3001','click', "'WT.ac','modules/m21_apply_online/m21htm301,Web Saver'");
analyticsElementArray['lkm21txt3002'] =  new AnalyticsElement('lkm21txt3002','click', "'WT.ac','modules/m21_apply_online/m21htm301,Everyday Saver'");
analyticsElementArray['lkm21txt3003'] =  new AnalyticsElement('lkm21txt3003','click', "'WT.ac','modules/m21_apply_online/m21htm301,Reward Saver'");
analyticsElementArray['lkm21txt3004'] =  new AnalyticsElement('lkm21txt3004','click', "'WT.ac','modules/m21_apply_online/m21htm301,Regular Saver'");
analyticsElementArray['lkm21txt3005'] =  new AnalyticsElement('lkm21txt3005','click', "'WT.ac','modules/m21_apply_online/m21htm301,ISA fixed'");
analyticsElementArray['lkm21txt3006'] =  new AnalyticsElement('lkm21txt3006','click', "'WT.ac','modules/m21_apply_online/m21htm301,ISA variable'");
analyticsElementArray['lkm21txt3008'] =  new AnalyticsElement('lkm21txt3008','click', "'WT.ac','modules/m21_apply_online/m21htm301,Fixed rate term deposit'");
analyticsElementArray['lkm21txt3009'] =  new AnalyticsElement('lkm21txt3009','click', "'WT.ac','modules/m21_apply_online/m21htm301,Online fixed rate term deposit'");
analyticsElementArray['lkm21txt3007'] =  new AnalyticsElement('lkm21txt3007','click', "'WT.ac','modules/m21_apply_online/m21htm301,ISA online'");
analyticsElementArray['lkM21txt013'] =  new AnalyticsElement('lkM21txt013','click', "'WT.ac','modules/m21_apply_online/m21htm301,Day to Day'");
analyticsElementArray['lkM21txt021'] =  new AnalyticsElement('lkM21txt021','click', "'WT.ac','modules/m21_apply_online/m21htm301,Current Accounts','WT.si_x','Step 0 ','WT.si_n','Primary Current Account Application'");
analyticsElementArray['lkM21txt014'] =  new AnalyticsElement('lkM21txt014','click', "'WT.ac','modules/m21_apply_online/m21htm301,Added Value Accounts','WT.si_x','Step 0 ','WT.si_n','AVA Application'");
analyticsElementArray['lkM21txt015'] =  new AnalyticsElement('lkM21txt015','click', "'WT.ac','modules/m21_apply_online/m21htm301,ID theft Protection'");
analyticsElementArray['lkM21txt016'] =  new AnalyticsElement('lkM21txt016','click', "'WT.ac','modules/m21_apply_online/m21htm301,Travel Money','WT.si_x','Step 0 ','WT.si_n','Apply for Travel Money'");
analyticsElementArray['lkM21txt017'] =  new AnalyticsElement('lkM21txt017','click', "'WT.ac','modules/m21_apply_online/m21htm301,Text alert and Mobile Banking'");
analyticsElementArray['lkM21txt018'] =  new AnalyticsElement('lkM21txt018','click', "'WT.ac','modules/m21_apply_online/m21htm301,Insurance'");
analyticsElementArray['lkM21txt019'] =  new AnalyticsElement('lkM21txt019','click', "'WT.ac','modules/m21_apply_online/m21htm301,Car Insurance'");
analyticsElementArray['lkM21txt020'] =  new AnalyticsElement('lkM21txt020','click', "'WT.ac','modules/m21_apply_online/m21htm301,Home Insurance'");


analyticsElementArray['rhc:lstAccToolList:0:lkBillManager'] =  new AnalyticsElement('rhc:lstAccToolList:0:lkBillManager','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt006,modules/M29_Account_Tools/properties.m29txt303,Bill Manager'");  
analyticsElementArray['rhc:lstAccToolList:0:lkNavigateLinkTarget$Text Alerts & Mobile Banking'] =  new AnalyticsElement('rhc:lstAccToolList:0:lkNavigateLinkTarget$Text Alerts & Mobile Banking','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt002,modules/M29_Account_Tools/properties.m29txt300,Text Alerts & Mobile Banking'");  
analyticsElementArray['rhc:lstAccToolList:1:lkNavigateLinkTarget$Text Alerts & Mobile Banking'] =  new AnalyticsElement('rhc:lstAccToolList:1:lkNavigateLinkTarget$Text Alerts & Mobile Banking','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt002,modules/M29_Account_Tools/properties.m29txt300,Text Alerts & Mobile Banking'");  
analyticsElementArray['rhc:lstAccToolList:2:lkNavigateLinkTarget$Text Alerts & Mobile Banking'] =  new AnalyticsElement('rhc:lstAccToolList:2:lkNavigateLinkTarget$Text Alerts & Mobile Banking','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt002,modules/M29_Account_Tools/properties.m29txt300,Text Alerts & Mobile Banking'");  
analyticsElementArray['rhc:lstAccToolList:3:lkNavigateLinkTarget$Text Alerts & Mobile Banking'] =  new AnalyticsElement('rhc:lstAccToolList:3:lkNavigateLinkTarget$Text Alerts & Mobile Banking','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt002,modules/M29_Account_Tools/properties.m29txt300,Text Alerts & Mobile Banking'");  

analyticsElementArray['rhc:lstAccToolList:0:lkNavigateLinkTarget$Start/stop paper statements'] =  new AnalyticsElement('rhc:lstAccToolList:0:lkNavigateLinkTarget$Start/stop paper statements','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt003,modules/M29_Account_Tools/properties.m29txt301,Start/stop paper statements'");  
analyticsElementArray['rhc:lstAccToolList:1:lkNavigateLinkTarget$Start/stop paper statements'] =  new AnalyticsElement('rhc:lstAccToolList:1:lkNavigateLinkTarget$Start/stop paper statements','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt003,modules/M29_Account_Tools/properties.m29txt301,Start/stop paper statements'");  
analyticsElementArray['rhc:lstAccToolList:2:lkNavigateLinkTarget$Start/stop paper statements'] =  new AnalyticsElement('rhc:lstAccToolList:2:lkNavigateLinkTarget$Start/stop paper statements','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt003,modules/M29_Account_Tools/properties.m29txt301,Start/stop paper statements'"); 
analyticsElementArray['rhc:lstAccToolList:3:lkNavigateLinkTarget$Start/stop paper statements'] =  new AnalyticsElement('rhc:lstAccToolList:3:lkNavigateLinkTarget$Start/stop paper statements','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt003,modules/M29_Account_Tools/properties.m29txt301,Start/stop paper statements'"); 
  
analyticsElementArray['rhc:lstAccToolList:0:lkNavigateLinkTarget$Save the Change'] =  new AnalyticsElement('rhc:lstAccToolList:0:lkNavigateLinkTarget$Save the Change','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt004,modules/M29_Account_Tools/properties.m29txt302,Save the Change'");  
analyticsElementArray['rhc:lstAccToolList:1:lkNavigateLinkTarget$Save the Change'] =  new AnalyticsElement('rhc:lstAccToolList:1:lkNavigateLinkTarget$Save the Change','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt004,modules/M29_Account_Tools/properties.m29txt302,Save the Change'");  
analyticsElementArray['rhc:lstAccToolList:2:lkNavigateLinkTarget$Save the Change'] =  new AnalyticsElement('rhc:lstAccToolList:2:lkNavigateLinkTarget$Save the Change','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt004,modules/M29_Account_Tools/properties.m29txt302,Save the Change'");  
analyticsElementArray['rhc:lstAccToolList:3:lkNavigateLinkTarget$Save the Change'] =  new AnalyticsElement('rhc:lstAccToolList:3:lkNavigateLinkTarget$Save the Change','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt004,modules/M29_Account_Tools/properties.m29txt302,Save the Change'");  

analyticsElementArray['selfservice'] =  new AnalyticsElement('selfservice','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt007,Self Service'");
analyticsElementArray['moreinterestcharges'] =  new AnalyticsElement('moreinterestcharges','click', "'WT.ac','modules/M29_Account_Tools/properties.m29txt005,Rates and charges'");



analyticsElementArray['m50htm600'] =  new AnalyticsElement('m50htm600','click', "'WT.ac','m50htm600,AARA'");



analyticsElementArray['m50htm501'] =  new AnalyticsElement('m50htm501','click', "'WT.ac','m50htm501,AAUR'");



analyticsElementArray['m50htm502'] =  new AnalyticsElement('m50htm502','click', "'WT.ac','m50htm502,AARH'");



analyticsElementArray['m50htm503'] =  new AnalyticsElement('m50htm503','click', "'WT.ac','m50htm503,AARS'");



analyticsElementArray['m50htm604'] =  new AnalyticsElement('m50htm604','click', "'WT.ac','m50htm604,TIGO'");



analyticsElementArray['m50htm605'] =  new AnalyticsElement('m50htm605','click', "'WT.ac','m50htm605,TIPL'");



analyticsElementArray['m50htm606'] =  new AnalyticsElement('m50htm606','click', "'WT.ac','m50htm606,TIPR'");



analyticsElementArray['m50htm607'] =  new AnalyticsElement('m50htm607','click', "'WT.ac','m50htm607,TIUR'");



analyticsElementArray['m50htm608'] =  new AnalyticsElement('m50htm608','click', "'WT.ac','m50htm608,TIEU'");



analyticsElementArray['m50htm509'] =  new AnalyticsElement('m50htm509','click', "'WT.ac','m50htm509,MOPI'");



analyticsElementArray['m50htm510'] =  new AnalyticsElement('m50htm510','click', "'WT.ac','m50htm510,SECI'");



analyticsElementArray['m50htm511'] =  new AnalyticsElement('m50htm511','click', "'WT.ac','m50htm511,CIUR'");



analyticsElementArray['m50htm512'] =  new AnalyticsElement('m50htm512','click', "'WT.ac','m50htm512,HECP'");



analyticsElementArray['m50htm513'] =  new AnalyticsElement('m50htm513','click', "'WT.ac','m50htm513,HECU'");



analyticsElementArray['m50htm514'] =  new AnalyticsElement('m50htm514','click', "'WT.ac','m50htm514,IDAW'");



analyticsElementArray['m50htm515'] =  new AnalyticsElement('m50htm515','click', "'WT.ac','m50htm515,SWMA'");



analyticsElementArray['m50htm516'] =  new AnalyticsElement('m50htm516','click', "'WT.ac','m50htm516,ODUR'");



analyticsElementArray['m50htm517'] =  new AnalyticsElement('m50htm517','click', "'WT.ac','m50htm517,TMUR'");



analyticsElementArray['m50htm518'] =  new AnalyticsElement('m50htm518','click', "'WT.ac','m50htm518,COFE'");



analyticsElementArray['m50htm610b'] =  new AnalyticsElement('m50htm610b','click', "'WT.ac','m50htm610b,SECI'");



analyticsElementArray['m50htm615'] =  new AnalyticsElement('m50htm615','click', "'WT.ac','m50htm615,SWMA'");


analyticsElementArray['m50htm609b'] =  new AnalyticsElement('m50htm609b','click', "'WT.ac','m50htm609b,MOPI'");


analyticsElementArray['m50htm614b'] =  new AnalyticsElement('m50htm614b','click', "'WT.ac','m50htm614b,IDAW'");


analyticsElementArray['pnlgrpStatement:conS1:frmVSPUpper:lnkShow'] =  new AnalyticsElement('pnlgrpStatement:conS1:frmVSPUpper:lnkShow','click', "'WT.ac','pages/p27_00_account_statement_summary/properties.p2700lnk500a,pages/p27_00_account_statement_summary/properties.p2700lnk500c,Show / Hide Cashback Extras offers below'");

analyticsElementArray['pnlgrpStatement:conS1:frmVSPUpper:lnkHide'] =  new AnalyticsElement('pnlgrpStatement:conS1:frmVSPUpper:lnkHide','click', "'WT.ac','pages/p27_00_account_statement_summary/properties.p2700lnk500a,pages/p27_00_account_statement_summary/properties.p2700lnk500c,Show / Hide Cashback Extras offers below'");


analyticsElementArray['rhc:lnkofferLink'] =  new AnalyticsElement('rhc:lnkofferLink','click', "'WT.ac','modules/m326/properties.m326lnk501a,modules/m326/properties.m326lnk501c,cashback offers'");



analyticsElementArray['rhc:lnkviewalloffer'] =  new AnalyticsElement('rhc:lnkviewalloffer','click', "'WT.ac','modules/m326/properties.m326btn500a,modules/m326/properties.m326btn500b,View all offers'");



analyticsElementArray['rhc:lklearnmore'] =  new AnalyticsElement('rhc:lklearnmore','click', "'WT.ac','modules/m326/properties.m326lnk502a,modules/m326/properties.m326lnk502c,Find out more about Cashback Extras'");
