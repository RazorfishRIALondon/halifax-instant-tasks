var analyticsElementArray=[];
var pageAnalyticsElementArray=[];


// ************* PAGE SPECIFIC ANALYTICS CONTENT START ***********************//
function PageAnalyticsElement(name, content){
   this.tagAttribute=name;
   this.tagValue = content;
}


pageAnalyticsElementArray['WT.ti'] =  new PageAnalyticsElement('WT.ti','Make transfer');

pageAnalyticsElementArray['WT.sp'] =  new PageAnalyticsElement('WT.sp','Retail Banking;Account Transaction');

pageAnalyticsElementArray['WT.cg_n'] =  new PageAnalyticsElement('WT.cg_n','Payments');

pageAnalyticsElementArray['WT.cg_s'] =  new PageAnalyticsElement('WT.cg_s','Make transfer');

pageAnalyticsElementArray['DCS.dcsaut'] =  new PageAnalyticsElement('DCS.dcsaut','[txtUserName]');


// ************* EVENT SPECIFIC ANALYTICS CONTENT START ***********************//
function AnalyticsElement(element, action, tagvalue){
    this.id=element;
    this.event=action;
    this.metatag=tagvalue;
}


analyticsElementArray['frmMakeTransfer:lkMakeAPayment2'] =  new AnalyticsElement('frmMakeTransfer:lkMakeAPayment2','click', "'WT.ac','common/properties.lnk0007a,common/properties.lnk0007b,Make a payment'");

analyticsElementArray['frmMakeTransfer:lnkCancel'] =  new AnalyticsElement('frmMakeTransfer:lnkCancel','click', "'WT.ac','common/properties.lnk0001a,common/properties.lnk0001b,Cancel'");

analyticsElementArray['frmMakeTransfer:btncontinue'] =  new AnalyticsElement('frmMakeTransfer:btncontinue','click', "'WT.ac','common/properties.btn0001a,common/properties.btn0001b,Continue','WT.si_x','step 1','WT.si_n','Make transfer','DCS.MakeTransferAmount','<Amount>','DCS.MakeTransferFromAcc','<Account name,Account Number,SortCode>','DCS.MakeTransferToAcc','<Account name,Account Number,SortCode>'");

