var analyticsElementArray=[];
var pageAnalyticsElementArray=[];


// ************* PAGE SPECIFIC ANALYTICS CONTENT START ***********************//
function PageAnalyticsElement(name, content){
   this.tagAttribute=name;
   this.tagValue = content;
}


pageAnalyticsElementArray['WT.ti'] =  new PageAnalyticsElement('WT.ti','Make Payment');

pageAnalyticsElementArray['WT.sp'] =  new PageAnalyticsElement('WT.sp','Retail Banking;Account Transaction');

pageAnalyticsElementArray['WT.cg_n'] =  new PageAnalyticsElement('WT.cg_n','Payments');

pageAnalyticsElementArray['WT.cg_s'] =  new PageAnalyticsElement('WT.cg_s','Make Payment');

pageAnalyticsElementArray['DCS.dcsaut'] =  new PageAnalyticsElement('DCS.dcsaut','[txtUserName]');


// ************* EVENT SPECIFIC ANALYTICS CONTENT START ***********************//
function AnalyticsElement(element, action, tagvalue){
    this.id=element;
    this.event=action;
    this.metatag=tagvalue;
}


analyticsElementArray['frmMakePayment:lkSetupNewPayee'] =  new AnalyticsElement('frmMakePayment:lkSetupNewPayee','click', "'WT.ac','pages/p08_01_make_payment/properties.p0801txt02, Set up a new recipient'");

analyticsElementArray['frmMakePayment:lkMakeTransfer1'] =  new AnalyticsElement('frmMakePayment:lkMakeTransfer1','click', "'WT.ac','common/properties.lnk0011a,common/properties.lnk0011b,Make a transfer'");

analyticsElementArray['frmMakePayment:lkSetupStandingOrder1'] =  new AnalyticsElement('frmMakePayment:lkSetupStandingOrder1','click', "'WT.ac','common/properties.lnk0012a,common/properties.lnk0012b,Set up a standing order'");

analyticsElementArray['frmMakePayment:lnkCancel'] =  new AnalyticsElement('frmMakePayment:lnkCancel','click', "'WT.ac','common/properties.lnk0001b,common/properties.lnk0001a,Cancel'");

analyticsElementArray['frmMakePayment:Btncontinue'] =  new AnalyticsElement('frmMakePayment:Btncontinue','click', "'WT.ac','common/properties.btn0001b,common/properties.btn0001a,Continue','WT.si_x','Step 1','WT.si_n','Make Payment'");

