var analyticsElementArray=[];

var pageAnalyticsElementArray=[];





/************* PAGE SPECIFIC ANALYTICS CONTENT START ***********************/

function PageAnalyticsElement(name, content){

   this.tagAttribute=name;

   this.tagValue = content;

}





pageAnalyticsElementArray['WT.ti'] =  new PageAnalyticsElement('WT.ti','DCFenteramount');



pageAnalyticsElementArray['WT.sp'] =  new PageAnalyticsElement('WT.sp','IB;CurrentAccounts;Savings');



pageAnalyticsElementArray['WT.cg_n'] =  new PageAnalyticsElement('WT.cg_n','debitcardfunding');



pageAnalyticsElementArray['WT.cg_s'] =  new PageAnalyticsElement('WT.cg_s','dcfenteramount');



pageAnalyticsElementArray['WT.si_x'] =  new PageAnalyticsElement('WT.si_x','1');



pageAnalyticsElementArray['WT.si_n'] =  new PageAnalyticsElement('WT.si_n','debitcardfunding');



pageAnalyticsElementArray['WT.tx_u'] =  new PageAnalyticsElement('WT.tx_u','1');



pageAnalyticsElementArray['WT.tx_e'] =  new PageAnalyticsElement('WT.tx_e','v');



pageAnalyticsElementArray['WT.pn_sku'] =  new PageAnalyticsElement('WT.pn_sku','debitcardfunding');



pageAnalyticsElementArray['DCSext.pagename'] =  new PageAnalyticsElement('DCSext.pagename','dcfenteramount');



pageAnalyticsElementArray['DCS.dcsaut'] =  new PageAnalyticsElement('DCS.dcsaut','[txtUserName]');



pageAnalyticsElementArray['DCSext.Brand'] =  new PageAnalyticsElement('DCSext.Brand','[strBrandName]');



pageAnalyticsElementArray['DCSext.pageid'] =  new PageAnalyticsElement('DCSext.pageid','P221.00');



pageAnalyticsElementArray['DCSext.userjourney'] =  new PageAnalyticsElement('DCSext.userjourney','UJ-221');





/************* EVENT SPECIFIC ANALYTICS CONTENT START ***********************/

function AnalyticsElement(element, action, tagvalue){

    this.id=element;

    this.event=action;

    this.metatag=tagvalue;

}





analyticsElementArray['frmDCFEnterAmount:lnkCancel'] =  new AnalyticsElement('frmDCFEnterAmount:lnkCancel','click', "'WT.ac','pages/p221_00/properties/P22100lnk500c, pages/p221_00/properties/P22100lnk500a, Cancel'");



analyticsElementArray['frmDCFEnterAmount:btncontinue'] =  new AnalyticsElement('frmDCFEnterAmount:btncontinue','click', "'WT.ac','pages/p221_00/properties/p22100btn500a, pages/p221_00/properties/p22100btn500b, Continue'");


