var analyticsElementArray=[];

var pageAnalyticsElementArray=[];




/************* PAGE SPECIFIC ANALYTICS CONTENT START ***********************/

function PageAnalyticsElement(name, content){

   this.tagAttribute=name;

   this.tagValue = content;

}





pageAnalyticsElementArray['WT.ti'] =  new PageAnalyticsElement('WT.ti','Your personal details');



pageAnalyticsElementArray['WT.sp'] =  new PageAnalyticsElement('WT.sp','IB;SpendingRewards');



pageAnalyticsElementArray['WT.cg_n'] =  new PageAnalyticsElement('WT.cg_n','SpendingRewards');



pageAnalyticsElementArray['WT.cg_s'] =  new PageAnalyticsElement('WT.cg_s','customerstatemanagement');



pageAnalyticsElementArray['DCSext.pagename'] =  new PageAnalyticsElement('DCSext.pagename','yourpersonaldetails');



pageAnalyticsElementArray['DCS.dcsaut'] =  new PageAnalyticsElement('DCS.dcsaut','[txtUserName]');





/************* EVENT SPECIFIC ANALYTICS CONTENT START ***********************/

function AnalyticsElement(element, action, tagvalue){

    this.id=element;

    this.event=action;

    this.metatag=tagvalue;

}





analyticsElementArray['frmChangePersonalDetails:lkToFindMore'] =  new AnalyticsElement('frmChangePersonalDetails:lkToFindMore','click', "'WT.ac','pages/p15_00_your_personlal_details/properties.p1500lnk503a,pages/p15_00_your_personlal_details/properties.p1500lnk503c,Find out more'");


