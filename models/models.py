from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import blobstore
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError

"""
  @name: BaseModel
  @description:
    Base db.Model Class for all other Data Entity Models
"""
class BaseModel(db.Model):
  def save(self):
    try:
      def txn():
        return self.put()
      return db.run_in_transaction(txn)
    except CapabilityDisabledError, capability_error:
      logging.error(self.__class__.__name__+' : CapabilityDisabledError')
      logging.error(capability_error)
      raise
    except Exception, e:
      logging.error(self.__class__.__name__+' : Exception')
      logging.error(e)
      raise

class User(BaseModel):
  email = db.EmailProperty(
    required=False,
    indexed=True
  )
  nickname = db.StringProperty(
    required=False,
    indexed=True
  )
  facebook_id = db.StringProperty(
    required=False,
    indexed=True
  )
  current_account_balance = db.FloatProperty(
    required=False,
    indexed=True
  )
  
class Task(BaseModel):
  user = db.ReferenceProperty(
    User,
    collection_name='user',
    verbose_name='User'
  )
  created = db.DateTimeProperty(
    auto_now_add=True,
    required=True,
    indexed=False
	)
  date_last_modified = db.DateTimeProperty(
    auto_now=True,
    required=False,
    indexed=True
	)
  title = db.StringProperty(
    required=False,
    indexed=True,
    default='My task'
  )
  active = db.BooleanProperty(
    required=False,
    indexed=True,
    default=True
  )



class Notification(BaseModel):
  text = db.PhoneNumberProperty(
    required=False,
    indexed=True,
    default=None
  )
  text_message = db.StringProperty(
    required=False,
    indexed=True
  )
  phone = db.PhoneNumberProperty(
    required=False,
    indexed=True
  )
  email = db.EmailProperty(
    required=False,
    indexed=True,
    default=None
  )
  facebook = db.BooleanProperty(
    required=False,
    indexed=True,
    default=None
  )

class CurrentAccount(Task):
  status = db.StringProperty(
    required=True,
    indexed=True,
    choices=set(['above','below','equal'])
  )
  balance = db.FloatProperty(
    required=True,
    indexed=True
  )
  notification = db.ReferenceProperty(
    Notification,
    collection_name='notification',
    verbose_name='Notification'
  )
  date = db.DateTimeProperty(
    required=False,
    indexed=True
  )
  date_ref = db.StringProperty(
    required=False,
    indexed=True,
    choices=set(['today','first','last'])
  )


class AmexAccount(BaseModel):
  created = db.DateTimeProperty(
    required=True,
    indexed=True,
    auto_now_add=True
  )
  last_modified = db.DateTimeProperty(
    required=True,
    indexed=True,
    auto_now=True
  )
  user_id = db.StringProperty(
    required=True,
    indexed=True
  )
  password = db.StringProperty(
    required=True,
    indexed=True  
  )
  card_member = db.StringProperty(
    required=True,
    indexed=True  
  )
  card_image = db.StringProperty(
    required=True,
    indexed=True  
  )
  card_name = db.StringProperty(
    required=True,
    indexed=True  
  )
  card_number = db.StringProperty(
    required=True,
    indexed=True  
  )
  card_balance = db.FloatProperty(
    required=True,
    indexed=True
  )
  card_credit_limit = db.FloatProperty(
    required=True,
    indexed=True
  )
  latest_transactions = db.TextProperty(
    required=False
  )