from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import blobstore
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
import logging
"""
  @name: BaseModel
  @description:
    Base db.Model Class for all other Data Entity Models
"""

class AlchemyBaseModel(db.Model):
  created = db.DateTimeProperty(
    required=True,
    indexed=True,
    auto_now_add=True
  )
  last_modified = db.DateTimeProperty(
    required=True,
    indexed=True,
    auto_now=True
  )
  def save(self):
    try:
      def txn():
        return self.put()
      return db.run_in_transaction(txn)
    except CapabilityDisabledError, capability_error:
      logging.exception(self.__class__.__name__+' : CapabilityDisabledError')
      logging.exception(capability_error)
      raise
    except Exception, e:
      logging.exception(self.__class__.__name__+' : Exception')
      logging.exception(e)
      raise

class Member(AlchemyBaseModel):
  first_name = db.StringProperty(
    required=True,
    indexed=True
  )
  last_name = db.StringProperty(
    required=True,
    indexed=True
  )
  phone = db.PhoneNumberProperty(
    required=False,
    indexed=True
  )
  mobile = db.PhoneNumberProperty(
    required=False,
    indexed=True
  )
  email = db.EmailProperty(
    required=False,
    indexed=True
  )
  
class Group(AlchemyBaseModel):
  account_id = db.StringProperty(
    required=True,
    indexed=True
  )
  title = db.StringProperty(
    required=True,
    indexed=True
  )
  description = db.TextProperty(
    required=False,
    indexed=False
  )
  balance = db.FloatProperty(
    required=True,
    indexed=True,
    default=0.0
  )
  members = db.ListProperty(db.Key)

class Purse(AlchemyBaseModel):
  group = db.ReferenceProperty(
    Group,
    collection_name='purses'
  )
  purse_type = db.StringProperty(
    choices=('open','closed'),
    required=True,
    indexed=True
  )
  title = db.StringProperty(
    required=True,
    indexed=True
  )
  description = db.TextProperty(
    required=False,
    indexed=False
  )
  balance = db.FloatProperty(
    required=False,
    indexed=True,
    default=0.0
  )  
  floor = db.FloatProperty(
    required=False,
    indexed=True,
    default=0.0
  )
  ceiling = db.FloatProperty(
    required=False,
    indexed=True,
    default=0.0
  )

class Transaction(AlchemyBaseModel):
  group = db.ReferenceProperty(
    Group,
    collection_name='transactions'
  )
  purse = db.ReferenceProperty(
    Purse,
    collection_name='transactions'
  )
  member = db.ReferenceProperty(
    Member,
    collection_name='transactions'
  )
  value = db.FloatProperty(
    required=True,
    indexed=True
  )

class Notification(AlchemyBaseModel):
  member = db.ReferenceProperty(
    Member,
    collection_name='notifications'
  )
  phone = db.BooleanProperty(
    required=False,
    indexed=True
  )
  mobile = db.BooleanProperty(
    required=False,
    indexed=True
  )
  email = db.BooleanProperty(
    required=False,
    indexed=True
  )