import os
import logging
import datetime
import base64
import cgi
import time
import urllib
import cStringIO
from google.appengine.ext import db
import webapp2
from google.appengine.api import memcache
from google.appengine.ext import blobstore
from google.appengine.api import taskqueue
from google.appengine.api import mail
from google.appengine.api import urlfetch
from google.appengine.api import users
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime import apiproxy_errors 
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.ext.db import BadValueError
from google.appengine.api import images
import json
from collections import deque
from operator import itemgetter

from models import models
from controllers import datastore

class RunAllUserTasksHandler(webapp2.RequestHandler):
	def process(self):
		try:
			# Check for App Engine CRON Header or System Admin access
			if not self.request.headers.has_key('X-Appengine-Cron'):
				
				# Else check for System Admin access
				is_system_admin = users.is_current_user_admin()
				
				if not is_system_admin:
					logging.warning('WARNING: Illegal access to CRON job by user')
					user = users.get_current_user()
					if user is not None:
						logging.warning('User nickname : '+str(user.nickname()))
						logging.warning('User email : '+str(user.email()))
						logging.warning('User ID : '+str(user.user_id()))
					# Return immediately and do not run the tasks
					return False

			# Flush the cache
			memcache.flush_all()

			# Get all prefiltered active Tasks
			all_tasks = datastore.get_current_account_tasks()
			
			for task in all_tasks:
				taskqueue.add(
					queue_name='allusertasks', 
					url='/tasks/getcurrentaccountbalance', 
					params={
						'user_id':task.user.key().name()
					}
				)

			return True

		except Exception, e:
			raise e		

	def get(self):
		self.process()
	def post(self):
		self.process()
