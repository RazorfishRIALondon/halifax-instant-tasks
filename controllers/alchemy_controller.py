from google.appengine.api import memcache
import os
import re
import time
import logging
import datetime
import webapp2
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
from google.appengine.ext import blobstore
from google.appengine.api import users
from google.appengine.api.datastore import Key
from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.api import mail
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError   
from google.appengine.ext.db import BadValueError
from google.appengine.ext import db
from google.appengine.runtime import DeadlineExceededError
from operator import itemgetter
import urllib
import urllib2
import json
import hashlib

# Import local scripts
import httpagentparser
from controllers import sms_controller
from controllers import utils
from controllers import datastore
import main

class Alchemy(utils.BaseHandler):
	def options(self):
		is_alchemy = self.set_alchemy_response_headers()
		try:
			# This is for CORS Preflight Requests, so return nothing other than the CORS Response Headers set in 
			# utils.BaseHandler.set_alchemy_response_headers()
			return
		except Exception, e:
			raise e


class GetTransactions(Alchemy):
	def process(self):
		is_alchemy = self.set_alchemy_response_headers()
		self.set_request_arguments()
		self.response_body = ''
		try:
			memcache_space_key = 'sharedspace:'+self.context['request_args']['space_name']
			memcache_space_response = memcache.get(memcache_space_key)

			logging.info('memcache_space_response')
			logging.info(memcache_space_response)
			
			# If we do return the LBG User ID as the data payload, so that the Account HTML can be requested
			self.response_body += 'retry: 5000\n'
			self.response_body += 'id: '+self.now.isoformat()+'\n'
			self.response_body += 'event: Transactions\n'

			# Check if push devices dict is available
			if memcache_space_response is not None and len(memcache_space_response) > 0:
				self.response_body += 'data: '+json.dumps(memcache_space_response)+'\n\n'
			# Else return empty data and allow the client to keep trying
			else:
				self.response_body += 'data: \n\n'
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			self.response.headers['Content-Type'] = 'text/event-stream'
			self.response.headers['Cache-Control'] = 'No-cache'
			self.response.headers['Connection'] = 'Keep-alive'
			self.response.out.write(self.response_body)
	def get(self):
		self.process()
	def post(self):
		self.process()

class AddTransaction(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		try:
			if 'space_name' not in self.context['request_args'] or self.context['request_args']['space_name'] == '':
				raise Exception('No space_name provided')

			if 'date' not in self.context['request_args'] or self.context['request_args']['date'] == '':
				raise Exception('No date provided')

			if 'user' not in self.context['request_args'] or self.context['request_args']['user'] == '':
				raise Exception('No user provided')

			if 'amount' not in self.context['request_args'] or self.context['request_args']['amount'] == '':
				raise Exception('No amount provided')

			memcache_space_key = 'sharedspace:'+self.context['request_args']['space_name']
			memcache_space_response = memcache.get(memcache_space_key)
			if memcache_space_response is None:
				memcache_space_response = dict()

			if self.context['request_args']['user'] not in memcache_space_response:
				memcache_space_response[self.context['request_args']['user']] = []

			memcache_space_response[self.context['request_args']['user']].append(dict(
				date=self.context['request_args']['date'],
				amount=self.context['request_args']['amount']
			))

			# Set the cache
			memcache.set(memcache_space_key, memcache_space_response)

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict()
			)

		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

	def get(self):
		self.process()
	def post(self):
		self.process()

class ReceiveScreenshots(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		try:
			pass
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			return

	def get(self):
		self.process()
	def post(self):
		self.process()

class SharedSpacesEmail(utils.BaseHandler):
	def process(self):
		template = 'email'
		try:
			pass
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			self.render_alchemy_cvp1_template(template)

	def get(self):
		self.process()
	def post(self):
		self.process()

class SharedSpaces(utils.BaseHandler):
	def process(self):
		template = 'index'
		try:
			memcache_key='space:invitees'
			self.context['invitees'] = memcache.get(memcache_key)
		except Exception, e:
			logging.exception(e)			
		finally:
			self.render_alchemy_cvp1_template(template)

	def get(self):
		self.process()
	def post(self):
		self.process()

class SharedSpacesIB(utils.BaseHandler):
	def process(self):
		template = 'ib'
		try:
			memcache_key='space:invitees'
			self.context['invitees'] = memcache.get(memcache_key)
		except Exception, e:
			logging.exception(e)			
		finally:
			self.render_alchemy_cvp1_template(template)

	def get(self):
		self.process()
	def post(self):
		self.process()


"""
	@name: Push
	@description:
		Alchemy Concept for demonstratic Push Notifications For Web

"""

class PushAdmin(utils.BaseHandler):
	def get(self):
		template = 'alchemy-push-admin'
		try:
			# Check to see if we have data available
			memcache_key = 'push:devices'
			push_devices = dict()
			memcache.set(memcache_key, value=push_devices)

		except Exception, e:
			logging.exception(e)
			self.set_response_error(e, 500)
			raise e
		finally:
			self.render(template)
	def post(self):
		self.set_request_arguments()
		template = 'alchemy-push-admin'
		try:
			action = None
			# Set Push Notification
			if 'action' in self.context['request_args'] and self.context['request_args']['action'] != '':
				action = self.context['request_args']['action']
				memcache_key = 'action:'+action
				if action == 'custom':
					memcache.set(memcache_key, dict(
						title=self.context['request_args']['title'],
						description=self.context['request_args']['description'],
						url=self.context['request_args']['url'],
						timestamp=self.now
					))
				else:					
					memcache.set(memcache_key, dict(timestamp=self.now))
			else:
				memcache_keys = ['action:message_1','action:message_2','action:message_3','action:custom']
				memcache.delete_multi(memcache_keys)

		except Exception, e:
			logging.exception(e)
			self.set_response_error(e, 500)
			raise e
		finally:
			self.render(template)

class PushAdminSSE(utils.BaseHandler):
	def get(self):
		self.set_request_arguments()
		self.response_body = ''
		try:
			# If This is the Admin User, push back the number of connections
			user = users.get_current_user()
			is_system_admin = users.is_current_user_admin()

			# If we do return the LBG User ID as the data payload, so that the Account HTML can be requested
			self.response_body += 'retry: 2000\n'
			self.response_body += 'id: '+self.now.isoformat()+'\n'
			self.response_body += 'event: Devices\n'

			# Check to see if we have data available
			memcache_key = 'push:devices'
			client = memcache.Client()
			push_devices = client.gets(memcache_key)
			
			logging.info('push_devices')
			logging.info(push_devices)

			# Check if push devices dict is available
			if push_devices is not None and len(push_devices) > 0:
				# For each key,value check to see if the client is still connected, and if not remove it form the dict
				for key, value in push_devices.items():
					if memcache.get(key) is None:
						del push_devices[key]
				self.response_body += 'data: '+json.dumps(dict(devices=push_devices))+'\n\n'
			# Else return empty data and allow the client to keep trying
			else:
				self.response_body += 'data: \n\n'
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			self.response.headers['Content-Type'] = 'text/event-stream'
			self.response.headers['Cache-Control'] = 'No-cache'
			self.response.headers['Connection'] = 'Keep-alive'
			self.response.out.write(self.response_body)

class Push(utils.BaseHandler):
	def process(self):
		template = 'alchemy-push'
		try:
			md5 = hashlib.md5()
			md5.update(datetime.datetime.now().isoformat('T'))
			hexdigest = md5.hexdigest()
			self.context['push_token'] = hexdigest
		except Exception, e:
			logging.exception(e)
			self.set_response_error(e, 500)
			raise e
		finally:
			self.render(template)
	def get(self):
		self.process()
	def post(self):
		self.process()

class RequestPushToken(Alchemy):
	def post(self):
		is_alchemy = self.set_alchemy_response_headers()
		hexdigest = None
		try:
			if not is_alchemy:
				raise Exception('Unauthorised')
				
			md5 = hashlib.md5()
			md5.update(datetime.datetime.now().isoformat('T'))
			hexdigest = md5.hexdigest()
			self.response.out.write(hexdigest)
		except Exception, e:
			logging.exception(e)
			self.error(403)			

class PushSSE(Alchemy):
	def get(self):
		is_alchemy = self.set_alchemy_response_headers()
		self.set_request_arguments()
		self.response_body = ''
		try:
			if not is_alchemy:
				raise Exception('Unauthorised')
				
			if 'push_token' not in self.context['request_args']:
				raise Exception('No push_token provided')
				
			user_agent = httpagentparser.detect(self.request.headers.get('User-Agent'))

			# Add the Push Token for the Client to Memcache
			memcache_key_push_token = self.context['request_args']['push_token']
			# Flush it in 8 seconds unless this is reset
			memcache.set(memcache_key_push_token, value=self.context['request_args']['push_token'], time=1*8)

			memcache_key = 'push:devices'
			
			client = memcache.Client()
			while True:
				push_devices = client.gets(memcache_key)				
				push_devices[self.context['request_args']['push_token']] = user_agent
					
				if client.cas(memcache_key, value=push_devices):
					break
				
			
			# If we do return the LBG User ID as the data payload, so that the Account HTML can be requested
			self.response_body += 'retry: 5000\n'
			self.response_body += 'id: '+self.now.isoformat()+'\n'

			# Check to see if we have data available for Custom message
			memcache_key_custom = 'action:custom'
			custom = memcache.get(memcache_key_custom)
			if custom is not None:
				self.response_body += 'id: custom_'+custom['timestamp'].isoformat()+'\n'
				self.response_body += 'event: custom\n'
				self.response_body += 'data: {\n'
				self.response_body += 'data: "id":"custom_'+custom['timestamp'].isoformat()+'",\n'
				self.response_body += 'data: "timestamp":"'+custom['timestamp'].isoformat('T')+'",\n'
				self.response_body += 'data: "iconUrl":"https://razorfish-lbg.appspot.com/alchemy/static/img/ltsb_icon.png",\n'
				self.response_body += 'data: "title":"'+custom['title']+'",\n'
				self.response_body += 'data: "body":"'+custom['description']+'",\n'
				self.response_body += 'data: "url":"'+custom['url']+'"\n'
				self.response_body += 'data: }\n\n'				
			else:
				self.response_body += 'id: custom_'+self.now.isoformat()+'\n'
				self.response_body += 'event: custom\n'
				self.response_body += 'data: \n\n'
			

			self.response.headers['Content-Type'] = 'text/event-stream'
			self.response.headers['Cache-Control'] = 'No-cache'
			self.response.headers['Connection'] = 'Keep-alive'
			self.response.out.write(self.response_body)
		except Exception, e:
			logging.exception(e)
			self.error(403)
			


class AccountaggregationSSE(Alchemy):
	def get(self):
		self.set_request_arguments()
		is_alchemy = self.set_alchemy_response_headers()
		self.response_body = ''
		try:
			if is_alchemy:
				if 'lbg_user_id' not in self.context['request_args'] or self.context['request_args']['lbg_user_id'] == '':
					raise Exception('No lbg_user_id provided')

				# Set the LBG User ID
				lbg_user_id = self.context['request_args']['lbg_user_id']

				# If we do return the LBG User ID as the data payload, so that the Account HTML can be requested
				self.response_body += 'retry: 1000\n'
				self.response_body += 'id: '+self.now.isoformat()+'\n'
				self.response_body += 'event: AccountStatus\n'

				# Check to see if we have data available
				amex_account = datastore.get_amex_account(**dict(
					lbg_user_id=lbg_user_id
					)
				)
				# If we have an account now saved by our Task then return the LBG User ID
				if amex_account is not None:
					self.response_body += 'data: '+lbg_user_id+'\n\n'
				# Else return empty data and allow the client to keep trying
				else:
					self.response_body += 'data: \n\n'
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			self.response.headers['Content-Type'] = 'text/event-stream'
			self.response.headers['Cache-Control'] = 'No-cache'
			self.response.headers['Connection'] = 'Keep-alive'
			self.response.out.write(self.response_body)

class AccountAggregationCTA(Alchemy):
	def post(self):
		self.set_request_arguments()
		is_alchemy = self.set_alchemy_response_headers()
		template = None
		try:
			if is_alchemy:
				self.context['alchemy'] = dict(
					account=dict(
						lbg_user_id='919668954'
					)					
				)
				template = 'alchemy-account-aggregation-cta'
		except Exception, e:
			logging.exception(e)
			self.context['error'] = str(e)
			template = 'alchemy-account-aggregation-failure'
			raise e
		finally:
			self.render(template)

class AccountAggregationInit(Alchemy):
	def post(self):
		self.set_request_arguments()
		is_alchemy = self.set_alchemy_response_headers()
		template = None
		amex_account = None
		try:
			if is_alchemy:
				if 'lbg_user_id' not in self.context['request_args'] or self.context['request_args']['lbg_user_id'] == '':
					raise Exception('No lbg_user_id provided')

				# Collect the LBG User ID
				lbg_user_id = self.context['request_args']['lbg_user_id'] or None
				
				if lbg_user_id is not None:
					# Check for existing AMEX credentials
					amex_account = datastore.get_amex_account(**dict(
						lbg_user_id=lbg_user_id
						)
					)

				if amex_account is not None:
					# Return the Account Summary
					self.context['alchemy'] = dict(
						account=amex_account,
						timestamp=datetime.datetime.now(tz=utils.GMT1())
					)
					template = 'alchemy-account-aggregation-summary'
				else:
					# Return the Login Form
					self.context['alchemy'] = dict(
						form_action=main.requestAccountAggregationSubmit,
						account=dict(
							lbg_user_id=lbg_user_id
						)						
					)

					template = 'alchemy-account-aggregation-login'
		except Exception, e:
			logging.exception(e)
			self.context['error'] = str(e)
			template = 'alchemy-account-aggregation-failure'
			raise e
		finally:
			self.render(template)

class AccountAggregationSubmit(Alchemy):
	def post(self):
		self.set_request_arguments()
		is_alchemy = self.set_alchemy_response_headers()
		template = 'alchemy-account-aggregation-failure'
		try:
			if is_alchemy:
				if 'lbg_user_id' not in self.context['request_args'] or self.context['request_args']['lbg_user_id'] == '':
					raise Exception('No lbg_user_id provided')

				# Kick off a Task to collect the AMEX account
				taskqueue.add(
					queue_name='currentaccount', 
					url='/tasks/getamexcard', 
					params={
						'lbg_user_id':self.context['request_args']['lbg_user_id'],
						'user_id':self.context['request_args']['user_id'],
						'password':self.context['request_args']['password']
					}
				)

				template = 'alchemy-account-aggregation-pending'
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			self.render(template)

class AccountAggregationDisconnect(Alchemy):
	def post(self):
		self.set_request_arguments()
		is_alchemy = self.set_alchemy_response_headers()
		template = None
		try:
			if is_alchemy:
				# Collect the LBG User ID
				lbg_user_id = self.context['request_args']['lbg_user_id'] or None
				if lbg_user_id is not None:
					lbg_user_id = str(lbg_user_id)
					# Clear Memcache record
					memcache_key_1 = 'aggregation:amex:'+lbg_user_id
					memcache_key_2 = 'amex:'+lbg_user_id
					memcache_key_3 = 'amex:request:'+lbg_user_id
					deleted_memcache_keys = memcache.delete_multi(keys=[memcache_key_1,memcache_key_2,memcache_key_3], seconds=0)
					
					if not deleted_memcache_keys:
						raise Exception('Failed to delete AMEX Memcache keys')

					amex_account = datastore.get_amex_account(**dict(
						lbg_user_id=lbg_user_id
						)
					)
					if amex_account is not None:
						def delete_account():
							db.delete(amex_account)

						deleted = db.run_in_transaction(delete_account)
						
					else:
						logging.warning('No AMEX account found in datastore')

					template = 'alchemy-account-aggregation-disconnected'
				else:
					raise Exception('No lbg_user_id provided')

		except Exception, e:
			logging.exception(e)
			self.set_response_error(e, 500)
			raise e
		finally:
			self.render(template)

class AlchemyStaticHandler(Alchemy):
	def get(self, path):
		self.set_request_arguments()
		# The request for Require itself should not use CORS when using the Proxy Server
		# This is because it is injected using a HTML Response Body Rewrite rule, and therefore won't use XHR headers
		#is_alchemy = self.set_alchemy_response_headers()
		try:
			pass
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			# [ST]TODO: We need the Origin Request header to be set by the source site, otherwise the static content 
			# will never be served
			#if is_alchemy:
			self.render_alchemy_static_file(path)
			
class IntelligntSearchView(Alchemy):
	def post(self):
		self.set_request_arguments()
		is_alchemy = self.set_alchemy_response_headers()
		template = None
		try:
			if is_alchemy:
				template = 'alchemy-intelligent-search'
		except Exception, e:
			logging.exception(e)
			self.set_response_error(e, 500)
			raise e
		finally:
			self.render(template)

class Background(utils.BaseHandler):
	def get(self):
		template = 'background'
		try:
			pass
		except Exception, e:
			logging.exception(e)
			self.set_response_error(e, 500)
			raise e
		finally:
			self.render(template)

class CreateGroup(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# Create Fund
			group = datastore.create_group(**self.context['request_args'])

			group_dict = db.to_dict(group)
			group_dict['key'] = group.key().id()
			group_dict['created'] = group.created.isoformat('T')
			group_dict['last_modified'] = group.last_modified.isoformat('T')

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					group=group_dict
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class GetGroup(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# Create Fund
			group = datastore.get_group(**dict(key=self.context['request_args']['group']))

			group_dict = db.to_dict(group)
			group_dict['key'] = group.key().id()
			group_dict['created'] = group.created.isoformat('T')
			group_dict['last_modified'] = group.last_modified.isoformat('T')
			# Get the list of Members
			members_list = []
			for member_key in group.members:
				member = datastore.get_member(**dict(key=member_key.id()))
				member_dict = dict()
				member_dict['key'] = member.key().id()
				member_dict['created'] = member.created.isoformat('T')
				member_dict['first_name'] = member.first_name
				member_dict['last_name'] = member.first_name
				member_dict['phone'] = member.phone
				member_dict['mobile'] = member.mobile
				member_dict['email'] = member.email
				members_list.append(member_dict)

			group_dict['members'] = members_list

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					group=group_dict
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class AddMembersToGroup(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# Add Members to Fund
			group = datastore.add_members_to_group(**self.context['request_args'])

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict()
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class CreateMember(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			member = datastore.create_member(**self.context['request_args'])

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					member=dict(
						key=member.key().id(),
						created=member.created.isoformat('T'),
						first_name=member.first_name,
						last_name=member.last_name,
						phone=member.phone,
						mobile=member.mobile,
						email=member.email
					)
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class CreatePurse(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:	
			# Create a new Purse
			purse = datastore.create_purse(**self.context['request_args'])
			purse_dict = db.to_dict(purse)
			if purse.group is not None:
				purse_dict['group'] = purse.group.key().id()
			purse_dict['created'] = purse.created.isoformat('T')
			purse_dict['last_modified'] = purse.last_modified.isoformat('T')
			purse_dict['key'] = purse.key().id()

			# Create the data output
			self.data_output = dict(
				status='success',
				data=dict(
					purse=purse_dict
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class UpdatePurse(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# Update an existing Purse
			purse = datastore.update_purse(**self.context['request_args'])

			# Create the data output
			self.data_output = dict(
				status='success',
				data=dict()
			)

		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class DeletePurse(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:

			# Delete an existing Purse
			deleted_purse = datastore.delete_purse(**self.context['request_args'])

			# Create the data output
			self.data_output = dict(
				status='success',
				data=dict()
			)

		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class GetPurses(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			if 'group' not in self.context['request_args'] or self.context['request_args']['group'] == '':
				raise Exception('No group provided')

			# Get Group
			group = datastore.get_group(**dict(key=self.context['request_args']['group']))

			if group is None:
				raise Exception('No group found in datastore')

			# Get Purses
			purses = [db.to_dict(p) for p in group.purses]

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					purses=purses
				)
			)
			
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()
"""
	@name: SplitPay
	@description:

	@arguments:


"""
class SplitPay(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# Get list of Members
			if 'members' not in self.context['request_args'] or self.context['request_args']['members'] == '':
				raise Exception('No members provided')

			# Get Payee ID
			if 'payee' not in self.context['request_args'] or self.context['request_args']['payee'] == '':
				raise Exception('No payee provided')

			# Get Group Account ID
			if 'group' not in self.context['request_args'] or self.context['request_args']['group'] == '':
				if 'account_id' not in self.context['request_args'] or self.context['request_args']['account_id'] == '':
					raise Exception('No group or account_id provided')

				# Get Group Title
				if 'title' not in self.context['request_args'] or self.context['request_args']['title'] == '':
					raise Exception('No title provided')

			# Save the Transactions in the Datastore
			transactions = datastore.split_pay(**self.context['request_args'])
			logging.info(transactions)

			# Run background Tasks to issue the payment
			taskqueue.add(
				queue_name='splitpay', 
				url='/tasks/alchemy/split-pay', 
				params={
					'members':self.context['request_args']['members'],
					'payee':self.context['request_args']['payee']
				}
			)

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					transactions=transactions
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class SendSMS(Alchemy):
	def post(self):
		is_alchemy = self.set_alchemy_response_headers()
		self.set_request_arguments()
		sender = 'SHARED SPACES'
		try:

			if 'mobile' not in self.context['request_args'] or self.context['request_args']['mobile'] == '':
				raise Exception('No mobile provided')

			if 'message' not in self.context['request_args'] or self.context['request_args']['message'] == '':
				raise Exception('No message provided')

			if 'sender' in self.context['request_args'] and self.context['request_args']['sender'] != '':
				sender = self.context['request_args']['sender']
				
			task = taskqueue.add(
				queue_name='sms', 
				url=main.taskAlchemySendSMS, 
				params={
					'to':self.context['request_args']['mobile'],
					'sender':sender,
					'message':self.context['request_args']['message']
				}
			)

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					task=dict(
						id=task.name,
						eta=task.eta.isoformat('T')
					)
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()

class SendEmail(Alchemy):
	def post(self):
		is_alchemy = self.set_alchemy_response_headers()
		self.set_request_arguments()
		try:
			if 'payload' not in self.context['request_args'] or self.context['request_args']['payload'] == '':
				raise Exception('No payload provided')

			# Kick off screenshot Worker
			"""
			taskqueue.add(
				queue_name='browserstack', 
				url=main.taskBrowserstackWorker, 
				params={
					'url':'https://razorfish-lbg.appspot.com/shared-spaces#account'
				}
			)
			"""
			payload = json.loads(self.context['request_args']['payload'])
			

			subject = ''
			tasks = []
			space_name = ''
			members = []
			members_names = []

			# Set Space Name
			if payload.get('name') is not None:
				space_name = payload.get('name')

			# Create text list of Member names
			if payload.get('members') is not None:
				for member in payload.get('members'):
					members.append(member)
					members_names.append(member.get('name'))

			logging.info(members)
			
			memcache_key='space:invitees'
			memcache.set(key=memcache_key, value=members)

			# Create task for EmailMessage
			for member in members:
				if 'email' in member and 'name' in member and 'link' in member:
					if payload.get('subject') is not None:
						subject = payload.get('subject')
					task = taskqueue.add(
						queue_name='email', 
						url=main.taskAlchemySendEmail, 
						params={
							'sender':'Shared Spaces <sharedspaces.a1@gmail.com>',
							'space_name':space_name,
							'email':member['email'],
							'subject':subject,
							'name':member['name'],
							'link':member['link'],
							'members':','.join(members_names)
						}
					)

					tasks.append(dict(id=task.name, eta=task.eta.isoformat('T')))

			# Create data output
			self.data_output = dict(
				status='success',
				data=dict(
					tasks=tasks
				)
			)
		except Exception, e:
			logging.exception(e)
			self.set_json_response_error(e, 500)
		finally:
			self.render_json()