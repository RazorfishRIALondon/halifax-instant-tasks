import os
import logging
import datetime
import base64
import cgi
import time
import urllib
import cStringIO
from google.appengine.ext import db
from google.appengine.api.datastore import Key
from google.appengine.api import taskqueue
from google.appengine.api import mail
from google.appengine.api import urlfetch
from google.appengine.api import memcache
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime import apiproxy_errors 
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.ext.db import BadValueError
from ConfigParser import ConfigParser
from operator import itemgetter

"""
	@name: send_sms_alert()
	@description:
		Sends an SMS message to a User's mobile phone
"""
def send_sms_alert(**kwargs):
	try:
		
		if not kwargs.has_key('to') or kwargs['to'] == '':
			raise Exception('send_sms_alert() : No to (mobile) provided')

		if not kwargs.has_key('sender') or kwargs['sender'] == '':
			raise Exception('send_sms_alert() : No sender (mobile or issuer name) provided')

		if not kwargs.has_key('message') or kwargs['message'] == '':
			raise Exception('send_sms_alert() : No message provided')


		sms_properties = load_property_file('sms')
		sms_http_service = sms_properties.get('Intellisoft', 'http_service')
		sms_username = sms_properties.get('Intellisoft', 'username')
		sms_password = sms_properties.get('Intellisoft', 'password')
		sms_link = sms_properties.get('Intellisoft', 'link')
		args = dict()
		args['username'] = sms_username
		args['password'] = sms_password
		args['to'] = str(kwargs['to'])
		args['from'] = str(kwargs['sender'])
		args['text'] = str(kwargs['message'])
		args['maxconcat'] = 1
		args['type'] = 1

		args = urllib.urlencode(args)
		url = sms_http_service+'?%s' % args
		
		response = urlfetch.fetch(url=url, method='POST', deadline=30)
		if response.status_code == 200:
			logging.info('send_sms_alert() : Succesful Response')
			logging.info(response.content)
		else:
			logging.info('send_sms_alert() : Error Response')
			logging.info(response.status_code)
			logging.info(response.content)

		return response.content
	except Exception, e:
		logging.error(e)
		raise e


def load_property_file(file_name):
	try:
		property_file = memcache.get(file_name+'.properties', namespace='razorfish')
		if property_file is not None:
			return property_file
		else:
			property_file_path = 'properties/'+file_name+'.properties'
			property_file = ConfigParser()
			property_file.read(property_file_path)
			memcache.set(file_name+'.properties', property_file, namespace='razorfish')
			return property_file
	except Exception, e:
		logging.error(e)
		raise