import logging
import json
from google.appengine.api import urlfetch
from google.appengine.api import memcache
import urllib
import base64
import hashlib
import hmac

# import local scripts

"""
	@name: post_to_users_feed
	@description:
		Post data to a User's feed, so that it will appear in their Wall/Timeline

"""
def post_to_users_feed(fb_user_id, app_access_token, message, picture, link, name, caption, description):
	response = None
	result = None
	try:
		args = dict()
		args['access_token'] = app_access_token
		args['picture'] = picture
		args['link'] = link
		args['name'] = name

		# Avoid unicode characters
		args['message'] = message.encode('ascii', 'xmlcharrefreplace')
		args['caption'] = caption.encode('ascii','xmlcharrefreplace')
		args['description'] = description.encode('ascii', 'xmlcharrefreplace')
		if args['description'] == '':
			args['description'] = ' '
		
		args = urllib.urlencode(args)
		
		url = 'https://graph.facebook.com/'+fb_user_id+'/feed?%s' % args
		
		response = urlfetch.fetch(url=url, method='POST', deadline=30)
		if response.status_code == 200:
			result = response.content
			#logging.debug('facebook_helper.py : post_to_users_feed() : Getting User response')
			#logging.debug(result)
		else:
			result = json.loads(response.content)
			if result.has_key('error') and result['error'].has_key('code') and result['error']['code'] == 200:
				logging.warning('facebook_helper.py : post_to_users_feed() : Error Status code is 200, no system errors. Proceed as normal.')
				result = True
			else:
				logging.warning('facebook_helper.py : post_to_users_feed() : Error Response received')
				logging.warning(response.status_code)
			
		response = result
	except Exception, e:
		logging.error(e)
		raise
	finally:
		return response

"""
	@name: exchange_oauth_code_for_accesstoken
	@description:
		Exchange an OAuth code fort an access token
"""
def exchange_code_for_accesstoken(app_id, app_secret, app_url, code):
	response = None
	# [ST] Fixed error where urlfetch response is unexpected
	result = None
	try:
		args = dict()
		args['client_id'] = app_id
		#args['redirect_uri'] = app_url
		args['redirect_uri'] = ''
		args['client_secret'] = app_secret
		args['code'] = code
		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/oauth/access_token?%s' % args

		logging.debug('exchange_code_for_accesstoken() : url')
		logging.debug(url)
		response = urlfetch.fetch(url=url, method='GET', deadline=30)
		if response.status_code == 200:
			result = response.content
			#logging.debug('facebook_helper.py : exchange_code_for_accesstoken() : Getting User response')
			#logging.debug(result)
		else:
			logging.error('facebook_helper.py : exchange_code_for_accesstoken() : Error Response received')
			logging.error(response.status_code)
			logging.error(response.content)

		response = result

	except Exception, e:
		logging.error(e)
	finally:
		return response


"""
	@name: get_facebook_user
	@description:
		Get the permission-provided information about the current User
	@arguments:
		oauth_token
		user_id
"""
def get_facebook_user(fb_user_id, user_oauth_token):
	result = None
	response = None
	data_dict = dict()
	try:
		
		#logging.debug('facebook_helper.py : get_facebook_user() : fb_user_id : '+str(fb_user_id))
		#logging.debug('facebook_helper.py : get_facebook_user() : user_oauth_token : '+str(user_oauth_token))
		# Set default parameters for the User
		args = dict()
		args['access_token'] = user_oauth_token
		args = urllib.urlencode(args)
		
		url = 'https://graph.facebook.com/'+fb_user_id+'?%s' % args
		
		response = urlfetch.fetch(url=url, method='GET', deadline=30)
		if response.status_code == 200:
			result = json.loads(response.content)
			#logging.debug('facebook_helper.py : get_facebook_user() : Getting User response')
			#logging.debug(result)
			for key,value in result.items():
				# Do not attempt to coerce the value to a string!! If it's a unicode special character
				# it will throw an error
				data_dict[str(key)] = value

			result = data_dict
		else:
			logging.error('facebook_helper.py : get_facebook_user() : Error Response received')
			logging.error(response.status_code)
			logging.error(response.content)

		return result
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error('get_facebook_user()')
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error('get_facebook_user()')
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error('get_facebook_user()')
		logging.error(e)
		raise

"""
	@name: get_facebook_user_friends
	@description:
		Get the permission-provided information about the current User
	@arguments:
		oauth_token
		user_id
"""
def get_facebook_user_friends(fb_user_id, user_oauth_token):
	result = None
	response = None
	try:
		if fb_user_id is None:
			raise Exception('facebook_helper.py : get_facebook_user_friends() : No fb_user_id provided')
		if user_oauth_token is None:
			raise Exception('facebook_helper.py : get_facebook_user_friends() : No user_oauth_token provided')
		# Set default parameters for the User
		args = dict()
		args['access_token'] = user_oauth_token
		args['limit'] = '200'
		args = urllib.urlencode(args)
		
		url = 'https://graph.facebook.com/'+fb_user_id+'/friends?%s' % args
		
		response = urlfetch.fetch(url=url, method='GET', deadline=30)
		if response.status_code == 200:
			result = json.loads(response.content)
			#logging.debug('facebook_helper.py : get_facebook_user_friends() : Getting User Friends response')
			#logging.debug(result)
		else:
			logging.error('facebook_helper.py : get_facebook_user_friends() : Error Response received')
			logging.error(response.status_code)
			logging.error(response.content)

		return result
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error('get_facebook_user_friends()')
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error('get_facebook_user_friends()')
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error('get_facebook_user_friends()')
		logging.error(e)
		raise

"""
	@name: get_facebook_app_access_token
	@description:
		Request an App Access Token. We can use this, for example, to request the creation of a Facebook Test User
	@arguments:
		None
	@returns:
		Facebook App Access Token [String]
"""
def get_facebook_app_access_token(app_id, app_secret):
	try:
		app_access_token = None
		memcache_key = 'app_access_token:'+app_id
		memcache_result = memcache.get(memcache_key)
		if memcache_result is not None:
			return memcache_result
		else:
			args = dict()
			args['client_id'] = app_id
			args['client_secret'] = app_secret
			args['grant_type'] = 'client_credentials'
			args = urllib.urlencode(args)
			url = 'https://graph.facebook.com/oauth/access_token?%s' % args
			result = urlfetch.fetch(url, deadline=30)
			if result.status_code == 200:
				# Facebook's HTTP Response Body content includes a String with the prefix of 'access_token='
				# We don't want this, so strip it out if it exists
				app_access_token = result.content.replace('access_token=','')
				memcache.set(memcache_key, value=app_access_token, time=800)

				return app_access_token
			else:
				logging.error('facebook_helper.py : get_facebook_app_access_token() : Error response received')  		
				logging.error(result.content)
				raise Exception('Error getting App Access Token')
  		
  	except urlfetch.InvalidURLError, invalid_url_error:
  		logging.error('get_facebook_app_access_token()')
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error('get_facebook_app_access_token()')
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error('get_facebook_app_access_token()')
		logging.error(e)
		raise

"""
	@name: set_app_restrictions
	@description:
		HTTP POST to set location, age restrictions
	@arguments:
		app_id
		app_access_token
	@returns:
		JSON object of restrictions set
"""
def set_app_restrictions(app_id, app_access_token, restrictionsJSONString):
	result = None
	try:
		response = None
		# Set default parameters for Test User
		args = dict()
		args['access_token'] = app_access_token
		args['restrictions'] = restrictionsJSONString
		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/'+app_id+'?%s' % args
		
		response = urlfetch.fetch(url=url, method='POST', deadline=60)
		if response.status_code == 200:
			#logging.debug('facebook_helper.py : set_app_restrictions() : response received')
			#logging.debug(response.content)
			url2 = 'https://graph.facebook.com/'+app_id+'?fields=restrictions&access_token='+app_access_token
			response2 = urlfetch.fetch(url=url2, method='GET', deadline=30)
			#logging.debug('facebook_helper.py : get_app_restrictions() : response received')
			#logging.debug(response2.content)
			result = response2.content
		else:
			logging.error('facebook_helper.py : set_app_restrictions() : Error Response received')
			logging.error(response.content)
			result = response.content


	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error('set_app_restrictions()')
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error('set_app_restrictions()')
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error('set_app_restrictions()')
		logging.error(e)
		raise
	finally:
		return result


"""
	@name: get_facebook_test_users
	@description:
		Get all Test Users for the App
	@arguments:
		app_acces_token: The Access Token for our app
	@returns:
		A JSON object with a data array of Test User objects
		or
		An error object
"""
def get_facebook_test_users(app_id, app_access_token):
	result = dict()
	response = None
	r = None
	try:
		
		# Set default parameters for Test User
		args = dict()
		args['access_token'] = app_access_token
		args['limit'] = '50'#510'
		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/'+app_id+'/accounts/test-users?%s' % args
		
		r = urlfetch.fetch(url=url, method='GET', deadline=30)
		if r.status_code == 200:
			json_data = json.loads(r.content)
			
			if json_data.has_key('data'):
				result = json_data['data']
				response = result
			else:
				logging.warning('facebook_helper.py : get_facebook_test_users() : No Facebook Test Users received')
				logging.debug(r.content)
			
		else:
			logging.error('facebook_helper.py : get_facebook_test_users() : Error Response received')
			logging.debug(r.content)

		return response
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise



"""
	@name: create_facebook_test_user
 	@description:
 		Create a single Facebook Test User
 	@arguments:
 		Valid Parameters:

		installed : true/false [DEFAIULT=false]
		locale : e.g. de_DE
		name : FULL_NAME
		permissions : e.g. read_stream
		method : e.g. post [DEFAULT=post]
		access_token : APP_ACCESS_TOKEN

		Invalid Parameters:

		birthday : e.g. 05/10/1975
"""
def create_facebook_test_user(app_id, app_access_token, test_user_data):
	try:
		args = dict()
		args['access_token'] = app_access_token
		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/'+app_id+'/accounts/test-users?%s' % args
		url += '&'+test_user_data
		
		logging.debug('create_facebook_test_user() : url')
		logging.debug(url)

		result = urlfetch.fetch(url=url, method='POST', deadline=30)
		if result.status_code == 200:
			return json.loads(result.content)
		else:
			logging.error('facebook_helper.py : create_facebook_test_user() : Error creating Facebook Test Users')
			logging.error(result.content)
			return None
		
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise


"""
	@name: create_facebook_test_user
 	@description:
 		Create a single Facebook Test User
 	@arguments:
 		Valid Parameters:

		installed : true/false [DEFAIULT=false]
		locale : e.g. de_DE
		name : FULL_NAME
		permissions : e.g. read_stream
		method : e.g. post [DEFAULT=post]
		access_token : APP_ACCESS_TOKEN

		Invalid Parameters:

		birthday : e.g. 05/10/1975
"""
def change_facebook_test_user_name_password(app_access_token, test_user_id, **kwargs):
	try:
		args = dict()
		args['access_token'] = app_access_token
		if kwargs.has_key('name') and kwargs['name'] != '':
			args['name'] = kwargs['name']

		if kwargs.has_key('password') and kwargs['password'] != '':
			args['password'] = kwargs['password']
		else:
			args['password'] = 'razorf1sh'

		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/'+test_user_id+'?%s' % args
		
		result = urlfetch.fetch(url=url, method='POST', deadline=30)
		if result.status_code == 200:
			return json.loads(result.content)
		else:
			logging.error('facebook_helper.py : change_facebook_test_user_name_password() : Error changing Facebook Test User Name or Password')
			logging.error(result.content)
			return None
		
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise

"""
	@name: get_facebook_test_user
	@description:
		Get a specific Test User for the App
	@arguments:
		app_acces_token: The Access Token for our app
	@returns:
		A JSON object with a data array of Test User objects (should just be one)
		or
		An error object
"""
def get_facebook_test_user(app_access_token, test_user_id):
	try:
		result = None
		response = None
		# Set default parameters for Test User
		args = dict()
		args['access_token'] = app_access_token
		args = urllib.urlencode(args)
		
		url = 'https://graph.facebook.com/'+test_user_id+'?%s' % args
		
		response = urlfetch.fetch(url=url, method='GET', deadline=30)
		if response.status_code == 200:
			result = json.loads(response.content)
			#logging.debug('Getting Test User response')
			#logging.debug(result)
		else:
			logging.error('facebook_helper.py : get_facebook_test_user() : Error Response received, when attempting to get individual Test User')
			logging.error(response)

		return result
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise

"""
"""
def delete_facebook_test_user(app_access_token, test_user_id):
	try:
		test_user_id = str(test_user_id)
		args = dict()
		args['method'] = 'delete'
		args['access_token'] = app_access_token
		args = urllib.urlencode(args)

		url = 'https://graph.facebook.com/'+test_user_id+'?%s' % args
		
		result = urlfetch.fetch(url=url, method='GET', deadline=30)
		if result.status_code == 200:
			return result.content
		else:
			logging.error('facebook_helper.py : delete_facebook_test_user() : Error deleting Test User with ID: '+test_user_id)
			logging.error(result.content)
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise

"""
	@name: make_facebook_test_user_friends
	@description:
		Connect 2 Test User accounts together as Friends
	@arguments:
		TEST_USER_1_ID
		TEST_USER_1_ACCESS_TOKEN
		TEST_USER_1_ID
		TEST_USER_2_ACCESS_TOKEN

		https://graph.facebook.com/TEST_USER_1_ID/friends/TEST_USER_2_ID?
		  method=post
		  &access_token=TEST_USER_1_ACCESS_TOKEN

		https://graph.facebook.com/TEST_USER_2_ID/friends/TEST_USER_1_ID?
		  method=post
		  &access_token=TEST_USER_2_ACCESS_TOKEN
	@response: true on success, false otherwise

"""
def make_facebook_test_user_friends(test_user_1_id, test_user_1_access_token, test_user_2_id, test_user_2_access_token):
	result_1 = None
	result_2 = None
	response = None
	try:
		# Set default parameters for Test User 1
		args_step_1 = dict()
		args_step_1['access_token'] = test_user_1_access_token
		args_step_1 = urllib.urlencode(args_step_1)
		
		url_step_1 = 'https://graph.facebook.com/'+test_user_1_id+'/friends/'+test_user_2_id+'?%s' % args_step_1
		
		response_step_1 = urlfetch.fetch(url=url_step_1, method='POST', deadline=30)
		if response_step_1.status_code == 200:
			result_1 = response_step_1.content
			#logging.debug('Facebook : Make Test User '+test_user_1_id+' friends with '+test_user_2_id)
			#logging.debug(result_1)

			# Set default parameters for Test User 2
			args_step_2 = dict()
			args_step_2['access_token'] = test_user_2_access_token
			args_step_2 = urllib.urlencode(args_step_2)
			
			url_step_2 = 'https://graph.facebook.com/'+test_user_2_id+'/friends/'+test_user_1_id+'?%s' % args_step_2
			
			response_step_2 = urlfetch.fetch(url=url_step_2, method='POST', deadline=30)
			if response_step_2.status_code == 200:
				result_2 = response_step_2.content
				#logging.debug('Facebook : Make Test User '+test_user_2_id+' friends with '+test_user_1_id)
				#logging.debug(result_2)
				response = result_2
			else:
				logging.debug(response_step_2.content)
				raise Exception('Make Test Users Friends Step 1 failed')
		else:
			logging.error('facebook_helper.py : make_facebook_test_user_friends() : Error Response received, when attempting to make friends Step 1')
			logging.error(response_step_1.content)
			raise Exception('Make Test Users Friends Step 1 failed')
		
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise
	finally:
		return response



"""
	@name: create_facebook_user_app_notification(user_id, app_access_token, href, template)
	@description:
		Create a Facebook App Notification
		Facebook Notifications API Documentation: https://developers.facebook.com/docs/app-notifications/
	@arguments:
		user_id: The Facebook User ID of the User to send an App Request to
		app_access_token: App Access Token
		href: The relative path/GET params of the target (for example, "index.html?gift_id=123", or "?gift_id=123")
		template: The customized text of the notification. (https://developers.facebook.com/docs/app-notifications/#templating)
	@returns:
	Success: {
			 	"success": true
			 }
"""
def create_facebook_user_app_notification(**kwargs):
	response = None
	try:
		args = dict()
		args['href'] = kwargs['href']
		args['template'] = kwargs['template']
		args['access_token'] = kwargs['app_access_token']
		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/'+kwargs['user_id']+'/notifications?%s' % args
		

		result = urlfetch.fetch(url=url, method='POST', deadline=30)
		if result.status_code == 200:
			logging.debug('facebook_helper.py : create_facebook_user_app_notification() : result.content')
			logging.debug(result.content)
			response = json.loads(result.content)
		else:
			logging.error('facebook_helper.py : create_facebook_user_app_notification() : Error creating App Notification for User ID: '+kwargs['user_id'])
			logging.error(result.content)
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise
	finally:
		return response
"""
	@name: create_facebook_user_app_request(user_id, app_access_token, message, data)
	@description:
		Create a Facebook App Request
	@arguments:
		user_id: The Facebook User ID of the User to send an App Request to
		app_access_token: App Access Token
		message: The App message
		data: Returned along with an App Request referral
	@returns:
	Success: request [Request Object ID], to [array of recipient User IDs]
"""
def create_facebook_user_app_request(user_id, app_access_token, message, data):
	response = None
	try:
		args = dict()
		args['message'] = message
		args['data'] = data
		args['access_token'] = app_access_token
		args = urllib.urlencode(args)
		url = 'https://graph.facebook.com/'+user_id+'/apprequests?%s' % args
		

		result = urlfetch.fetch(url=url, method='POST', deadline=30)
		if result.status_code == 200:
			logging.debug('facebook_helper.py : create_facebook_user_app_request() : result.content')
			logging.debug(result.content)
			response = json.loads(result.content)
		else:
			logging.error('facebook_helper.py : create_facebook_user_app_request() : Error creating App Request for User ID: '+user_id)
			logging.error(result.content)
	except urlfetch.InvalidURLError, invalid_url_error:
		logging.error(invalid_url_error)
		raise
	except urlfetch.Error, generic_error:
		logging.error(generic_error)
		raise
	except Exception, e:
		logging.error(e)
		raise
	finally:
		return response

		
"""
	@name: delete_facebook_user_app_request
	@description:
		Delete a Facebook User's App Request, e.g. after they have accepted one
		Use the HTTP DELETE method to do this at:
		https://graph.facebook.com/<request_object_id>_<user_id>
		Returns HTTP Response with body content of boolean True if successful, error otherwise
		Ref: https://developers.facebook.com/docs/requests/#deleting
	@arguments:
		user_id : The Facebook User ID from a Signed Request (as we won't have stored the User in the Datastore at this stage)
		app_request_id : The AppRequest ID in the POST query string parameters
		oauth_token : The User OAuth Token from the Signed Request
	@returns
		Success: Boolean True
		Failure: Boolean False
"""
def delete_facebook_user_app_request(user_id, app_request_id, oauth_token):
	response = False
	try:
		oauth_token = str(oauth_token)
		user_id = str(user_id)
		app_request_id = str(app_request_id)
		url = 'https://graph.facebook.com/'+app_request_id+'_'+user_id+'?access_token='+oauth_token
		response = urlfetch.fetch(url=url, method='DELETE', deadline=30)
		if response.status_code == 200:
			#logging.debug('Deleted App Request ID '+app_request_id+' for User ID '+user_id)
			response = True		
	except Exception, e:
		logging.error('facebook_helper.py : delete_facebook_user_app_request() : Failed to delete App Request ID '+app_request_id+' for User ID '+user_id)
		logging.error(e)
	finally:
		return response


"""
	@description:
		Parses an OAuth 2.0 Facebook Signed Request, received as a POST Request param to a Facebook Canvas App
	@arguments:
		signed_request: the HMAC-SHA256 signed request byte array string
		secret: The Facebook Canvas App Secret Key
	@returns:
		JSON (dict) object or None
"""	
def parse_signed_request(signed_request, secret):
	try:
		l = signed_request.split('.', 2)
		encoded_sig = l[0]
		payload = l[1]
		sig = base64_url_decode(encoded_sig)        
		decoded_payload = base64_url_decode(payload)
		data = json.loads(decoded_payload)
		if data.get('algorithm').upper() != 'HMAC-SHA256':
			logging.error('Unknown algorithm')
			return None
		else:
			expected_sig = hmac.new(secret, msg=payload, digestmod=hashlib.sha256).digest()
		if sig != expected_sig:
			logging.error('invalid signed request received...')
			return None
		else:
			#logging.debug('valid signed request received...')
			return data   
   	except Exception, e:
		logging.error('facebook_helper.py : parse_signed_request()')
		logging.error(e)
		raise

def base64_url_decode(inp):
	padding_factor = (4 - len(inp) % 4) % 4
  	inp += "="*padding_factor
  	return base64.b64decode(unicode(inp).translate(dict(zip(map(ord, u'-_'), u'+/')))) 
