from google.appengine.api import memcache
from ConfigParser import ConfigParser
from google.appengine.runtime import apiproxy_errors
from google.appengine.api import mail
import logging

# Import local scripts
from controllers import datastore
from models import models


def send_email(**kwargs):
	subject = ''
	try:
		mail_properties = load_property_file('mail')
		# Create Email
		message = mail.EmailMessage()
		# Set the Mail Sender
		message.sender = kwargs['sender']
		# Set the mail recipients 
		message.to = kwargs['email']
		# Set the Mail subject
		if 'subject' not in kwargs or kwargs['subject'] == '':
			subject = mail_properties.get('SharedSpaces', 'subject')
		else:
			subject = kwargs['subject']
			
		message.subject = subject

		if kwargs['name'] != '':
			name = kwargs['name']
		else:
			name = kwargs['email']


		body_hmtl = mail_properties.get('SharedSpaces', 'body_html')
		body_hmtl = body_hmtl.replace('@space_name@', kwargs['space_name'])
		body_hmtl = body_hmtl.replace('@name@', name)
		body_hmtl = body_hmtl.replace('@link@', kwargs['link'])

		members = kwargs['members'].split(',')
		logging.info(members)
		
		# Set Member list
		if name in members:
			del members[members.index(name)]

		if len(members) > 0:
			if len(members) == 1:
				body_hmtl = body_hmtl.replace('@members@', members[0])
			elif len(members) == 2:
				body_hmtl = body_hmtl.replace('@members@', members[0]+' &amp; '+members[1])
			else:
				last_member = members.pop()
				members = ', '.join(members)
				members = members + ' &amp; ' + last_member
				body_hmtl = body_hmtl.replace('@members@', members)
		else:
			body_hmtl = body_hmtl.replace('@members@', 'other members who are waiting for you!')

		# Set the HTML
		message.html = body_hmtl

		body_text = mail_properties.get('SharedSpaces', 'body_text')
		body_text = body_text.replace('@name@', name)
		body_text = body_text.replace('@link@', kwargs['link'])
		# Set the Body
		message.body = body_text

		# Send the Email
		message.send()
	except Exception, e:
		logging.exception(e)
		raise e

def send_notification(**kwargs):
	response = False
	body_notification = ''
	html_notification = ''
	try:
		if not kwargs.has_key('user_id') or kwargs['user_id'] == None or kwargs['user_id'] == '':
			raise Exception('send_notification() : No user_id provided')

		if not kwargs.has_key('account_balance') or kwargs['account_balance'] == None or kwargs['account_balance'] == '':
			raise Exception('send_notification() : No account_balance provided')

		if not kwargs.has_key('balance') or kwargs['balance'] == None or kwargs['balance'] == '':
			raise Exception('send_notification() : No balance provided')

		if not kwargs.has_key('status') or kwargs['status'] == None or kwargs['status'] == '':
			raise Exception('send_notification() : No status provided')

		if not kwargs.has_key('title') or kwargs['title'] == None or kwargs['title'] == '':
			raise Exception('send_notification() : No title provided')

		if not kwargs.has_key('actions') or kwargs['actions'] == None or kwargs['actions'] == '':
			raise Exception('send_notification() : No actions provided')

		if not kwargs.has_key('email') or kwargs['email'] == None or kwargs['email'] == '':
			raise Exception('send_notification() : No email provided')

		if not kwargs.has_key('sender') or kwargs['sender'] == None or kwargs['sender'] == '':
			raise Exception('send_notification() : No sender provided')

		
		body_notification = 'Your Current Account Balance is '+kwargs['status']+' &pound;'+str(kwargs['balance'])+'.\n\nIt\'s currently at &pound;'+str(kwargs['account_balance'])
		user = models.User.get_by_key_name(str(kwargs['user_id']))
		mail_properties = load_property_file('mail')
		html_notification = mail_properties.get('HTMLTemplate', 'body')
		html_notification = html_notification.replace('@user.nickname@', user.email)
		html_notification = html_notification.replace('@account_balance@', str(kwargs['account_balance']))
		html_notification = html_notification.replace('@balance@',  str(kwargs['balance']))
		html_notification = html_notification.replace('@status@',  str(kwargs['status']))
		html_notification = html_notification.replace('@task_title@',  str(kwargs['title']))
		html_notification = html_notification.replace('@actions@',  str(kwargs['actions']))
		

		# Create Email
		message = mail.EmailMessage()
		# Set the Mail Sender
		message.sender = kwargs['sender']
		# Set the mail recipients 
		message.to = kwargs['email']
		# Set the Mail subject
		message.subject = mail_properties.get('HTMLTemplate', 'subject')
		# Set the Mail HTML Body
		message.html = html_notification
		# Set the Mail Body
		message.body = body_notification
		# Send the Email
		message.send()
		
		return True
		
	except apiproxy_errors.ApplicationError, app_error:
		logging.error('send_notification() : apiproxy_errors.ApplicationError')
		logging.error(app_error)
		raise app_error
	except apiproxy_errors.OverQuotaError, over_quota_error:
		logging.error('send_notification() : apiproxy_errors.OverQuotaError')
		logging.error(over_quota_error)
		raise over_quota_error
	except Exception, e:
		logging.error('send_notification() : Exception')
		logging.error(e)
		raise e
		

def load_property_file(file_name):
	try:
		property_file = memcache.get(file_name+'.properties', namespace='razorfish')
		if property_file is not None:
			return property_file
		else:
			property_file_path = 'properties/'+file_name+'.properties'
			property_file = ConfigParser()
			property_file.read(property_file_path)
			memcache.set(file_name+'.properties', property_file, namespace='razorfish')
			return property_file
	except Exception, e:
		logging.error(e)
		raise
