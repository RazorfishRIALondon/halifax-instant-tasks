from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.ext import db
from google.appengine.api.datastore import Key
from google.appengine.api import memcache
import logging
import os
import time
import cgi
import datetime
import random
import re
import itertools
import json

# Import local scripts
from models import models
from models import alchemy_models
from controllers import date_utils


def get_group(**kwargs):
	response = None
	try:
		if not 'key' in kwargs or kwargs['key'] == '':
			raise Exception('No group key provided')

		memcache_key = 'group:'+str(kwargs['key'])
		memcache_response = memcache.get(memcache_key)
		if memcache_response is not None:
			response = memcache_response
		else:
			group = alchemy_models.Group.get_by_id(long(kwargs['key']))
			if group is not None:
				memcache.set(memcache_key, value=group)
				response = group

		return response
	except Exception, e:
		logging.exception(e)
		raise e

def create_group(**kwargs):
	try:
		if not 'account_id' in kwargs or kwargs['account_id'] == '':
			raise Exception('No account_id provided')

		if not 'title' in kwargs or kwargs['title'] == '':
			raise Exception('No title provided')

		# Create the new Group
		group = alchemy_models.Group(
			account_id=kwargs['account_id'],
			title=kwargs['title'],
			description=kwargs['description'],
			balance=float(kwargs['balance'])
		)

		# Sve the Group
		group.save()

		return group
	except Exception, e:
		logging.exception(e)
		raise e

def get_member(**kwargs):
	response = None
	try:
		if not 'key' in kwargs or kwargs['key'] == '':
			raise Exception('No member key provided')

		memcache_key = 'member:'+str(kwargs['key'])
		memcache_response = memcache.get(memcache_key)
		if memcache_response is not None:
			response = memcache_response
		else:
			member = alchemy_models.Member.get_by_id(long(kwargs['key']))
			if member is not None:
				memcache.set(memcache_key, value=member)
				response = member

		return response
	except Exception, e:
		logging.exception(e)
		raise e

def create_member(**kwargs):
	try:
		member = alchemy_models.Member(
			first_name=kwargs['first_name'].encode('ascii', 'xmlcharrefreplace'),
			last_name=kwargs['last_name'].encode('ascii', 'xmlcharrefreplace'),
			email=kwargs['email'],
			phone=kwargs['phone'],
			mobile=kwargs['mobile']
		)

		member.save()

		return member
	except Exception, e:
		logging.exception(e)
		raise e

def add_members_to_group(**kwargs):
	try:
		if not 'group' in kwargs or kwargs['group'] == '':
			raise Exception('No group provided')

		group = get_group(**dict(key=kwargs['group']))

		if group is None:
			raise Exception('No group found in datastore with Key ID %s', kwargs['group'])

		if not 'members' in kwargs or kwargs['members'] == '':
			raise Exception('No members provided')

		member_keys = [Key.from_path('Member', long(k)) for k in kwargs['members'].split(',')]
		for m in member_keys:
			# Append the Member Key (not key.id or key.name)
			if m not in group.members:
				group.members.append(m)

		# Save the Group
		group.save()
		
		# Reset the Group in Memcache
		memcache_key = 'group:'+str(kwargs['group'])
		memcache.set(memcache_key, value=group)

		return group
	except Exception, e:
		logging.exception(e)
		raise e

def remove_members_from_grouo(**kwargs):
	try:
		if not 'group' in kwargs or kwargs['group'] == '':
			raise Exception('No group provided')

		group = get_group(**dict(key=kwargs['group']))

		if not 'members' in kwargs or kwargs['members'] == '':
			raise Exception('No members provided')

		member_keys = [Key.from_path('Member', long(k)) for k in kwargs['members'].split(',')]

		for m in member_keys:
			# Remove the Member Key from the Group
			if m in group.members:
				group.members.remove(m)

		group.save()
		
		# Reset the Group in Memcache
		memcache_key = 'group:'+str(kwargs['group'])
		memcache.set(memcache_key, value=group)

		return group
	except Exception, e:
		logging.exception(e)
		raise e
	
def create_purse(**kwargs):
	try:
		if not 'group' in kwargs or kwargs['group'] == '':
			raise Exception('No group provided')

		if not 'title' in kwargs or kwargs['title'] == '':
			raise Exception('No title provided')

		if not 'purse_type' in kwargs or kwargs['purse_type'] == '':
			raise Exception('No purse_type provided')

		group = get_group(**dict(key=kwargs['group']))

		if group is None:
			raise Exception('No Group found in datastore with Key ID %s', kwargs['group'])

		member = get_member(**dict(key=kwargs['member']))

		if member is None:
			raise Exception('No Member found in datastore with Key ID %s', kwargs['member'])

		purse = alchemy_models.Purse(
			group=group,
			member=member,
			title=kwargs['title'],
			purse_type=kwargs['purse_type'],
			description=kwargs['description'],
			balance=float(kwargs['balance']),
			floor=float(kwargs['floor']),
			ceiling=float(kwargs['ceiling'])
		)

		purse.save()

		return purse
	except Exception, e:
		logging.exception(e)
		raise e

def update_purse(**kwargs):
	try:
		if not 'key' in kwargs or kwargs['key'] == '':
			raise Exception('No key (ID) provided')

		purse = alchemy_models.Purse.get_by_id(long(kwargs['key']))
		if purse is None:
			raise Exception('No Purse found in datastore with Key ID %s', kwargs['key'])

		# Now update any properties
		if 'title' in kwargs and kwargs['title'] != '':
			purse.title = kwargs['title']

		if 'description' in kwargs and kwargs['description'] != '':
			purse.description = kwargs['description']

		if 'balance' in kwargs and kwargs['balance'] != '':
			purse.balance = float(kwargs['description'])

		if 'floor' in kwargs and kwargs['floor'] != '':
			purse.floor = float(kwargs['floor'])

		if 'ceiling' in kwargs and kwargs['ceiling'] != '':
			purse.ceiling = float(kwargs['ceiling'])

		purse.save()
		
		return purse
	except Exception, e:
		logging.exception(e)
		raise e

def delete_purse(**kwargs):
	try:
		if not 'key' in kwargs or kwargs['key'] == '':
			raise Exception('No key (ID) provided')

		purse_key = db.Key.from_path('Purse', long(kwargs['key']))

		def txn():
			return db.delete(purse_key)

		return db.run_in_transaction(txn)

	except Exception, e:
		logging.exception(e)
		raise e

def split_pay(**kwargs):
	try:
		if not 'members' in kwargs or kwargs['members'] == '':
			raise Exception('No members provided')

		member_objects = json.loads(kwargs['members'])

		if not 'group' in kwargs or kwargs['group'] == '':
			logging.warning('No group provided, creating a new one')
			# Create a new Group
			group = create_group(**dict(
				account_id=kwargs['account_id'],
				title=kwargs['title'],
				balance=0.0,
				description=''
			))
		else:
			group = get_group(**dict(key=kwargs['group']))

		# Create a list of Member Keys
		# The add_members_to_group() method requires the 'members' argument to be a comma separated string
		member_keys_str = ','.join([k['key'] for k in member_objects])

		# Add Members to Group
		add_members_to_group(**dict(
			group=group.key().id(),
			members=member_keys_str
		))

		# Create an empty list for Transactions
		transactions = []
		for member_obj in member_objects:
			# Get the Member
			member = get_member(**dict(key=member_obj['key']))

			# Set the Member Value amount
			member_value = member_obj['value']

			# Create a new Transaction
			transaction = alchemy_models.Transaction(
				group=group,
				member=member,
				value=float(member_value)
			)

			# Save the Transaction
			transaction.save()

			# Create a dict of the Transaction
			transaction_dict = db.to_dict(transaction)
			# Reset dict transformations that don't regress down to ReferenceProperties or complex types
			# E.g. dtaetime
			transaction_dict['group'] = transaction.group.key().id()
			transaction_dict['member'] = transaction.member.key().id()
			transaction_dict['created'] = transaction.created.isoformat('T')
			transaction_dict['last_modified'] = transaction.last_modified.isoformat('T')

			# Add the Transaction to the list
			transactions.append(transaction_dict)

		return transactions
	except Exception, e:
		logging.exception(e)
		raise e

def set_user(user_obj):
	try:

		user = models.User.get_or_insert(user_obj.user_id(), **dict(
			email=user_obj.email(),
			nickname=user_obj.nickname()
			)
		)
		logging.info(user)
		
		return user
	except Exception, e:
		logging.error(e)
		raise e

def set_amex_account(**kwargs):
	try:
		if 'lbg_user_id' not in kwargs or kwargs['lbg_user_id'] == '':
			raise Exception('set_amex_account() : No lbg_user_id provided')
		
		if 'user_id' not in kwargs or kwargs['user_id'] == '':
			raise Exception('set_amex_account() : No AMEX user_id provided')
		
		if 'password' not in kwargs or kwargs['password'] == '':
			raise Exception('set_amex_account() : No AMEX password provided')
		
		if 'card_member' not in kwargs or kwargs['card_member'] == '':
			raise Exception('set_amex_account() : No AMEX card_member provided')

		if 'card_name' not in kwargs or kwargs['card_name'] == '':
			raise Exception('set_amex_account() : No AMEX card_name provided')

		if 'card_number' not in kwargs or kwargs['card_number'] == '':
			raise Exception('set_amex_account() : No AMEX card_number provided')

		if 'card_balance' not in kwargs or kwargs['card_balance'] == '':
			raise Exception('set_amex_account() : No AMEX card_balance provided')

		if 'card_credit_limit' not in kwargs or kwargs['card_credit_limit'] == '':
			raise Exception('set_amex_account() : No AMEX card_credit_limit provided')

		if 'card_image' not in kwargs or kwargs['card_image'] == '':
			raise Exception('set_amex_account() : No AMEX card_image provided')

		if 'latest_transactions' not in kwargs or kwargs['latest_transactions'] == '':
			raise Exception('set_amex_account() : No AMEX latest_transactions provided')
			


		amex_account = models.AmexAccount.get_or_insert(
			key_name=kwargs['lbg_user_id'],
			user_id=kwargs['user_id'],
			password=kwargs['password'],
			card_member=kwargs['card_member'],
			card_name=kwargs['card_name'],
			card_number=kwargs['card_number'],
			card_balance=float(kwargs['card_balance']),
			card_credit_limit=float(kwargs['card_credit_limit']),
			card_image=kwargs['card_image'],
			latest_transactions=kwargs['latest_transactions']
		)

		# Add the Account to Memcache
		memcache_key = 'amex:'+str(kwargs['lbg_user_id'])
		memcache.set(memcache_key, value=amex_account)
		
		return amex_account
	except Exception, e:
		logging.exception(e)
		raise e

def get_amex_account(**kwargs):
	response = None
	try:
		if 'lbg_user_id' not in kwargs or kwargs['lbg_user_id'] == '':
			raise Exception('No lbg_user_id provided')

		memcache_key = 'amex:'+str(kwargs['lbg_user_id'])
		memcache_result = memcache.get(memcache_key)
		if memcache_result is not None:
			response = memcache_result
		else:
			query = models.AmexAccount.get_by_key_name(str(kwargs['lbg_user_id']))
			if query is not None:
				response = query
				
	except Exception, e:
		logging.exception(e)
		raise e
	finally:
		return response

def set_user_facebook_id(user_id, facebook_id):
	try:

		user = models.User.get_by_key_name(str(user_id))
		if user is None:
			raise Exception('set_user_facebook_id() : User not found in datastore')

		user.facebook_id = str(facebook_id)
		user.save()
		return user
	except Exception, e:
		logging.error(e)
		raise e
		
def get_user(user_id):
	try:

		if user_id is None or user_id == '':
			raise Exception('datastore.py : get_user() : No user_id provided')

		user = models.User.get_by_key_name(str(user_id))

		return user
	except Exception, e:
		logging.error(e)
		raise e

def get_users(**kwargs):
	response = None
	try:
		memcache_key = 'allusers'
		memcache_result = memcache.get(memcache_key)
		if memcache_result is not None:
			response = memcache_result
		else:

			users = models.User.all()


		return response
	except Exception, e:
		logging.error(e)
		raise e


def set_task(**kwargs):
	task = None
	try:
		if not kwargs.has_key('user_id') or kwargs['user_id'] == '':
			raise Exception('datastore.py : set_task() : No user_id provided')

		user = models.User.get_by_key_name(str(kwargs['user_id']))
		
		if user is None:
			raise Exception('datastore.py : set_task() : No user found in datastore')

		if kwargs.has_key('task_id') and kwargs['task_id'] != '':
			task = models.CurrentAccount.get_by_id(int(kwargs['task_id']))

		if task is not None:
			if task.notification is None:
				task.notification = models.Notification()
				
			if kwargs.has_key('email') and kwargs['email'] != '':
				task.notification.email = str(kwargs['email'])
			elif kwargs.has_key('email') and kwargs['email'] == '':
				task.notification.email = None
			else:
				task.notification.email = None

			if kwargs.has_key('text') and kwargs['text'] != '':
				task.notification.text = str(kwargs['text'])
			elif kwargs.has_key('text') and kwargs['text'] == '':
				task.notification.text = None
			else:
				task.notification.text = None

			if kwargs.has_key('text_message') and kwargs['text_message'] != '':
				task.notification.text_message = str(kwargs['text_message'])
			elif kwargs.has_key('text_message') and kwargs['text_message'] == '':
				task.notification.text_message = ''
			

			if kwargs.has_key('phone') and kwargs['phone'] != '':
				task.notification.phone = str(kwargs['phone'])
			elif kwargs.has_key('phone') and kwargs['phone'] == '':
				task.notification.phone = None
			#else:
			#	task.notification.phone = None

			if kwargs.has_key('facebook') and kwargs['facebook'] != '':
				if str(kwargs['facebook']) == 'True':
					task.notification.facebook = True
				else:
					task.notification.facebook = False
			else:
				task.notification.facebook = False
			task.notification.save()

			if kwargs.has_key('status') and kwargs['status'] != '':
				task.status=str(kwargs['status'])

			if kwargs.has_key('balance') and kwargs['balance'] != '':
				task.balance=float(kwargs['balance'])
			
			if kwargs.has_key('active') and kwargs['active'] != '':
				if kwargs['active'] == 'True':
					task.active = True
				else:
					task.active = False



			if kwargs.has_key('title') and kwargs['title'] != '':
				task.title=str(kwargs['title'].replace('<','').replace('>','').encode('ascii', 'xmlcharrefreplace'))
			elif kwargs.has_key('title') and kwargs['title'] == '':
				task.title = None

			# Save Task Date
			if kwargs.has_key('date_ref') and kwargs['date_ref'] != '':
				# Save the date_ref
				task.date_ref = str(kwargs['date_ref'])

				# Set Today's date
				today = datetime.datetime.now()

				if kwargs['date_ref'] == 'today':
					task.date = today

				elif kwargs['date_ref'] == 'first':
					#[ST]TODO: No need to implement for POC
					pass

				elif kwargs['date_ref'] == 'last':
					#[ST]TODO: No need to implement for POC
					pass

			elif kwargs.has_key('date_ref') and kwargs['date_ref'] == '':
				# Save the date_ref
				task.date_ref = None				
				task.date = None	
		else:
			notification = models.Notification()

			if kwargs.has_key('email') and kwargs['email'] != '':
				notification.email = str(kwargs['email'])
			elif kwargs.has_key('email') and kwargs['email'] == '':
				notification.email = None
			else:
				notification.email = None

			if kwargs.has_key('text') and kwargs['text'] != '':
				notification.text = str(kwargs['text'])
			elif kwargs.has_key('text') and kwargs['text'] == '':
				notification.text = None
			else:
				notification.text = None

			if kwargs.has_key('text_message') and kwargs['text_message'] != '':
				notification.text_message = str(kwargs['text_message'])
			elif kwargs.has_key('text_message') and kwargs['text_message'] == '':
				notification.text_message = ''
			
			if kwargs.has_key('phone') and kwargs['phone'] != '':
				notification.phone = str(kwargs['phone'])
			elif kwargs.has_key('phone') and kwargs['phone'] == '':
				notification.phone = None
			#else:
			#	notification.phone = None

			if kwargs.has_key('facebook') and kwargs['facebook'] != '':
				if str(kwargs['facebook']) == 'True':
					notification.facebook = True
				else:
					notification.facebook = False

			else:
				notification.facebook = False
			notification.save()

			task = models.CurrentAccount(
				status=str(kwargs['status']),
				balance=float(kwargs['balance']),
				user=user,
				notification=notification
			)

			if kwargs.has_key('title') and kwargs['title'] != '':
				task.title=str(kwargs['title'].replace('<','').replace('>','').encode('ascii', 'xmlcharrefreplace'))
			elif kwargs.has_key('title') and kwargs['title'] == '':
				task.title = None
				
			if kwargs.has_key('active') and kwargs['active'] != '':
				if kwargs['active'] == 'True':
					task.active = True
				else:
					task.active = False

			# Save Task Date
			if kwargs.has_key('date_ref') and kwargs['date_ref'] != '':
				# Save the date_ref
				task.date_ref = str(kwargs['date_ref'])
				
				# Set Today's date
				today = datetime.datetime.now()

				if kwargs['date_ref'] == 'today':
					task.date = today

				elif kwargs['date_ref'] == 'first':
					#[ST]TODO: No need to implement for POC
					pass

				elif kwargs['date_ref'] == 'last':
					#[ST]TODO: No need to implement for POC
					pass
			elif kwargs.has_key('date_ref') and kwargs['date_ref'] == '':
				# Save the date_ref
				task.date_ref = None				
				task.date = None
								
		task.save()
		return task
	except Exception, e:
		logging.error(e)
		raise e

def get_current_account_tasks(**kwargs):
	try:
		tasks = models.CurrentAccount.all()
		tasks.filter('active = ', True)

		return tasks
	except Exception, e:
		logging.error(e)
		raise e

def get_user_tasks(**kwargs):
	try:
		if not kwargs.has_key('user_id') or kwargs['user_id'] == '':
			raise Exception('get_user_tasks() : No user_id provided')

		user = models.User.get_by_key_name(str(kwargs['user_id']))

		tasks = models.CurrentAccount.all()
		tasks.filter('user', user)
		tasks.order('-date_last_modified')
		return tasks.fetch(limit=None)

	except Exception, e:
		logging.error(e)
		raise e

def get_task(task_id):
	try:
		if task_id is None or task_id == '':
			raise Exception('datastore.py : get_task() : No task_id provided')

		task = models.CurrentAccount.get_by_id(int(str(task_id)))

		return task
	except Exception, e:
		logging.error(e)
		raise e

def create_memcache_key(key_prefix, **kwargs):
	memcache_key = key_prefix+':'
	if kwargs is not None:
		for request_arg, value in kwargs.items():
			if request_arg != 'signed_request' and request_arg != 'ref':
				if type(value) == list or type(value) == int:
					value = str(value)
				memcache_key+=request_arg+'='+value

	#logging.debug('memcache_key : %s', memcache_key)
	return memcache_key