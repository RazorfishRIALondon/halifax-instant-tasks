import os
import logging
import datetime
import base64
import cgi
import time
import urllib
import urllib2
import cStringIO
from google.appengine.ext import db
from google.appengine.api.datastore import Key
from google.appengine.ext import webapp
from google.appengine.api import memcache
from google.appengine.ext import blobstore
from google.appengine.api import taskqueue
from google.appengine.api import mail
from google.appengine.api import urlfetch
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime import apiproxy_errors 
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.ext.db import BadValueError
from google.appengine.api import images
import json
from collections import deque
from operator import itemgetter

# Import local modules
from controllers import datastore
from models import models
from controllers import utils
from controllers import mail_controller
from controllers import sms_controller
from controllers import facebook_helper
import main

class CreateBrowserstackWorker(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# BrowserStack.com API
			# first encode the username & password 
			userData = 'Basic ' + 'stuartthorne:16QnZrcCCWZ54YR2copJ'.encode("base64").rstrip()

			"""
			# create a new Urllib2 Request object	
			req = urllib2.Request('http://api.browserstack.com/3/browsers.json')
			# add any additional headers you like 
			req.add_header('Accept', 'application/json')
			req.add_header("Content-type", "application/x-www-form-urlencoded")
			# add the authentication header, required
			req.add_header('Authorization', userData)
			# make the request and print the results
			res = urllib2.urlopen(req)
			"""

			# Create Worker
			worker = urllib2.Request('http://api.browserstack.com/3/worker')
			worker.add_header('Accept', 'application/json')
			worker.add_header("Content-type", "application/x-www-form-urlencoded")
			worker.add_header('Authorization', userData)

			args = dict()
			args['os'] = 'OS X'
			args['os_version'] = 'Mountain Lion'
			args['browser'] = 'safari'
			args['browser_version'] = '6.0'
			args['name'] = 'cvp1'
			args['url'] = self.context['request_args']['url']
			args['callback_url'] = 'https://razorfish-lbg.appspot.com/alchemy/screenshots'
			args = urllib.urlencode(args)

			request = urllib2.urlopen(url=worker, data=args, timeout=30)
			response = request.read()

			json_data = json.loads(response)
			logging.info(json_data)

			if 'id' in json_data:
				# Instruct Worker to take Screenshot
				worker_id = json_data['id']
				logging.info(worker_id)
				taskqueue.add(
					queue_name='browserstack', 
					url=main.taskBrowserstackScreenshot, 
					params={
						'worker_id':worker_id
					},
					countdown=5
				)
		
		except Exception, e:
			logging.exception(e)
			raise e

class TakeScreenshot(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			# BrowserStack.com API
			# first encode the username & password 
			userData = 'Basic ' + 'stuartthorne:16QnZrcCCWZ54YR2copJ'.encode("base64").rstrip()
		
			worker_id = self.context['request_args']['worker_id']

			worker = urllib2.Request('http://api.browserstack.com/3/worker/'+worker_id+'/screenshot.png')
			worker.add_header('Accept', 'image/png')
			worker.add_header("Content-type", "application/x-www-form-urlencoded")
			worker.add_header('Authorization', userData)

			request = urllib2.urlopen(url=worker)
			response = request.read()
			logging.info(response)		
		
		except Exception, e:
			logging.exception(e)
			raise e

class SendSMS(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			sms_controller.send_sms_alert(**dict(
				to=self.context['request_args']['to'],
				sender=self.context['request_args']['sender'],
				message=self.context['request_args']['message']
			))
		except Exception, e:
			logging.exception(e)
			raise e


class SendEmail(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			mail_controller.send_email(**dict(
				sender=self.context['request_args']['sender'],
				email=self.context['request_args']['email'],
				subject=self.context['request_args']['subject'],
				name=self.context['request_args']['name'],
				link=self.context['request_args']['link'],
				space_name=self.context['request_args']['space_name'],
				members=self.context['request_args']['members']
			))
		except Exception, e:
			logging.exception(e)
			raise e


class AlchemySplitPay(utils.BaseHandler):
	def post(self):
		self.set_request_arguments()
		try:
			members = self.context['request_args']['members']
			logging.info(members)

			payee = self.context['request_args']['payee']
			logging.info(payee)

			# Kick Off Notification Tasks


		except Exception, e:
			logging.exception(e)
			raise e

class GetAMEXCardHandler(utils.BaseHandler):
	def process(self):
		response = None
		self.set_request_arguments()
		try:
			# Check for the LBG User ID
			if not 'lbg_user_id' in self.context['request_args'] or self.context['request_args']['lbg_user_id'] == '':
				raise Exception(self.__class__.__name__+' : No lbg_user_id provided')

			# Check for the AMEX User ID
			if not 'user_id' in self.context['request_args'] or self.context['request_args']['user_id'] == '':
				raise Exception(self.__class__.__name__+' : No AMEX user_id provided')

			# Check for the AMEX Password
			if not 'password' in self.context['request_args'] or self.context['request_args']['password'] == '':
				raise Exception(self.__class__.__name__+' : No AMEX password provided')

			memcache_key = 'amex:request:'+self.context['request_args']['lbg_user_id']
			memcache_response = memcache.get(memcache_key)

			
			if memcache_response is not None:
				response = memcache_response
			else:
				# The Payload will already include the AMEX URL
				payload = self.context['kapow']['amex']['request_payload']
				payload = payload.replace('@USER_ID@', self.context['request_args']['user_id'])
				payload = payload.replace('@PASSWORD@', self.context['request_args']['password'])
				
				logging.info('ODS Payload')
				logging.info(payload)

				f = urlfetch.fetch(
					url=self.context['kapow']['amex']['url'], 
					payload=payload,
					method='POST',
					deadline=55,
					headers={'Content-Type':'application/json; charset=UTF-8;', 'Accept':'application/json'}
				)

				if f.status_code == 200:
					response = f.content
					logging.info('Resonse Success received')
					logging.info(response)

					json_response = json.loads(response)
					logging.info(json_response)

					"""
					{
						"executionTime":19551,
						"values":[
							{
								"typeName":"amex_output","attribute":[
									{"name":"card_name","type":"text","value":"Blue Credit Card"},
									{"name":"card_number","type":"text","value":"XXX-71005"},
									{"name":"card_balance","type":"text","value":"3.94"},
									{"name":"card_credit_limit","type":"text","value":"3990"},
									{"name":"card_member","type":"text","value":"Mr Alex Gray"},
									{"name":"card_art","type":"text","value":"https://secure.cmax.americanexpress.com/Internet/CardArt/EMEA/gb-cardasset-config/images/GGBBLUE00001S3.gif"},
									{"name":"latest_transactions","type":"text","value":"<table id=\"summary-transaction-table\" class=\"summary-recent-transactions\" summary=\"Detailing your lastest transactions\"><thead><tr><th scope=\"col\" class=\"date\">Date</th><th scope=\"col\" class=\"description\">Description</th><th scope=\"col\" class=\"favourites\">Favourites</th><th scope=\"col\" class=\"amount\">Amount</th></tr></thead><tbody><tr><td class=\"date\" data=\"01/05/2013\">01 May 2013</td><td class=\"description\" title=\"PAIR NETWORKS INC 0489 PITTSBURGH\" data=\"AT131220008000010352815---005 ---20130501\">PAIR NETWORKS INC 0489 PITTSBURGH</td><td class=\"favourites\"><div class=\"center favourite\"><a class=\"favourite  animaniac-heart\" data=\"2373400019\" rel=\"\" href=\"https://global.americanexpress.com/myca/intl/isummary/emea/summary.do?request_type=&amp;Face=en_GB&amp;method=displaySummary&amp;sorted_index=-1#:\" title=\"Toggles or un-toggles merchant as a Favourite\"><span class=\"copy\">false</span></a></div></td><td class=\"amount\"><span class=\"financial-detail\" data=\"&pound;3.94\">&pound;3.94</span></td></tr><tr class=\"payment\"><td class=\"date\" data=\"29/04/2013\">29 Apr 2013</td><td class=\"description\" title=\"PAYMENT RECEIVED - THANK YOU\" data=\"AT131190039000010075939---020 ---20130429\">PAYMENT RECEIVED - THANK YOU</td><td class=\"non-favouritable\"><div class=\"center favourite\"></div></td><td class=\"amount\"><span class=\"financial-detail\" data=\"-&pound;4.03\">-&pound;4.03</span></td></tr></tbody></table>"}
								]
							}
						]
					}
					"""
					if json_response.get('values') is not None:
						values = json_response.get('values')
						if len(values) != 0:
							amex_output = values[0]
							type_name = amex_output.get('attribute')
							if type_name is not None:
								card_name = type_name[0].get('value')
								card_number = type_name[1].get('value')
								card_balance = type_name[2].get('value')
								card_credit_limit = type_name[3].get('value')
								card_member = type_name[4].get('value')								
								card_image = type_name[5].get('value')
								latest_transactions = type_name[6].get('value')
										
								# Attempt to set & get an existing User Account
								amex_account = datastore.set_amex_account(**dict(
										lbg_user_id=self.context['request_args']['lbg_user_id'],
										user_id=self.context['request_args']['user_id'],
										password=self.context['request_args']['password'],
										card_member=card_member,
										card_name=card_name,
										card_number=card_number,
										card_balance=card_balance,
										card_credit_limit=card_credit_limit,
										card_image=card_image,
										latest_transactions=latest_transactions
									)
								)
								# Set the AMEX Request Response in Memcache
								memcache.set(memcache_key, value=response, time=self.memcache['time'])

					# Successful response, but data collection failure
					else:
						# Set fake data in the database for this User
						amex_account = datastore.set_amex_account(**dict(
								lbg_user_id=self.context['request_args']['lbg_user_id'],
								user_id=self.context['request_args']['user_id'],
								password=self.context['request_args']['password'],
								card_member='MR A NOTHER',
								card_name='UNKNOWN CARD TYPE',
								card_number='XXX-11111',
								card_balance=float(0.0),
								card_credit_limit=float(0.0),
								card_image='#',
								latest_transactions='<table></table>'
							)
						)
						# Set the AMEX Request Response in Memcache
						memcache.set(memcache_key, value=response, time=self.memcache['time'])
				else:
					logging.exception('Response ERROR received')
					
					#response = '{"executionTime":19551,"values":[{"typeName":"amex_output","attribute":[{"name":"card_name","type":"text","value":"Blue Credit Card"},{"name":"card_number","type":"text","value":"XXX-71005"},{"name":"card_balance","type":"text","value":"3.94"},{"name":"card_credit_limit","type":"text","value":"3990"},{"name":"card_member","type":"text","value":"Mr Alex Gray"},{"name":"card_art","type":"text","value":"https://secure.cmax.americanexpress.com/Internet/CardArt/EMEA/gb-cardasset-config/images/GGBBLUE00001S3.gif"},{"name":"latest_transactions","type":"text","value":"<table id=\"summary-transaction-table\" class=\"summary-recent-transactions\" summary=\"Detailing your lastest transactions\"><thead><tr><th scope=\"col\" class=\"date\">Date</th><th scope=\"col\" class=\"description\">Description</th><th scope=\"col\" class=\"favourites\">Favourites</th><th scope=\"col\" class=\"amount\">Amount</th></tr></thead><tbody><tr><td class=\"date\" data=\"01/05/2013\">01 May 2013</td><td class=\"description\" title=\"PAIR NETWORKS INC 0489 PITTSBURGH\" data=\"AT131220008000010352815---005 ---20130501\">PAIR NETWORKS INC 0489 PITTSBURGH</td><td class=\"favourites\"><div class=\"center favourite\"><a class=\"favourite animaniac-heart\" data=\"2373400019\" rel=\"\" href=\"https://global.americanexpress.com/myca/intl/isummary/emea/summary.do?request_type=&amp;Face=en_GB&amp;method=displaySummary&amp;sorted_index=-1#:\" title=\"Toggles or un-toggles merchant as a Favourite\"><span class=\"copy\">false</span></a></div></td><td class=\"amount\"><span class=\"financial-detail\" data=\"&pound;3.94\">&pound;3.94</span></td></tr><tr class=\"payment\"><td class=\"date\" data=\"29/04/2013\">29 Apr 2013</td><td class=\"description\" title=\"PAYMENT RECEIVED - THANK YOU\" data=\"AT131190039000010075939---020 ---20130429\">PAYMENT RECEIVED - THANK YOU</td><td class=\"non-favouritable\"><div class=\"center favourite\"></div></td><td class=\"amount\"><span class=\"financial-detail\" data=\"-&pound;4.03\">-&pound;4.03</span></td></tr></tbody></table>"}]}]}'
					json_response = {"executionTime":19551,"values":[{"typeName":"amex_output","attribute":[{"name":"card_name","type":"text","value":"Blue Credit Card"},{"name":"card_number","type":"text","value":"XXX-71005"},{"name":"card_balance","type":"text","value":"3.94"},{"name":"card_credit_limit","type":"text","value":"3990"},{"name":"card_member","type":"text","value":"Mr Alex Gray"},{"name":"card_art","type":"text","value":"https://secure.cmax.americanexpress.com/Internet/CardArt/EMEA/gb-cardasset-config/images/GGBBLUE00001S3.gif"},{"name":"latest_transactions","type":"text","value":"<table id=\"summary-transaction-table\" class=\"summary-recent-transactions\" summary=\"Detailing your lastest transactions\">\n  <thead>\n    <tr>\n      <th scope=\"col\" class=\"date\">\n        Date\n      </th>\n      <th scope=\"col\" class=\"description\">\n        Description\n      </th>\n      <th scope=\"col\" class=\"favourites\">\n        Favourites\n      </th>\n      <th scope=\"col\" class=\"amount\">\n        Amount\n      </th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr>\n      <td class=\"date\" data=\"01/05/2013\">01 May 2013</td><td class=\"description\" title=\"PAIR NETWORKS INC 0489 PITTSBURGH\" data=\"AT131220008000010352815---005 ---20130501\">PAIR NETWORKS INC 0489 PITTSBURGH</td><td class=\"favourites\">\n        <div class=\"center favourite\">\n          <a class=\"favourite  animaniac-heart\" data=\"2373400019\" rel=\"\" href=\"https://global.americanexpress.com/myca/intl/isummary/emea/summary.do?request_type=&amp;Face=en_GB&amp;method=displaySummary&amp;sorted_index=-1#:\" title=\"Toggles or un-toggles merchant as a Favourite\"><span class=\"copy\">false</span></a>\n        </div>\n      </td><td class=\"amount\"><span class=\"financial-detail\" data=\"&pound;3.94\">&pound;3.94</span></td>\n    </tr>\n    <tr class=\"payment\">\n      <td class=\"date\" data=\"29/04/2013\">29 Apr 2013</td><td class=\"description\" title=\"PAYMENT RECEIVED - THANK YOU\" data=\"AT131190039000010075939---020 ---20130429\">PAYMENT RECEIVED - THANK YOU</td><td class=\"non-favouritable\">\n        <div class=\"center favourite\">\n        </div>\n      </td><td class=\"amount\"><span class=\"financial-detail\" data=\"-&pound;4.03\">-&pound;4.03</span></td>\n    </tr>\n  </tbody>\n</table>"}]}]}
					
					if json_response.get('values') is not None:
						values = json_response.get('values')
						if len(values) != 0:
							amex_output = values[0]
							type_name = amex_output.get('attribute')
							if type_name is not None:
								card_name = type_name[0].get('value')
								card_number = type_name[1].get('value')
								card_balance = type_name[2].get('value')
								card_credit_limit = type_name[3].get('value')
								card_member = type_name[4].get('value')
								card_image = type_name[5].get('value')
								latest_transactions = type_name[6].get('value')
										
								# Attempt to set & get an existing User Account
								amex_account = datastore.set_amex_account(**dict(
										lbg_user_id=self.context['request_args']['lbg_user_id'],
										user_id=self.context['request_args']['user_id'],
										password=self.context['request_args']['password'],
										card_member=card_member,
										card_name=card_name,
										card_number=card_number,
										card_balance=card_balance,
										card_credit_limit=card_credit_limit,
										card_image=card_image,
										latest_transactions=latest_transactions
									)
								)
								# Set the AMEX Request Response in Memcache
								memcache.set(memcache_key, value=response, time=self.memcache['time'])
				
		except Exception, e:
			logging.exception(e)
			raise e
		finally:
			return response

	def get(self):
		self.process()
	def post(self):
		self.process()

class AuthenticationTaskHandler(utils.BaseHandler):
	def process(self):
		response = None
		self.set_request_arguments()
		self.set_current_user()
		try:
			f = urlfetch.fetch(
				url=self.context['kapow']['authentication']['url'], 
				payload=self.context['kapow']['authentication']['request_payload'], 
				method='POST', 
				deadline=30,
				headers=self.context['kapow']['headers']
			)

			logging.debug(f)

			if f.status_code == 200:
				response = f.content
				logging.debug('AuthenticationHandler : Resonse received')
				logging.debug(response)
			else:
				logging.error('AuthenticationHandler : Error received')
				logging.debug(f.content)
				response = f.content
				
				
		except Exception, e:
			logging.error(e)
			raise
		finally:
			return response
	def get(self):
		self.process()
	def post(self):
		self.process()

class GetCurrentAccountBalanceHandler(utils.BaseHandler):
	def post(self):
		response = None
		account_balance = None
		self.set_request_arguments()
		try:
			memcache_key = 'user:currentaccountbalance:'+self.context['request_args']['user_id']
			memcache_result = memcache.get(memcache_key)
			
			if memcache_result is not None:
				logging.debug('Getting Current Account Balance from Memcache')
				response = memcache_result
			else:
				f = urlfetch.fetch(
					url=self.context['kapow']['currentaccountbalance']['url'], 
					payload=self.context['kapow']['currentaccountbalance']['request_payload'], 
					method='POST', 
					deadline=50,
					headers={'Content-Type':'application/json; charset=UTF-8;', 'Accept':'application/json'}
				)

				if f.status_code == 200:
					response = f.content

					response_obj = json.loads(response)
					if response_obj.has_key('values'):
						values = response_obj['values']
						if len(values) >= 1:
							values = values[0]
							if values.has_key('attribute'):
								values = values['attribute']
								if len(values) >= 1:
									values = values[0]
									if values.has_key('value'):
										account_balance = response_obj['values'][0]['attribute'][0]['value']
										if account_balance == 'NIL':
											response = float(0.0)
										else:
											response = float(account_balance)
					
					else:
						raise Exception('GetCurrentAccountBalanceHandler() : Error Object Response')

					# Cache for 15 minutes
					memcache.set(memcache_key, value=response, time=8000)
				else:
					logging.error('GetCurrentAccountBalanceHandler : Error received')
					logging.debug(f.content)
					
			# Save the Current Accont Balance againts the User
			user = datastore.get_user(self.context['request_args']['user_id'])
			logging.debug(user)
			if user is None:
				raise Exception('GetCurrentAccountBalanceHandler() : User not found in datastore');

			user.current_account_balance = response
			user.save()

			return response
		except Exception, e:
			logging.error(e)
			raise

	
class CurrentAccountBalanceTaskHandler(utils.BaseHandler):
	def process(self):
		response = None
		account_balance = None
		self.set_request_arguments()
		try:
			memcache_key = 'currentaccountbalance:'+self.context['request_args']['user_id']
			memcache_result = memcache.get(memcache_key)
			
			if memcache_result is not None:
				logging.debug('Getting Current Account Balance from Memcache')
				account_balance = memcache_result
			else:
				f = urlfetch.fetch(
					url=self.context['kapow']['currentaccountbalance']['url'], 
					payload=self.context['kapow']['currentaccountbalance']['request_payload'], 
					method='POST', 
					deadline=50,
					headers={'Content-Type':'application/json; charset=UTF-8;', 'Accept':'application/json'}
				)

				if f.status_code == 200:
					"""
					{
						"executionTime":12312,
						"values":[
							{
								"typeName":"balance_output",
								"attribute":[
									{
										"name":"balance",
										"type":"text",
										"value":"NIL"
									},
									{
										"name":"available",
										"type":"text",
										"value":"1,000.00"
									},
									{
										"name":"overdraft_limit",
										"type":"text",
										"value":"1,000.00"
									}
								]
							}
						]
					}
					"""
					response = f.content

					response_obj = json.loads(response)
					if response_obj.has_key('values'):
						values = response_obj['values']
						if len(values) >= 1:
							values = values[0]
							if values.has_key('attribute'):
								values = values['attribute']
								if len(values) >= 1:
									values = values[0]
									if values.has_key('value'):
										account_balance = response_obj['values'][0]['attribute'][0]['value']
										if account_balance == 'NIL':
											account_balance = float(0.0)
										else:
											account_balance = float(account_balance)
					
					else:
						pass
						#raise Exception('CurrentAccountBalanceTaskHandler() : Error Object Response')

					# Cache for 15 minutes
					memcache.set(memcache_key, value=account_balance, time=8000)
				else:
					logging.error('CurrentAccountBalanceTaskHandler : Error received')
					logging.debug(f.content)
					response = f.content
					account_balance = float(0.0)

			# Dispatch Notifications
			if account_balance is not None:

				user = datastore.get_user(self.context['request_args']['user_id'])
				task = datastore.get_task(self.context['request_args']['task_id'])

				notify = current_account_balance_rule(**dict(
					account_balance=account_balance, 
					user_id=self.context['request_args']['user_id'],
					task_id=self.context['request_args']['task_id']
				))

				if notify:
					status_text = None
					if task.status == 'equal':
						status_text = 'equal to'
					else:
						status_text = task.status
						
					if task.notification.text is not None and task.notification.text != 'None' and task.notification.text != '':
						# Send SMS Notification
						if task.notification.text_message is not None and task.notification.text_message != '':
							sms_template = task.notification.text_message+''
						else:
							sms_template = 'Your Current Account balance is '+status_text+' '+str(task.balance)+'. It\'s currently '+str(account_balance)+'.View, edit or create a new task'

						sms_controller.send_sms_alert(**dict(
							to=task.notification.text,
							sender='HALIFAX',
							text=sms_template
						))
						
					if task.notification.email is not None and task.notification.email != 'None' and task.notification.email != '':
						actions = 'Sent you this email'
						if task.notification.text is not None and task.notification.text != 'None':
							actions = actions+'<br/>And sent you a text to '+str(task.notification.text)
						if task.notification.facebook is not None and user.facebook_id is not None:
							if task.notification.facebook == True:
								actions = actions+'<br/>And sent you a Facebook Notification'
						if task.notification.phone is not None and task.notification.phone != '':
							actions = actions+'<br/>And called you on '+str(task.notification.phone)

						mail_controller.send_notification(**dict(
							user_id=user.key().name(),
							status=status_text,
							balance=task.balance,
							account_balance=account_balance,
							email=task.notification.email,
							title=task.title,
							actions=actions,
							sender=self.context['mail']['sender']
						))
						
					if task.notification.facebook is not None and user.facebook_id is not None:
						if task.notification.facebook == True:

							facebook_template = 'Task Alert \"'+task.title+'\": Your Current Account balance is '+status_text+' the value you wanted to monitor'

							# Get the App ID
							app_id = self.context['facebook']['app_id']

							# Get the App Secret
							app_secret = self.system['facebook']['app_secret']

							# Get the App Access Token
							app_access_token = facebook_helper.get_facebook_app_access_token(app_id, app_secret)
							
							fb_result = facebook_helper.create_facebook_user_app_notification(**dict(
								user_id=user.facebook_id,
								app_access_token=app_access_token, 
								href='', 
								template=facebook_template
							))

					if task.notification.phone is not None and task.notification.phone != '':
						voxeo_properties = utils.load_property_file('voxeo')
						args = dict()
						args['tokenid'] = voxeo_properties.get('Voxeo','tokenid')
						args['callerid'] = voxeo_properties.get('Voxeo','callerid')
						args['numbertodial'] = '+'+task.notification.phone
						args['status'] = task.status
						args['balance'] = task.balance
						args['accountbalance'] = account_balance
						args['task'] = task.title
						args = urllib.urlencode(args)
						
						url = voxeo_properties.get('Voxeo','api_service_url')
						
						f = urlfetch.fetch(url=url, method='POST', payload=args, deadline=50)
						if f.status_code == 200:
							logging.debug('Voxeo API call response SUCCESS')
							logging.debug(f.content)							
						else:
							logging.debug('Voxeo API call response FAILURE')
							logging.debug(f.content)
							logging.error(f.status_code)
							
						
			return response
			
		except Exception, e:
			logging.error(e)
			raise
		
	def get(self):
		self.process()
	def post(self):
		self.process()

def current_account_balance_rule(**kwargs):
	notify = False
	
	try:
		if not kwargs.has_key('account_balance') or kwargs['account_balance'] == '':
			raise Exception('tasks.py : current_account_balance_rule() : No account_balance provided')

		if not kwargs.has_key('user_id') or kwargs['user_id'] is None:
			raise Exception('tasks.py : current_account_balance_rule() : No user_id provided')

		if not kwargs.has_key('task_id') or kwargs['task_id'] == '':
			raise Exception('tasks.py : current_account_balance_rule() : No task_id provided')

		user = datastore.get_user(kwargs['user_id'])
		task = datastore.get_task(kwargs['task_id'])

		#logging.debug('account_balance %s', str(kwargs['account_balance']))
		#logging.debug('balance %s', str(kwargs['balance']))
		if task.status == 'above':
			if float(kwargs['account_balance']) > task.balance:
				notify = True
			
		elif task.status == 'below':
			if float(kwargs['account_balance']) < task.balance:
				notify = True

		elif task.status == 'equal':
			#logging.debug(float(kwargs['account_balance']))
			#logging.debug(task.balance)
			if float(kwargs['account_balance']) == task.balance:
				notify = True

		# Check if the Task has a date for today
		if task.date is not None and task.date_ref is not None:
			#logging.debug('task.date is not None')
			#logging.debug(task.date)
			#logging.debug(datetime.date.today())
			if task.date_ref == 'today' and (task.date.date() != datetime.date.today()):
				#logging.debug('Task date is not for today, so do not issue notifications')
				notify = False

		# Check if the Task is active
		if task.active is not None and task.active == False:
			notify = False
			
		return notify
			
	except Exception, e:
		logging.error(e)
		raise e

def send_facebook_notification(**kwargs):
	try:
		pass
	except Exception, e:
		logging.error(e)
		raise e
