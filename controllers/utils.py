import os
import logging
import datetime
import json
import cgi
import hashlib
import time
from ConfigParser import ConfigParser
from datetime import timedelta, tzinfo
from google.appengine.api import taskqueue
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError   
from google.appengine.ext.db import BadValueError
from google.appengine.runtime import DeadlineExceededError
from google.appengine.api import users
from google.appengine.api import memcache
from google.appengine.api import app_identity
import logging
from random import choice
import webapp2
import jinja2
jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+'/../views'))
jinja_alchemy_static = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+'/../alchemy_static'))

jinja_environment_alchemy_cvp1 = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+'/../alchemy_cvp_1/shared-spaces'))

# Import local scripts
from controllers import datastore
from controllers import date_utils
from models import models
import main

"""
	@description:
		Base Handler Class for all webapp.RequestHandlers
"""
class BaseHandler(webapp2.RequestHandler):

	context = {}
	system = {}
	now = None
	
	def __init__(self, request=None, response=None):
		self.initialize(request, response)
		
		# Set Timestamp
		self.now = datetime.datetime.now()
		self.data_output = None
		self.loadConfig()		
		self.populateContext()

		self.lbg_origins = [
			'razorfish-lbg.appspot.com',
			'https://razorfish-lbg.appspot.com',
			'https://ibglxysit4a-lp.lloydstsb.co.uk',
			'https://ibglxysit4s-lp.lloydstsb.co.uk',
			'https://online.lloydstsb.co.uk',
			'https://secure.lloydstsb.co.uk',
			'https://secure1.lloydstsb.co.uk',
			'https://secure2.lloydstsb.co.uk',
			'http://alchemy-cvp1.lloydstsb.com',
			'https://alchemy-cvp1.lloydstsb.com',
			'https://alchemy-cvp1.lloydstsb.com:8890',
			'http://alchemy-cvp1.lloydstsb.com:8890',
			'http://localhost',
			'http://localhost:8110'
		]
		self.cors = {
			'max_age':1*60*10
		}
		self.memcache = {
			'time':1*60*10
		}
	

	def loadConfig(self):
		self.config_properties = load_property_file('app_config')
		self.kapow_properties = load_property_file('kapow')
		self.facebook_properties = load_property_file('facebook')

	def populateContext(self):

		self.context['now'] = self.now
		self.context['error'] = None
		self.context['view'] = None
		self.context['current_user'] = None
		self.context['logout_url'] = None
		self.context['build_version'] = os.environ['CURRENT_VERSION_ID']

		self.context['push_token'] = None
		self.context['push_concept'] = 'http://bit.ly/11Umst0'

		self.context['user_whitelist'] = [
			'stuart.thorne@razorfishsupport.com'
		]
		
		# UI Data
		self.context['ui'] = {}
		self.context['ui']['data'] = {
			'theme':'g',
			'transition':'flow'
		}

		self.context['invitees'] = None
		
		self.context['task'] = None
		self.context['tasks'] = None
		
		# Get the current GAE Application
		self.context['gae_application_id'] = app_identity.get_application_id()
		
		self.context['gae_environment'] = self.config_properties.get(self.context['gae_application_id'],'gae_environment')
		self.context['short_url'] = self.config_properties.get(self.context['gae_application_id'],'short_url')
		self.context['ui_debug'] = self.config_properties.getboolean(self.context['gae_application_id'],'ui_debug')
			

		self.context['mail'] = {}
		self.context['mail']['sender'] = self.config_properties.get(self.context['gae_application_id'],'mail_sender')
		
		# Facebook
		self.context['facebook'] = {}
		self.context['facebook']['app_id'] = self.facebook_properties.get('Facebook','app_id')

		self.system['facebook'] = {}
		self.system['facebook']['app_secret'] = self.facebook_properties.get('Facebook','app_secret')
		self.system['facebook']['client_token'] = self.facebook_properties.get('Facebook','client_token')

		# Kapow Request/Response Config
		self.context['kapow'] = {
			'headers':self.kapow_properties.get('Kapow','headers')
		}
		self.context['kapow']['authentication'] = {
			'url':self.kapow_properties.get('Kapow','login_url'),
			'request_payload':'{"parameters":[{"variableName":"input","attribute":[{"name":"username","type":"text","value":"'+self.config_properties.get('kapow','username')+'"},{"name":"password","type":"text","value":"'+self.config_properties.get('kapow','password')+'"},{"name":"memorableanswer","type":"text","value":"'+self.config_properties.get('kapow','memorableanswer')+'"}]}]}'
		}

		self.context['kapow']['currentaccountbalance'] = {
			'url':self.kapow_properties.get('Kapow','account_balance_url'),
			'request_payload':'{"parameters":[{"variableName":"input","attribute":[{"name":"username","type":"text","value":"'+self.config_properties.get('kapow','username')+'"},{"name":"password","type":"text","value":"'+self.config_properties.get('kapow','password')+'"},{"name":"memorableanswer","type":"text","value":"'+self.config_properties.get('kapow','memorableanswer')+'"}]}]}'			
		}

		# Tokenized Kapow Payload for AMEX, such that we can inject User ID and Password upon request
		self.context['kapow']['amex'] = {
			'url':self.kapow_properties.get('Kapow','amex_account_url'),
			'request_payload':'{"parameters":[{"variableName":"input","attribute":[{"name":"username","type":"text","value":"@USER_ID@"},{"name":"password","type":"text","value":"@PASSWORD@"}]}]}'
		}

		# Alchemy Context Data
		self.context['alchemy'] = None

	def set_current_user(self):
		self.context['current_user'] = datastore.set_user(users.get_current_user())
		logging.debug(self.context['current_user'].key().name())
		self.context['logout_url'] = users.create_logout_url("/")

	def set_request_arguments(self):
		request_args = dict()
		for arg in self.request.arguments():
			request_args[arg] = self.request.get(arg)
				
		self.context['request_args'] = request_args
		
		logging.info(self.context['request_args'])

		return self.context['request_args']
	
	def set_response_error(self, error, status_code):
		self.context['error'] = error
		self.response.set_status(status_code)
		return

	def set_alchemy_response_headers(self, **kwargs):
		logging.info(self.request.headers)
		_origin = None
		try:
			_origin = self.request.headers['Origin']
		except:
			logging.warning('No Origin issued by Request')
			# If we are using a Same-Domain request form this App, we will not have an Origin header
			# Instead, check if the Host matches the App Domain
			try:
				_origin = self.request.headers['Host']
			except:
				logging.warning('No Host issued by Request')
		

		# Check to see if the Requested Origin is in our Allowed Origin list
		if _origin in self.lbg_origins:
			self.response.headers.add_header('Access-Control-Allow-Origin', _origin)
			self.response.headers.add_header('Access-Control-Allow-Credentials', 'true')
			self.response.headers.add_header('Access-Control-Allow-Headers', 'origin, x-requested-with, content-type, accept')
			self.response.headers.add_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
			self.response.headers.add_header('Access-Control-Max-Age', str(self.cors.get('max_age')))
			return True
		# Else, do not respond with any allowed Origins, or any other headers
		else:
			self.response.set_status(403)
			return False

	def render_template(self, template_name):
		return jinja_environment.get_template(template_name+'.html')
		
	def render(self, template_name):
		template = jinja_environment.get_template(template_name+'.html')
		self.response.out.write(template.render(self.context))
		return

	def render_alchemy_cvp1_template(self, template_name):
		template = jinja_environment_alchemy_cvp1.get_template(template_name+'.html')
		self.response.out.write(template.render(self.context))
		return

	def render_alchemy_static_file(self, content_path, **kwargs):
		static_file = jinja_alchemy_static.get_template(content_path)
		if 'Content-Type' in kwargs:
			self.response.headers['Content-Type'] = kwargs['Content-Type']
		elif content_path.find('.css') != -1:
			self.response.headers['Content-Type'] = 'text/css'
		elif content_path.find('.png') != -1:
			self.response.headers['Content-Type'] = 'image/png'
		elif content_path.find('.js') != -1:
			self.response.headers['Content-Type'] = 'application/x-javascript'
		self.response.out.write(static_file.render())
		return

	def render_xml(self, template_name):
		path = os.path.join(os.path.dirname(__file__),'../controllers/'+template_name+'.xml')
		self.response.out.write(template.render(path, self.context))
		return

	def render_json(self, **kwargs):
		if kwargs.has_key('status_code'):
			self.response.set_status(kwargs['status_code'])
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(json.dumps(self.data_output))
		return

	def set_json_response_error(self, error, status_code):
		self.response.set_status(status_code)
		self.data_output = dict(status='failure', error=str(error))
		return

	def output_response(self, responseMessage, **kwargs):
		if kwargs.has_key('status_code'):
			self.response.set_status(kwargs['status_code'])			
		self.response.out.write(responseMessage)
		return


	def authenticate_admin_user(self):
		user = None
		is_system_admin= False
		try:
			user = users.get_current_user()
			
			if user is None:
				logging.error('utils.BaseHandler() : authenticate_admin_user() : user was None')
				raise Exception('Unable to get User from GAE API')
			
			is_system_admin = users.is_current_user_admin()
			self.admin_user = dict(
				user=user,
				is_system_admin=is_system_admin,
				logout_url=users.create_logout_url("/")
			)
			self.context['admin_user'] = self.admin_user

			return self.context['admin_user']
		except Exception, e:
			logging.error('utils.BaseHandler() : authenticate_admin_user()')
			logging.error(e)
			raise

def load_property_file(file_name):
	try:
		property_file = memcache.get(file_name+'.properties', namespace='razorfish')
		if property_file is not None:
			return property_file
		else:
			property_file_path = 'properties/'+file_name+'.properties'
			property_file = ConfigParser()
			property_file.read(property_file_path)
			#memcache.set(file_name+'.properties', property_file, namespace='razorfish')
			return property_file

	except Exception, e:
		logging.error(e)
		raise

class GMT1(tzinfo):
	def __init__(self):
		try:			
			now = datetime.datetime.now()
			year = now.year
			# DST starts last Sunday in March
			d = datetime.datetime(year, 4, 1)
			self.dston = d - timedelta(days=d.weekday() + 1)
			# ends last Sunday in October
			d = datetime.datetime(year, 11, 1)
			self.dstoff = d - timedelta(days=d.weekday() + 1)
		except Exception, e:
			logging.error(e)
			raise
	def utcoffset(self, dt):
		return timedelta(hours=1) + self.dst(dt)
	def dst(self, dt):
		if self.dston <= dt.replace(tzinfo=None) < self.dstoff:
			return timedelta(hours=0)
		else:
			return timedelta(0)
	def tzname(self,dt):
		return "GMT +1"