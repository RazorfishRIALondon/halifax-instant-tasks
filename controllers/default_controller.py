from google.appengine.api import memcache
import os
import re
import time
import logging
import datetime
import webapp2
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
from google.appengine.ext import blobstore
from google.appengine.api.datastore import Key
from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.api import mail
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError   
from google.appengine.ext.db import BadValueError
from google.appengine.ext import db
from google.appengine.runtime import DeadlineExceededError
from operator import itemgetter
import urllib

# Import local scripts
from controllers import utils
from controllers import datastore
from controllers import date_utils
from controllers import tasks
from models import models



class DefaultHandler(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		self.set_current_user()
		template = 'index'
		try:
			if self.context['current_user'].email not in self.context['user_whitelist']:
				template = 'unauthorized'
			

			self.context['tasks'] = datastore.get_user_tasks(**dict(user_id=self.context['current_user'].key().name()))
			
		except Exception, e:
			logging.error('DefaultHandler() : Exception')
			logging.error(e)
			self.set_response_error(e, 500)
		finally:
			self.render(template)
		
	def get(self):
		self.process()
	def post(self):
		self.process()


class SaveUserTaskStep1(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		self.set_current_user()
		template = 'create-task-step2'
		try:
			self.context['task'] = task = datastore.set_task(**self.context['request_args'])

		except Exception, e:
			logging.error('DefaultHandler() : Exception')
			logging.error(e)
			self.set_response_error(e, 500)
		finally:
			self.render(template)
		
	def get(self):
		self.process()
	def post(self):
		self.process()

class SaveUserTaskStep2(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		self.set_current_user()
		template = 'create-task-step3'
		try:
			self.context['task'] = task = datastore.set_task(**self.context['request_args'])
			
		except Exception, e:
			logging.error('DefaultHandler() : Exception')
			logging.error(e)
			self.set_response_error(e, 500)
		finally:
			self.render(template)
	def get(self):
		self.process()
	def post(self):
		self.process()


class SaveUserTaskStep3(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		try:
			self.context['task'] = task = datastore.set_task(**self.context['request_args'])
			
			if task.active == True:
				taskqueue.add(
					queue_name='currentaccount', 
					url='/tasks/currentaccountbalance', 
					params={
						'user_id':task.user.key().name(),
						'task_id':task.key().id()
					}
				)
			
			return 'success'
		except Exception, e:
			logging.error('DefaultHandler() : Exception')
			logging.error(e)
			self.set_response_error(e, 500)
		
	def get(self):
		self.process()
	def post(self):
		self.process()

class GetTaskList(utils.BaseHandler):
	def get(self):
		self.set_request_arguments()
		self.set_current_user()
		template='intro-new'
		try:
			if not self.context['request_args']['user_id']:
				raise Exception('No user_id provided')

			self.context['tasks'] = datastore.get_user_tasks(**dict(user_id=self.context['request_args']['user_id']))

			if len(self.context['tasks']) > 0:
				template = 'intro-manage-tasks'
			else:
				template = 'intro-new'
		
		except Exception, e:
			raise e
		finally:
			self.render(template)		


class GetTask(utils.BaseHandler):
	def get(self):
		self.set_request_arguments()
		self.set_current_user()
		template = 'edit-task'
		try:
			self.context['task'] = models.CurrentAccount.get_by_id(int(self.context['request_args']['task_id']))

		except Exception, e:
			raise e
		finally:
			self.render(template)

class SaveUserFacebookID(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		self.set_current_user()
		response = False
		try:
			user = datastore.set_user_facebook_id(
				self.context['current_user'].key().name(), 
				self.context['request_args']['facebook_id']
			)

			response = True
		except Exception, e:
			raise e
		finally:
			return response
	def get(self):
		self.process()
	def post(self):
		self.process()

class WarmupHandler(webapp2.RequestHandler):
	def get(self):
		logging.debug('Warmup Request')
		pass

class VoxeoVoiceManifestHandler(utils.BaseHandler):
	def process(self):
		try:
			self.response.headers.add_header('Content-Type', 'application/xml')
			self.render_xml('voxeo_manifest')
		except Exception, e:
			logging.error('VoxeoVoiceManifestHandler() : process()')
			logging.error(e)
			raise e
	def get(self):
		self.process()
	def post(self):
		self.process()

class VoxeoVoiceApplicationHandler(utils.BaseHandler):
	def process(self):
		self.set_request_arguments()
		try:
			self.response.headers.add_header('Content-Type', 'application/xml')
			self.render_xml('voxeo_application')
		except Exception, e:
			logging.error('VoxeoVoiceApplicationHandler() : process()')
			logging.error(e)
			raise e
	def get(self):
		self.process()
	def post(self):
		self.process()