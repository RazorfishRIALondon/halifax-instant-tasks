requirejs.config({

    baseUrl: '//alchemy-test.lloydstsb.com:8890/js',
    paths: {
        "utils":"utils"
    },
    waitSeconds: 1,
    config: {
        text: {
            useXhr: function(url, protocol, hostname, port) {
                // allow cross-domain requests
                // remote server allows CORS
                return true;
            },
            onXhr: function (xhr, url) {
                //Called after the XHR has been created and after the
                //xhr.open() call, but before the xhr.send() call.
                //Useful time to set headers.
                //xhr: the xhr object
                //url: the url that is being used with the xhr object.
            }
        }
    }

});

// Start the main app logic.
requirejs([
           
            "modules/intelligent-type",

            //"utils/api-linkedin",
            "utils/core"

            ], function (itype, core) {

	core.log("init alchemy");

    itype.init();
    //linkedin_api.init();

});
