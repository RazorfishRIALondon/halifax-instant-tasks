$(function() {


    $.ajax({
       type: 'GET',
        
        url: "data/balance.jsonp",
        async: false,
        jsonpCallback: 'onDataComplete',
        contentType: "application/json",
        dataType: 'jsonp',

        success: function(json) {
           onDataComplete(json);
        },
        error: function(e) {
           console.log(e.message);
        }
    });


    function onDataComplete (data) {

        console.log(data);

        // Create the chart
        $('.graph-container').highcharts('StockChart', {
            

            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Account Balance'
            },
            
            series : [{
                name : 'Current Account',
                data : data,
                tooltip: {
                    valueDecimals: 2,
                    valuePrefix: "£"
                }
            }],

            colors: [
               '#5abd19', 
               '#0d233a', 
               '#8bbc21', 
               '#910000', 
               '#1aadce', 
               '#492970',
               '#f28f43', 
               '#77a1e5', 
               '#c42525', 
               '#a6c96a'
            ]



        });

    };


});