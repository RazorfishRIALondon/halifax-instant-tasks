/*
*	@name: intelligent_Search.js
*	@description:
*		Provides Intelligent Type and Results features
*/
define([

    "utils/options","utils/core","utils/sessionstorage","utils/event",
    
    "modules/account-balance",

    "utils/api-linkedin"

    ], function(options,core,storage,broadcast,
        
        view_module_account_balance,

        apilinkedin){

    return {
        
        init: function() {

            var self = this;
            self.linkedIn = {};

            self.currentViewModule = "";
       
            // init linkedin
            apilinkedin.init(function(profile, connections) {
                
                self.linkedIn.profile = profile;

                connections = connections.concat(self.companyList);
                self.linkedIn.connections = connections;


            }, function(msg) {
                core.log("api linkedin error: " + msg);
            } );


            self.routeView();

            //

        },

        commandHelp: [
            
            {title:"Your Balance", icon:"balance"},
            {title:"Pay Bills", icon:"pay"},
            {title:"Transfers", icon:"transfer"},
            {title:"Cards", icon:"card"},
            {title:"Check Rates", icon:"rates"},
            {title:"Customer Service", icon:"customer"},
            {title:"Mortgages", icon:"mortgage"},
            {title:"Your Profile", icon:"profile"},
            {title:"Standing Orders", icon:"orders"}

        ],

        companyList: [
    
            {id:"t-mobile", companyName:"T-Mobile", pictureUrl:"/img/logo-tmobile.png" },
            {id:"o2", companyName:"O2", pictureUrl:"/img/logo-o2.png" },
            {id:"vodafone", companyName:"Vodafone", pictureUrl:"/img/logo-vodafone.png" }

        ],

        relatedContacts: [
    
        // to be filled in

        ],

        randomContacts: [
    
        // to be filled in

        ],

        queryTriggers: [
            
            {group:"create",    id:"create_pay",            title:"Pay {num}",                    icon:"pay", handleNum:true, handleContact:true,                     trigger:"pay,buy,bill",                                         module:"pay"},
            {group:"create",    id:"create_transfer",       title:"Transfer {num}",         icon:"transfer", handleNum:true, handleContact:true,               trigger:"send,move,transfer,wire,domestic",            module:"transfer"},
            {group:"create",    id:"create_standing_order", title:"Setup Standing Order {num}",   icon:"orders", handleNum:true, handleContact:true,  trigger:"add,setup,create,standing,order",             module:"standing_order"},
            {group:"create",    id:"create_correspondence", title:"Contact the Bank",       icon:"customer", handleNum:false, handleContact:false,       trigger:"contact,bank,write,customer,service,ask,help,question",                       module:"correspondence"},
            {group:"create",    id:"create_credit_card",    title:"Get a Credit Card",      icon:"card", handleNum:false, handleContact:false,             trigger:"apply,get,card,credit,visa,mastercard,american,express",   module:"apply_card"},
            {group:"create",    id:"create_debit_card",     title:"Get a Debit Card",       icon:"card", handleNum:false, handleContact:false,             trigger:"apply,get,card,debit,visa,mastercard",   module:"apply_card"},
            {group:"create",    id:"create_mortgage",       title:"Get a Mortgage {num}",         icon:"mortgage", handleNum:true, handleContact:false,   trigger:"apply,get,mortgage",                                             module:"mortgage"},
            {group:"create",    id:"create_travel_insurance",title:"Get a Travel Insurance",         icon:"travel_insurance", handleNum:false, handleContact:false,   trigger:"apply,get,insurance,travel,abroad,cover",                                             module:"insurances"},
            {group:"create",    id:"create_home_insurance",title:"Get a Home Insurance",         icon:"home_insurance", handleNum:false, handleContact:false,   trigger:"apply,get,insurance,home,house,cover",                                             module:"insurances"},

            {group:"read",      id:"read_balance",          title:"Your Balance",           icon:"balance", handleNum:false, handleContact:false,          trigger:"check,read,see,my,balance,limit,credit,account,savings,statement",  module:"balance"},
            {group:"read",      id:"read_correspondence",   title:"Your Bank Mail",         icon:"mail", handleNum:false, handleContact:false,     trigger:"check,read,see,bank,service,customer,ask,help,message",               module:"correspondence"},
            {group:"read",      id:"read_interest_rate",    title:"Account Interest Rates", icon:"rates", handleNum:false, handleContact:false,   trigger:"check,read,see,account,interest,rate,rates",                             module:"interest_rates"},
            {group:"read",      id:"read_mortgages",        title:"Review Your Mortgage",   icon:"mortgage", handleNum:false, handleContact:false,     trigger:"check,read,see,my,review,mortgage",                                 module:"mortgages"},
            {group:"read",      id:"read_exchange_rate",    title:"Show exchange rate {num}",     icon:"exchange", handleNum:true, handleContact:false,   trigger:"check,change,read,see,exchange,rate,rates,foreign",                           module:"exchange_rate"},
            {group:"read",      id:"read_transations",      title:"Find Transaction",       icon:"transaction", handleNum:false, handleContact:true,   trigger:"check,transations,search,find,read,see",                           module:"search_transations"},
            {group:"read",      id:"read_remaining",        title:"Remaining Funds this month", icon:"wallet", handleNum:false, handleContact:false,   trigger:"check,remaining,funds,left,month,bills",                           module:"remaining_funds"},

            {group:"update",    id:"update_profile",        title:"Your Profile",           icon:"profile", handleNum:false, handleContact:false,    trigger:"update,address,profile,change,move,home,work,billing",                           module:"profile"},
            {group:"update",    id:"update_standing_order", title:"Your Standing Orders",   icon:"orders", handleNum:false, handleContact:true, trigger:"update,change,standing,order",            module:"standing_order"},

            {group:"delete",    id:"delete_standing_order", title:"Cancel Standing Order",  icon:"orders", handleNum:false, handleContact:true,  trigger:"delete,remove,cancel,standing,order",         module:"standing_order"}

        ],

        /*
        *   @description:
        *       Simple task router based on current view
        */
        routeView: function() {
            
            var self = this,
            url = window.location.pathname;

            self.intelligentTypeReady();

            core.loadCss(options.urls.css.intelligentType);
            core.loadCss(options.urls.css.moduleViewCommonCss);

        },

        /*
        *	@description:
        *		Callback for boradcast event listener for when IS view has been returned by the server
        */
        intelligentTypeReady: function() {
            
            //core.log("intelligentTypeReady");

            var typehtml = '<div class="itelligent-type-wrapper"><div class="itelligent-type-info"><span>Lloyds TSB SmartSuggest&trade;</span><img src="/img/close-button.png" width="25" height="25" /></div>';
            typehtml += '<div class="itelligent-type">';
            typehtml += '';
            //html += '<img src="'+self.userProfile.pictureUrl+'" class="profile-img" />';
            typehtml += '<input type="text" name="itype" maxlength="70" />'
            typehtml += '</div></div>';

            var optionshtml = '<div class="itelligent-options-wrapper"><div class="itelligent-options">';
            optionshtml += '<div class="itelligent-options-list"></div>';
            optionshtml += '<div class="itelligent-options-action"><div class="alchemy-module-view"></div></div>';
            optionshtml += '</div></div>';

        	var self = this,
            page = $("#wrapper");

            page.before("<div class='itelligent-wrapper'><div class='itelligent'>"+typehtml+optionshtml+"</div><div class='itelligent-bg'></div></div>");

            self.itelligentDiv = $(".itelligent");
            self.itelligentBgDiv = $(".itelligent-bg");
            self.itelligentWrapperDiv = $(".itelligent-wrapper");
            self.itelligentOptionsWrapper = $(".itelligent-options-wrapper");
            self.itelligentInfoDiv = $(".itelligent-type-info");
            self.itelligentOptions = self.itelligentOptionsWrapper.find(".itelligent-options");
            self.itelligentOptionsList = self.itelligentOptionsWrapper.find(".itelligent-options-list");
            self.itelligentOptionsAction = self.itelligentOptionsWrapper.find(".itelligent-options-action");
            self.alchemyModuleView = self.itelligentOptionsAction.find(".alchemy-module-view");

            self.itelligentTypeWrapper = $(".itelligent-type-wrapper");
            self.itelligentType =  self.itelligentTypeWrapper.find(".itelligent-type");

            self.inputField = self.itelligentType.find("input");

            var focusOutCopy = "";
            //"Hey "+ self.userProfile.firstName + "!";
            //core.log(self.userProfile.pictureUrl);

            var latestCopy = "";

            self.inputField.val(focusOutCopy);
            self.inputField.focused = false;

            self.itelligentBgDiv.click(function() {
                self.showSearch(false);
            });

            self.itelligentInfoDiv.click(function() {
                self.showSearch(false);
            });

            self.inputField.focus(function() {
                self.inputField.focused = true;
            });

            self.inputField.blur(function() {
                self.inputField.focused = false;
                //if(self.showSearchOnStage) self.showSearch(false);
            });

            $('input').focus(function() {
                self.inputFocused = true;
            });

            $('input').blur(function() {
                self.inputFocused = false;
            });

            self.inputFocused = false;

            $('body').keyup(function(event) {
                
                if(event.keyCode==27) {
                    if(self.showSearchOnStage) self.showSearch(false);

                }else if(event.keyCode==13) {
                    // submit / enter
                    if(self.showSearchOnStage) self.showSearch(false);


                }else if((event.keyCode>=48 && event.keyCode<=90) || (event.keyCode>=96 && event.keyCode<=105)) {
                    //console.log(String.fromCharCode(event.which));
                  
                    if(!self.inputFocused) {

                        self.showSearch(true);

                        if(!self.inputField.focused) {
                            self.inputField.val(String.fromCharCode(event.keyCode));
                            self.inputField.focus();
                        }

                    }

                }

                if(self.inputFocused) {
                    //self.inputField.val(self.inputField.val().toLowerCase());
                    self.handleFieldChange(self.inputField.val());
                }

            });

            self.showSearchOnStage = false;
            self.showSearch = function ( val ) {

                if(val) {
                    if(!self.showSearchOnStage) {
                        self.itelligentWrapperDiv.fadeIn(200, function () {
                            self.itelligentOptions.css("margin-top",0);
                        });
                    }
                    self.showSearchOnStage = true;
                }else{
                    if(self.showSearchOnStage) {
                        //self.inputField.val("");
                        if(self.inputField.focused) self.inputField.blur();
                        self.itelligentWrapperDiv.fadeOut(200);
                        self.itelligentOptions.css("margin-top",0);
                        self.itelligentOptions.css("height",0);
                    }
                    self.showSearchOnStage = false;
                }

            };

        },

        resizeOptions: function( height ) {
            var self = this;
            self.itelligentOptions.css("height",Math.ceil(height));
        },

        handleFieldChange: function( str ) {
            
            var self = this;
            var inputStr = str.toLowerCase();
            var resultList = self.getResultsByTrigger(inputStr);

            console.log("s: " + inputStr + " / results: " + resultList.length );

            self.displayResults(resultList,inputStr);

        },

        displayResults: function( results, str ) {

            var self = this;
            var listContainer = self.itelligentOptionsList;
            var actionContainer = self.itelligentOptionsAction;
            var moduleViewContainer = self.alchemyModuleView;
            
            var moduleResultHTML = "";
            var contactResultHTML = "";

            var showList = true;
            var numOfWords = str.split(" ");

            var resultModule = false;
            var resultModuleObj = {};
            var resultContact = false;
            var resultContactObj = {};
            var showHelp = false;
            var resultHTML = "";
            var numOfModules = 0;
            var numOfContacts = 0;
            var resultContactModule = false;

            var numOfResults = results.length;
            if(numOfResults>18) numOfResults = 18; 
            var numPossible = false;

            for (var i = 0; i < numOfResults; i++) {
                
                var thumb = "";
                var itemTitle = "";
                var chtml = "";
                var cicon = "default";

                if(results[i].type=="contact") {

                    resultContact = true;
                    resultContactObj = results[i];
                    numOfContacts++;

                }else{
                   
                    resultModule = true;
                    resultModuleObj = results[i];
                    numOfModules++;

                    if(results[i].handleContact==true) {
                        resultContactModule = true;
                    }

                }

                if(results[i].firstName) itemTitle = results[i].firstName + " " + results[i].lastName;
                if(results[i].companyName) itemTitle = results[i].companyName;
                if(results[i].title) itemTitle = results[i].title;       
                if(results[i].pictureUrl) thumb = "<img src='"+results[i].pictureUrl+"'/>";      
                if(results[i].icon) cicon = results[i].icon;

                var classType = "result-module";
                if(results[i].type=="contact") classType = "result-contact";

                chtml += "<div class='result-item " + results[i].id +" "+classType+"'><a href='#'>";
                
                //console.log(results[i].pictureUrl);

                if(thumb) {
                    chtml += "<div class='thumb svg-icon svg-icon-contact'>"+thumb+"</div>";
                }else{
                    chtml += "<div class='thumb svg-icon svg-icon-"+cicon+"'></div>";
                }

                var resnum = results[i].num;
                if(resnum<=0 || resnum==undefined) {
                    resnum = "";
                    numPossible = true;
                    itemTitle = itemTitle.replace(" {num}","");
                }else{
                    itemTitle = itemTitle.replace(" {num}"," <b>"+formatPound(resnum)+"</b>");
                }

                if(itemTitle!="") chtml += "<div class='title'>" + itemTitle+ "</div>";

                chtml += "</a></div>";

                if(results[i].type=="contact") {
                    if(numOfContacts<10) contactResultHTML += chtml;
                }else{
                    if(numOfModules<10) moduleResultHTML += chtml;
                }

                resultHTML += chtml;


            };

            var optionsHeight = 0;
            var actionMsg = "";
            var showAction = false;

            var newViewModule = "";
            var moduleChanged = false;
            var cmoduleObj = null;

            if (((numOfResults==2 && resultModule==true && resultContact==true  && numOfWords.length>1) || (numOfModules==1 && resultModule==true && resultContact==false))) {
                
                if(resultContactObj.firstName) {
                    // = resultModuleObj.title + " / " + resultContactObj.firstName + " " + resultContactObj.lastName;

                }else if(resultContactObj.companyName) {
                    //actionMsg = resultModuleObj.title + " / " + resultContactObj.companyName;

                }else{
                    //actionMsg = resultModuleObj.title;
                }

                newViewModule = resultModuleObj.module;


                if(newViewModule!=self.currentViewModule) moduleChanged = true;

                if(resultModuleObj.module=="balance") {
                    cmoduleObj = view_module_account_balance;
                }else{

                }

                self.currentViewModule = newViewModule;

                if(moduleChanged) {
                    
                    moduleViewContainer.html("");

                    if(cmoduleObj) {
                        showAction = true;
                        cmoduleObj.init(moduleViewContainer);
                        cmoduleObj.update(resultModuleObj, resultContactObj);
                    }

                }else{
                    
                    if(cmoduleObj) {
                        showAction = true;
                        cmoduleObj.update(resultModuleObj, resultContactObj);
                    }

                }

            }

            //

            //if(moduleChanged) actionContainer.html("");
            
            listContainer.html("");

            if(showList) {
                
                var numOfColumns = 0;
                var maxRowHeight = 0;

                if(resultContactModule==true && numOfContacts<1 && numOfModules==1) {
                    /*
                    var helpStr = "...type a name";
                    if(numPossible) {
                        helpStr = "...type amount and name";
                    }
                    contactResultHTML = "<div class='result-item type-help'>"+helpStr+"</div>";
                   
                    numOfContacts = 1;
                    */
                }


                if(numOfModules>0) numOfColumns++;
                if(numOfContacts>0) numOfColumns++;

                optionsHeight = 0;
                resultHTML = "";

                if(numOfModules>0) {
                    resultHTML += "<div class='option-list"+numOfColumns+" c1'>"+moduleResultHTML+"</div>";
                }

                if(numOfContacts>0) {
                    resultHTML += "<div class='option-list"+numOfColumns+" c2'>"+contactResultHTML+"</div>";
                }

                if(numOfModules>numOfContacts) {
                    maxRowHeight = numOfModules;
                }else{
                    maxRowHeight = numOfContacts;
                }

                if(maxRowHeight>9) maxRowHeight = 9;

                listContainer.html(resultHTML);

                if(showAction) {
                    actionContainer.fadeIn();
                    optionsHeight += 300; 
                }else{
                    actionContainer.hide();
                }

                optionsHeight += (Math.ceil(maxRowHeight)*39);

            }

            self.resizeOptions(optionsHeight);

        },

        getResultsByTrigger: function( str ) {

            var self = this;
            var resultList = [];
            var contactMatched = false;
            var userStr = str.toLowerCase();
            var modulesAsList = "";
            var splittedStr = userStr.split(" ");
            var modulesFound = false;
            var moduleHandleContacts = false;


            //

            var numberFound = 0;
            for (var i = 0; i < splittedStr.length; i++) {
                var bw = Number(splittedStr[i]);
                if(bw>0) {
                    numberFound = bw;
                }
            }

            //

            var modulesArray = [];
            // search modules
            for (var i = 0; i < self.queryTriggers.length; i++) {

                var lineTriggers = self.queryTriggers[i].trigger.split(",");
                var pickHits = 0;

                for (var u = 0; u < lineTriggers.length; u++) {
                    if(userStr.indexOf(lineTriggers[u])>=0) {
                       pickHits++;
                    }
                };

                for (var s = 0; s < splittedStr.length; s++) {
                    for (var r = 0; r < lineTriggers.length; r++) {
                        if(lineTriggers[r]==splittedStr[s]) {
                            pickHits++;
                            break;
                        }
                    }
                };

                var newObj = self.queryTriggers[i];

                if(pickHits>0) {

                    modulesAsList += newObj.id;
                    if(newObj.handleNum) newObj.num = numberFound;
                    newObj.hits = pickHits;
                    newObj.type = "module";
                    modulesArray.push(newObj);

                    if(newObj.handleContact) moduleHandleContacts = true;
                    modulesFound = true;

                    //console.log(self.queryTriggers[i].hits, self.queryTriggers[i].title);

                }else{
                    if(newObj.handleNum) newObj.num = null;
                    newObj.hits = 0;
                }

            };


               
            var optimizedModuleArray = [];
            var list = modulesArray.sort(orderByHits);

            var nums = [];

             if(list.length>1) {
                if(list[0].hits>list[1].hits) {
                    optimizedModuleArray = [];
                    optimizedModuleArray.push(list[0]);
                }else{
                    optimizedModuleArray = list;
                }
            }else{
                optimizedModuleArray = list;
            }



            resultList = resultList.concat(optimizedModuleArray);

            //


            var contactsArray = [];

            // search people
            if(self.linkedIn.connections) {
            
                if(userStr.length>1) {

                    var connectionList = self.linkedIn.connections;

                    for (var c = 0; c < connectionList.length; c++) { 

                        var thiscontactMatched = 0;

                        var cfirstname = "";
                        var clastname = "";

                        if(connectionList[c].firstName) cfirstname = connectionList[c].firstName.toLowerCase();
                        if(connectionList[c].lastName) clastname = connectionList[c].lastName.toLowerCase();
                        
                        var cfullname = "";
                        if(cfirstname && clastname) cfullname = cfirstname + " " + clastname;
                        
                        if(connectionList[c].companyName) {
                            
                            cfullname = connectionList[c].companyName.toLowerCase();

                            if(cfullname.indexOf(userStr)>=0) {
                                thiscontactMatched += 1;
                            }

                            if(userStr.indexOf(cfullname)>=0) {
                                thiscontactMatched += 10;
                            }

                        }else{

                            var cHeadline = "";
                            var cHeadlineBreakDown = [];

                            if(connectionList[c].headline) {
                                cHeadline = connectionList[c].headline.toLowerCase();
                                cHeadlineBreakDown = cHeadline.split(" ");
                            }
                            
                            var clocationName = "";
                            var clocationNameBreakDown = [];
                            if(connectionList[c].location) {
                                if(connectionList[c].location.name) {
                                    clocationName = connectionList[c].location.name.toLowerCase();
                                    clocationNameBreakDown = clocationName.split(", ");
                                }
                            }

                            var sifirstname = userStr.indexOf(cfirstname);
                            var silastname = userStr.indexOf(clastname);

                            var firstNameLastWord = (userStr.length-sifirstname-cfirstname.length);
                            var lastNameLastWord = (userStr.length-silastname-clastname.length);

                            if( (sifirstname>=0  && cfirstname.length>1 ) || (silastname>=0 && clastname.length>1)) {
                                thiscontactMatched++;
                            }

                            if(cfirstname.indexOf(userStr)>=0 || clastname.indexOf(userStr)>=0) {
                                thiscontactMatched++;
                            }

                            for (var w = 0; w < clocationNameBreakDown.length; w++) {
                                if(userStr.indexOf(clocationNameBreakDown[w])>=0) {
                                    if(clocationNameBreakDown[w].length>4) thiscontactMatched += 1;
                                }
                            }

                            for (var y = 0; y < cHeadlineBreakDown.length; y++) {
                                if(userStr.indexOf(cHeadlineBreakDown[y])>=0) {
                                    if(cHeadlineBreakDown[y].length>4) thiscontactMatched += 1;
                                }
                            }

                            if(userStr.indexOf(cHeadline)>=0) {
                                thiscontactMatched += 1;
                            }


                            if(userStr.indexOf(cfullname)>=0) {
                                thiscontactMatched += 10;
                            }

                        }

                        if(cfullname.indexOf("private")>=0) {
                            thiscontactMatched = 0;
                        }
                        
                        if(thiscontactMatched>0) {
                            
                            contactMatched = true;
                            connectionList[c].hits = thiscontactMatched;
                            connectionList[c].type = "contact";
                            contactsArray.push(connectionList[c]);

                            // more than two hits
                            if(thiscontactMatched>2) {
                                
                                var existingInRelated = false;
                                for (var r = 0; r < self.relatedContacts.length; r++) {
                                    if(self.relatedContacts[r].id==connectionList[c].id) {
                                        existingInRelated = true;
                                        break;
                                    }
                                };

                                if(!existingInRelated) self.relatedContacts.push(connectionList[c]);

                                contactsArray = [];
                                contactsArray.push(connectionList[c]);      

                                break;

                            }

                        }

                    }

                }else{
                    contactsArray = [];
                }

            }

            resultList = resultList.concat(contactsArray);

            //

            // check modules compatible with contactal contacts
            if(contactMatched==true && modulesAsList=="") {
                for (var s = 0; s < self.queryTriggers.length; s++) {
                    if(self.queryTriggers[s].handleContact==true) {
                        resultList.push(self.queryTriggers[s]);
                        modulesFound = true;
                    };
                };
            };

            //

            function orderByHits(a,b) {
              if (a.hits < b.hits)
                 return 1;
              if (a.hits > b.hits)
                return -1;
              return 0;
            }

            //self.relatedContacts = self.relatedContacts.sort(orderByHits);

            if(!modulesFound) {
                resultList = resultList.concat(self.commandHelp);
            }

            if(!contactMatched) {
                //if(moduleHandleContacts) resultList = resultList.concat(self.relatedContacts);
            }

            resultList = resultList.sort(orderByHits);


            //


            //


            return resultList;

        }

    }
});