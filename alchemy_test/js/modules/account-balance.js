


define(["utils/options","utils/core","utils/sessionstorage","utils/event"], function(options,core,storage,broadcast){

    return {
        
        init: function( container ) {

            var self = this;
            self.container = container;

            var newDOM = "";

            //core.loadCss(options.urls.css.intelligentType);

            self.accountData = [];

            self.accountData.push({ id:"current_personal", name:"John's Current", type:"Current Account", balance:1456.55 });
            self.accountData.push({ id:"savings_personal", name:"John's Monthly Saving", type:"Saving Account", balance:6540.50 });
            self.accountData.push({ id:"savings_personal", name:"John's Yearly Saving", type:"Saving Account", balance:15445.20 });
            self.accountData.push({ id:"credit_personal", name:"My Credit Account", type:"Credit Account", balance:4995.00 });

            self.accountData.push({ id:"current_joint", name:"John &amp; Emma's Current", type:"Current Account", balance:504.30 });
            self.accountData.push({ id:"savings_joint", name:"John &amp; Emma's Monthly Savings", type:"Savings Account", balance:780.50 });
            self.accountData.push({ id:"savings_joint", name:"John &amp; Emma's Longterm Savings", type:"Savings Account", balance:54665.25 });


            // create DOM

            for (var i = 0; i < self.accountData.length; i++) {
                var nob = "";
                if(i==(self.accountData.length-1)) nob = "noborder";
                newDOM += "<div class='sheet-row "+nob+"'>";
                newDOM += "<div class='sheet-column pr50 name'><span>"+self.accountData[i].name+"</span></div>";
                newDOM += "<div class='sheet-column pr25 type'><span>"+self.accountData[i].type+"</span></div>";
                newDOM += "<div class='sheet-column pr25 amount noborder'><span num='"+self.accountData[i].balance+"'>&pound;0</span></div>";
                newDOM += "</div>";
            };

            newDOM += "";
            newDOM += "";
            newDOM += "";
            newDOM += "";
            newDOM += "";
            newDOM += "";
            newDOM += "";
            newDOM += "";


            newDOM += "";

            // 
            self.container.html(newDOM);
            
            // set references

            self.view = container.find(".alchemy-module-view");




        },

        update: function( moduleObj, contactObj ) {

            var self = this;

            var valueList = self.container.find(".amount span");

            for (var i = 0; i < valueList.length; i++) {
               
                $(valueList[i]).html(formatPound(Number($(valueList[i]).attr("num"))));

            };
            //self.container.html(moduleObj.id);


        }

    }
});