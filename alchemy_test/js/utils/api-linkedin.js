/*
*	@name: event.js
*	@description:
*		Event broadcast registration. Bind to an event with a listener
*/
define(["utils/core"], function(core) {
	
	return {

		init: function( onAuth, onError ) {	

			var self = this;
			var userProfile = {};
			var userConnections = [];
			var onAuthCallback = onAuth;
			var onErrorCallback = onError;

			core.log("utils/api-linkedin.init()");

			window.linkedinAsycOnLoad = function () {
				
				core.log("utils/api-linkedin.onLoad()");
				
				IN.Event.on(IN, "auth", self.onAuth);
				
				if(!self.isAuthorized()) IN.UI.Authorize().place();

			};

			self.onInit = function() {	
				
				core.log("utils/api-linkedin.onInit()");
				IN.init({ onLoad: "linkedinAsycOnLoad", api_key: "q4umh90xrytv",  authorize:true, scope:"r_fullprofile r_network" });

			};

			self.onAuth = function() {	
				
				core.log("utils/api-linkedin.onAuth()");

			    if(self.isAuthorized()) {
			    	self.getMyProfile();
			    }else{
			    	self.onErrorCallback("onAuth Error");	
			    }

			}

			self.isAuthorized = function() {	
				return IN.User.isAuthorized();
			}

			self.getMyProfile = function() {	
				IN.API.Profile("me").result(self.onMyProfileRetrieved);
			}
			
			self.onMyProfileRetrieved = function(data) {	
				
				core.log("utils/api-linkedin.onMyProfileRetrieved()");

				self.userProfile = data.values[0];
				
				//

				self.getConnections();
				
				//

			}

			self.getConnections = function() {	
				IN.API.Connections("me").result(self.onConnectionsRetrieved);
			}

			self.onConnectionsRetrieved = function(data) {	
				
				core.log("utils/api-linkedin.onConnectionsRetrieved("+data.values.length+")");

				self.userConnections = data.values;
				self.onInitComplete();

			}

			self.onInitComplete = function() {	

				onAuthCallback(self.userProfile,self.userConnections);

			}

			$.getScript("//platform.linkedin.com/in.js?async=true", self.onInit);

		}

	}

});