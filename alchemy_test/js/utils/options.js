/*
*	@name: options.js
*	@description:
*		Options and URL context pathsfor XHR or Routing logic
*/
define({	
	debug:true,
	storageKeys:{
		omm:{
			pieChart:"chart"
		},
		aggregation:{
			amex:"amex"
		},
		user:{
			id:"userID"
		},
		search:{
			view:"search_view"
		}
	},
	google:{
		api_key:"AIzaSyBNTwHWyj1Y1DtPiVfIRcqSHBGiOqyhMFw"
	},
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	urls:{
		sseOrigin:"https://razorfish-lbg.appspot.com/alchemy/sse?lbg_user_id=",
		accountsHome:"/personal/a/account_overview_personal",
		moneyManager:{
			dashboard:"/personal/a/mm_dashboard",
			bookmark:"/personal/a/mm_bookmark"
		},
		login:{
			unauth:"/personal/logon/login"
		},
		logoff:{
			unauth:"/personal/unauth/pages/loggedoff"
		},
		css: {
			moduleViewCommonCss:"/css/module-view-common.css",
			intelligentType:"/css/intelligent-type.css"
		}
	}
});