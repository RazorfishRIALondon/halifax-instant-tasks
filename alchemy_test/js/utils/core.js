String.prototype.contains = function(a) { 
	return this.toLowerCase().indexOf(a.toLowerCase()) != -1; 
};

String.prototype.containsAny = function(args) {
	var h = this.toLowerCase();
	for(var i=0, l=args.length; i<l; i++) {
		if(h.indexOf(args[i].toLowerCase()) != -1) return true;
	}	
};

function formatPound(num) {
    var p = num.toFixed(2).split(".");
    return "&pound;" + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}

/*
*	@name: core.js
*	@description:
*		Core utility methods
*/
define(["utils/options"], function(options) {
	return {
	    /*
	    *   @description:
	    *       Local logger, in case IE is watching...
	    */
	    log: function(msg) {
	        if(options.debug && (typeof window.console != "undefined")) {
	            console.log(msg);
	        }
	    },
	    
	    /*
	    *	@description:
	    *		Return truthy response for whether a string exists within a URL
	    */
	    urlContains: function(url, string) {
	    	return url.indexOf(string) != -1;
	    },

	    /*
		*	@description:
		*		Load a CSS file
		*/
		loadCss: function(url) {
		    var link = document.createElement("link");
		    link.type = "text/css";
		    link.rel = "stylesheet";
		    link.href = url;
		    document.getElementsByTagName("head")[0].appendChild(link);
		},

		/*
		*	@description:
		*		Test if Browser supports CORS
		*/
		browserDoesCORS: function() {
			/* XHR2 available */
			if("withCredentials" in new XMLHttpRequest()) {			    
			    return true;
			}
			/* IE with XDR available */
			else if(typeof XDomainRequest !== "undefined"){				
			  	return true;
			} 
			/* Ah... */
			else {
			  	return false;
			}
		}
	}
});