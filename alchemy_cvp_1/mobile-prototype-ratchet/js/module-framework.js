LBGAlchemy.modules.frameworkModule = function () {

	console.log("LBGAlchemy.modules.frameworkModule()");

	var ro = {};
	ro.title = "Framework";
	ro.id = "framework";
	ro.viewClass = "";
	ro.view = "";

	// header
	ro.view += '<header class="bar-title">';
	ro.view += '<h1 class="title"><a href="#home"><img class="header-logo" src="img/header-logo.png" width="148" height="30" /></a></h1>';
	ro.view += '</header>';

	// content
	ro.view += '<div class="content"></div>';

	// bottom menu
	ro.view += '<nav class="bar-tab">';
	ro.view += '<ul class="tab-inner">';

	for (var i = 0; i < 5; i++) {
		ro.view += '<li class="tab-item">'; // active
		ro.view += '<img class="tab-icon" src="img/svg/noun_project_542.svg" alt="">';
		ro.view += '<div class="tab-label">Item '+i+'</div>';
		ro.view += '</li>';
	};

	ro.view += '</ul>';
	ro.view += '</nav>';

	// footer
	ro.view += '<footer>';
    ro.view += '';
	ro.view += '</footer>';

	ro.init = function ( body, complete ) {

		// init module

		body.append(ro.view);

		complete();

	}

	return ro;

};