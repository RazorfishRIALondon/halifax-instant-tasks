// --------------------------------------
// GET var from URL
// --------------------------------------

if (!document.getUrlVar) {

	document.getUrlVar = function(isToString, isSlash) {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });

	    function objToString (obj) {
		    var str = '';
		    var count = 0;
		    for (var p in obj) {
		        if (obj.hasOwnProperty(p)) {
		        	str += (count!=0)?'&':'';
		            str += p + '=' + obj[p];
		        }
		        count++;
		    }
		    return str;
		}

		var out = (isToString)?objToString(vars):vars;
	    return out;
	}

	document.getsubLink = function(sttr) {
		// var str = ''; //^[A-Z0-9._%+-]+@[A-Z0-9.-]
		// var parts = window.location.href.replace(/\/[A-Z0-9]+\//, '');
		var parts = window.location.href.replace(sttr, '');
	    return parts;
	}
}
