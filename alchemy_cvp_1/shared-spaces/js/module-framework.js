LBGAlchemy.modules.frameworkModule = function () {

	console.log("LBGAlchemy.modules.frameworkModule()");

	var ro = {};
	ro.title = "Framework";
	ro.id = "framework";
	ro.viewClass = "";
	ro.view = "";

	ro.view += '<div class="alchemy">';

	// header
	ro.view += '<header>';
	ro.view += '<div class="header-inner">';
	
	var headerImg = LBGAlchemy.url+"img/header-logo-"+ LBGAlchemy.theme +".png";

		ro.view += '<div class="header-inner-left">';
			ro.view += "<div class='logo'><img src='"+headerImg+"' height='26' /></div>";
		ro.view += '</div>';
		

		ro.view += '<div class="header-inner-right">';

			//

		ro.view += '</div>';

	ro.view += '</div>';
	ro.view += '</header>';

	// subhead
	ro.view += '<div class="subhead">';
	ro.view += '<div class="subhead-inner">';
	ro.view += '<div class="subhead-inner-x">';
	ro.view += '</div>';
	ro.view += '</div>';
	ro.view += '</div>';

	// masthead
	ro.view += '<div class="masthead">';
	ro.view += '<div class="masthead-inner">';
	ro.view += '</div>';
	ro.view += '</div>';

	// content
	ro.view += '<div class="content">';
	ro.view += '<div class="content-inner">';
		//ro.view += '';
		ro.view += '<div class="content-inner-main"></div>';
		ro.view += '<div class="content-inner-panel"></div>';
	ro.view += '</div>';
	ro.view += '</div>';

	// footer
	ro.view += '<footer>';
	ro.view += '<div class="footer-inner">';
	ro.view += '<div class="footer-split"></div>';
    
    if(LBGAlchemy.theme=="lbg") {
    	ro.view += '<ul><li class="right-border"><a href="http://www.lloydstsb.com/legal.asp" title="Legal: Opens in a new window" class="newwin">Legal</a></li><li class="right-border"><a href="http://www.lloydstsb.com/privacy2.asp" title="Privacy: Opens in a new window" class="newwin">Privacy</a></li><li class="right-border"><a href="http://www.lloydstsb.com/security.asp" title="Security: Opens in a new window" class="newwin">Security</a></li><li class="right-border"><a href="http://www.lloydsbankinggroup.com/" title="www.lloydsbankinggroup.com: Opens in a new window" class="newwin">www.lloydsbankinggroup.com</a></li><li><a href="http://www.lloydstsb.com/rates_and_charges.asp" title="Rates and Charges: Opens in a new window" class="newwin">Rates and Charges</a></li></ul>';
    }else{
    	ro.view += '<ul><li class="right-border"><a href="javascript:;" title="Legal: Opens in a new window" class="newwin">Legal</a></li><li class="right-border"><a href="javascript:;" title="Privacy: Opens in a new window" class="newwin">Privacy</a></li><li class="right-border"><a href="javascript:;" title="Security: Opens in a new window" class="newwin">Security</a></li><li><a href="javascript:;" title="Rates and Charges: Opens in a new window" class="newwin">Rates and Charges</a></li></ul>';
 	}

	ro.view += '<div class="footer-split"></div>';
	ro.view += '</div>';
	ro.view += '</footer>';

	ro.view += '</div>';

	// pay module

	ro.payModuleUIHTML = function ( type, amount ) {

		var amountVal = "";

		if(amount) {
			amountVal = amount;
		}

		var ht = "";

		if(type=="bt") {
			type = "bt";
		}else{
			type = "dc";
		}

		ht += "<div class='pay-module "+type+"'>";
		
			if(type=="bt") {

				ht += "<div class='account_holder half-column'>Account Holder<br /><div class='pay-text'>"+LBGAlchemy.session.account.account_holder+"</div></span></div>";
				ht += "<div class='account_number half-column'>Account Number<br /><div class='pay-text'>"+LBGAlchemy.session.account.account_number+"</div></span></div>";
				ht += "<div class='account_sort_code half-column'>Sort Code<br /><div class='pay-text'>"+LBGAlchemy.session.account.account_sort_code+"</div></span></div>";
				ht += "<div class='account_reference half-column'>Reference*<div class='pay-text'>"+LBGAlchemy.session.currentUser.phone+"</div></span></div>";
				ht += "<div class='pay-note'>*The reference is your mobile phone number, please make sure you specify it when transfering money to this account.</div>";
		
			}else{

				ht += "<div class='pay_amount'><div class='amount-title'>Enter an amount</div><div class='amount-field'><span class='txt'>&pound;</span> <input name='pay_amount_field' type='text' maxlength='5' value='"+amountVal+"' "+LBGAlchemy.func.inputOnlyNumbers()+" /></div></div>";
			
				ht += "<div class='card_name'>Card holder name<br /><input name='card_name_field' type='text' maxlength='30' /></div>";
				ht += "<div class='card_number'>Card number<br /><input name='card_number_field' type='text' maxlength='16' "+LBGAlchemy.func.inputOnlyNumbers()+" /></div>";
			
				ht += "<div class='card_expiry'>Expiry date<br /><input name='card_expiry_mm_field' type='text' maxlength='2' "+LBGAlchemy.func.inputOnlyNumbers()+" /><span class='txt'> / </span><input name='card_expiry_yy_field' type='text' maxlength='2' "+LBGAlchemy.func.inputOnlyNumbers()+" /> MM/YY</div>";
				ht += "<div class='card_csv'>CSV<br /><input name='card_csv_field' type='text' maxlength='4' "+LBGAlchemy.func.inputOnlyNumbers()+" /></div>";

			}

		ht += "</div>";

		return ht;

	}

	ro.checkAndSubmitLoginDetails  = function ( username, password ) {

		//

		if(LBGAlchemy.func.IsEmail(username.val()) || (Number(username.val())>0 && username.val().length>5)) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(username, "Please correct your email!");
			return false;
		}

		if(password.val()>999 && password.val()<10000) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(password, "Please correct the pin! (Must be 4 digits)");
			return false;
		}

		//

		if(!LBGAlchemy.func.loginUser(username.val(), password.val())) {

			LBGAlchemy.func.flashInputField(username, "Invalid username or passoword!");
			LBGAlchemy.func.flashInputField(password, "");

			return false;

		}

		//

	}

	ro.validateCardPaymentForm = function ( amount, card_name, card_number, expiry_mm, expiry_yy, card_csv ) {

		if(amount.val()>0) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(amount, "Please correct the amount!");
			return false;
		}
		
		if(card_name.val().length>3) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(card_name, "Please correct the name!");
			return false;
		}

		if(card_number.val().length==16) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(card_number, "Please correct the card number!");
			return false;
		}

		if(expiry_mm.val()>0 && expiry_mm.val()<13) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(expiry_mm, "Please correct the expiry date (month)!");
			return false;
		}

		if(expiry_yy.val()>12 && expiry_yy.val()<30) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(expiry_yy, "Please correct the expiry date (year)!");
			return false;
		}

		if(card_csv.val()>100 && card_csv.val()<10000) {
			// ok
		}else{
			LBGAlchemy.func.flashInputField(card_csv, "Please correct the CSV!");
			return false;
		}


		return true;

	}

	ro.subheaderMenuHTML = function () {
		
		var ht = "";

		//

		var spaceList = LBGAlchemy.func.getMemberSpaces();

		ht += "<div class='menu'>";

		for (var i = 0; i < spaceList.length; i++) {

			var itemSelectedClass = "";

			if(spaceList[i].id==LBGAlchemy.session.account.id) {
				itemSelectedClass = " selected ";
			}

			ht += "<div class='item"+itemSelectedClass+" space-menu-item'>";
				ht += "<a href='javascript:;' data='"+spaceList[i].id+"'>";
				ht += "<h2>" + spaceList[i].name + "</h2>";
				ht += "<div class='numofmembers'>" + spaceList[i].members + " members</div>";
				ht += "<div class='amount'>" + LBGAlchemy.func.formatPound(spaceList[i].amount) + "</div>";
				ht += "</a>";
			ht += "</div>";

		};

		ht += "</div>";

		//

		return ht;

	}

	ro.subheaderProfileHTML = function () {
		
		var ht = "";

		ht += "<div class='profile'>";

			ht += "<div class='item c1'>";

				ht += "<div class='title'>Your name</div><input name='input_name' type='text' maxlength='30' value='"+LBGAlchemy.session.currentUser.name+"' />";
				ht += "<div class='title'>Email</div><input name='input_email' type='text' maxlength='50' value='"+LBGAlchemy.session.currentUser.email+"' />";
				ht += "<div class='title'>Email (alternative)</div><input name='input_email2' type='text' maxlength='50' />";
				ht += "<div class='title'>Passcode</div><input name='input_pin' type='text' maxlength='4' "+LBGAlchemy.func.inputOnlyNumbers()+" />";
				ht += "<div class='title'>Mobile number</div><input name='input_mobile' type='text' maxlength='12' value='"+LBGAlchemy.session.currentUser.phone+"' "+LBGAlchemy.func.inputOnlyNumbers()+" />";

			ht += "</div>";

			ht += "<div class='item c2'>";

				ht += "<div class='title'>Bank Account Number</div><input name='input_bank_account_number' type='text' maxlength='10' value='"+LBGAlchemy.session.currentUser.account_number+"' "+LBGAlchemy.func.inputOnlyNumbers()+" />";
				ht += "<div class='title'>Bank Sort Code</div><input name='input_bank_sort_code' type='text' value='"+LBGAlchemy.session.currentUser.account_sort_code+"' maxlength='8'/>";

				ht += "<div class='title'>Card Name</div><input name='input_card_name' type='text' maxlength='16' />";
				ht += "<div class='title'>Card Number</div><input name='input_card_number' type='text' maxlength='16' "+LBGAlchemy.func.inputOnlyNumbers()+" />";
				ht += "<div class='title'>Card Expiry Date</div><input name='input_card_expiry_mm_field' type='text' maxlength='2' "+LBGAlchemy.func.inputOnlyNumbers()+" /><span class='txt'> / </span><input name='input_card_expiry_yy_field' type='text' maxlength='2' "+LBGAlchemy.func.inputOnlyNumbers()+" /> MM/YY";

			ht += "</div>";
			
			ht += "<div class='item c3'>";

				ht += "<div class='min-height'><div class='title'>Notifications</div>Would you like to receive notifications?</div>";	
				ht += "<div class='min-height'><div class='title'>Your Account</div><a href='javascript:;'>Remove your Shared Spaces account</a></div>";


			ht += "</div>";

			ht += "<div class='wrapper-btns'>";

				ht += '<a class="cancel-btn" href="javascript:;"><div class="button button-light cancel-payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-black-button-cancel.png" width="16" height="16" />Cancel</div></a>';
				ht += '<a class="update-btn" href="javascript:;"><div class="button button-green update-payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-tick.png" width="21" height="18" />Update your details</div></a>';

			ht += "</div>";


		ht += "</div>";

		return ht;

	}

	ro.headerInnerRightHTML = function () {

		var ht ="";
		if(LBGAlchemy.session.adminView) {
			if(LBGAlchemy.theme=="lbg") {
				ht += '<a href="https://ibglxysit4a-lp.lloydstsb.co.uk/personal/logon/login.jsp?testenv=https://ibglxysit4s-lp.lloydstsb.co.uk/personal/login" target="_blank"><div class="button button-green">Internet Banking</div></a>';
			}else{
				ht += '<a href="hjavascript:;"><div class="button button-green">Internet Banking</div></a>';
			}
		}
		
		ht += '<a class="logout-btn" href="javascript:;"><div class="button button-blue">Log out</div></a>';
		
		ht += '<a href="javascript:;" class="menu-btn"><div class="button button-dark icon icon-button"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-button-menu.png" width="16" height="16" /></div></a>';
		ht += '<a href="javascript:;" class="profile-btn"><div class="button button-dark icon-button"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-button-profile.png" width="16" height="16" /></div></a>';

		if(LBGAlchemy.session.adminView) {
			//ht += '<div class="admin-badge">Admin</div>';
		}

		ht += '<h3>'+LBGAlchemy.session.currentUser.name+'</h3>';

		//

		return ht;

	}

	//

	ro.init = function ( body, complete ) {

		// init module

		body.append(ro.view);

		ro.body = body;

		complete();

	}

	return ro;

};
