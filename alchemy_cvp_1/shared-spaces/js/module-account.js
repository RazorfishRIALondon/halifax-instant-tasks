LBGAlchemy.modules.accountModule = function () {

	console.log("LBGAlchemy.views.accountModule()");

	var ro = {};
	ro.title = "Account";
	ro.id = "account";
	ro.viewClass = "content-"+ro.id;

	ro.panelView = "";
	ro.mainView = "";
	ro.mastheadView = "";


	// masthead
	//ro.mastheadView += "<img src='img/masthead-photo.jpg' width='320' height='180' />";
	
	//if(LBGAlchemy.session.adminView) {
		//ro.mastheadView += '<input class="account-name" type="text" maxlength="25" value="'+LBGAlchemy.session.account.name+'">';
	//}else{
		ro.mastheadView += "<h2>"+LBGAlchemy.session.account.name+"</h2>";
	//}

	//ro.mastheadView += "<h3>Let's go for a trip!</h3>";

	// content
	ro.mainView += '<div class="'+ro.viewClass+'">';	

	ro.mainView += "<h3 class='left'>Overview</h3>";

	ro.mainView += "<div class='balance-wrapper'>";

		ro.mainView += "<div class='balance-left'>Target</div><div class='balance-right'>"+LBGAlchemy.func.formatPound(LBGAlchemy.func.getTargetAmount())+"</div>";
		ro.mainView += "<div class='balance-left'>Collected</div><div class='balance-right'>"+LBGAlchemy.func.formatPound(LBGAlchemy.func.getCollectedAmount())+"</div>";
		ro.mainView += "<div class='balance-left dark-grey-text'>Balance</div><div class='balance-right dark-grey-text'>"+LBGAlchemy.func.formatPound(LBGAlchemy.func.getBalanceAmount())+"</div>";

	ro.mainView += "</div>";

	ro.mainView += '<hr/>';

	ro.mainView += '<div class="overview-list">';

		ro.mainView += '<div class="line green-text"><div class="c1">Member</div><div class="c3">Paid in<div class="payed"></div></div><div class="c2">Requested</div></div>';

		ro.mainView += '<hr/>';

	//

	var m = [];

	m.push(LBGAlchemy.session.currentUser);

	for (var i = 0; i < LBGAlchemy.session.account.members.length; i++) {
		if(LBGAlchemy.session.currentUser.id!=LBGAlchemy.session.account.members[i].id) {
			m.push(LBGAlchemy.session.account.members[i]);
		}
	};

	var ev = true;
	var evc = "";
	var payedc = "";
	for (var i = 0; i < m.length; i++) {
		
		if(ev) {
			ev = false;
			evc = " light-grey-bg";
		}else{
			ev = true;
			evc = "";
		}

		var topayVal = m[i].amount;
		var topaidVal = LBGAlchemy.func.getBalanceByMember(m[i].id);

		if(topaidVal>=topayVal && topayVal>0) {
			payedc = " green-text";
		}else{
			payedc = "";
		}

		ro.mainView += '<div class="line'+evc+'" user="'+m[i].id+'">';    
		ro.mainView += '<div class="c1">';

		if(LBGAlchemy.session.adminView && m[i].id!=LBGAlchemy.session.currentUser.id) {
			ro.mainView += '<div class="remove"><a href="javascript:LBGAlchemy.func.removeMember(\''+m[i].id+'\');"><img width="15" height="15" src="'+LBGAlchemy.url+'img/icon-circle-remove.png" /></a></div>';
		}

		ro.mainView += m[i].name;

		if(LBGAlchemy.session.account.admin==m[i].id) {
			ro.mainView += '<div class="admin-badge">Admin</div>';
		}




		ro.mainView += '</div>';
		
		ro.mainView += '<div class="c3'+payedc+'">'+LBGAlchemy.func.formatPound(topaidVal);
		ro.mainView += '<div class="payed">';
		if(payedc.length>0) {
			ro.mainView += '<img src="'+LBGAlchemy.url+'img/icon-payed-tick.png" />';
		}
		ro.mainView += '</div>';
		ro.mainView += '</div>';

		ro.mainView += '<div class="c2 light-grey-text">';

			if(LBGAlchemy.session.adminView) {
				ro.mainView += "<input class='request-payment-field' type='text' maxlength='7' value='"+LBGAlchemy.func.formatPound(topayVal)+"' "+LBGAlchemy.func.inputOnlyNumbers()+">";
			}else{
				ro.mainView += LBGAlchemy.func.formatPound(topayVal);
			}

		ro.mainView += '</div>';


		if(LBGAlchemy.session.currentUser.id==m[i].id) {

			ro.mainView += '<div class="paynow-promt">';
			ro.mainView += 'Pay in by';
			ro.mainView += '<a class="open-payin-bt-btn" href="javascript:;"><div class="button button-green payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-button-plus.png" width="16" height="16" />Bank Transfer</div></a>';
			ro.mainView += '<a class="open-payin-dc-btn" href="javascript:;"><div class="button button-green payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-button-plus.png" width="16" height="16" />Debit Card</div></a>';
			ro.mainView += '</div>';

			ro.mainView += '<div class="paynow-wrapper">';

				var toPayNow = topayVal-topaidVal;
				if(toPayNow<0) toPayNow = null; 

				// bank transfer
				ro.mainView += '<div class="bank-transfer">';
					
					ro.mainView += '<hr />';

					ro.mainView += LBGAlchemy.modules.frameworkModule().payModuleUIHTML("bt", toPayNow);
					
					ro.mainView += '<div class="paynow-wrapper-btns">';
					ro.mainView += '<a class="cancel-payin-btn" href="javascript:;"><div class="button button-light cancel-payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-black-button-cancel.png" width="16" height="16" />Cancel</div></a>';
					ro.mainView += '<a class="submit-email-details-btn" href="javascript:;" ><div class="button button-green half-button submit-payin">Email</div></a>';
					ro.mainView += '<a class="submit-print-details-btn" href="javascript:;" ><div class="button button-green half-button submit-payin">Print</div></a>';
					ro.mainView += '<a class="submit-sms-details-btn" href="javascript:;" ><div class="button button-green half-button submit-payin">SMS</div></a>';
					ro.mainView += '</div>';

				ro.mainView += '</div>';
				
				// debit card
				ro.mainView += '<div class="debit-card">';
	
					ro.mainView += '<hr />';

					ro.mainView += LBGAlchemy.modules.frameworkModule().payModuleUIHTML("dc", toPayNow);
					
					ro.mainView += '<div class="paynow-wrapper-btns">';
					ro.mainView += '<a class="cancel-payin-btn" href="javascript:;"><div class="button button-light cancel-payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-black-button-cancel.png" width="16" height="16" />Cancel</div></a>';
					ro.mainView += '<a class="submit-payin-btn" href="javascript:;"><div class="button button-blue submit-payin"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-tick.png" width="21" height="18" />Pay now</div></a>';
					ro.mainView += '</div>';
				ro.mainView += '</div>';

			ro.mainView += '</div>';

		}


		ro.mainView += '</div>';


	};

	ro.mainView += '</div>';



	//

	ro.mainView += '<hr/>';

	//

	if(LBGAlchemy.session.adminView) {

		ro.mainView += '<div class="invite-members">';

			ro.mainView += '<div class="add-field-button"><a href="javascript:;"><div class="text-button">Add another invitee <div class="icon"><img src="'+LBGAlchemy.url+'img/icon-plus.png" width="10" height="10" /></div></div></a></div>';

			ro.mainView += '<div class="new-members">';

				ro.mainView += "<div class='header'>";

				ro.mainView += "<div class='name'>Name</div>";
				ro.mainView += "<div class='amount'>Amount</div>";
				ro.mainView += "<div class='email'>Recipient email</div>";

				ro.mainView += "</div>";

				ro.mainView += '<div class="fields"></div>';

				//

				ro.mainView += '<div class="actions">';
				ro.mainView += '<div class="action-output"></div>';
				ro.mainView += '<a class="cancel-button-a" href="javascript:;"><div class="button button-light cancel-button"><img class="icon" src="'+LBGAlchemy.url+'img/icon-black-button-cancel.png" width="16" height="16" />Cancel</div></a>';
				ro.mainView += '<a href="javascript:;" class="send-button-a"><div class="button button-blue send-button"><img class="icon" src="'+LBGAlchemy.url+'img/icon-add-members.png" width="21" height="18" />Send</div></a>';
				ro.mainView += '</div>';
			
				//

			ro.mainView += '</div>';

		ro.mainView += '</div>';

		//

		ro.mainView += '<hr/>';

	}

	ro.mainView += '</div>';



	// panel
	ro.panelView += "<h3>Activity</h3>";


	ro.panelView += "<div class='activity-list'>";

	ro.panelView += "<ul>";

		var acl = LBGAlchemy.func.getAccountActivity();

		for (var i = 0; i < acl.length; i++) {

			var lefthtml = "";
			var righthtml = "";
			var labeltxt = "Paid in <span class='bright-green-text'>" + LBGAlchemy.func.formatPound(acl[i].amount) + "</span>";
			var inorout = "";

			var itemDate = acl[i].date.substr(0,10);
			var itemUser = LBGAlchemy.func.getMemberNameByUser(acl[i].user);

			if(!itemUser) {
				itemUser = acl[i].user.split("@")[0];
				itemUser = itemUser.split(".").join(" ");
			}


			if(acl[i].amount<0) {
				
				labeltxt = acl[i].label + " <span class='red-text'>" + LBGAlchemy.func.formatPound(acl[i].amount) + "</span>";
				inorout = "out"; 
				
				lefthtml = "<img src='"+LBGAlchemy.url+"img/activity-out-left.png' width='50' height='62' />";
				righthtml = "<img src='"+LBGAlchemy.url+"img/activity-out-right.png' width='18' height='62' />";

			}else if(acl[i].amount>0) {
				inorout = "in"; 

				lefthtml = "<img src='"+LBGAlchemy.url+"img/activity-in-left.png' width='18' height='62' />";
				righthtml = "<img src='"+LBGAlchemy.url+"img/activity-in-right.png' width='50' height='62' />";

			}else{
				inorout = "neutral"; 
				labeltxt = acl[i].label;
				lefthtml = "<img src='"+LBGAlchemy.url+"img/activity-in-left.png' width='18' height='62' />";
				righthtml = "<img src='"+LBGAlchemy.url+"img/activity-out-right.png' width='18' height='62' />";

			}

			ro.panelView += "<li class='"+inorout+"'>";
				
				ro.panelView += "<div class='left'>"+lefthtml+"</div>";
				ro.panelView += "<div class='right'>"+righthtml+"</div>";

				ro.panelView += "<div class='inner'>";
					ro.panelView += "<div class='date light-grey-text'>"+itemDate+"</div>";
					ro.panelView += "<div class='user light-grey-text'>"+itemUser+"</div>";
					if(LBGAlchemy.session.account.admin==acl[i].user) {
						ro.panelView += '<div class="admin-badge">Admin</div>';
					}
					ro.panelView += "<div class='label dark-grey-text'>"+labeltxt+"</div>";
				ro.panelView += "</div>";
			
			ro.panelView += "</li>";

		};

	ro.panelView += "</ul>";

	ro.panelView += "</div>";



	//

	ro.getNewMemberFieldHTML = function () {

		rhtml = "";

		rhtml += "<div class='new-column'>";

		rhtml += "<div class='name'><input name='fullname' type='text' maxlength='30' /></div>";
		rhtml += "<div class='email'><input name='email' type='text' maxlength='60' /></div>";
		rhtml += "<div class='amount'><span class='txt'>&pound;</span> <input name='amount' type='text' maxlength='7' "+LBGAlchemy.func.inputOnlyNumbers()+" /></div>";


		rhtml += "</div>";

		return rhtml;

	}


	ro.checkAndInviteMembers  = function ( fields ) {

		var inviteArray = [];

		for (var i = fields.length - 1; i >= 0; i--) {
			
			var cf = $(fields[i])
			var cfName = cf.find(".name input");
			var cfEmail = cf.find(".email input");
			var cfAmount = cf.find(".amount input");

			//

			if(cfName.val().length>2) {
				// ok
			}else{
				LBGAlchemy.func.flashInputField(cfName, "Please correct the name!");
				return null;
			}

			if(LBGAlchemy.func.IsEmail(cfEmail.val())) {
				// ok
			}else{
				LBGAlchemy.func.flashInputField(cfEmail, "Please correct the email!");
				return null;
			}

			if(cfAmount.val()>0) {
				// ok
			}else{
				LBGAlchemy.func.flashInputField(cfAmount, "Please correct the amount!");
				return null;
			}

			var userLink = LBGAlchemy.appURL + "?invite=" + cfEmail.val().toLowerCase();

			inviteArray.push({
				"name": cfName.val(),"email": cfEmail.val().toLowerCase(),"amount": Number(cfAmount.val()), "link": userLink
			});

		};

		//

		//console.log(inviteArray);

		//

		LBGAlchemy.func.inviteMembers(inviteArray);

		//

	}

	ro.payNow = function ( amount, card_name, card_number, expiry_mm, expiry_yy, card_csv ) {

		if(LBGAlchemy.modules.frameworkModule().validateCardPaymentForm(amount, card_name, card_number, expiry_mm, expiry_yy, card_csv)) {
			LBGAlchemy.func.submitPayment(amount.val(), card_name.val(), card_number.val(), expiry_mm.val(), expiry_yy.val(), card_csv.val());
		}

	}

	ro.addNewMemberField = function () {

		ro.newMembersFields.append(ro.getNewMemberFieldHTML());

		ro.newMembersFields.children().fadeIn();

		ro.newMembersContainer.fadeIn();

		LBGAlchemy.func.onStageResize();

	}

	ro.init = function ( body, complete ) {

		// init module

		ro.body = body;
		
		body.find(".alchemy .content-inner-main").append(ro.mainView);
		body.find(".alchemy .content-inner-panel").append(ro.panelView);
		body.find(".alchemy .masthead-inner").append(ro.mastheadView);

		console.log("LBGAlchemy.views.accountModule().init()");

		//

		var overViewList = body.find(".alchemy .content-inner-main").find("div.line");

		for (var i = 0; i < overViewList.length; i++) {
			//$(overViewList[i]).delay(((i*100))).fadeIn(200);
			$(overViewList[i]).show();
		};

		//

		var activityChildren = body.find(".alchemy .content-inner-panel").find(".activity-list li ");

		for (var i = 0; i < activityChildren.length; i++) {
			$(activityChildren[i]).delay((100+(i*100))).fadeIn(100);
			//$(activityChildren[i]).show();
		};

		// invites

		ro.newMembersContainer = ro.body.find(".alchemy .content-inner .new-members");
		ro.newMembersFields = ro.newMembersContainer.find(".fields");
		ro.errorOutputField = ro.body.find(".alchemy .content-inner .action-output");

		ro.addAnotherInviteeBtn = $(body.find(".alchemy .content-inner .add-field-button a"));
		ro.addAnotherInviteeBtn.click(function () {
			
			ro.errorOutputField.html("");

			ro.cancelPayin.click();
			ro.addNewMemberField();

		});

		ro.cancelInvites = $(body.find(".alchemy .content-inner .new-members .actions .cancel-button-a"));
		ro.cancelInvites.click(function () {
			ro.newMembersContainer.fadeOut(100,function() {
				ro.newMembersFields.html("");
			});
		});

		ro.sendInvites = $(body.find(".alchemy .content-inner .new-members .actions .send-button-a"));
		ro.sendInvites.click(function () {
			
			ro.checkAndInviteMembers( ro.newMembersFields.children() );

		});

		// pay in

		ro.payNowPromt = ro.body.find(".alchemy .content-inner .paynow-promt");
		ro.payNowWrapper = ro.body.find(".paynow-wrapper");

		ro.showDCPayin = $(ro.payNowPromt.find(".open-payin-dc-btn"));
		ro.showDCPayin.click(function () {
			
			ro.payNowWrapper.find(".card_csv input").val("");
			ro.cancelInvites.click();

			ro.payNowWrapper.find(".bank-transfer").hide();
			ro.payNowWrapper.find(".debit-card").show();
			ro.payNowWrapper.hide();
			ro.payNowWrapper.fadeIn(200);
			LBGAlchemy.func.onStageResize();

		});

		ro.showBTPayin = $(ro.payNowPromt.find(".open-payin-bt-btn"));
		ro.showBTPayin.click(function () {
			
			ro.cancelInvites.click();
			ro.payNowWrapper.find(".bank-transfer").show();
			ro.payNowWrapper.find(".debit-card").hide();
			ro.payNowWrapper.hide();
			ro.payNowWrapper.fadeIn(200);
			LBGAlchemy.func.onStageResize();

		});

		ro.emailBTDetails = $(ro.payNowWrapper.find(".bank-transfer .submit-email-details-btn"));
		ro.emailBTDetails.click(function () {
			console.log("email");
		});

		ro.smsBTDetails = $(ro.payNowWrapper.find(".bank-transfer .submit-sms-details-btn"));
		ro.smsBTDetails.click(function () {
			console.log("sms");
		});

		ro.printBTDetails = $(ro.payNowWrapper.find(".bank-transfer .submit-print-details-btn"));
		ro.printBTDetails.click(function () {
			console.log("print");
		});

		ro.submitPayin = $(ro.payNowWrapper.find(".debit-card .submit-payin-btn"));
		ro.submitPayin.click(function () {
			
			ro.payNow( ro.payNowWrapper.find(".pay_amount input"),
				ro.payNowWrapper.find(".card_name input"),
				ro.payNowWrapper.find(".card_number input"),
				ro.payNowWrapper.find('.card_expiry input[name="card_expiry_mm_field"]'),
				ro.payNowWrapper.find('.card_expiry input[name="card_expiry_yy_field"]'),
				ro.payNowWrapper.find(".card_csv input")
			);

		});

		ro.cancelPayin = $(ro.payNowWrapper.find(".cancel-payin-btn"));
		ro.cancelPayin.click(function () {
			ro.payNowWrapper.fadeOut(100,function() {
				LBGAlchemy.func.onStageResize();
			});
		});

		ro.requestPaymentField = $(ro.body.find(".alchemy .content-inner .request-payment-field"));
		ro.requestPaymentField.focus(function () {
			
			$(this).attr("data",$(this).val());
			$(this).val("");

		});
		ro.requestPaymentField.focusout(function () {

			var num = Number($(this).val());
			
			if($(this).val()=="") {
				$(this).val($(this).attr("data"));

			}else if(num>=0) {
				LBGAlchemy.func.changeRequestedAmount(num, $(this).parent().parent().attr("user"));

			}else{
				$(this).val($(this).attr("data"));
			}

		});

		//


		//

		complete();

	};


	return ro;

};
