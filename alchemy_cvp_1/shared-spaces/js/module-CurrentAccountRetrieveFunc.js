LBGAlchemy.modules.CurrentAccountRetrieveFunc = function() {


	var p = {};
	this._sortCode 		= -1;
	this._accountNumber = -1;
	this._overdraft 	= 'null';
	this._balance 	 	= 'null';
	
	p.defaultTitleLink = 'https://ibglxysit4s-lp.lloydstsb.co.uk/personal/a/viewaccount/accountoverviewpersonalbase.jsp?NOMINATED_ACCOUNT=OWEGXWFPRK2YXBRGFC5XKV2AJKNWQAOID57GTKYEDN75P6TZOSMQ&REFERRING_LOCATION=accountOverview&NON_MIGRATED_FLAG=false&lnkcmd=frm1%3AlstAccLst%3AlkImageRetail1&al=';

	p.Init = function() {
		console.log("LBGAlchemy.modules.CurrentAccountRetrieveFunc Initialised");
		this.ExtractHTMLData();
	}

	p.Exit = function() {

	}

	p.ExtractHTMLData = function() {

		//account details
		var oU 			= this._getAccountByName(':lstAccLst .accountDetails h2 a', 'classic');
		var tTText 		= $(oU.find('.numbers.wider')[0]).text();
		var bBText 		= $(oU.parent().find('.accountBalance')[0]).text();
		var tT 			= tTText.replace(/\s+/g, "");
		var bB 			= bBText.replace(/\s+/g, "");
		var sPos 		= tT.indexOf("SortCode");
		var aPos 		= tT.indexOf("AccountNumber");
		var sortCode 	= tT.slice((sPos+8), (sPos+16));
		var accNum 		= tT.slice((aPos+13), (aPos+21));
		//balance
		var bPos 		= bB.indexOf("Balance");
		var oPos 		= bB.indexOf("Overdraftlimit:");
		var apPos 		= bB.indexOf("Apply");
		var balance 	= bB.slice((bPos+7), oPos);
		var overdraft 	= bB.slice((oPos+15), apPos);
		this._sortCode 	= sortCode;
		this._accountNumber = accNum;
		this._balance 	= balance;
		this._overdraft = overdraft;
	}

	p.createSharedAccount = function (classicAccount, options) {

		console.log("LBGAlchemy.modules.CurrentAccountRetrieveFunc createSharedAccount");

		var defaultOption = {

			ssName: 					"Shared Account",
			ssTitleImgSrc: 				LBGAlchemy.url+"img/shared-icon-"+LBGAlchemy.theme+".png",
			ssTitleLink: 				p.defaultTitleLink,
			ssUrlVars: 					{},
			ssTitleLinkTarget: 			'_self',
			ssAccountNumber: 			getRandomizer(10000000,99999999),
			ssSortCode: 				LBGAlchemy.currentData.getSortCode(),
			ssFunds: 					0,
			ssOverdraft: 				0
		};

		var op = (options)?this._optionChecker(options, defaultOption):defaultOption;
		console.log('op', op);
		// var classicAccount = null;
		var sharedAccount = null;
		// var ssAccountNumber = getRandomizer(10000000,99999999);
		// var ssSortCode = LBGAlchemy.currentData.getSortCode(); //getRandomizer(10,99) + "-"+getRandomizer(10,99)+"-"+getRandomizer(10,99);
		// var ssFunds = 0;
		// var ssOverdraft = 0;

		if(classicAccount) {

			sharedAccount = $(classicAccount).clone();
			sharedAccount.attr('id', op.ssAccountNumber);
			sharedAccount.css("opacity","0");
			sharedAccount.css("display","none");
			sharedAccount.css("overflow-y","hide");
			
			sharedAccount.find(".balanceShowMeAnchor").html(formatPound(op.ssFunds));
			
			console.log(sharedAccount.find(".linkList .miniStatement li"))

			//change title
			sharedAccount.find("h2 img").attr("src",LBGAlchemy.url+"img/shared-icon-"+LBGAlchemy.theme+".png");
			var ts = sharedAccount.find("h2 a");
			ts[0].innerHTML = '<img id="frm1:lstAccLst:0:lkImageRetail1Img" src="'+ op.ssTitleImgSrc +'" alt="Classic">' + op.ssName;
			console.log(ts);
			// ts[0].setAttribute('href', op.ssTitleLink + '&fromSharedSpace=true');
			// ts[0].setAttribute('target', op.ssTitleLinkTarget);
			
			if(LBGAlchemy.theme=="lbg") {
				ts[0].setAttribute('href', '#');
			}else{
				ts[0].setAttribute('href', 'javascript:;');
			}
			
			localStorage.accountTitle = op.ssName;
			if(LBGAlchemy.theme=="lbg") ts[0].setAttribute('onclick', 'LBGAlchemy.func.openAccountDetail();');


			// change bank account
			var numbersWider = '<span class="postit">Sort Code</span>'+op.ssSortCode+'<span class="sortCode"></span>, <span class="postit">Account Number </span>'+op.ssAccountNumber;
			sharedAccount.find("p.numbers.wider").html(numbersWider);


			// var mn = sharedAccount.find(".linkList.miniStatement");
			// var tDiv = document.createElement('div');
			// tDiv.setAttribute('class', 'miniStatement');
			// // tDiv.innerHtml= '<table id="tblTransactionListViewError" summary="Table showing a summary of your account 86883468"><thead><tr><th id="clmnDateViewError" class="first">Date</th><th id="clmnDescriptionViewError">Description</th><th id="clmnInView" class="numeric">In (£)</th><th id="clmnOutViewError" class="numeric">Out (£)</th></tr></thead><tbody><tr><td class="errorMsg" colspan="4">Sorry, but we can\'t show you some recent transactions as your name wasn\'t on the account when these transactions were processed.</td></tr></tbody></table><form id="miniaccountstatements" name="miniaccountstatements" method="post" action="/personal/ajax/fragment/miniaccountstatement.jsp" class="validationName:(miniaccountstatements) validate:()" autocomplete="off" enctype="application/x-www-form-urlencoded"><fieldset><ul class="linkList clearfix"><li class="view"><a id="miniaccountstatements:lkViewFullStatement" name="miniaccountstatements:lkViewFullStatement" href="/personal/link/lp27_00_viewStmt?NOMINATED_ACCOUNT=OWEGXWFPRK2YXBRGFC5XKV2AJKNWQAOID57GTKYEDN75P6TZOSMQ&amp;StatementFlag=false" title="View latest statement">View latest statement</a></li></ul></fieldset><input type="hidden" name="miniaccountstatements" value="miniaccountstatements"><input type="hidden" name="submitToken" value="5312795"></form>';
			// var mnp = mn.parent();
			// $(mnp[0]).find('.miniStatementSlide')[0].appendChild(tDiv);
			// console.log($(mnp[0]).find('.miniStatementSlide')[0].getElementByClassName('miniStatement'))

			//hide mini statement, standing order
			var mnl = sharedAccount.find(".linkList.miniStatement li");
			$(mnl[0]).find('strong')[0].innerHTML = 'Close Shared Spaces';
			var ttmp = $(mnl[0]).find('strong').parent();
			ttmp.attr('href', '#');
			
			if(LBGAlchemy.theme=="lbg") ttmp[0].setAttribute('onclick', 'LBGAlchemy.func.closeSharedSpace('+ op.ssAccountNumber +');');
			
			$(mnl[1]).hide();
			
			// remove misc number
			sharedAccount.find("p.numbers strong").html("");

			// add ss button
			var ssBtn = '<a class="alchemy-blue-button" style="margin-top: 0px;" href="#" alt="Shared Spaces" onclick="LBGAlchemy.func.openSharedSpaces();">Shared Spaces</a><br/>';
			if(LBGAlchemy.theme=="alchemy") ssBtn = '<a class="alchemy-button" style="margin-top: 0px;" href="#" alt="Shared Spaces" onclick="LBGAlchemy.func.openSharedSpaces();">Shared Spaces</a><br/>';

			var ssBtn2 	= '<a class="alchemy-blue-button" style="margin-top: 5px;" href="#" alt="Request Debit Card" onclick="LBGAlchemy.func.openRequestDebitCard();">Request Debit Card</a>';

			// remove upgrade button
			sharedAccount.find(".accountMarketingWrapper").html(ssBtn+ssBtn2);

			// remove overdraft
			sharedAccount.find("p.accountMsg").html("&nbsp;");

			// change account name
			sharedAccount.html(sharedAccount.html().replace(/Classic/g,'Shared Account'));

			//sharedAccount.($(classicAccount).parent());
			sharedAccount.insertAfter(classicAccount);
			
			sharedAccount.hide();

			sharedAccount.height(0);
			sharedAccount.animate({
			    height: 130,
			    opacity: 1,
			}, 500 );

			//sharedAccount.fadeIn(2000);

			//setting the local storage
			localStorage.ssAccountNumber = op.ssAccountNumber;
			localStorage.ssBalance 		 = formatPound(op.ssFunds);
		}

	}


	p.getSortCode = function() {
		return this._sortCode;
	}

	p.getAccountNo = function() {
		return this._accountNumber;
	}

	p.getBalance = function() {
		return this._convertMoneyToNumber(this._balance);
	}

	p.getOverdraft = function() {
		return this._convertMoneyToNumber(this._overdraft);
	}

	p._convertMoneyToNumber = function(nN) {

		var mo = Number(nN.replace(/[^0-9\.]+/g,""));
		return mo;
	}

	p._convertNumberToMoney = function(nN) {
		var nS = nN.split("");
		var nL = nN.toString().length;
		var out = '00.';
		for(var i=0; i<nL; i++) {
			if ((i%3)==0 && i!=0) {
				out+=',';
			}
			out+=nS.pop();
		}
		return(out.split("").reverse().join(""));
	}

	p._getAccountByName = function(aC, name) {

		var mM = $(aC).filter(function(){
			return $(this).text().toLowerCase() == name;
		});
		return mM.parent().parent();
	}

	p._optionChecker = function(op, checkOp) {
		out = {};
		for(var i in checkOp) {

			out[i] = checkOp[i];
			for(var j in op) {
				if (i===j) {
					out[i] = op[j];
					break;
				}
			}
		}
		return out;
	}


	p.Init();
	return p;
};