LBGAlchemy.modules.homeModule = function () {

	console.log("LBGAlchemy.views.homeModule()");

	var ro = {};
	ro.title = "Home";
	ro.id = "home";
	ro.viewClass = "content-"+ro.id;

	ro.panelView = "";
	ro.mainView = "";
	ro.mastheadView = "";



	//
	ro.mastheadView += "<h2>Shared Spaces</h2>";


	// content
	ro.mainView += '<div class="'+ro.viewClass+'">';	


	ro.mainView += "<h3 class='left'>Sign In</h3>";

	ro.mainView += '<div class="small-form-wrapper">';	

		ro.mainView += "<p>Please fill in your login details.</p>";
		ro.mainView += "<div class='username'><div class='field-title'>Your Mobile Phone</div><input name='username_field' type='text' maxlength='12' value='' "+LBGAlchemy.func.inputOnlyNumbers()+" /></div>";
		ro.mainView += "<div class='password'><div class='field-title'>Your Passcode</div><input type='password' name='password_field' "+LBGAlchemy.func.inputOnlyNumbers()+" type='text' value='' maxlength='4' /></div>";
		ro.mainView += '<a class="submit-btn" href="javascript:;"><div class="button button-green submit-button"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-tick.png" width="21" height="18" />Sign In</div></a><br/><br/><span class="example"><a href="javascript:;">Lost your phone?</a><br /><a href="javascript:;">Forgot your passcode?</a></span>';
		ro.mainView += '<hr />';

	ro.mainView += '</div>';	


	ro.mainView += '</div>';

	//



	// panel
	ro.panelView += "<h3>Your own Shared Space?</h3>";


	ro.panelView += "<div class='how-to'>";
	ro.panelView += "<p>If you would like to create your own Shared Space you must be LBG customer. More information to come shortly.</p>";
	ro.panelView += "</div>";

	ro.panelView = "";


	ro.init = function ( body, complete ) {

		// init module

		ro.body = body;

	
		body.find(".alchemy .content-inner-main").append(ro.mainView);
		body.find(".alchemy .content-inner-panel").append(ro.panelView);
		body.find(".alchemy .masthead-inner").append(ro.mastheadView);

		console.log("LBGAlchemy.views.homeModule().init()");
		//

		ro.formWrapper = ro.body.find(".alchemy .content-inner .small-form-wrapper");

		ro.submitLogin = $(ro.formWrapper.find(".submit-btn"));
		ro.submitLogin.click(function () {
			LBGAlchemy.modules.frameworkModule().checkAndSubmitLoginDetails(ro.formWrapper.find(".username input"),ro.formWrapper.find(".password input"));
		});


		//

		complete();

	};


	return ro;

};
