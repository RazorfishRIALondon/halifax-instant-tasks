LBGAlchemy.modules.signupModule = function () {

	console.log("LBGAlchemy.views.signupModule()");

	var ro = {};
	ro.title = "Signup";
	ro.id = "signup";
	ro.viewClass = "content-"+ro.id;

	ro.panelView = "";
	ro.mainView = "";
	ro.mastheadView = "";

	ro.currentSignUpStep = 0;



	//
	ro.mastheadView += "<h2>"+LBGAlchemy.session.account.name+"</h2>";




	// content
	ro.mainView += '<div class="'+ro.viewClass+'">';	

		ro.mainView += "<h3 class='left'>Welcome "+LBGAlchemy.session.currentUser.name.split(" ")[0]+"!</h3>";
		
		var proceedButton = '<a class="submit-btn" href="javascript:;"><div class="button button-green submit-button">Continue</div></a>';
		var backButton = '<a class="back-btn" href="javascript:;"><div class="button button-light back-button">Back</div></a>';
		var stepPrefix = '<div class="signup-step">';
		stepPrefix += '<h4 class="steppos"></h4>';
		var stepPostfix = '<br /><br /><br /></div>';

		ro.mainView += '<div class="small-form-wrapper">';	

		ro.mainView += '</div>';	

		ro.mainView += '<hr />';

	ro.mainView += '</div>';



	// panel
	ro.panelView += "<h3>Already a member?</h3>";
	ro.panelView += '<div class="small-form-wrapper">';	

		ro.panelView += "<p>Please fill in your login details.</p>";
		ro.panelView += "<div class='username'><div class='field-title'>Your Mobile Phone</div><input name='username_field' type='text' maxlength='12' value='' "+LBGAlchemy.func.inputOnlyNumbers()+" /></div>";
		ro.panelView += "<div class='password'><div class='field-title'>Your Passcode</div><input type='password' name='password_field' "+LBGAlchemy.func.inputOnlyNumbers()+" type='text' value='' maxlength='4' /></div>";
		ro.panelView += '<a class="submit-btn" href="javascript:;"><div class="button button-green submit-button"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-tick.png" width="21" height="18" />Sign In</div></a><br/><br/><span class="example"><a href="javascript:;">Lost your phone?</a><br /><a href="javascript:;">Forgot your passcode?</a></span>';

	ro.panelView += '</div>';	



	// sign up steps

	ro.signupSteps = [];
	var newStep = {};
	var stepHTML = "";

	// step 1
	stepHTML = stepPrefix;	
	stepHTML += "<p>Please enter your phone number so we can identify who you are.</p>";
	stepHTML += "<div class='mobile-number'><div class='field-title'>Mobile number</div><input name='mobile_field' type='text' maxlength='12' value='' "+LBGAlchemy.func.inputOnlyNumbers()+" /> <span class='example light-grey-text'>e.g.: 07540666853</span></div>";
	stepHTML += proceedButton;
	stepHTML += stepPostfix;
	newStep = {
		"title": "Welcome "+LBGAlchemy.session.currentUser.name.split(" ")[0]+"!",
		"miniTitle": "Welcome",
		"html": stepHTML,
	};
	ro.signupSteps.push(newStep);

	// step 2
	stepHTML = stepPrefix;	
	stepHTML += "<p>Please enter the Passcode we sent to your mobile.</p>";
	stepHTML += "<div class='pin'><div class='field-title'>Passcode</div><input name='pin_field' type='text' maxlength='4' value='' "+LBGAlchemy.func.inputOnlyNumbers()+" /> <span class='example light-grey-text'>e.g.: 1234</span></div>";
	stepHTML += backButton;
	stepHTML += proceedButton;
	stepHTML += stepPostfix;
	newStep = {
		"title": "Verification",
		"miniTitle": "Verification",
		"html": stepHTML,
	};
	ro.signupSteps.push(newStep);

	// step 3
	stepHTML = stepPrefix;	
	stepHTML += "<div class='security-notify checkbox'><input type='checkbox' name='security-notify' value='1'> <div>We can notify you when your account is accessed from a computer or mobile device that you haven't used before.</div></div>";
	stepHTML += "<div class='security-add-device checkbox'><input type='checkbox' name='security-add-device' value='1'> <div>If yes would you like to save this device?</div></div>";
	stepHTML += proceedButton;
	stepHTML += stepPostfix;
	newStep = {
		"title": "Security notifications",
		"miniTitle": "Notifications",
		"html": stepHTML,
	};
	ro.signupSteps.push(newStep);


	// step 4
	stepHTML = stepPrefix;	
	stepHTML += "<p>Please enter your card details so that you can pay now or simply pay later.</p>";
	stepHTML += LBGAlchemy.modules.frameworkModule().payModuleUIHTML(LBGAlchemy.session.currentUser.amount);
	stepHTML += '<a class="submit-paynow-btn" href="javascript:;" target="_blank"><div class="button button-blue submit-paynow"><img class="icon" src="'+LBGAlchemy.url+'img/icon-white-tick.png" width="21" height="18" />Pay now</div></a>';
	stepHTML += '<br /><a class="submit-paylater-btn" href="javascript:;"><div class="button button-green submit-paylater">Continue &amp; Pay later</div></a>';
	stepHTML += "<br /><br />" + backButton;
	stepHTML += stepPostfix;
	newStep = {
		"title": "Payment",
		"miniTitle": "Payment",
		"html": stepHTML,
	};
	//ro.signupSteps.push(newStep);

	// step 5
	stepHTML = stepPrefix;	
	stepHTML += "<p>To log in next time use your phone number and the Passcode sent to you via SMS.</p>";
	stepHTML += "<div class='your-mobile-number'><div class='field-title'>Mobile number</div><h4></h4></div>";
	stepHTML += "<div class='your-pin'><div class='field-title'>The Passcode sent to you</div><h4></h4></div>";
	stepHTML += proceedButton;
	stepHTML += stepPostfix;
	newStep = {
		"title": "You've sucessfully signed up",
		"miniTitle": "Congratulations",
		"html": stepHTML,
	};
	ro.signupSteps.push(newStep);

	//

	ro.showSignupStep  = function ( num ) {
		
		if(num<0) num = 0;
		if(num>(ro.signupSteps.length-1)) {
			num = (ro.signupSteps.length-1);
			LBGAlchemy.func.registerCurrentUser(LBGAlchemy.session.currentUser.id,ro.currentPIN.val());
			return false;
		}

		if(num==1) {
			
			if(ro.currentMobileNumber.val()>0 && ro.currentMobileNumber.val().length>8) {
				
				var newMobileNum = ro.currentMobileNumber.val();
				var newPIN = LBGAlchemy.func.getRandomizer(1000,9999);

				ro.sentPIN = newPIN;
				LBGAlchemy.session.currentUser.phone = newMobileNum;
				LBGAlchemy.func.smsMember( newMobileNum, "Your Shared Spaces Passcode code is " + newPIN );

			}else{
				
				num = 0;
				LBGAlchemy.func.flashInputField(ro.currentMobileNumber, "Please correct the mobile phone number!");

				return false;

			}

		}else if(num==2) {

			if(ro.currentPIN.val()>0 && ro.currentPIN.val().length==4 && ro.currentPIN.val()==ro.sentPIN) {
				LBGAlchemy.session.currentUser.password = ro.currentPIN.val();
			}else{
				LBGAlchemy.func.flashInputField(ro.currentPIN, "Please correct the Passcode!");
				return false;
			}

		}else if(num==3) {

			ro.yourMobileNumberField.html(LBGAlchemy.session.currentUser.phone);
			ro.yourPINField.html(LBGAlchemy.session.currentUser.password);

		}

		for (var i = 0; i < ro.stepsView.children().length; i++) {
			if(i==num) {
				
				$(ro.stepsView.children()[i]).fadeIn();

				// swap the title
				$(ro.stepsTitle).hide();
				$(ro.stepsTitle).html(ro.signupSteps[i].title);
				$(ro.stepsTitle).fadeIn();

				// change step pos
				$(ro.stepsView.children()[i]).find(".steppos").html("<span class='green-text'>Step " + (i+1) + "</span><span class='light-grey-text'> / " + ro.signupSteps.length + "</span>");

			}else{
				$(ro.stepsView.children()[i]).hide();
			}
		};

		ro.currentSignUpStep = num;

		if(num==0) {
			ro.body.find(".alchemy .content-inner-panel").children().fadeIn();
		}else{
			ro.body.find(".alchemy .content-inner-panel").children().fadeOut();
		}

 		$('html, body').animate({scrollTop:0}, 'fast');

	}

	ro.prevSignupStep = function () {
		ro.showSignupStep(ro.currentSignUpStep-1);
	}

	ro.nextSignupStep = function () {
		ro.showSignupStep(ro.currentSignUpStep+1);
	}

	ro.init = function ( body, complete ) {

		// init module

		ro.body = body;
		
		body.find(".alchemy .content-inner-main").append(ro.mainView);
		body.find(".alchemy .content-inner-panel").append(ro.panelView);
		body.find(".alchemy .masthead-inner").append(ro.mastheadView);

		console.log("LBGAlchemy.views.signupModule().init()");

		//

		ro.sentPIN = 0;

		//

		ro.stepsView = body.find(".alchemy .content-inner-main .small-form-wrapper");

		for (var i = 0; i < ro.signupSteps.length; i++) {
			ro.stepsView.append(ro.signupSteps[i].html);
		};

		ro.stepsTitle = body.find(".alchemy .content-inner-main ."+ro.viewClass + " h3");

		ro.currentMobileNumber = body.find(".alchemy .content-inner-main .small-form-wrapper .mobile-number input");
		ro.currentPIN = body.find(".alchemy .content-inner-main .small-form-wrapper .pin input");

		//

		ro.payNowWrapper = body.find(".alchemy .content-inner-main .small-form-wrapper .pay-module");

		ro.yourMobileNumberField = body.find(".alchemy .content-inner-main .small-form-wrapper .signup-step .your-mobile-number h4");
		ro.yourPINField = body.find(".alchemy .content-inner-main .small-form-wrapper .signup-step .your-pin h4");

		//

		ro.showSignupStep(0);

		//

		//

		ro.submitPayButton = $(ro.stepsView.find(".submit-paynow-btn"));
		ro.submitPayButton.click(function () {
			
			if(ro.currentSignUpStep==3) {
				
				if(!LBGAlchemy.modules.frameworkModule().validateCardPaymentForm(
					ro.payNowWrapper.find(".pay_amount input"),
					ro.payNowWrapper.find(".card_name input"),
					ro.payNowWrapper.find(".card_number input"),
					ro.payNowWrapper.find('.card_expiry input[name="card_expiry_mm_field"]'),
					ro.payNowWrapper.find('.card_expiry input[name="card_expiry_yy_field"]'),
					ro.payNowWrapper.find(".card_csv input"))
				) {
					return null;
				}else{

					// make transaction
					LBGAlchemy.func.addTransaction(ro.payNowWrapper.find(".pay_amount input").val(), LBGAlchemy.session.currentUser.id)
				
					// Issue notification to Admin
					LBGAlchemy.func.smsMember(LBGAlchemy.func.getAccountAdmin().phone, LBGAlchemy.session.account.name+" has new activity: "+ro.payNowWrapper.find(".card_name input").val()+", "+ro.payNowWrapper.find(".pay_amount input").val()+" pounds");
				}

			}

			ro.nextSignupStep();

		});

		ro.submitPayLaterButton = $(ro.stepsView.find(".submit-paylater-btn"));
		ro.submitPayLaterButton.click(function () {	
			
			ro.nextSignupStep();

		});

		ro.submitButton = $(ro.stepsView.find(".submit-btn"));
		ro.submitButton.click(function () {	
			
			ro.nextSignupStep();

		});

		ro.backButton = $(ro.stepsView.find(".back-btn"));
		ro.backButton.click(function () {
			
			ro.prevSignupStep();

		});


		// 

		ro.signInformWrapper = ro.body.find(".alchemy .content-inner .content-inner-panel .small-form-wrapper");

		ro.submitLogin = $(ro.signInformWrapper.find(".submit-btn"));
		ro.submitLogin.click(function () {
			
			LBGAlchemy.modules.frameworkModule().checkAndSubmitLoginDetails(ro.signInformWrapper.find(".username input"),ro.signInformWrapper.find(".password input"));
		
		});

		//

		complete();

	};


	return ro;

};
