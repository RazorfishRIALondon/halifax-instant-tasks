/*! LAB.js (LABjs :: Loading And Blocking JavaScript)
    v2.0.3 (c) Kyle Simpson
    MIT License
*/
(function(o){var K=o.$LAB,y="UseLocalXHR",z="AlwaysPreserveOrder",u="AllowDuplicates",A="CacheBust",B="BasePath",C=/^[^?#]*\//.exec(location.href)[0],D=/^\w+\:\/\/\/?[^\/]+/.exec(C)[0],i=document.head||document.getElementsByTagName("head"),L=(o.opera&&Object.prototype.toString.call(o.opera)=="[object Opera]")||("MozAppearance"in document.documentElement.style),q=document.createElement("script"),E=typeof q.preload=="boolean",r=E||(q.readyState&&q.readyState=="uninitialized"),F=!r&&q.async===true,M=!r&&!F&&!L;function G(a){return Object.prototype.toString.call(a)=="[object Function]"}function H(a){return Object.prototype.toString.call(a)=="[object Array]"}function N(a,c){var b=/^\w+\:\/\//;if(/^\/\/\/?/.test(a)){a=location.protocol+a}else if(!b.test(a)&&a.charAt(0)!="/"){a=(c||"")+a}return b.test(a)?a:((a.charAt(0)=="/"?D:C)+a)}function s(a,c){for(var b in a){if(a.hasOwnProperty(b)){c[b]=a[b]}}return c}function O(a){var c=false;for(var b=0;b<a.scripts.length;b++){if(a.scripts[b].ready&&a.scripts[b].exec_trigger){c=true;a.scripts[b].exec_trigger();a.scripts[b].exec_trigger=null}}return c}function t(a,c,b,d){a.onload=a.onreadystatechange=function(){if((a.readyState&&a.readyState!="complete"&&a.readyState!="loaded")||c[b])return;a.onload=a.onreadystatechange=null;d()}}function I(a){a.ready=a.finished=true;for(var c=0;c<a.finished_listeners.length;c++){a.finished_listeners[c]()}a.ready_listeners=[];a.finished_listeners=[]}function P(d,f,e,g,h){setTimeout(function(){var a,c=f.real_src,b;if("item"in i){if(!i[0]){setTimeout(arguments.callee,25);return}i=i[0]}a=document.createElement("script");if(f.type)a.type=f.type;if(f.charset)a.charset=f.charset;if(h){if(r){e.elem=a;if(E){a.preload=true;a.onpreload=g}else{a.onreadystatechange=function(){if(a.readyState=="loaded")g()}}a.src=c}else if(h&&c.indexOf(D)==0&&d[y]){b=new XMLHttpRequest();b.onreadystatechange=function(){if(b.readyState==4){b.onreadystatechange=function(){};e.text=b.responseText+"\n//@ sourceURL="+c;g()}};b.open("GET",c);b.send()}else{a.type="text/cache-script";t(a,e,"ready",function(){i.removeChild(a);g()});a.src=c;i.insertBefore(a,i.firstChild)}}else if(F){a.async=false;t(a,e,"finished",g);a.src=c;i.insertBefore(a,i.firstChild)}else{t(a,e,"finished",g);a.src=c;i.insertBefore(a,i.firstChild)}},0)}function J(){var l={},Q=r||M,n=[],p={},m;l[y]=true;l[z]=false;l[u]=false;l[A]=false;l[B]="";function R(a,c,b){var d;function f(){if(d!=null){d=null;I(b)}}if(p[c.src].finished)return;if(!a[u])p[c.src].finished=true;d=b.elem||document.createElement("script");if(c.type)d.type=c.type;if(c.charset)d.charset=c.charset;t(d,b,"finished",f);if(b.elem){b.elem=null}else if(b.text){d.onload=d.onreadystatechange=null;d.text=b.text}else{d.src=c.real_src}i.insertBefore(d,i.firstChild);if(b.text){f()}}function S(c,b,d,f){var e,g,h=function(){b.ready_cb(b,function(){R(c,b,e)})},j=function(){b.finished_cb(b,d)};b.src=N(b.src,c[B]);b.real_src=b.src+(c[A]?((/\?.*$/.test(b.src)?"&_":"?_")+~~(Math.random()*1E9)+"="):"");if(!p[b.src])p[b.src]={items:[],finished:false};g=p[b.src].items;if(c[u]||g.length==0){e=g[g.length]={ready:false,finished:false,ready_listeners:[h],finished_listeners:[j]};P(c,b,e,((f)?function(){e.ready=true;for(var a=0;a<e.ready_listeners.length;a++){e.ready_listeners[a]()}e.ready_listeners=[]}:function(){I(e)}),f)}else{e=g[0];if(e.finished){j()}else{e.finished_listeners.push(j)}}}function v(){var e,g=s(l,{}),h=[],j=0,w=false,k;function T(a,c){a.ready=true;a.exec_trigger=c;x()}function U(a,c){a.ready=a.finished=true;a.exec_trigger=null;for(var b=0;b<c.scripts.length;b++){if(!c.scripts[b].finished)return}c.finished=true;x()}function x(){while(j<h.length){if(G(h[j])){try{h[j++]()}catch(err){}continue}else if(!h[j].finished){if(O(h[j]))continue;break}j++}if(j==h.length){w=false;k=false}}function V(){if(!k||!k.scripts){h.push(k={scripts:[],finished:true})}}e={script:function(){for(var f=0;f<arguments.length;f++){(function(a,c){var b;if(!H(a)){c=[a]}for(var d=0;d<c.length;d++){V();a=c[d];if(G(a))a=a();if(!a)continue;if(H(a)){b=[].slice.call(a);b.unshift(d,1);[].splice.apply(c,b);d--;continue}if(typeof a=="string")a={src:a};a=s(a,{ready:false,ready_cb:T,finished:false,finished_cb:U});k.finished=false;k.scripts.push(a);S(g,a,k,(Q&&w));w=true;if(g[z])e.wait()}})(arguments[f],arguments[f])}return e},wait:function(){if(arguments.length>0){for(var a=0;a<arguments.length;a++){h.push(arguments[a])}k=h[h.length-1]}else k=false;x();return e}};return{script:e.script,wait:e.wait,setOptions:function(a){s(a,g);return e}}}m={setGlobalDefaults:function(a){s(a,l);return m},setOptions:function(){return v().setOptions.apply(null,arguments)},script:function(){return v().script.apply(null,arguments)},wait:function(){return v().wait.apply(null,arguments)},queueScript:function(){n[n.length]={type:"script",args:[].slice.call(arguments)};return m},queueWait:function(){n[n.length]={type:"wait",args:[].slice.call(arguments)};return m},runQueue:function(){var a=m,c=n.length,b=c,d;for(;--b>=0;){d=n.shift();a=a[d.type].apply(null,d.args)}return a},noConflict:function(){o.$LAB=K;return m},sandbox:function(){return J()}};return m}o.$LAB=J();(function(a,c,b){if(document.readyState==null&&document[a]){document.readyState="loading";document[a](c,b=function(){document.removeEventListener(c,b,false);document.readyState="complete"},false)}})("addEventListener","DOMContentLoaded")})(this);

// ------------------------------------
// Variables
// ------------------------------------
var LBGAlchemy = {};
LBGAlchemy.isLoaded = false;
LBGAlchemy.func = {};
LBGAlchemy.settings = {};
LBGAlchemy.modules = {};
LBGAlchemy.view = {};
LBGAlchemy.session = {};
LBGAlchemy.defaultUrl = "/shared-spaces/"; ///razorfish-lbg.appspot.com/
LBGAlchemy.selfIbUrl = "//razorfish-lbg.appspot.com/shared-spaces/ib";
LBGAlchemy.ibUrl = "https://ibglxysit4s-lp.lloydstsb.co.uk";

//localDev setting
LBGAlchemy.localDev = {

	state: 	false,
	url: 	"http://alchemy-cvp1.lloydstsb.com:8890/shared-spaces/",
	defaultTitle: 'Shared Spaces'

};

//user list
LBGAlchemy.userList = {
	john: "?c=john.smith@abcdefg.com&empty=false&theme=",
	george: "?c=georg.fasching@lloydsbanking.com&empty=true&theme=",
	henry: "?c=henry.ho@razorfish.com&empty=true&theme=",
	kamil: "?c=kamil.gottwald@razorfish.com&empty=true&theme="
}

//sub url for different pages
LBGAlchemy.subUrl = {
	ibDefault: 			"/personal/a/account_overview_personal/",
	requestDebitCard: 	"/personal/a/savings/applyForSavingsProduct.jsp",
	accountDetails: 	"/personal/a/account_details/",
	renameAccount: 		"/personal/a/viewproductdetails/rename.jsp",
	resetAccount: 		"/personal/a/viewproductdetails/rename.jsp?link=reset",
	closeAccount: 		"/personal/a/viewproductdetails/rename.jsp?link=close",
	makeTransfer: 		"/personal/a/make_transfer/",
	makePayment: 		"/personal/a/make_payment/"
}

LBGAlchemy.copy = {};
LBGAlchemy.copy.text = {

	title: 'Register for a Shared Spaces Debit Card',
	tandc: 'Some info about charges',
	descriptionTitle: 'About the Shared Spaces Debit Card',
	accountDetailTitle: 'Shared Account',
	renameTitle: 'Rename Shared Space Account',
	accountDetailBalance: '£0.00',
	renameAccountinputTitle: 'Rename Shared Account',
	buttonLink: 'https://ibglxysit4s-lp.lloydstsb.co.uk/personal/a/account_overview_personal/',
	resetTitle: 'Reset Shared Space Account',
	resetAccountinputTitle: 'Reset Shared Account',
	closeTitle: 'Close Shared Space Account',
	closeAccountinputTitle: 'Close Shared Account',
	makeTransferTitle: 'Transfer money between Shared Spaces accounts',
	makePaymentTitle: 'Shared Account',
}


// Settings
LBGAlchemy.htmlBody = null;
LBGAlchemy.settings.bankAPI = {};
LBGAlchemy.settings.bankAPI.endPoint = "";
LBGAlchemy.view.classicAccountNode = null;

// ------------------------------------
// Common page initialisation
// ------------------------------------

LBGAlchemy.func.init = function () {
	
	if (LBGAlchemy.isLoaded) return;
	LBGAlchemy.isLoaded = true;
	LBGAlchemy.session.getvars = LBGAlchemy.func.getQueryParams(document.location.search);

	console.log("LBGAlchemy.func.init()");

	if(LBGAlchemy.session.getvars.theme) {
		LBGAlchemy.theme = LBGAlchemy.session.getvars.theme;
	}else{
		LBGAlchemy.func.replaceLBG();
		LBGAlchemy.theme = "alchemy";
	}

	if (LBGAlchemy.session.getvars.user) {
		console.log('haha session');
		switch(LBGAlchemy.session.getvars.user) {
			case 'john.smith':
				LBGAlchemy.userUrl = LBGAlchemy.userList.john;
				break;
			case 'george.fasching':
				LBGAlchemy.userUrl = LBGAlchemy.userList.george;
				break;
			case 'henry.ho':
				LBGAlchemy.userUrl = LBGAlchemy.userList.henry;
				break;
			case 'kamil.gottwald':
				LBGAlchemy.userUrl = LBGAlchemy.userList.kamil;
				break;
		}
	}
	else {
		LBGAlchemy.userUrl = LBGAlchemy.userList.john;
	}

	LBGAlchemy.htmlBody = $("body");
	LBGAlchemy.htmlBody.fadeTo(0, 0);
	LBGAlchemy.htmlBody.hide();
	LBGAlchemy.func.initBasics();

};

LBGAlchemy.func.initBasics = function () {
 
 	// URL for local development
 	// LBGAlchemy.url =  (!LBGAlchemy.localDev.state)? LBGAlchemy.defaultUrl:LBGAlchemy.localDev.url; 
 	// URL for remote deployment
 	LBGAlchemy.url =  LBGAlchemy.defaultUrl; 



   $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', LBGAlchemy.url+'css/ib.css') )
   $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', LBGAlchemy.url+'css/ib-'+LBGAlchemy.theme+'.css') )
   
   $LAB
   // .script(LBGAlchemy.url + "js/monkiki/polyfill/DomFunc.js")
   .script(LBGAlchemy.url + "js/monkiki/url/HttpMethodFunc.js")
   .script(LBGAlchemy.url + "js/canvasloader.js")
   .script(LBGAlchemy.url + "js/module-CurrentAccountRetrieveFunc.js")
   .script(LBGAlchemy.url + "js/commonfunc/ib-commonFunc.js")
   .wait(LBGAlchemy.func.initBasicsComplete);

};

LBGAlchemy.func.replaceLBG = function () {

	$("body").html($("body").html().replace(/Lloyds TSB/g,''));
	$("body").html($("body").html().replace(/Easy Saver/g,'Savings Account'));
	
	// replace account images
	$("body").html($("body").html().replace(/static-ib-assets\/accimg_classic_account-13-1373461523/g,'img/classic-account-alchemy'));
	$("body").html($("body").html().replace(/static-ib-assets\/accimg_platinum_mastercard-12-1372346646/g,'img/mc-account-alchemy'));
	$("body").html($("body").html().replace(/static-ib-assets\/accimg_savings-16-1372951971/g,'img/savings-account-alchemy'));

	// replace all links
 	$('body a').each(function(){
    	var x=this.href;
    	this.href="javascript:;";
  	});

 	$(".clearfix p#logo").html("<h1>Online Banking</h1>");
 	$(".clearfix p#logo").next("h1").html("");


 	$("#lnkCustomerLogoff").html("Log out");

	document.title = document.title.replace(/Lloyds TSB/g,'Online Banking');

}

LBGAlchemy.func.initBasicsComplete = function () {

	var loaderHTML = "<div class='main-loader'>";
	loaderHTML += "<div id='main-loader-logo'><img src='" + LBGAlchemy.url + "img/header-logo-blue-text.png' width='200' height='41' /></div>";
	//loaderHTML += "<h1>Shared Spaces</h1>";
	loaderHTML += "<div id='main-loader-spinner'></div>";
	loaderHTML += "</div>";


	// Init Loader
	//console.log(LBGAlchemy.htmlBody);

	// add main loader
	LBGAlchemy.htmlBody.append(loaderHTML);

	LBGAlchemy.view.loader = LBGAlchemy.htmlBody.find(".main-loader");
	LBGAlchemy.view.loaderSpinner = LBGAlchemy.view.loader.find("#main-loader-spinner");

	var cl = new CanvasLoader("main-loader-spinner");
	cl.setColor('#666666'); // default is '#000000'
	cl.setShape('spiral'); // default is 'oval'
	cl.setDiameter(40); // default is 40
	cl.setDensity(14); // default is 40
	cl.setSpeed(1); // default is 2
	cl.setFPS(30); // default is 24
	cl.show(); // Hidden by default

	LBGAlchemy.view.loaderSpinnerObj = cl;

	$(window).resize(function() {
		LBGAlchemy.func.onStageResize();
	});

	LBGAlchemy.func.onStageResize();

	//

	LBGAlchemy.view.loader.hide();

	//

	LBGAlchemy.func.initComplete();

};

//switch between pages
LBGAlchemy.func.initComplete = function () {

	var pageSwap = document.getsubLink(LBGAlchemy.ibUrl).replace(/#/, '').replace(/\//gi, '');

	var pSwapAccCheck = document.getsubLink(LBGAlchemy.ibUrl);
	var isFakeIb = pageSwap.slice(-2);

	if (!isFakeIb == 'ib') {
		switch(pageSwap) {
			case LBGAlchemy.subUrl.ibDefault.replace(/\//gi, ''):
				LBGAlchemy.func.ibFacade();
				break;
			case LBGAlchemy.subUrl.requestDebitCard.replace(/\//gi, ''):
				LBGAlchemy.func.requestDebitCardFacade();
				break;
			case LBGAlchemy.subUrl.renameAccount.replace(/\//gi, ''):
				LBGAlchemy.func.renameAccountFacade();
				break;
			case LBGAlchemy.subUrl.resetAccount.replace(/\//gi, ''):
				LBGAlchemy.func.resetAccountFacade();
				break;
			case LBGAlchemy.subUrl.closeAccount.replace(/\//gi, ''):
				LBGAlchemy.func.closeAccountFacade();
				break;
			default:
				break;
		}
	}
	else {
		LBGAlchemy.func.ibFacade();
	}

	var ccCheck  = pSwapAccCheck.substr(0,27).replace(/\//gi, '');
	var ccCheck2 = pSwapAccCheck.substr(0,30).replace(/\//gi, '');
	var ccCheck3 = pSwapAccCheck.substr(0,25).replace(/\//gi, '');
	var ccCheck4 = pSwapAccCheck.substr(0,24).replace(/\//gi, '');

	if (ccCheck == LBGAlchemy.subUrl.accountDetails.replace(/\//gi, '')) {
		LBGAlchemy.func.accountDetailsFacade();
	}
	else if (ccCheck3 == LBGAlchemy.subUrl.makeTransfer.replace(/\//gi, '')) {
		LBGAlchemy.func.makeTransferFacade();
	}
	else if (ccCheck4 == LBGAlchemy.subUrl.makePayment.replace(/\//gi, '')) {
		LBGAlchemy.func.makePaymentFacade();		
	}

	$(LBGAlchemy.htmlBody).fadeTo(2000, 1, function(){ $(this).show() });
};

// ------------------------------------
// page facade functions
// ------------------------------------
//default ib main page
LBGAlchemy.func.ibFacade = function () {

	//get all the current account data from HTML
	LBGAlchemy.currentData = LBGAlchemy.modules.CurrentAccountRetrieveFunc();

	// get the current account
	var accounts = LBGAlchemy.htmlBody.find("#frm1:lstAccLst li");
	if(accounts.length>0) {
		
		LBGAlchemy.view.classicAccountNode = accounts[0];

		// change the upgrade button
		var ssBtn 	= '<a class="alchemy-blue-button" href="'+$(LBGAlchemy.view.classicAccountNode).find(".accountMarketingWrapper a").attr("href")+'" alt="Upgrade Account">Upgrade account</a>';
		var classicAN = $(LBGAlchemy.view.classicAccountNode).find(".accountMarketingWrapper");
		classicAN.html(ssBtn);

		LBGAlchemy.func.removeSharedAccount();
		LBGAlchemy.func.createSharedAccount({ssName: LBGAlchemy.localDev.defaultTitle, ssTitleLink: LBGAlchemy.url+"?c=kamil.gottwald@razorfish.com&empty=false#account"});

	}

	// add create shared account button in right panel
	LBGAlchemy.func.addButton();
	localStorage.isSSAccountDetail = false;
	console.log("LBGAlchemy.func.initComplete()");

}

//debit card request page
LBGAlchemy.func.requestDebitCardFacade = function () {

	LBGAlchemy.copy.title = $('.primary .panel .panelWrap h1');
	LBGAlchemy.copy.title.text(LBGAlchemy.copy.text.title);

	LBGAlchemy.copy.linking = $('#frmSavingsApply a');
	$(LBGAlchemy.copy.linking[0]).text(LBGAlchemy.copy.text.tandc);
	$(LBGAlchemy.copy.linking[0]).click(function(event){ return false; });
	$(LBGAlchemy.copy.linking[1]).hide();
	$(LBGAlchemy.copy.linking[2]).click(function(event){ return false; });

	LBGAlchemy.copy.descriptionTitle = $('.secondary .panel .item.savings h2');
	LBGAlchemy.copy.descriptionTitle.text(LBGAlchemy.copy.text.descriptionTitle);

	$('.feature.clearfix').hide();

	LBGAlchemy.copy.descriptionBody = $('.secondary .panel .item.savings .part ul');
	LBGAlchemy.copy.descriptionBody.html('<li>Hello Secondary Body text</li>');

	$('#frmSavingsApply .primaryAction').html('<a class="alchemy-blue-button" href="'+LBGAlchemy.ibUrl+LBGAlchemy.subUrl.ibDefault+'" alt="Register" style="color: #ffffff;text-decoration:none;box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-webkit-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-moz-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-ms-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-o-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);text-shadow: 2px 0px 4px #2a2a94;">&nbsp; Register &nbsp;</a>');
}

//account detailed page
LBGAlchemy.func.accountDetailsFacade = function() {

	if (localStorage.isSSAccountDetail=='true') {
		//function for shared saver page
		var tmpout = (localStorage.accountTitle)?localStorage.accountTitle:LBGAlchemy.copy.text.accountDetailTitle;
		$('.accountDetails .splitString').text(tmpout);

		$('.accountDetails img')[0].setAttribute('src', LBGAlchemy.url+'img/shared-icon-'+LBGAlchemy.theme+'.png');

		var tmpBalance = (localStorage.ssBalance)?localStorage.ssBalance:LBGAlchemy.copy.text.accountDetailBalance;
		$('.balanceActionsWrapper .balance').text(localStorage.ssBalance);

		//hide stuff
		$('.linkList .right a').hide();
		$('#lkApplyOverDraft').parent().hide();
		$('.accountMsg').hide();

		//relink stuff
		// $('#lnkRenametestNotGia')[0].setAttribute('href', '#');
		// $('#lnkRenametestNotGia').unbind('click');
		// $('#lnkRenametestNotGia').click(function(){window.open(LBGAlchemy.url, '_blank');});

		//add reset and close account
		ttmp1 = $('#lnkRenametestNotGia').parent().clone().find('a').text('Reset Account');
		ttmp2 = $('#lnkRenametestNotGia').parent().clone().find('a').text('Close Account');
		var ttmmpp1 = ttmp1.attr('href','https://ibglxysit4s-lp.lloydstsb.co.uk/personal/a/viewproductdetails/rename.jsp?link=reset').parent();
		var ttmmpp2 = ttmp2.attr('href','https://ibglxysit4s-lp.lloydstsb.co.uk/personal/a/viewproductdetails/rename.jsp?link=close').parent();

		$('#lnkRenametestNotGia').parent().parent().append(ttmmpp1);
		$('#lnkRenametestNotGia').parent().parent().append(ttmmpp2);
	}

}

//rename account page
LBGAlchemy.func.renameAccountFacade = function() {

	if (localStorage.isSSAccountDetail=='true') {
		$('#lkAccOverView_retail').attr('href', LBGAlchemy.copy.text.buttonLink);

		var tmpli = $('.actions li');

		$('.primary h1').text(LBGAlchemy.copy.text.renameTitle);
		$(tmpli[0]).find('input').remove();
		var ssBtn = '<a class="alchemy-blue-button" href="'+ LBGAlchemy.copy.text.buttonLink +'" alt="Register" style="color: #ffffff;text-decoration:none;box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-webkit-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-moz-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-ms-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-o-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);text-shadow: 2px 0px 4px #2a2a94;">&nbsp; '+ LBGAlchemy.copy.text.renameAccountinputTitle +' &nbsp;</a>';
		tmpli[0].innerHTML = ssBtn;


		console.log($('.formFieldInner input'));
		$('.formFieldInner input').attr('value', LBGAlchemy.copy.text.renameAccountinputTitle.replace('Rename ', ''));
		// $('.linkList a').attr('href', '#');

		$('#frmViewAccountOverview .linkList li').hide();
	}
	// localStorage.isSSAccountDetail = false;	
}

//reset account page
LBGAlchemy.func.resetAccountFacade = function() {

	if (localStorage.isSSAccountDetail=='true') {
		$('#lkAccOverView_retail').attr('href', LBGAlchemy.copy.text.buttonLink);

		var tmpli = $('.actions li');

		$('.primary h1').text(LBGAlchemy.copy.text.resetTitle);
		$(tmpli[0]).find('input').remove();
		var ssBtn = '<a class="alchemy-blue-button" href="'+ LBGAlchemy.copy.text.buttonLink +'" alt="Register" style="color: #ffffff;text-decoration:none;box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-webkit-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-moz-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-ms-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-o-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);text-shadow: 2px 0px 4px #2a2a94;">&nbsp; '+ LBGAlchemy.copy.text.resetAccountinputTitle +' &nbsp;</a>';
		tmpli[0].innerHTML = ssBtn;


		console.log($('.formFieldInner input'));
		$('.formFieldInner input').attr('value', LBGAlchemy.copy.text.resetAccountinputTitle.replace('Reset ', ''));
		// $('.linkList a').attr('href', '#');
		$($('.inner p')[0]).text('Please confirm the shared account name below to be reset.');
		$('#frmViewAccountOverview .linkList li').hide();
	}
}

//close account page
LBGAlchemy.func.closeAccountFacade = function() {
	if (localStorage.isSSAccountDetail=='true') {
		$('#lkAccOverView_retail').attr('href', LBGAlchemy.copy.text.buttonLink);

		var tmpli = $('.actions li');

		$('.primary h1').text(LBGAlchemy.copy.text.closeTitle);
		$(tmpli[0]).find('input').remove();
		var ssBtn = '<a class="alchemy-blue-button" href="'+ LBGAlchemy.copy.text.buttonLink +'" alt="Register" style="color: #ffffff;text-decoration:none;box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-webkit-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-moz-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-ms-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);-o-box-shadow: inset 0px -10px 1px rgba(51,51,153,0.8), 0px 2px 4px rgba(0,0,0,0.4);text-shadow: 2px 0px 4px #2a2a94;">&nbsp; '+ LBGAlchemy.copy.text.closeAccountinputTitle +' &nbsp;</a>';
		tmpli[0].innerHTML = ssBtn;


		console.log($('.formFieldInner input'));
		$('.formFieldInner input').attr('value', LBGAlchemy.copy.text.closeAccountinputTitle.replace('Close ', ''));
		// $('.linkList a').attr('href', '#');
		$($('.inner p')[0]).text('Please confirm the shared account name below to be closed.');
		$('#frmViewAccountOverview .linkList li').hide();
	}
}

//make transfer
LBGAlchemy.func.makeTransferFacade = function() {
	if (localStorage.isSSAccountDetail=='true') {
		$('#page h1').text(LBGAlchemy.copy.text.makeTransferTitle);
		$('.accountBalance').text('Balance: £1,000.00 ');
		$('.availableFunds').text('(Available Funds: £1,000.00) ');
		var tT = $($('select').find('option')[1]).clone().text('Shared Account (65-43-21, '+localStorage.ssAccountNumber+')').removeAttr('selected');
		tT.appendTo($('select'));

	}	
}

//make payment
LBGAlchemy.func.makePaymentFacade = function() {
	if (localStorage.isSSAccountDetail=='true') {
		console.log('I am here payment');
		$('.panelWrap dd em').text(LBGAlchemy.copy.text.makePaymentTitle);
		$('.balance').text('Balance: £1,000.00 (Available Funds: £1,000.00) ')
		var tT = $($('select').find('option')[1]).clone().text('Shared Account (65-43-21, '+localStorage.ssAccountNumber+')').removeAttr('selected');
		tT.appendTo($('select'));
		tT.appendTo($('select'));
		tT.text('The Alchemy Party (65-43-21, '+localStorage.ssAccountNumber+')').appendTo($('select'));

	}	
}


// ------------------------------------
// ib default page functions
// ------------------------------------

LBGAlchemy.func.addButton = function () {

	var allLinks = LBGAlchemy.htmlBody.find("li .linkSubHeading");

	console.log("LBGAlchemy.func.addButton()");

	for (var i = 0; i < allLinks.length; i++) {
		
		if($(allLinks[i]).text().toLowerCase()=="current account") {
			
			var nl = $(allLinks[i]).clone();
			nl.find("a").attr("href","javascript:LBGAlchemy.func.createSharedAccount();");
			nl.find("a").html("<b>Create Shared Account</b>");
			nl.prependTo($(allLinks[i]).parent());

		}

	};

}

LBGAlchemy.func.removeSharedAccount = function(options) {
	var tT = $('#frm1:lstAccLst li');

	for(var i in tT) {
		console.log('lkasdf', tT.length);
	}
}

LBGAlchemy.func.createSharedAccount = function(options) {
	LBGAlchemy.currentData.createSharedAccount(LBGAlchemy.view.classicAccountNode, options);
}

// ------------------------------------
// URL functions
// ------------------------------------

LBGAlchemy.func.openSharedSpaces = function () {

	var ssurl = LBGAlchemy.url + LBGAlchemy.userUrl + LBGAlchemy.theme;

	console.log("LBGAlchemy.func.openSharedSpaces("+ssurl+")");

	window.open(ssurl);

}

LBGAlchemy.func.openAccountDetail = function () {
	console.log('lolo', localStorage.isSSAccountDetail, typeof(Storage));
	if(typeof(Storage)!="undefined" && localStorage.isSSAccountDetail=="false") {
		localStorage.isSSAccountDetail = true;

		window.location.href=LBGAlchemy.currentData.defaultTitleLink;
	}
}

LBGAlchemy.func.openRequestDebitCard = function() {

	var ssurl = "https://ibglxysit4s-lp.lloydstsb.co.uk/personal/a/savings_product/easysaver/";

	console.log("LBGAlchemy.func.openRequestDebitCard("+ssurl+")");

	if(LBGAlchemy.theme!="alchemy") {
		window.open(ssurl, '_self');
	}

	var ele = document.createElement('div');
	ele.id = "dummyDebitCard";
	ele.style.opacity = 0;
	ele.innerHTML = '<div class="dummyoverlay"></div><div class="dummylayer"></div>';
	document.body.appendChild(ele);
	$(ele).fadeTo(250, 1);
	$(ele).click(function(){

		// $(this).hide();
		$(ele).remove();
		return false;
	});
}


//window resize
LBGAlchemy.func.onStageResize = function () {

	var stageWidth = $(window).width();
	var stageHeight = $(window).height();

	LBGAlchemy.view.loader.css("padding-top", (stageHeight/2)-70);

};

LBGAlchemy.func.getQueryParams = function (qs) {
    qs = qs.split("+").join(" ");
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
};

// ------------------------------------
// Initialise
// ------------------------------------
$(function() {

	console.log('haha hoho init');
	LBGAlchemy.func.init();

});
