/*! LAB.js (LABjs :: Loading And Blocking JavaScript)
    v2.0.3 (c) Kyle Simpson
    MIT License
*/
(function(o){var K=o.$LAB,y="UseLocalXHR",z="AlwaysPreserveOrder",u="AllowDuplicates",A="CacheBust",B="BasePath",C=/^[^?#]*\//.exec(location.href)[0],D=/^\w+\:\/\/\/?[^\/]+/.exec(C)[0],i=document.head||document.getElementsByTagName("head"),L=(o.opera&&Object.prototype.toString.call(o.opera)=="[object Opera]")||("MozAppearance"in document.documentElement.style),q=document.createElement("script"),E=typeof q.preload=="boolean",r=E||(q.readyState&&q.readyState=="uninitialized"),F=!r&&q.async===true,M=!r&&!F&&!L;function G(a){return Object.prototype.toString.call(a)=="[object Function]"}function H(a){return Object.prototype.toString.call(a)=="[object Array]"}function N(a,c){var b=/^\w+\:\/\//;if(/^\/\/\/?/.test(a)){a=location.protocol+a}else if(!b.test(a)&&a.charAt(0)!="/"){a=(c||"")+a}return b.test(a)?a:((a.charAt(0)=="/"?D:C)+a)}function s(a,c){for(var b in a){if(a.hasOwnProperty(b)){c[b]=a[b]}}return c}function O(a){var c=false;for(var b=0;b<a.scripts.length;b++){if(a.scripts[b].ready&&a.scripts[b].exec_trigger){c=true;a.scripts[b].exec_trigger();a.scripts[b].exec_trigger=null}}return c}function t(a,c,b,d){a.onload=a.onreadystatechange=function(){if((a.readyState&&a.readyState!="complete"&&a.readyState!="loaded")||c[b])return;a.onload=a.onreadystatechange=null;d()}}function I(a){a.ready=a.finished=true;for(var c=0;c<a.finished_listeners.length;c++){a.finished_listeners[c]()}a.ready_listeners=[];a.finished_listeners=[]}function P(d,f,e,g,h){setTimeout(function(){var a,c=f.real_src,b;if("item"in i){if(!i[0]){setTimeout(arguments.callee,25);return}i=i[0]}a=document.createElement("script");if(f.type)a.type=f.type;if(f.charset)a.charset=f.charset;if(h){if(r){e.elem=a;if(E){a.preload=true;a.onpreload=g}else{a.onreadystatechange=function(){if(a.readyState=="loaded")g()}}a.src=c}else if(h&&c.indexOf(D)==0&&d[y]){b=new XMLHttpRequest();b.onreadystatechange=function(){if(b.readyState==4){b.onreadystatechange=function(){};e.text=b.responseText+"\n//@ sourceURL="+c;g()}};b.open("GET",c);b.send()}else{a.type="text/cache-script";t(a,e,"ready",function(){i.removeChild(a);g()});a.src=c;i.insertBefore(a,i.firstChild)}}else if(F){a.async=false;t(a,e,"finished",g);a.src=c;i.insertBefore(a,i.firstChild)}else{t(a,e,"finished",g);a.src=c;i.insertBefore(a,i.firstChild)}},0)}function J(){var l={},Q=r||M,n=[],p={},m;l[y]=true;l[z]=false;l[u]=false;l[A]=false;l[B]="";function R(a,c,b){var d;function f(){if(d!=null){d=null;I(b)}}if(p[c.src].finished)return;if(!a[u])p[c.src].finished=true;d=b.elem||document.createElement("script");if(c.type)d.type=c.type;if(c.charset)d.charset=c.charset;t(d,b,"finished",f);if(b.elem){b.elem=null}else if(b.text){d.onload=d.onreadystatechange=null;d.text=b.text}else{d.src=c.real_src}i.insertBefore(d,i.firstChild);if(b.text){f()}}function S(c,b,d,f){var e,g,h=function(){b.ready_cb(b,function(){R(c,b,e)})},j=function(){b.finished_cb(b,d)};b.src=N(b.src,c[B]);b.real_src=b.src+(c[A]?((/\?.*$/.test(b.src)?"&_":"?_")+~~(Math.random()*1E9)+"="):"");if(!p[b.src])p[b.src]={items:[],finished:false};g=p[b.src].items;if(c[u]||g.length==0){e=g[g.length]={ready:false,finished:false,ready_listeners:[h],finished_listeners:[j]};P(c,b,e,((f)?function(){e.ready=true;for(var a=0;a<e.ready_listeners.length;a++){e.ready_listeners[a]()}e.ready_listeners=[]}:function(){I(e)}),f)}else{e=g[0];if(e.finished){j()}else{e.finished_listeners.push(j)}}}function v(){var e,g=s(l,{}),h=[],j=0,w=false,k;function T(a,c){a.ready=true;a.exec_trigger=c;x()}function U(a,c){a.ready=a.finished=true;a.exec_trigger=null;for(var b=0;b<c.scripts.length;b++){if(!c.scripts[b].finished)return}c.finished=true;x()}function x(){while(j<h.length){if(G(h[j])){try{h[j++]()}catch(err){}continue}else if(!h[j].finished){if(O(h[j]))continue;break}j++}if(j==h.length){w=false;k=false}}function V(){if(!k||!k.scripts){h.push(k={scripts:[],finished:true})}}e={script:function(){for(var f=0;f<arguments.length;f++){(function(a,c){var b;if(!H(a)){c=[a]}for(var d=0;d<c.length;d++){V();a=c[d];if(G(a))a=a();if(!a)continue;if(H(a)){b=[].slice.call(a);b.unshift(d,1);[].splice.apply(c,b);d--;continue}if(typeof a=="string")a={src:a};a=s(a,{ready:false,ready_cb:T,finished:false,finished_cb:U});k.finished=false;k.scripts.push(a);S(g,a,k,(Q&&w));w=true;if(g[z])e.wait()}})(arguments[f],arguments[f])}return e},wait:function(){if(arguments.length>0){for(var a=0;a<arguments.length;a++){h.push(arguments[a])}k=h[h.length-1]}else k=false;x();return e}};return{script:e.script,wait:e.wait,setOptions:function(a){s(a,g);return e}}}m={setGlobalDefaults:function(a){s(a,l);return m},setOptions:function(){return v().setOptions.apply(null,arguments)},script:function(){return v().script.apply(null,arguments)},wait:function(){return v().wait.apply(null,arguments)},queueScript:function(){n[n.length]={type:"script",args:[].slice.call(arguments)};return m},queueWait:function(){n[n.length]={type:"wait",args:[].slice.call(arguments)};return m},runQueue:function(){var a=m,c=n.length,b=c,d;for(;--b>=0;){d=n.shift();a=a[d.type].apply(null,d.args)}return a},noConflict:function(){o.$LAB=K;return m},sandbox:function(){return J()}};return m}o.$LAB=J();(function(a,c,b){if(document.readyState==null&&document[a]){document.readyState="loading";document[a](c,b=function(){document.removeEventListener(c,b,false);document.readyState="complete"},false)}})("addEventListener","DOMContentLoaded")})(this);




var LBGAlchemy = {};
LBGAlchemy.func = {};
LBGAlchemy.settings = {};
LBGAlchemy.modules = {};
LBGAlchemy.view = {};

LBGAlchemy.session = {};
LBGAlchemy.session.getvars = {};
LBGAlchemy.session.account = null;
LBGAlchemy.session.adminView = false;
LBGAlchemy.session.currentUser = {};
LBGAlchemy.session.lastInvitedMemberList = [];

LBGAlchemy.url = "/shared-spaces/";
LBGAlchemy.inviteMembersURL = "//razorfish-lbg.appspot.com/alchemy/send-email";
LBGAlchemy.smsMembersURL = "//razorfish-lbg.appspot.com/alchemy/send-sms";
LBGAlchemy.appURL = "https://razorfish-lbg.appspot.com/shared-spaces/";
LBGAlchemy.addTransactionURL = "https://razorfish-lbg.appspot.com/shared-spaces/add-transaction";
LBGAlchemy.getTransactionsURL = "https://razorfish-lbg.appspot.com/shared-spaces/get-transactions?space_name=";

// Settings

LBGAlchemy.htmlBody = null;
LBGAlchemy.settings.bankAPI = {};
LBGAlchemy.settings.bankAPI.endPoint = "";

//

LBGAlchemy.session.emptyState = false;
LBGAlchemy.session.loggedIn = false;
LBGAlchemy.session.prevSection = "";
LBGAlchemy.session.currentSection = "";
LBGAlchemy.session.currentSubheadId = "";

//

LBGAlchemy.modules.defaultViewId = "home";
LBGAlchemy.modules.list = [];

//


LBGAlchemy.func.init = function () {

	LBGAlchemy.session.getvars = LBGAlchemy.func.getQueryParams(document.location.search);

	if(LBGAlchemy.session.getvars.empty=="true") {
		LBGAlchemy.session.emptyState = true;
	}

	console.log("LBGAlchemy.func.init()", LBGAlchemy.session.getvars, "Empty state: " + LBGAlchemy.session.emptyState);

	LBGAlchemy.htmlBody = $("body");
	
	if(LBGAlchemy.session.getvars.theme) {
		LBGAlchemy.theme = LBGAlchemy.session.getvars.theme;
	}else{
		LBGAlchemy.theme = "alchemy";
	}

	LBGAlchemy.func.initBasics();

};

LBGAlchemy.func.initBasics = function () {

	console.log("LBGAlchemy.initBasics()");

	var d = new Date();

	// load theme css
 	$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', LBGAlchemy.url+'css/ss-'+ LBGAlchemy.theme +'.css?c='+d.getTime()) );
 
	$LAB
	.script(LBGAlchemy.url+"js/canvasloader.js?c="+d.getTime())
	.script(LBGAlchemy.url+"js/jeditable.js")
	.wait(LBGAlchemy.func.initBasicsComplete);

};

LBGAlchemy.func.initBasicsComplete = function () {

	// Init Loader

	console.log("LBGAlchemy.initBasicsComplete()");

	var loaderHTML = "<div class='main-loader'>";
	//loaderHTML += "<div id='main-loader-logo'><img src='img/header-logo-blue-text-small.png' width='200' height='41' /></div>";
	//loaderHTML += "<h1>Shared Spaces</h1>";
	loaderHTML += "<div id='main-loader-spinner'></div>";
	loaderHTML += "</div>";

	// add main loader
	LBGAlchemy.htmlBody.append(loaderHTML);

	LBGAlchemy.view.loader = LBGAlchemy.htmlBody.find(".main-loader");
	LBGAlchemy.view.loaderSpinner = LBGAlchemy.view.loader.find("#main-loader-spinner");


	LBGAlchemy.view.loaderSpinnerObj = LBGAlchemy.view.getSpinnerUI("main-loader-spinner", 40, 14);

	//

	$(window).resize(function() {
		LBGAlchemy.func.onStageResize();
	});
	LBGAlchemy.func.onStageResize();

	LBGAlchemy.view.loaderSpinner.fadeIn();


	//

	if(!LBGAlchemy.session.getvars.account) LBGAlchemy.session.getvars.account = "default";


	// load user config
	LBGAlchemy.func.loadAccountConfig();

	//
};

LBGAlchemy.func.initTransactionsEventListener = function() {
	
	// Create an EventSource connection to a specific Origin URL
	var sse = new EventSource(LBGAlchemy.getTransactionsURL+LBGAlchemy.session.account.name, {withCredentials: true});

	sse.addEventListener("Transactions", function(data) {
		// Add any new transactions to the list
		
		if(LBGAlchemy.session.loggedIn) {

			try{
				if(data.data == "") return;

				var sharedSpaceData = JSON.parse(data.data);
				
				if(sharedSpaceData && sharedSpaceData != "" && sharedSpaceData != "None") {
					var objKeys = Object.keys(sharedSpaceData);
					
					for(var i=0,l=objKeys.length; i !== l; i++) {
						var userKey = objKeys[i];
						var user = sharedSpaceData[userKey];

						for(var j=0, k=user.length; j !== k; j++) {

							var newEntry = {
						        "date":     user[j].date,
						        "user":     userKey,
						        "label":    "",
						        "amount":   parseInt(user[j].amount)
							};

							var exists = false;
							// If the transaction doesn't already exist
							for(var m = 0, n=LBGAlchemy.session.account.transactions.length; m !== n; m++) {
								var transaction = LBGAlchemy.session.account.transactions[m];

								if(transaction.user == newEntry.user && transaction.date == newEntry.date && transaction.amount == newEntry.amount) {
									exists = true;
									break;
								}
								
							}

							if(!exists) {
								LBGAlchemy.func.addAsyncTransaction(userKey, user[j].date, user[j].amount);
							}
							
						}
						
					}	
				}
			} catch(e) {
				//
			}

		};
		
	}, false);

};

LBGAlchemy.func.addAsyncTransaction = function(user, date, amount) {
	
	// Push new entry
	LBGAlchemy.session.account.transactions.push({
        "date":     date,
        "user":     user,
        "label":    "",
        "amount":   parseInt(amount)
	});

	// Refresh section
	if(LBGAlchemy.session.loggedIn) {
		LBGAlchemy.view.refreshSection();
	}

};

LBGAlchemy.view.getSpinnerUI = function ( id, di, de ) {

	var cl = new CanvasLoader(id);
	cl.setColor('#666666'); // default is '#000000'
	cl.setShape('spiral'); // default is 'oval'
	cl.setDiameter(di); // default is 40
	cl.setDensity(de); // default is 40
	cl.setSpeed(1); // default is 2
	cl.setFPS(30); // default is 24
	cl.show(); // Hidden by default

}

LBGAlchemy.func.loadAccountConfig = function () {
	LBGAlchemy.func.loadConfigFile(LBGAlchemy.url + "data/account-"+LBGAlchemy.session.getvars.account+".json?", LBGAlchemy.func.loadAccountConfigComplete);
}

LBGAlchemy.func.loadAccountConfigComplete = function ( data ) {

	console.log("LBGAlchemy.loadAccountConfigComplete()");

	// add pending members
	if(pendingMembers) {
		
		for (var i = 0; i < pendingMembers.members.length; i++) {

			var userAlreadyExists = false;

			var mem = data.members;
			for (var y = 0; y < mem.length; y++) {
				
				if(mem[y].id.toLowerCase()==pendingMembers.members[i].id.toLowerCase()) {
					userAlreadyExists = true;
				};

			};

			if(!userAlreadyExists) data.members.push(pendingMembers.members[i]);

		};
	};

	// show only admin if account is supposed to be empty
	if(LBGAlchemy.session.emptyState) {
		
		//data.name = "Our Shared Space";

		var dataMem = data.members;
		var dataNewMem = [];
		
		for (var i = 0; i < dataMem.length; i++) {
			if(data.admin==dataMem[i].id) {
				dataMem[i].amount = 0;
				dataNewMem.push(dataMem[i]);
				break;
			}
		};

		data.members = dataNewMem;

		// push in empty state
		ar = [];
		ar.push({
            "date":     "2013-07-31 01:00:00",
            "user":     data.admin,
            "label":    "Shared Account has been created",
            "amount":   0
        });

        data.transactions = ar;

	}

	//

	LBGAlchemy.session.account = data;

	// init modules
	LBGAlchemy.func.initModules();

}

LBGAlchemy.func.initModules = function () {

	//

	console.log("Current User: ", LBGAlchemy.session.currentUser);
	console.log("Current Account: ", LBGAlchemy.session.account);
	console.log("Admin View: ", LBGAlchemy.session.adminView);

	var d = new Date();

	$LAB

	.script(LBGAlchemy.url+"js/module-framework.js?c="+d.getTime())
	.script(LBGAlchemy.url+"js/module-signup.js?c="+d.getTime())
	.script(LBGAlchemy.url+"js/module-home.js?c="+d.getTime())
	.script(LBGAlchemy.url+"js/module-account.js?c="+d.getTime())

	.wait(LBGAlchemy.func.modulesLoaded);

};

LBGAlchemy.func.loadConfigFile = function ( url, cbf ) {

	$.ajax({
        'async': false,
        'global': false,
        'cache': false,
        'url': url,
        'dataType': "json",
        'error': function () {
        	console.log("LBGAlchemy.func.loadConfigFile("+url+").error");
        },
        'success': cbf
	});

}

LBGAlchemy.func.modulesLoaded = function () {

	//

	console.log("LBGAlchemy.func.modulesLoaded()");

	//

	LBGAlchemy.modules.list.push({
		"id":"framework",	"m":LBGAlchemy.modules.frameworkModule, "section":"null"
	});

	LBGAlchemy.modules.list.push({
		"id":"signup",	"m":LBGAlchemy.modules.signupModule, "section":"signup"
	});

	LBGAlchemy.modules.list.push({
		"id":"home",	"m":LBGAlchemy.modules.homeModule, "section":"home"
	});

	LBGAlchemy.modules.list.push({
		"id":"account",	"m":LBGAlchemy.modules.accountModule, "section":"account"
	});

	//

	// init framework
	LBGAlchemy.func.initModule(LBGAlchemy.htmlBody, LBGAlchemy.modules.frameworkModule(),LBGAlchemy.func.initComplete);

};

LBGAlchemy.func.initModule = function ( body, module, cbf ) {

	console.log("LBGAlchemy.func.initModule("+module.title+")");

	module.init(body, cbf);

};

LBGAlchemy.func.showSection = function ( id, cbf ) {

	console.log("LBGAlchemy.func.showSection("+id+")");

	var mod = LBGAlchemy.modules.list;
	var cmod = null;
	var lmod = null;

	for (var i = 0; i < mod.length; i++) {
		if(mod[i].section=="login") lmod = mod[i];
		if(mod[i].section==id) {
			cmod = mod[i];
			break;
		};
	};

	if(!cmod) cmod = lmod;

	LBGAlchemy.func.initModule(LBGAlchemy.htmlBody,cmod.m(),cbf);

};

LBGAlchemy.view.refreshSection = function () {

	console.log("LBGAlchemy.view.updateCurrentSection()");

	// content
	//LBGAlchemy.htmlBody.find(".alchemy .content-inner").children().fadeOut(200);
	//LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").hide();
	//LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").hide();

		LBGAlchemy.htmlBody.find(".alchemy .masthead-inner").html("");
		LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").html("");
		LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").html("");

		LBGAlchemy.func.showSection(LBGAlchemy.session.currentSection,LBGAlchemy.func.handleSitemapChangeComplete);



	// masthead
	/*
	LBGAlchemy.htmlBody.find(".alchemy .masthead-inner").children().delay(2).fadeOut(1,function () {
		
		LBGAlchemy.htmlBody.find(".alchemy .masthead-inner").html("");
		LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").html("");
		LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").html("");

		LBGAlchemy.func.showSection(LBGAlchemy.session.currentSection,LBGAlchemy.func.handleSitemapChangeComplete);

	});
*/

}

LBGAlchemy.func.getTargetAmount = function () {
	var a = 0;
	var m = LBGAlchemy.func.getAccountMembers();
	for (var i = 0; i < m.length; i++) {
		a += m[i].amount;
	};
	return a;
}

LBGAlchemy.func.getCollectedAmount = function () {
	var a = 0;
	var m = LBGAlchemy.func.getAccountActivity();
	for (var i = 0; i < m.length; i++) {
		if(m[i].amount>0) a += m[i].amount;
	};
	return a;
}

LBGAlchemy.func.getBalanceAmount = function () {
	var a = 0;
	var m = LBGAlchemy.func.getAccountActivity();
	for (var i = 0; i < m.length; i++) {
		a += m[i].amount;
	};
	return a;
}

LBGAlchemy.func.getMemberNameByUser = function ( user ) {
	var a = 0;
	var m = LBGAlchemy.func.getAccountMembers();
	for (var i = 0; i < m.length; i++) {
		if(m[i].id.toLowerCase()==user.toLowerCase()) {
			return m[i].name;
		}
	};
	return a;
}

LBGAlchemy.func.getBalanceByMember = function ( user ) {
	var a = 0;
	var m = LBGAlchemy.func.getAccountActivity();
	for (var i = 0; i < m.length; i++) {
		if(m[i].user.toLowerCase()==user.toLowerCase()) {
			if(m[i].amount>0) a += m[i].amount;
		}
	};
	return a;
}

LBGAlchemy.func.getMemberSpaces = function () {

	var ar = [];
	var m = LBGAlchemy.session.account.spaces;
	for (var i = 0; i < m.length; i++) {
		ar.push(m[i]);
	};
	return ar;

}

LBGAlchemy.func.getAccountMembers = function () {

	var ar = [];
	var m = LBGAlchemy.session.account.members;
	for (var i = 0; i < m.length; i++) {
		ar.push(m[i]);
	};
	return ar;

}

LBGAlchemy.func.getAccountAdmin = function () {

	var us = {};
	var m = LBGAlchemy.session.account.members;
	for (var i = 0; i < m.length; i++) {
		if(m[i].id==LBGAlchemy.session.account.admin) {
			us = m[i];
			break;
		}
	};
	return us;

}

LBGAlchemy.func.getAccountActivity = function () {

	//console.log("LBGAlchemy.func.getAccountActivity()");

	var ar = [];
	var m = LBGAlchemy.session.account.transactions;
	
	for (var i = 0; i < m.length; i++) {
		ar.push(m[i]);
	};

	ar = ar.sort(LBGAlchemy.func.sortDateDESCObj);

	return ar;

}

LBGAlchemy.func.removeMember = function ( id ) {
	
	console.log("LBGAlchemy.func.removeMember("+id+")");

	var ca = LBGAlchemy.func.getAccountMembers();
	var na = [];

	for (var i = 0; i < ca.length; i++) {
		if(ca[i].id.toLowerCase()!=id.toLowerCase()) {
			na.push(ca[i]);
		}
	};

	LBGAlchemy.session.account.members = na;

	// tbd
	LBGAlchemy.view.refreshSection();

}

LBGAlchemy.func.inviteMembers = function ( list ) {
	
	console.log("LBGAlchemy.func.inviteMembers()",list);

	var sendObj = {
		"name": LBGAlchemy.session.account.name,
		"subject": "Welcome to your Shared Space",
		"theme": LBGAlchemy.theme,
		"members": list
	};

	//

	LBGAlchemy.session.lastInvitedMemberList = list;

	if(list.length>0) {	

		//

		LBGAlchemy.htmlBody.find(".action-output").html("<div id='form-spinner'></div>");
		LBGAlchemy.view.getSpinnerUI("form-spinner", 30, 14);

		//

		$.ajax({
	        'async': false,
	        'type': "POST",
	        'global': false,
	        'cache': false,
	        'url': LBGAlchemy.inviteMembersURL,
	        'dataType': "json",
	        'data': "payload="+JSON.stringify(sendObj),
	        'error': function () {
	        	LBGAlchemy.func.inviteMembersComplete(null);
	        },
	        'success': LBGAlchemy.func.inviteMembersComplete
		});	

	}else{
		LBGAlchemy.func.inviteMembersComplete(null);

	}

	//

}

LBGAlchemy.func.inviteMembersComplete = function ( data ) {

	//

	console.log("LBGAlchemy.func.inviteMembersComplete()",data);

	var success = false;

	//

	if(data) {

		if(data.status=="success") {
			
			success = true;

		}

	}

	if(success) {

		for (var i = 0; i < LBGAlchemy.session.lastInvitedMemberList.length; i++) {
			LBGAlchemy.func.addMember(LBGAlchemy.session.lastInvitedMemberList[i]);
		};

		LBGAlchemy.view.refreshSection();

	}else{
		// show error

		LBGAlchemy.htmlBody.find(".action-output").html("Error, please try again later.");

		LBGAlchemy.session.lastInvitedMemberList = [];
		console.log("LBGAlchemy.func.inviteMembersComplete() error",data);

	}

}

LBGAlchemy.func.smsMember = function ( mobile, message ) {
	
	console.log("LBGAlchemy.func.smsMember("+mobile, message+")");

	$.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'cache': false,
        'url': LBGAlchemy.smsMembersURL,
        'dataType': "json",
        'data': {
        	"mobile": mobile,
        	"message": message,
        	"sender": "SHARED SPACES"
        },
        'error': function () {
        	LBGAlchemy.func.smsMemberComplete(null);
        },
        'success': LBGAlchemy.func.smsMemberComplete
	});	

	//

}

LBGAlchemy.func.smsMemberComplete = function ( data ) {

	//

	console.log("LBGAlchemy.func.smsMemberComplete()", data);

	var success = false;

	//

	if(data) {

		if(data.status=="success") {
			
			success = true;

		}

	}

	if(success) {
		// all good

	}else{
		// show error
		console.log("LBGAlchemy.func.inviteMembersComplete() error",data);

	}

}

LBGAlchemy.func.addMember = function ( obj ) {

	var newEntry = {
        "id":     obj.email,
        "name":     obj.name,
        "email":     obj.email,
        "phone":     "",
        "registered":    false,
        "amount":   Number(obj.amount)
	};

	console.log("LBGAlchemy.func.addMember()", newEntry);

	LBGAlchemy.session.account.members.push(newEntry);

}

LBGAlchemy.func.submitPayment = function ( amount, card_name, card_number, expiry_mm, expiry_yy, card_csv ) {
	
	console.log("LBGAlchemy.func.submitPayment("+amount, card_name, card_number, expiry_mm, expiry_yy, card_csv+")");

	//

	LBGAlchemy.func.addTransaction(amount, LBGAlchemy.session.currentUser.id);

	// Send Admin a text notification
	LBGAlchemy.func.smsMember(LBGAlchemy.func.getAccountAdmin().phone, LBGAlchemy.session.account.name+" has new activity: "+card_name+", "+amount+" pounds");

	//

	LBGAlchemy.view.refreshSection();



}

LBGAlchemy.func.addTransaction = function ( amount, user ) {

	
	var now = new Date();
	var strDateTime = [now.getFullYear(), AddZero(now.getMonth() + 1), AddZero(now.getDate())].join("-") + " " + [AddZero(now.getHours()), AddZero(now.getMinutes()), AddZero(now.getSeconds())].join(":");

	function AddZero(num) {
	    return (num >= 0 && num < 10) ? "0" + num : num + "";
	}

	var newEntry = {
        "date":     strDateTime,
        "user":     user,
        "label":    "",
        "amount":   Number(amount)
	};

	LBGAlchemy.session.account.transactions.push(newEntry);

	
	try {
		$.ajax({
	        'async': true,
	        'type': "POST",
	        'global': false,
	        'cache': false,
	        'url': LBGAlchemy.addTransactionURL,
	        'dataType': "json",
	        'data': "space_name="+LBGAlchemy.session.account.name+"&date="+strDateTime+"&user="+user+"&amount="+amount,
	        'error': function () {
	        	// do nothing
	        },
	        'success': function() {
	        	// do nothing
	        }
		});	
	} catch(e) {
		// do nothing
	}
	
}

LBGAlchemy.func.changeRequestedAmount = function ( amount, user ) {

	for (var i = 0; i < LBGAlchemy.session.account.members.length; i++) {
		
		if(user==LBGAlchemy.session.account.members[i].id) {
			LBGAlchemy.session.account.members[i].amount = amount;
			break;
		}

	};

	LBGAlchemy.view.refreshSection();
	
}

LBGAlchemy.func.registerCurrentUser = function ( username, password ) {

	var m = LBGAlchemy.func.getAccountMembers();
	for (var i = 0; i < m.length; i++) {
		if(m[i].id.toLowerCase()==username.toLowerCase()) {
			m[i].registered = true;
			LBGAlchemy.session.currentUser.registered = true;
		}
	};

	LBGAlchemy.func.loginUser(username, password);

}


LBGAlchemy.func.logoutUser = function () {
	LBGAlchemy.session.loggedIn = false;
	LBGAlchemy.session.currentUser = {};
	window.location.hash = "home";
}

LBGAlchemy.func.loginUser = function ( username, password ) {

	var userfound = false;
	var userRegistered = false;
	var currentUser = "";

	var m = LBGAlchemy.func.getAccountMembers();
	for (var i = 0; i < m.length; i++) {

		if(m[i].id.toLowerCase()==username.toLowerCase() || (m[i].phone==username && username.length>5)) {
			userfound = true;
			userRegistered = m[i].registered;
			currentUserId = m[i].id;
			break;
		}

	};

	if(userfound) {

		//

		var sessionData = LBGAlchemy.session.account;

		// set current user
		LBGAlchemy.session.currentUser = sessionData.members[0];
		for (var i = 0; i < sessionData.members.length; i++) {
			if(sessionData.members[i].id==currentUserId) {
				LBGAlchemy.session.currentUser = sessionData.members[i];
				break;
			}
		};

		// set admin view if it's admin
		if(sessionData.admin==LBGAlchemy.session.currentUser.id) {
			LBGAlchemy.session.adminView = true;
		} 

		//
		
		if(!userRegistered) {
			window.location.hash = "signup";

		}else{

			LBGAlchemy.session.loggedIn = true;


			// take them to the account
			window.location.hash = "account";

		}

		//

	}else{
		LBGAlchemy.session.loggedIn = false;
		return false;
	}

}

LBGAlchemy.func.showSpace = function ( id ) {
	console.log("LBGAlchemy.func.showSpace("+id+")");
}

LBGAlchemy.func.updateSubHeader = function ( type ) {

	console.log("LBGAlchemy.func.updateSubHeader("+type+")");

	var nhx = "";

	var masthead = LBGAlchemy.htmlBody.find(".alchemy .masthead");
	var content = LBGAlchemy.htmlBody.find(".alchemy .content");
	var header = LBGAlchemy.htmlBody.find(".alchemy header");
	var footer = LBGAlchemy.htmlBody.find(".alchemy footer");

	var showProfileBtn = $(header.find(".profile-btn .button"));
	var showMenuBtn = $(header.find(".menu-btn .button"));
	var showProfileBtnSelected = false;
	var showMenuBtnSelected = false;

	var subHeaded = LBGAlchemy.htmlBody.find(".alchemy .subhead");
	var subHeaderInnerX = LBGAlchemy.htmlBody.find(".alchemy .subhead .subhead-inner-x");
	var hideContent = false;
	var newSubheadHeight = 350;

	if(type=="profile") {
		// user profile
		showProfileBtnSelected = true;
		nhx = LBGAlchemy.modules.frameworkModule().subheaderProfileHTML();
	}else if(type=="menu") {
		// menu
		showMenuBtnSelected = true;
		nhx = LBGAlchemy.modules.frameworkModule().subheaderMenuHTML();
	}else{
		type = LBGAlchemy.session.currentSubheadId;
	}


	if(LBGAlchemy.session.currentSubheadId!=type) {

		LBGAlchemy.session.currentSubheadId = type;

		if(subHeaded.height()==0) {

			subHeaded.show();
			subHeaderInnerX.html(nhx);
			newSubheadHeight = subHeaderInnerX.height()+60;
			subHeaded.animate({height: newSubheadHeight, queue:false}, 500 );

			hideContent = true;

			if(type=="menu") {
				subHeaderInnerX.find(".space-menu-item a").click(function () {
					LBGAlchemy.func.updateSubHeader();
					LBGAlchemy.func.showSpace($(this).attr("data"));
					return false;
				});
			}else if(type=="profile") {
				subHeaderInnerX.find(".button").click(function () {
					LBGAlchemy.func.updateSubHeader();
					return false;
				});
			};

		}else{

			subHeaded.animate({height: 0, queue:false}, 250, function () {

				subHeaderInnerX.html(nhx);
				newSubheadHeight = subHeaderInnerX.height()+60;

				subHeaded.animate({
				    height: newSubheadHeight, queue:false
				}, 500 );

				if(type=="menu") {
					subHeaderInnerX.find(".space-menu-item a").click(function () {
						LBGAlchemy.func.updateSubHeader();
						LBGAlchemy.func.showSpace($(this).attr("data"));
						return false;
					});
				}else if(type=="profile") {
					subHeaderInnerX.find(".button").click(function () {
						LBGAlchemy.func.updateSubHeader();
						return false;
					});
				}

			});

			hideContent = true;
			
		}

	}else{
		
		LBGAlchemy.session.currentSubheadId = "";
		showMenuBtnSelected = false;
		showProfileBtnSelected = false;

		if(subHeaded.height()>0) {

			subHeaded.animate({height: 0, queue:false}, 250, function () {
				subHeaded.hide();
			});

		}else{

			subHeaded.hide();

		}

	}

	if(showProfileBtnSelected) {
		showMenuBtn.removeClass("button-dark-selected");
		showProfileBtn.addClass("button-dark-selected");
	}else if (showMenuBtnSelected){
		showMenuBtn.addClass("button-dark-selected");
		showProfileBtn.removeClass("button-dark-selected");
	}else{
		showMenuBtn.removeClass("button-dark-selected");
		showProfileBtn.removeClass("button-dark-selected");		
	}

}

LBGAlchemy.func.updateHeader = function () {

	var headerInnerRight = LBGAlchemy.htmlBody.find(".alchemy .header-inner-right");

	if(LBGAlchemy.session.loggedIn) {
		headerInnerRight.html(LBGAlchemy.modules.frameworkModule().headerInnerRightHTML());	
		headerInnerRight.fadeIn();

		//

		var logOutBtn = $(headerInnerRight.find(".logout-btn"));
		logOutBtn.click(function () {
			LBGAlchemy.func.logoutUser();
			return false;
		});

		var showProfileBtn = $(headerInnerRight.find(".profile-btn"));
		showProfileBtn.click(function () {
			LBGAlchemy.func.updateSubHeader("profile");
			return false;
		});

		var showMenuBtn = $(headerInnerRight.find(".menu-btn"));
		showMenuBtn.click(function () {
			LBGAlchemy.func.updateSubHeader("menu");
			return false;
		});

		//

	}else{
		headerInnerRight.fadeOut(function() {
			$(this).html("");
		});
	}

}

LBGAlchemy.func.handleSitemapChange = function ( id ) {

	id = id.replace("#","");
	id = id.toLowerCase();


	if(id=="") id = "home";

	if(!LBGAlchemy.session.loggedIn) {

		if(id=="signup") {
			id = "signup";
		}else{
			id = "home";
		}

		window.location.hash = id;

	}

	//

	if(LBGAlchemy.session.currentSection!=id) {

		LBGAlchemy.session.prevSection = LBGAlchemy.session.currentSection;
		LBGAlchemy.session.currentSection = id;

		console.log("LBGAlchemy.func.handleSitemapChange("+id+")");

		if(LBGAlchemy.session.prevSection!="") {

			LBGAlchemy.view.refreshSection();
			
		}else{

			LBGAlchemy.func.showSection(id,LBGAlchemy.func.handleSitemapChangeComplete);

		}

	}else{

		console.log("LBGAlchemy.func.handleSitemapChange("+id+")");

	}

	// update header
	LBGAlchemy.func.updateHeader();

	// update subhead
	LBGAlchemy.func.updateSubHeader(LBGAlchemy.session.currentSubheadId);

};

LBGAlchemy.func.handleSitemapChangeComplete = function () {

	//

	console.log("LBGAlchemy.func.handleSitemapChangeComplete()");

	//LBGAlchemy.htmlBody.find(".alchemy .masthead-inner").children().hide();
	//LBGAlchemy.htmlBody.find(".alchemy .masthead-inner").children().fadeIn(200);
	LBGAlchemy.htmlBody.find(".alchemy .masthead-inner").show();
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").show();
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").show();

	//
/*
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").hide();
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").delay(2).fadeIn(200);
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-main").show();

	LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").hide();
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").delay(2).fadeIn(200);
	LBGAlchemy.htmlBody.find(".alchemy .content-inner-panel").show();
*/
	//

	if(LBGAlchemy.session.prevSection=="") {
		LBGAlchemy.view.intro();
	}

	// check if title changed
	LBGAlchemy.htmlBody.find(".alchemy .masthead .account-name").focusout(function () {
		
		if($(this).val()=="" || $(this).val().length<2) {
			$(this).val(LBGAlchemy.session.account.name)
		}else{
			LBGAlchemy.session.account.name = $(this).val();
		}

		LBGAlchemy.view.refreshSection();

	}) 

};

LBGAlchemy.func.initComplete = function () {

	console.log("LBGAlchemy.func.initComplete()");

	LBGAlchemy.view.contentInnerPanel = LBGAlchemy.htmlBody.find(".content-inner-panel");
	LBGAlchemy.view.contentInnerMain = LBGAlchemy.htmlBody.find(".content-inner-main");
	LBGAlchemy.view.contentInner = LBGAlchemy.htmlBody.find(".content-inner");

	$(window).on('hashchange', function() {
		LBGAlchemy.func.handleSitemapChange(window.location.hash);
	});

	if(LBGAlchemy.session.getvars.invite) {
		window.location.hash = "home";
		LBGAlchemy.func.loginUser(LBGAlchemy.session.getvars.invite, null);

	}else if(LBGAlchemy.session.getvars.c) {
		window.location.hash = "home";
		LBGAlchemy.func.loginUser(LBGAlchemy.session.getvars.c, null);


	}else{
		LBGAlchemy.func.handleSitemapChange(window.location.hash);
	}

	// Start listening for transactions
	LBGAlchemy.func.initTransactionsEventListener();



};

LBGAlchemy.view.intro  = function () {

	console.log("LBGAlchemy.view.intro()");

	// temp delayed
	LBGAlchemy.view.loader.delay(20).fadeOut();

	LBGAlchemy.func.onStageResize();
	

	// editable title
	console.log('yo', $(".masthead-inner").children().length);
	$(".masthead-inner h2").editable(function(value, setting){ return(value)}, { 
      indicator : "<img src='img/indicator.gif'>",
      tooltip   : "Click to edit...",
      style: "font-size: 0.25em"
  	});
  	$(".masthead-inner h2 input").css('color', '#ee0033');
};

LBGAlchemy.func.onStageResize = function () {

	var stageWidth = $(document).width();
	var stageHeight = $(document).height();

	LBGAlchemy.view.loader.css("padding-top", (stageHeight/2)-20); //160

	var minContentHeight = $("body").height() + 200;

	if(LBGAlchemy.view.contentInnerMain) {
		if(LBGAlchemy.view.contentInnerMain.height()>minContentHeight) minContentHeight = LBGAlchemy.view.contentInnerMain.height();
	}

	if(LBGAlchemy.view.contentInnerPanel) {
		LBGAlchemy.view.contentInnerPanel.css("min-height", minContentHeight);
	}

};

$(function() {


	LBGAlchemy.func.init();




});






// utils

LBGAlchemy.func.getQueryParams = function (qs) {
    qs = qs.split("+").join(" ");
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
};

LBGAlchemy.func.formatPound = function (num) {
    var p = num.toFixed(2).split(".");
    var p2 = p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

    if(p2.indexOf("-,")==0) {
    	p2 = "-" + p2.substr(2);
    }

    return "&pound;" + p2;

};

LBGAlchemy.func.getRandomizer = function ( bottom, top) {
    return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;
};

LBGAlchemy.func.flashInputField  = function ( field, txt ) {
	LBGAlchemy.func.animateHighlight($(field), "#f3d900", 1000);
	$(field).focus();
}

LBGAlchemy.func.animateHighlight = function(field, highlightColor, duration) {
    
    var highlightBg = highlightColor || "#000000";
    var animateMs = duration || 1000;
    var originalBg = field.css("backgroundColor");
    var originalTxt = field.css("color");
    var fi = $(field);

    fi.css("background-color", highlightBg);
    fi.css("color", "#ffffff");

    setTimeout(function () {
    	fi.css("background-color", originalBg);
    	fi.css("color", originalTxt)
    } , 500);

};

/*
* Function : sortDate(Date, Date)
* Description : Sorts an object based on date
*/
LBGAlchemy.func.sortDateDESCObj = function (a,b)
{

	var ats = a.date.split(" ")[1].split(":");
	var as = a.date.split(" ")[0].split("-");

	var bts = b.date.split(" ")[1].split(":");
	var bs = b.date.split(" ")[0].split("-");

	a = new Date(Number(as[0]), Number(as[1])-1, Number(as[2]), ats[0], ats[1], ats[2], 0);
	b = new Date(Number(bs[0]), Number(bs[1])-1, Number(bs[2]), bts[0], bts[1], bts[2], 0);

	//console.log(ats,bts);

	if (a.getTime() > b.getTime()) //sort string ascending
		return -1 
	if (a.getTime() < b.getTime())
	return 1

	return 0 //default return value (no sorting)

}

LBGAlchemy.func.inputOnlyNumbers = function () {
	return "onkeydown='return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )'";
}

LBGAlchemy.func.IsEmail = function (email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

