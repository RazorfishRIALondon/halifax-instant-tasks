(function() {

	var namespace 		= monkikiNS.getNamespace('webgltest.display');
	var EventDispatcher	= monkikiNS.getNamespace('generic.events').EventDispatcher;


	if (!namespace.BasicPage) {

		var BasicPage = function() {
			this.container = document.createElement("div");
			this.isOpened = false;
		}

		namespace.BasicPage = BasicPage;
		var p = BasicPage.prototype = new EventDispatcher();

		p.initialise = function() {

		}

		p.open = function(){

		}

		p.setOpened = function() {
			this.isOpened = true;
		}

		p.close = function() {

		}

		p.destroy = function(){

		}

		p._onResize = function() {

		}

		p.getContainer = function () {
			return this.container;
		}
	}


})();