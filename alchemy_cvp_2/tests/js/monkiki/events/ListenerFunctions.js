(function(){
	
	var namespace = monkikiNS.getNamespace('generic.events');
	
	if (!namespace.ListenerFunctions) {
		var onceList = {};

		var ListenerFunctions = function ListenerFunctions() {
			
		};

		namespace.ListenerFunctions = ListenerFunctions;

		ListenerFunctions.addDOMListener = function(aElement, aEvent, aCallback) {
			if(typeof aElement.addEventListener === 'function')
				aElement.addEventListener(aEvent, aCallback, false);			
			else if(typeof aElement.attachEvent === 'function')
				aElement.attachEvent('on' + aEvent, aCallback);
			else
				aElement['on' + aEvent] = aCallback;
		};


		ListenerFunctions.removeDOMListener = function(aElement, aEvent, aCallback) {
		
			if(typeof aElement.removeEventListener === 'function')
				aElement.removeEventListener(aEvent, aCallback, false);
			else if(typeof aElement.attachEvent === 'function')
				aElement.detachEvent('on' + aEvent, aCallback);
			else
				aElement['on' + aEvent] = null;
			
		};	

		ListenerFunctions.createListenerFunction = function(aListenerObject, aListenerFunction) {
			
			if (aListenerFunction === undefined){
				throw new Error("ERROR ListenerFunctions :: createListenerFunction :: callback function was null when called by :: ", aListenerObject);
			}
			
			var returnFunction = function dynamicListenerFunction() {
				aListenerFunction.apply(aListenerObject, arguments);
			};
			return returnFunction;
			
		};
	}



}());