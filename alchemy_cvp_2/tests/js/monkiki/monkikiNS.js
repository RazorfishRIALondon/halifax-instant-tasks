(function(aGlobalObject) {
	if(aGlobalObject.monkikiNS) {
		console.warn("monkiki global object has already been constructed. Thx B-Reel :P ");
	}

	var monkikiNS = {};
	aGlobalObject.monkikiNS = monkikiNS;
	monkikiNS.projectName = '';

	monkikiNS.getNamespace = function(aPackagePath) {

		var currentObject 		= this;

		var currentArray 		= aPackagePath.split(".");
		var currentArrayLength 	= currentArray.length;
		for(var i=0; i<currentArrayLength; i++) {
			var currentName = currentArray[i];
			if (currentObject[currentName] === undefined) {
				currentObject[currentName] 	= {};
			}
			currentObject = currentObject[currentName];
		}
		return currentObject;
	}

	monkikiNS.getClass = function(aClassPath) {

		var lastSplitPosition = aClassPath.lastIndexOf(".");
		var packagePath = aClassPath.substring(0, lastSplitPosition);
		var className = aClassPath.substring(lastSplitPosition+1, aClassPath.length);
		
		var packageObject = this.getNamespace(packagePath);
		if(packageObject[className] === undefined) {
			console.error("Class " + aClassPath + " doesn't exist.");
			return null;
		}
		return packageObject[className];
	};
	
	monkikiNS.singletons = new Object();


})(window);