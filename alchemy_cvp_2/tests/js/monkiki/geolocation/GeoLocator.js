(function() {

	var namespace = monkikiNS.getNamespace('generic.geolocation');
	var ListenerFunctions = monkikiNS.getNamespace("generic.events").ListenerFunctions;

	if (!namespace.GeoLocator) {

		var GeoLocator = function GeoLocator() {
			
			this.currentPos = {};

			this.Init();
		};

		namespace.GeoLocator = GeoLocator;

		var p = GeoLocator.prototype;

		p.Init = function() {
			console.log('GeoLocator Initialised!');
			if (navigator.geolocation) {

				this.getSuccessPosBound = ListenerFunctions.createListenerFunction(this, this.geoPosSuccessFunc);
				this.getFailPosBound 	= ListenerFunctions.createListenerFunction(this, this.geoPosFailFunc);

			}
			else {
				console.log('geolocation API is not present!');
			}
		}

		p.getPos = function() {
			navigator.geolocation.getCurrentPosition(this.getSuccessPosBound, this.getFailPosBound);
		}

		p.geoPosSuccessFunc = function(position) {
			console.log('geolocation success');
			this.currentPos.lat = position.coords.latitude;
			this.currentPos.lng = position.coords.longitude;
			this.currentPos.accuracy = (position.coords.accuracy)?position.coords.accuracy:null;
		}

		p.geoPosFailFunc = function(position) {
			console.log('geolocation fail')
		}

	}

})();