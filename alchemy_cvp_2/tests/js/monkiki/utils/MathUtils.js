(function() {

	var namespace = monkikiNS.getNamespace('generic.utils');

	if (!namespace.MathUtils) {
		var MathUtils = function() {

		}

		namespace.MathUtils = MathUtils;
		
		/*
		* Maps 'n' from the range 'oA'->'oB' to the linear range 'dA'->'dB'
		*/
		MathUtils.map = function ( n, oA, oB, dA, dB, constrain ) {
			if(constrain == undefined) constrain = false;
			var value = dA + ( n - oA ) * ( dB - dA ) / ( oB - oA );
			if(constrain) MathUtils.constrain(value, dA, dB);
			return value;
		};

		MathUtils.constrain = function(value, min, max) {
			if(value < min) return min;
			else if(value > max ) return max;
			else return value;
		};

	}





})(window);