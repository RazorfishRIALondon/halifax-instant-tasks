
(function(){

   var namespace = monkikiNS.getNamespace('generic.events');
   var ListenerFunctions = monkikiNS.getNamespace("generic.events").ListenerFunctions;
   var GeoLocator = monkikiNS.getNamespace("generic.geolocation").GeoLocator;


	var Index = function() {

		this.canvas;
		this.radius = 80;
		this.data = {lat: 0, lng: 0};
	}

	var colours = ['0x000000'];
	var p = Index.prototype;
	var fps = 0.5;

	p.Init = function() {

		var mD = document.getElementById('mainDiv');
		this.canvas = document.createElement('canvas');
		this.canvas.id = 'canvas';
		mD.appendChild(this.canvas);
		this.gl = this.canvas.getContext('2d');

		this.geoLocate = new GeoLocator();
		this.Open();
	}

	p.Open = function() {

		this.winResizeBound = ListenerFunctions.createListenerFunction(this, this.winResize);
		window.onresize = this.winResizeBound;
		this.then = Date.now();
		//create stuff
		this.createMap();
		this.indicator 		= document.createElement('div');
		this.indicator.id 	= 'indicator';
		this.h1Title 		= document.createElement('h1');
		this.h3rawData		= document.createElement('h3');
		document.body.appendChild(this.indicator);
		this.indicator.appendChild(this.h1Title);
		this.indicator.appendChild(this.h3rawData);

		this.h1Title.innerHTML 	= 'Mobile Geolocation Test';
		this.h3rawData.innerHTML = 'Geo';
		this.animate();
		this.winResize();

	}

	p.createMap = function() {
		this.mapcanvas = document.createElement('div');
		this.mapcanvas.id = 'mapcanvas';
		document.body.appendChild(this.mapcanvas);
		this.isShown = false;
	}


	p.draw = function(data) {
		this.geoLocate.getPos();
		this.h3rawData.innerHTML = 'Latitude: ' + this.geoLocate.currentPos.lat + ' , Longitude: ' + this.geoLocate.currentPos.lng;
		
		if (this.geoLocate.currentPos.lat && this.geoLocate.currentPos.lng) {
			console.log('start draw map');
			var latlng = new google.maps.LatLng(this.geoLocate.currentPos.lat, this.geoLocate.currentPos.lng);
			
			if (this.isShown) {
				var myOptions = {
				zoom: 15,
				center: latlng,
				mapTypeControl: false,
				navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
				mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById("mapcanvas"), myOptions);
				this.isShown = true;
			}

			var marker = new google.maps.Marker({
			  position: latlng, 
			  map: map, 
			  title:"You are here! (at least within a "+ this.geoLocate.currentPos.accuracy +" meter radius)"
			});
		}

	}

	p.animate = function() {
		//set frame rate
		var now =  Date.now();
		var delta = this.delta = now - this.then;
		var interval = 1000/fps;

		if (delta > interval) {
			this.then = now - (delta%interval)
			this.draw(this.then);
		}

		var that = this;
		this.requestAnimFrame(function(){that.animate()});
	}

	p.requestAnimFrame = function(func){
		// console.log('index requestAnimFrame: ');
		window.requestAnimationFrame(func);
	};

	p.winResize = function() {
		console.log('window resize');
		this.canvas.width = this.cX = window.innerWidth;
		this.canvas.height = this.cY = window.innerHeight;
		this.mapcanvas.style.width =  window.innerWidth+ 'px';
		this.mapcanvas.style.height = window.innerHeight + 'px';
		this.draw();
	}

	var index = new Index();
	index.Init();


})();



