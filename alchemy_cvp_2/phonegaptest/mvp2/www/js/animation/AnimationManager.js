(function(GlobalObj) {


	var namespace = GlobalObj.namespace.LBGAlchemy;

	if(!namespace.AnimationManager) {

		var AnimationManager = function AnimationManager() {
			this._init();
		};

		namespace.AnimationManager = AnimationManager;
		var p = AnimationManager.prototype;

		p._init = function() {
			this._onRequestAnimationFrameBound = null;

			return this;
		};

		p.setup = function() {

			var that = this;
			this._onRequestAnimationFrameBound = function() { that._onRequestAnimationFrame() };
			requestAnimFrame(this._onRequestAnimationFrameBound);
		};

		p._onRequestAnimationFrame = function() {
			TWEEN.update();
			requestAnimFrame(this._onRequestAnimationFrameBound);
		};

		AnimationManager.create = function() {
			var newAnimationManager = new AnimationManager();
			newAnimationManager.setup();
			return newAnimationManager;
		};
	}

})(window);