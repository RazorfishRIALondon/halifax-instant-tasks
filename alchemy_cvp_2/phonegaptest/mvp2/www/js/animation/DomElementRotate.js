(function(globalObj){
	
	var namespace = globalObj.namespace.LBGAlchemy;

	if(!namespace.DomElementRotate) {

		var DomElementRotate = function DomElementRotate() {

			this._init();
		};

		namespace.DomElementRotate = DomElementRotate;

		var p = DomElementRotate.prototype;

		p._init = function() {
			console.log('LGBAlchemy.DomElementRotate Initialised.')
			this._element 			= null;
			this._rotate 			= 0;
			var that 				= this;
			this._updateCallBack = function() { that._update() };

			this._tween = new TWEEN.Tween(this);
			this._tween.onUpdate(this._updateCallBack);
		}

		p.setElement = function(ele) {
			this._element = ele;
			return this;
		}

		p.getElement = function() {
			return this._element;
		}

		p.getRotate = function() {
			return this._rotate;
		};


		p.setStartRotate = function(aX) {
			this._rotate = aX;
			return this;
		};

		p.setCallback = function(aCallback) {
			this._tween.onComplete(aCallback);
		}


		p.animateTo = function(aHor, aTime, aEasing, aDelay) {

			this._tween.to({"_rotate": aHor}, aTime).easing(aEasing).delay(aDelay);
			this._tween.start();

			return this;
		}

		p.update = function() {
			this._update();
		}

		p._update = function() {

			var elementTransform = "rotate(" + this._rotate + "deg)";
			if(this._element !== null) {
				this._element.style.setProperty("-khtml-transform", elementTransform, "");
				this._element.style.setProperty("-moz-transform", elementTransform, "");
				this._element.style.setProperty("-ms-transform", elementTransform, "");
				this._element.style.setProperty("-webkit-transform", elementTransform, "");
				this._element.style.setProperty("-o-transform", elementTransform, "");
				this._element.style.setProperty("transform", elementTransform, "");
				// console.log(elementTransform);
			}	
		}

		p.destroy = function() {
			this._element = null;
			this._updateCallback = null;
			this._tween = null;
		};

		DomElementRotate.create = function(aElement, aStartX) {
			var newDomElementRotate = new DomElementRotate();
			newDomElementRotate.setElement(aElement);
			newDomElementRotate.setStartRotate(aStartX);
			return newDomElementRotate;
		}

		DomElementRotate.createWithAnimation = function(aElement, aStartX, aX, aTime, aEasing, aDelay, aCallback) {
			var newDomElementRotate = new DomElementRotate();
			newDomElementRotate.setElement(aElement);
			// console.log('callback', aCallback);
			if(typeof aCallback == 'function') newDomElementRotate.setCallback(aCallback);
			newDomElementRotate.setStartRotate(aStartX);
			newDomElementRotate.animateTo(aX, aTime, aEasing, aDelay);
			return newDomElementRotate;			
		}



	}

})(window);