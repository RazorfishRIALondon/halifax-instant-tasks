(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasic = LBGAlchemy.ModuleBasic;

    if (!LBGAlchemy.ModuleHeader) {

        LBGAlchemy.ModuleHeader = function() {

            this.templateHolder = null;
            this.Init();
        };

        LBGAlchemy.ModuleHeader.ALERTTAP = 'ModuleHeader.ALERTTAP';
        var p = LBGAlchemy.ModuleHeader.prototype = new ModuleBasic();

        p.Init = function() {

            this.template = [

                // '<img class="logo"/>'
                '<div class="clock"></div>',
                '<h1 class="centered">Lisbon</h1>',
                '<div id="alertbut" class="alertbut"></div>'

              ].join('');

        }

        p.Opened = function() {
            this.initListTouchEvent();
        }

        p.Show = function() {
            this.container.setAttribute('class', 'bar-title main-title');
        }

        p.Hide = function() {
            this.container.setAttribute('class', '');
        }


        p.initListTouchEvent = function(e) {

            var tmp = document.getElementById('alertbut');
            var that = this;
            var tapHandlerBind = function(e) { that.tapHandler(e) };
            var hammertime = Hammer(tmp).on("tap", tapHandlerBind);
        }

        p.tapHandler = function(e) {
            console.log('taphandler called inside');
            this.dispatchCustomEvent(LBGAlchemy.ModuleHeader.ALERTTAP);
        }

    }


})(window);