(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleLogin) {

        LBGAlchemy.ModuleLogin = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleLogin.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        LBGAlchemy.ModuleLogin.LOGIN = 'LBGAlchemy.ModuleLogin.LOGIN';
        LBGAlchemy.ModuleLogin.BACK = 'LBGAlchemy.ModuleLogin.BACK';
        LBGAlchemy.ModuleLogin.LOGINSUCCESS = 'LBGAlchemy.ModuleLogin.LOGINSUCCESS';

        p.Init = function() {
            this.pageId = 'login';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   

                '<div class="title-section"><div id="backtomain" class="backBut"></div><p>Banking Abroad</p></div>',
                '<div class="top-section" id="customerlogin">',
                '   <form>',
                '     <p>User ID</p>',
                '     <input type="text" placeholder="Full name">',
                '     <p>Password</p>',
                '     <input type="password" placeholder="Password">',
                '     <p>&nbsp;</p>',
                '     <a id="existinglogin" class="button button-block">Choose existing</a>',
                '   </form>',
                '</div>',
                '<div class="logo-section">',
                '   <div class="inner"></div>',
                '</div>'
            ].join('');       
        }

        p.OpenedAfter = function() {

            var dD = document.getElementById('backtomain');
            var lI = document.getElementById('existinglogin');
            var that = this;
            //back
            var tapBackHandlerBind = function(e) { that.tapBackHandler(e) };
            var hammertime = Hammer(dD).on("tap", tapBackHandlerBind);
            //login
            var tapLoginHandlerBind = function(e) { that.tapLoginHandler(e) };
            var hammertime = Hammer(lI).on("tap", tapLoginHandlerBind);

        }

        p.tapLoginHandler = function(e) {
            //make sure the child click get the same attention as well
            var sSrc = (e.target.nodeName==="A")?e.target:e.target.parentNode;
            // var tT = sSrc.getAttribute('data-link');

            // var opt = this.butOptions;
            var details = '';
            this.dispatchCustomEvent(LBGAlchemy.ModuleLogin.LOGIN, details);
        }

        p.CloseAfter = function() {
            var details = '';
            this.dispatchCustomEvent(LBGAlchemy.ModuleLogin.LOGINSUCCESS, details);
        }

        p.tapBackHandler = function(e) {
            //make sure the child click get the same attention as well
            var sSrc = (e.target.nodeName==="A")?e.target:e.target.parentNode;
            // var tT = sSrc.getAttribute('data-link');

            // var opt = this.butOptions;
            var details = '';
            this.dispatchCustomEvent(LBGAlchemy.ModuleLogin.BACK, details);
        }

    }

})(window);