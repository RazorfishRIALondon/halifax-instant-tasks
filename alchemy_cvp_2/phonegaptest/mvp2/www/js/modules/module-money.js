(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleMoney) {

        LBGAlchemy.ModuleMoney = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleMoney.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        p.Init = function() {
            this.pageId = 'money';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   
                '<div class="navi-section">',
                '   <ul>',
                '       <li>Expenses</li>',
                '       <li>Accounts</li>',
                '       <li>Exchange</li>',
                '   </ul>',
                '</div>'
            ].join('');       
        }


    }

})(window);