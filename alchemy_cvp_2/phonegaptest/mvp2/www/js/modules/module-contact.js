(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementRotate = LBGAlchemy.DomElementRotate;
    var dataManager;

    if (!LBGAlchemy.ModuleContact) {

        LBGAlchemy.ModuleContact = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.bankContact = [
                'General inquiries',
                'Credit card',
                'Lost card',
                'Insurance'
            ];

            this.localContact = [
                'Police',
                'Fire',
                'Ambulance',
                'Hospital',
                'Pharmacy',
                'British embassy'
            ];

            this.Init();
        };

        var p = LBGAlchemy.ModuleContact.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        p.Init = function() {
            this.pageId = 'contact';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   

                '<div class="title">',
                '   <h1>Bank Contacts</h1>',
                '</div>',
                this.displayBankContacts(),
                 '<div class="title top-spacer">',
                '   <h1>Emergency Contacts</h1>',
                '</div>',               
                this.displayLocalContacts()
            ].join('');       
        }

        p.OpenedAfter = function() {
            dataManager         = LBGAlchemy.singletons.dataManager;
            this.initialRotate();
        }

        p.displayBankContacts = function() {

            var bContact            = this.bankContact;
            var bankContactLength   = bContact.length;
            var out = '<ul>'
            for(var i=0; i<bankContactLength; i++) {
                out+='<li id="'+ bContact[i].toLowerCase().replace(' ', '') +'">' + bContact[i] + '<div class="arrow"></div></li>';
            }

            out += '</ul>';

            return out;
        }

        p.displayLocalContacts = function() {

            var bContact            = this.localContact;
            var localContactLength   = bContact.length;
            var out = '<ul>'
            for(var i=0; i<localContactLength; i++) {
                out+='<li id="'+ bContact[i].toLowerCase().replace(' ', '') +'">' + bContact[i] + '<div class="arrow"></div></li>';
            }

            out += '</ul>';

            return out;
        }

        p.initialRotate = function() {

            var bB              = document.getElementById(this.pageId);
            var arrow           = bB.getElementsByClassName('arrow');
            var arrowLength     = arrow.length;
            var that            = this;
            var listTapHandlerBind = function(e) { that.listTapHandler(e) };

            for(var i=0; i<arrowLength; i++) {
                // console.log(arrow[i]);
                var dD = DomElementRotate.createWithAnimation(arrow[i], 0, 180, 0, dataManager.tweenOptions.verticalTweenEasingType, 0);
                var hammertime = Hammer(arrow[i].parentNode).on("tap", listTapHandlerBind);
                // console.log(dD);
            }

        }

        p.listTapHandler = function(e) {
            var sSrc = (e.target.nodeName==="LI")?e.target:e.target.parentNode;
            var arr = sSrc.getElementsByClassName('arrow');
            console.log(arr);
            var dD = DomElementRotate.createWithAnimation(arr[0], 180, 90, 500, dataManager.tweenOptions.verticalTweenEasingType, 0);

        }


    }

})(window);