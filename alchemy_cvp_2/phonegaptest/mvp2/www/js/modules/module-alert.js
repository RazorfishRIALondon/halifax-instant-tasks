(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementOpacity = LBGAlchemy.DomElementOpacity;
    var dataManager;

    if (!LBGAlchemy.ModuleAlert) {

        LBGAlchemy.ModuleAlert = function() {

            this.templateHolder = null;
            this.isOpened       = null;
            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleAlert.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        p.Init = function() {
            this.pageId = 'alert';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   

                '<div id="alert-bg"></div>',
                '<div id="alert-messagebox">',
                '   <div class="alert-triangle"></div>',
                '   <div id="alert-message"><h3>Lloyds Bank</h3><p>Welcome to Lisbon, we’ve made sure your account works throughout Portugal.</p></div>',
                '</div>'
            ].join('');       
        }

        p.OpenedAfter = function() {
            dataManager         = LBGAlchemy.singletons.dataManager;
            this.isOpened       = false;
        }

        p.tapToggle = function() {
            var that = this;
            if (this.isOpened) {
                console.log('tapOpen from alert')
                var dD = DomElementOpacity.createWithAnimation(this.innerContainer, 1, 0, dataManager.tweenOptions.opacityTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, function(){ that.isOpened=false });

            }
            else {
                console.log('tapOpen open from alert')
                var dD = DomElementOpacity.createWithAnimation(this.innerContainer, 0, 1, dataManager.tweenOptions.opacityTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, function(){ that.isOpened=true });

            }

            return !this.isOpened;
        }

        p.tapClose = function() {
             if (this.isOpened) {
                var that = this;
                console.log('tapClose from alert')
                var dD = DomElementOpacity.createWithAnimation(this.innerContainer, 1, 0, dataManager.tweenOptions.opacityTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, function(){ that.isOpened=false });

            }           
        }


    }

})(window);