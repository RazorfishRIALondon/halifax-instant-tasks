(function(globalObj){

    var LBGAlchemy      = globalObj.namespace.LBGAlchemy;
    var EventDispatcher = LBGAlchemy.EventDispatcher;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var DomElementScale = LBGAlchemy.DomElementScale;
    var ModuleBasic     = LBGAlchemy.ModuleBasic;
    var dataManager;

    if (!LBGAlchemy.ModuleBasicPage) {

        LBGAlchemy.ModuleBasicPage = function() {

            this.templateHolder = null;
            this.Init();
            this.innerContainer = null;
            this.isDefault = false;
        };

        var p = LBGAlchemy.ModuleBasicPage.prototype = new ModuleBasic();
        var s = LBGAlchemy.ModuleBasic.prototype;


        p.Init = function() {

        }

        p.Opened = function(tDiv) {
            this.innerContainer = tDiv;
            dataManager = LBGAlchemy.singletons.dataManager;

            this.OpenedAfter(tDiv);
        }

        p.OpenedAfter = function(tDiv) {

        }


        //open and close from footer menu
        p.tapOpen = function() {
            this.tapOpenBefore();
            this.Show();
            var dD = DomElementPosition.createWithAnimation(this.innerContainer, 0, dataManager.browserOptions.browserHeight, 0, 0, dataManager.tweenOptions.verticalTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0);
        }

        p.tapOpenBefore = function() {
            
        }

        p.tapClose = function() {
            var that = this;
            this.CloseAfterTweenBind = function(e) { that.CloseAfterTween(e) };
            var dD = DomElementPosition.createWithAnimation(this.innerContainer, 0, 0, 0, dataManager.browserOptions.browserHeight, 0, dataManager.tweenOptions.verticalTweenEasingType, 0, this.CloseAfterTweenBind);
            // console.log(dD);
        }     

        p.CloseAfterTween = function(e) {
            this.Hide();
            this.CloseAfter();
        }

        p.CloseAfter = function() {
            
        }

        p.Show = function() {
            this.innerContainer.style.display = 'block';
        }

        p.Hide = function() {
            this.innerContainer.style.display = 'none';
        }

        p.getZIndex = function() {
            return this.container.style.zIndex;
        }

        p.setZIndex = function(z) {
            this.innerContainer.style.zIndex = z;
        } 


    }


})(window);