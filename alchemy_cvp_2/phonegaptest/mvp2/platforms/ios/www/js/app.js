(function(globalObj){


    if (!globalObj.namespace) {
        globalObj.namespace = {};
    }
    var ns = globalObj.namespace;

    if (!ns.LBGAlchemy) {
        ns.LBGAlchemy = {};
    }

    var dataManager;
    var LBGAlchemy    = ns.LBGAlchemy;

    if (!ns.LBGAlchemy.App) {

        LBGAlchemy.App   = function () {

            this.Init();
            this.beginpages = [];
            this.pages = [];
            this.overlays = [];
            this.currentPage = null;
        };

        var p = LBGAlchemy.App.prototype;

        p.Init = function() {
          console.log('LGBAlchemy.func.Init called.');

          var that = this;
          //load all libraries modules
          $LAB
          //external libraries
          .script("js/tween.js")
          .script("js/hammer.js")
          .script("js/canvasloader.js")
          .wait()
          .wait(function(){that.InitLoaderComplete()})

          //internal libraries
          .script("js/utils/polyfill.js")
          .script("js/events/EventDispatcher.js")
          .script("js/data/data-manager.js")
          .script("js/animation/AnimationManager.js")
          .script("js/animation/DomElementScale.js")
          .script("js/animation/DomElementPosition.js")
          .script("js/animation/DomElementOpacity.js")
          .script("js/animation/DomElementRotate.js")
          .script("js/animation/DomElementBG.js")
          .wait()
          //modules with pages
          .script("js/modules/module-basic.js")
          .script("js/modules/module-basicpage.js")
          .wait()
          .script("js/modules/module-loader.js")
          .script("js/modules/module-header.js")
          .script("js/modules/module-footer.js")
          .script("js/modules/module-main.js")
          .script("js/modules/module-login.js")
          .script("js/modules/module-home.js")
          .script("js/modules/module-profile.js")
          .script("js/modules/module-money.js")
          .script("js/modules/module-atm.js")
          .script("js/modules/module-contact.js")
          .script("js/modules/module-alert.js")
          .wait(function(){that.InitLoaderComplete()})
          .wait(function(){that.InitComplete()});
        }


        p.InitLoaderComplete = function() {
            console.log('LGBAlchemy.func.InitLoaderComplete called.');
            var nN = new LBGAlchemy.ModuleLoader();
            var lD = document.getElementById('loader');
            nN.Open(lD);
            console.log(nN);
            this.loader = nN;
            //loading disable temporary
        }

        p.InitComplete = function() {
          console.log('LGBAlchemy.func.InitComplete called.');

          this.loader.Close();


          dataManager = LBGAlchemy.DataManager.createSingleton();
            var bData = dataManager.setBrowserData({
                browserWidth: document.body.clientWidth,
                browserHeight: document.body.clientHeight,

            });

            var fData = dataManager.setFooterMenuData([

                'profile',
                'abroad',
                'money',
                'atm',
                'contact'
            ]);

          //begin the Tween Animation
          this.animationManager = LBGAlchemy.AnimationManager.create();         
          var that = this;

          //prepare HEADER
          var mM = this.mM = new LBGAlchemy.ModuleHeader();
          var hD = document.getElementById('head');
          mM.Open(hD);
          mM.addEventListener(LBGAlchemy.ModuleHeader.ALERTTAP, function(e){ that.alertTapHandler(e) });


          //prepare FOOTER
          var oO = this.oO = new LBGAlchemy.ModuleFooter();
          var fD = document.getElementById('foot');
          oO.Open(fD);
          oO.addEventListener(LBGAlchemy.ModuleFooter.MENUTAP, function(e){ that.menuTapHandler(e) });

          var dBegin    = this.dBegin = document.getElementById('begin');
          var dContent  = document.getElementById('content');


          //prepare LOGIN
          var lO = new LBGAlchemy.ModuleLogin();
          this.initBeginPage(lO, dBegin);
          lO.addEventListener(LBGAlchemy.ModuleLogin.LOGIN, function(e){ that.loginExistingHandler(e) });
          lO.addEventListener(LBGAlchemy.ModuleLogin.BACK, function(e){ that.loginBackHandler(e) });
          lO.addEventListener(LBGAlchemy.ModuleLogin.LOGINSUCCESS, function(e){ that.loginExistingSuccessHandler(e) });



          //prepare MAIN
          var mA = new LBGAlchemy.ModuleMain();
          mA.setIsDefault(true);
          this.initBeginPage(mA, dBegin);
          mA.addEventListener(LBGAlchemy.ModuleMain.LBGCUSTOMERTARGET, function(e){ that.mainLBGHandler(e) });
          mA.addEventListener(LBGAlchemy.ModuleMain.NONLBGCUSTOMERTARGET, function(e){ that.mainNonLBGHandler(e) });

          //prepare PROFILE
          var pR = new LBGAlchemy.ModuleProfile();
          this.initPage(pR, dContent);

          //prepare HOME
          var h1 = new LBGAlchemy.ModuleHome();
          h1.setIsDefault(true);
          this.initPage(h1, dContent);

          //prepare MONEY
          var mO = new LBGAlchemy.ModuleMoney();
          this.initPage(mO, dContent);

          //prepare ATM
          var aT = new LBGAlchemy.ModuleAtm();
          this.initPage(aT, dContent);

          //prepare PROFILE
          var cO = new LBGAlchemy.ModuleContact();
          this.initPage(cO, dContent);

          //prepare ALERT
          var aL = new LBGAlchemy.ModuleAlert();
          this.initOverlay(aL, dContent);

          //window resize
          this.onresizeFunc();
          var that = this;
          window.onresize = function(){ that.onresizeFunc(); };
        }

        //resize all pages
        p.onresizeFunc = function() {

          var pP        = this.pages;
          var pPLength  = this.pages.length;
          for(var i=0; i<pPLength; i++) {
            console.log()
            if (typeof pP[i].onresize ==='function') pP[i].onresize();
          }
        }

        //init begin pages
        p.initBeginPage = function(pObj, el) {
          pObj.Open(el);
          this.beginpages.push(pObj);          
        }

        //init all pages
        p.initPage = function(pObj, el) {

          pObj.Open(el);
          this.pages.push(pObj);
        }

        //init overlays
        p.initOverlay = function(pObj, el) {

          pObj.Open(el);
          this.overlays.push(pObj);
        }

        p.closeAllOverlay = function() {
          var pP        = this.overlays;
          var pPLength  = this.overlays.length;
          for(var i=0; i<pPLength; i++) {
            pP[i].tapClose();
          }   
        }

        p.alertTapHandler = function(e) {
          console.log('alert Tap to outside');
          //check if something is tweening or it is current then return
          var currentTween = TWEEN.getAll().length;
          if (currentTween>0) return;

          var pP    = this.overlays;
          var isOpen = pP[0].tapToggle();
          this.resetOverlayZIndex(pP[0], isOpen);
        }

        p.mainLBGHandler = function(e) {

            var pP = this.beginpages;
            pP[1].tapClose();
        }

        p.mainNonLBGHandler = function(e) {
          console.log('mainNonLBG found');
          var pP = this.beginpages;
          pP[0].tapClose();

        }

        //LOGIN PAGE HANDLER
        p.loginExistingHandler = function(e) {
            var pP = this.beginpages;
            pP[0].tapClose();       
        }

        p.loginExistingSuccessHandler = function(e) {
            // hide the login section
            this.dBegin.style.display = 'none';
            this.mM.Show();  
        }

        p.loginBackHandler = function(e) {
            var pP = this.beginpages;
            pP[1].tapOpen();      
        }

        //menu handler
        p.menuTapHandler = function(e) {

          //check if something is tweening or it is current then return
          var currentTween = TWEEN.getAll().length;
          if (currentTween>0 || e.detail==this.currentPageName ||  e.detail==null) return;

          var opt = dataManager.getFooterMenuData();
          var pP = this.pages;

          switch(e.detail) {
            case opt[0]:
              //home
              pP[0].tapOpen();
              this.resetZIndex(pP[0]);
              this.currentPage = pP[0]; 
              this.oO.moveTriangle(0);
              this.oO.tapChanger(0);
              break;
            case opt[1]:
              //profile
              pP[1].tapOpen();
              this.resetZIndex(pP[1]);
              this.currentPage = pP[1]; 
              this.oO.moveTriangle(1);
              this.oO.tapChanger(1);
              break;
            case opt[2]:
              //money
              pP[2].tapOpen();
              this.resetZIndex(pP[2]);
              
              this.currentPage = pP[2]; 
              this.oO.moveTriangle(2);
              this.oO.tapChanger(2);
              break;
            case opt[3]:
              //atm
              pP[3].tapOpen();
              this.resetZIndex(pP[3]);
              
              this.currentPage = pP[3]; 
              this.oO.moveTriangle(3);
              this.oO.tapChanger(3);
              break;
            case opt[4]:
              //contact
              pP[4].tapOpen();
              this.resetZIndex(pP[4]);
              
              this.currentPage = pP[4]; 
              this.oO.moveTriangle(4);
              this.oO.tapChanger(4);
              break;
          }

          console.log('switch current page', this.currentPage)
          this.closeAllOverlay();
          this.currentPageName = e.detail;
        }

        p.resetZIndex = function(current) {
          var pP        = this.pages;
          var pPLength  = this.pages.length;
          console.log('current page', this.currentPage);
          for(var i=0; i<pPLength; i++) {

            //close tap of not current or previous page
            if (pP[i]!=current && pP[i]!=this.currentPage)    pP[i].tapClose(); 

            //set the current and previous page to top and second to top then rest are at the bottom
            if (pP[i]===current)                              current.setZIndex(99);
            else if (pP[i]==this.currentPage)                 pP[i].setZIndex(98);
            else                                              pP[i].setZIndex(i);

          }
        }

        p.resetOverlayZIndex = function(current, isOpen) {
          var pP        = this.overlays;
          var pPLength  = this.overlays.length;
          for(var i=0; i<pPLength; i++) {

            if (isOpen) pP[i].setZIndex(i+100); //set to be higher than page
            else        pP[i].setZIndex(i+10);  //set slightly higher page

          }                  
        }
    }


})(window);

if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
    alert('ready');
    document.addEventListener("deviceready", function(){

      var lbgalchemyApp = new window.namespace.LBGAlchemy.App();
    }, false);
} else {
    var lbgalchemyApp = new window.namespace.LBGAlchemy.App();
}


