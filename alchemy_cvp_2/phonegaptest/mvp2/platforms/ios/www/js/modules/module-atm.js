(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleAtm) {

        LBGAlchemy.ModuleAtm = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.map = null;
            this.Init();
        };

        var p = LBGAlchemy.ModuleAtm.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype; 

        p.Init = function() {

            this.pageId = 'atm';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   
                '<div id="map-canvas"></div>'
            ].join('');   

            this.loadScript();    
        }

        window.initMap = function() {
            console.log('init map calllll', google.maps);
            var mapOptions = {
                zoom: 8,
                center: new google.maps.LatLng(-34.397, 150.644),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
            };
            this.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);       
        }

        p.mapReady = function() {
            this.map = null;
            console.log('close map');
        }

        p.OpenedAfter = function() {
        }

        p.CloseAfter = function(e) {
            
        }


        p.tapOpenBefore = function() {
            // this.loadScript();
            // var that = this;
        }

        p.loadScript = function() {
            var that = this;
            $LAB
            .script("http://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBMqS6UQwcOrwcfEOXG7JkhsL1iLJ39PgI&sensor=true&callback=window.initMap")
            .wait(function(){that.mapReady()});
          // var script = document.createElement("script");
          // script.type = "text/javascript";
          // script.src = "http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true";
          // document.body.appendChild(script);
        }


    }

})(window);