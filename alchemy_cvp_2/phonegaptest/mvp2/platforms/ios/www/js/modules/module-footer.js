(function(globalObj){

    var LBGAlchemy          = globalObj.namespace.LBGAlchemy;
    var ModuleBasic         = LBGAlchemy.ModuleBasic;
    var dataManager;

    if (!LBGAlchemy.ModuleFooter) {

        LBGAlchemy.ModuleFooter = function() {

            this.templateHolder = null;
            this.butOptions = [
                'footprofile',
                'footabroad',
                'footmoney',
                'footatm',
                'footcontact'
            ];
            this.triangleId = "triangle";
            this.Init();
        };

        LBGAlchemy.ModuleFooter.MENUTAP = 'ModuleFooter.MENUTAP';
        var p = LBGAlchemy.ModuleFooter.prototype = new ModuleBasic();

        p.Init = function() {

            var listOpt = this.listCreator();
            console.log('ModuleFooter init: ');
            this.template = [

                '<div class="inner">',
                '    <ul id="footermenu" class="footermenu">',
                        listOpt,
                '    </ul>',
                '</div>',
                ''

              ].join('');

        }


        p.Opened = function() {
            dataManager = LBGAlchemy.singletons.dataManager;
            this.initTriangle(this.container);
            this.initListTouchEvent();
        }

        p.listCreator = function() {
            var list    = this.butOptions;
            listLength  = this.butOptions.length;
            var out = '';
            for(var i=0; i<listLength; i++) {
                out+='<li id="' + list[i] + '"" data-state="' + list[i].replace('foot', '') + '""><div id="icon'+list[i].replace('foot', '')+'"></div><h6>'+list[i].replace('foot', '')+'</h6></li>';
            }
            return out;
        }

        p.initTriangle = function(el) {
            var tD = document.createElement('div');
            tD.id = this.triangleId;
            listLength  = this.butOptions.length;

            var wW = Math.round(el.clientWidth/listLength);
            var hH = Math.round(el.clientHeight*.2);
            tD.style.borderLeftWidth    = wW/2 +'px';
            tD.style.borderRightWidth   = wW/2 +'px';
            tD.style.borderTopWidth     = hH + 'px';
            tD.style.marginTop          = -hH/2 + 'px';
            // console.log('border', wW, tD.style.borderRightWidth, hH)
            el.appendChild(tD);                     
        }

        p.moveTriangle = function(num) {
            el          = this.container;
            listLength  = this.butOptions.length;
            var tI = document.getElementById(this.triangleId);
            tI.style.left = num * Math.round(el.clientWidth/listLength) + 'px';
        }

        p.initListTouchEvent = function() {
            var list    = this.butOptions;
            listLength  = this.butOptions.length;
            var that = this;
            var tapHandlerBind = function(e) { that.tapHandler(e) };
            for(var i=0; i<listLength; i++) {
                var tmp = document.getElementById(list[i]);
                var hammertime = Hammer(tmp).on("tap", tapHandlerBind);
            }
        }

        p.tapHandler = function(e) {
            //make sure the child click get the same attention as well
            var sSrc = (e.target.nodeName==="LI")?e.target:e.target.parentNode;
            var tT = sSrc.getAttribute('data-state');

            var opt = this.butOptions;
            var details = tT;
            this.dispatchCustomEvent(LBGAlchemy.ModuleFooter.MENUTAP, details);
        }

        p.tapChanger = function(no) {
            if (no==null) return;
            var list    = this.butOptions;
            for(var i=0; i<listLength; i++) {
                var tmp = document.getElementById(list[i]);
                if (no==i)      { tmp.style.opacity = 1; }
                else            tmp.style.opacity = 0.5;
            }
        }

    }


})(window);