(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleMain) {

        LBGAlchemy.ModuleMain = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleMain.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        LBGAlchemy.ModuleMain.LBGCUSTOMERTARGET = 'LBGAlchemy.ModuleMain.LBGCUSTOMERTARGET';
        LBGAlchemy.ModuleMain.NONLBGCUSTOMERTARGET = 'LBGAlchemy.ModuleMain.NONLBGCUSTOMERTARGET';

        p.Init = function() {
            this.pageId = 'main';
            this.pageClass = 'page-container';
            this.options = {
                'cusTarget': 'lbgcustomer',
                'nonTarget': 'nonlbgcustomer',
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   

                '<div class="top-section" id="lbgcustomer">',
                '   <h1>Log into Banking Abroad</h1>',
                '   <h3>Lloyds account holders</h3>',
                '</div>',
                '<div class="bottom-section" id="nonlbgcustomer">',
                '   <h1>Get Lloyds Travel Insurance</h1>',
                '   <h3>From &pound;4.99 a day</h3>',
                '</div>',
                '<div class="logo-section">',
                '   <div class="inner"></div>',
                '</div>'
            ].join('');       
        }

        p.OpenedAfter = function() {

            dataManager         = LBGAlchemy.singletons.dataManager;

            //customer
            var cus = document.getElementById('lbgcustomer');
            cus.setAttribute('data-link', this.options.cusTarget);

            console.log('hello from main', cus, this.options.cusTarget);

            //non customer
            var non = document.getElementById('nonlbgcustomer');
            non.setAttribute('data-link', this.options.nonTarget);

            var that = this;
            var tapHandlerBind = function(e) { that.tapHandler(e) };
            var cusHammer = Hammer(cus).on("tap", tapHandlerBind);
            var nonHammer = Hammer(non).on("tap", tapHandlerBind);
        }

        p.tapHandler = function(e) {
            var sSrc = (e.target.nodeName==="DIV")?e.target:e.target.parentNode;
            var sLink = sSrc.getAttribute('data-link');
            switch(sLink) {
                case 'lbgcustomer':
                    console.log('lbg');
                    this.dispatchCustomEvent(LBGAlchemy.ModuleMain.LBGCUSTOMERTARGET);
                    break;
                case 'nonlbgcustomer':
                    console.log('non');
                    this.dispatchCustomEvent(LBGAlchemy.ModuleMain.NONLBGCUSTOMERTARGET);
                    break;
            }
        }

        p.tapClose = function() {

            var that = this;
            this.CloseAfterTweenBind = function(e) { that.CloseAfterTween(e) };
            var dD = DomElementPosition.createWithAnimation(this.innerContainer, 0, 0, 0, dataManager.browserOptions.browserHeight, dataManager.tweenOptions.verticalTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, this.CloseAfterTweenBind);           
        }



    }

})(window);