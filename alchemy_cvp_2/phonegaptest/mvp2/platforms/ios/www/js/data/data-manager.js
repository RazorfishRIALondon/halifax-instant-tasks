(function(globalObj){

    var LBGAlchemy      = globalObj.namespace.LBGAlchemy;
    var EventDispatcher = LBGAlchemy.EventDispatcher;

    if (!LBGAlchemy.DataManager) {

 		LBGAlchemy.DataManager = function() {

            //STATIC OPTIONS
            this.tweenOptions = {
                verticalTweenEasingType:    TWEEN.Easing.Quintic.InOut,
                verticalTweenSpeed:         500,
                horizontalTweenEasingType:  TWEEN.Easing.Elastic.InOut,
                opacityTweenSpeed:          250
            };

 			this.browserOptions = {
                browserWidth: 0,
                browserHeight: 0,


            };

            this._footerMenuOptions = [];
        };

        var p = LBGAlchemy.DataManager.prototype;  

        p.Setup = function() {
        	console.log('DataManager init');
        } 	

        p.setFooterMenuData = function(eE) {
            this._footerMenuOptions = eE;
        }

        p.getFooterMenuData = function() {
            return this._footerMenuOptions;
        }

        p._initRemoteServerData = function() {

        }

        p._initLocalServerData = function() {

        }

        p.setBrowserData = function(options) {
        	var opt = window.mergeObjects(this.browserOptions, options);
        	this.browserOptions = opt;
        	return opt;
        }

		LBGAlchemy.DataManager.createSingleton = function() {
			if (!LBGAlchemy.singletons) LBGAlchemy.singletons = {};
			if (!LBGAlchemy.singletons.dataManager)
			{
				LBGAlchemy.singletons.dataManager = new LBGAlchemy.DataManager();
				LBGAlchemy.singletons.dataManager.Setup();
			}	
			return LBGAlchemy.singletons.dataManager;		
		};

    }


})(window);