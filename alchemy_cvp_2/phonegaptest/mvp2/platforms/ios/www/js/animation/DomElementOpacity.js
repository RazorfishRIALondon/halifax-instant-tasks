(function(globalObj){
	
	var namespace = globalObj.namespace.LBGAlchemy;

	if(!namespace.DomElementOpacity) {

		var DomElementOpacity = function DomElementOpacity() {

			this._init();
		};

		namespace.DomElementOpacity = DomElementOpacity;

		var p = DomElementOpacity.prototype;

		p._init = function() {
			console.log('LGBAlchemy.DomElementOpacity Initialised.')
			this._element 			= null;
			this._x 				= 0;
			this._y 				= 0;
			var that 				= this;
			this._updateCallBack = function() { that._update() };

			this._tween = new TWEEN.Tween(this);
			this._tween.onUpdate(this._updateCallBack);
		}

		p.setElement = function(ele) {
			this._element = ele;
			return this;
		}

		p.getElement = function() {
			return this._element;
		}

		p.getOpacity = function() {
			return this._opacity;
		};

		p.setStartOpacity = function(aX) {
			this._opacity = aX;
			return this;
		};

		p.setCallback = function(aCallback) {
			this._tween.onComplete(aCallback);
		}


		p.animateTo = function(aHor, aTime, aEasing, aDelay) {

			this._tween.to({"_opacity": aHor}, aTime).easing(aEasing).delay(aDelay);
			this._tween.start();

			return this;
		}

		p.update = function() {
			// console.log('opacity updating');
			this._update();
		}

		p._update = function() {

			var elementTrans 		= this._opacity.toFixed(2);
			var msElementTrans 		= "alpha(opacity="+ this._opacity*100 +")";

			if(this._element !== null) {
				this._element.style.setProperty("-khtml-opacity", elementTrans, "");
				this._element.style.setProperty("filter", msElementTrans, "");
				this._element.style.setProperty("opacity", elementTrans, "");
				console.log(elementTrans);
			}	
		}

		p.destroy = function() {
			this._element = null;
			this._updateCallback = null;
			this._tween = null;
		};

		DomElementOpacity.create = function(aElement, aStartX) {
			var newDomElementOpacity = new DomElementOpacity();
			newDomElementOpacity.setElement(aElement);
			newDomElementOpacity.setStartOpacity(aStartX, aStartY);
			return newDomElementOpacity;
		}

		DomElementOpacity.createWithAnimation = function(aElement, aStartX, aX, aTime, aEasing, aDelay, aCallback) {
			var newDomElementOpacity = new DomElementOpacity();
			newDomElementOpacity.setElement(aElement);
			if(typeof aCallback == 'function') newDomElementOpacity.setCallback(aCallback);
			newDomElementOpacity.setStartOpacity(aStartX);
			newDomElementOpacity.animateTo(aX, aTime, aEasing, aDelay);
			return newDomElementOpacity;			
		}



	}

})(window);