(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasic = LBGAlchemy.ModuleBasic;

    if (!LBGAlchemy.ModuleLoader) {

        LBGAlchemy.ModuleLoader = function() {

            this.templateHolder = null;
            this.Init();
        };

        LBGAlchemy.ModuleLoader.CloseComplete = "LBGAlchemy.ModuleLoader.CloseComplete";

        var p = LBGAlchemy.ModuleLoader.prototype = new ModuleBasic();

        p.Init = function() {

                console.log('loader init');
                this.template = [

                    '<div id="main-loader-logo">',
                    '<img src="img/header-logo.png" width="250" height="51" />',
                    '</div>',
                    '<div id="main-loader-spinner">',
                    '</div>'

                ].join('');

        }

        p.Opened = function() {
                var cl = new CanvasLoader("main-loader-spinner");
                cl.setColor('#ffffff'); // default is '#000000'
                cl.setShape('spiral'); // default is 'oval'
                cl.setDiameter(60); // default is 40
                cl.setDensity(14); // default is 40
                cl.setSpeed(1); // default is 2
                cl.setFPS(30); // default is 24
                cl.show(); // Hidden by default
                this.cl = cl;
        }

        p.Close = function() {

            //temp
            this.CloseComplete();
        }

        p.CloseComplete = function() {
            this.templateHolder.style.display = 'none';
            this.cl.hide();
            this.dispatchEvent(LBGAlchemy.ModuleLoader.CloseComplete);
        }
    }


})(window);