(function(globalObj){

    var LBGAlchemy              = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage             = LBGAlchemy.ModuleBasicPage;
    var DomElementPosition      = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleProfile) {

        LBGAlchemy.ModuleProfile = function() {

            this.templateHolder = null;

            this.options = {
                'idName': 'profile',
                'className': 'profile',
            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleProfile.prototype = new ModuleBasicPage();
        var s = LBGAlchemy.ModuleBasicPage.prototype;

        p.Init = function() {

            this.template = [


                

              ].join('');

        }

        //setting the list id and class
        p.Opened = function(tDiv, opt) {
            s.Opened(tDiv, this.options);
            var tCon = this.container;
            dataManager = LBGAlchemy.singletons.dataManager;
            // set size
            this.onresize();

            var that = this;
            var tapHandlerBind = function(e) {that.tapHandler(e)};


        }

        //resize the list
        p.onresize = function() {
            this._conWidth = this.container.clientWidth;
            this._conHeight = this.container.clientHeight;
        }

        p.tapHandler = function(e) {
            console.log('haha')
            this.verTapOpen(this.container);
        }


    }


})(window);