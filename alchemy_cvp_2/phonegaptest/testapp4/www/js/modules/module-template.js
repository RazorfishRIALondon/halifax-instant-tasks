(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasic = LBGAlchemy.ModuleBasic;

    if (!LBGAlchemy.ModuleTemplate) {

        LBGAlchemy.ModuleTemplate = function() {

            this.templateHolder = null;
            this.Init();
        };

        var p = LBGAlchemy.ModuleTemplate.prototype = new ModuleBasic();

        p.Init = function() {

            this.template = [

                '<div class="content-padded">',
                '  <p class="welcome">Thanks for downloading Ratchet. This is an example HTML page that\'s linked up to compiled Ratchet CSS and JS, has the proper meta tags and the HTML structure. Need some more help before you start filling this with your own content? Check out some Ratchet resorces:</p>',
                '</div>',
                '',
                '<ul class="list inset">',
                '  <li>',
                '    <a data-link="http://maker.github.com/ratchet/">',
                '      <strong>Ratchet documentation</strong>',
                '      <span class="chevron"></span>',
                '    </a>',
                '  </li>',
                '  <li>',
                '    <a data-link="http://www.github.com/maker/ratchet/">',
                '      <strong>Ratchet on Github</strong>',
                '      <span class="chevron"></span>',
                '    </a>',
                '  </li>',
                '  <li>',
                '    <a data-link="https://groups.google.com/forum/#!forum/goratchet">',
                '      <strong>Ratchet Google group</strong>',
                '      <span class="chevron"></span>',
                '    </a>',
                '  </li>',
                '  <li>',
                '    <a data-link="http://www.twitter.com/GoRatchet">',
                '      <strong>Ratchet on Twitter</strong>',
                '      <span class="chevron"></span>',
                '    </a>',
                '  </li>',
                '</ul>'

              ].join('');

        }
    }


})(window);