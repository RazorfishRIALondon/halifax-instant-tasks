(function(globalObj){

    var LBGAlchemy              = globalObj.namespace.LBGAlchemy;
    var ModuleBasic             = LBGAlchemy.ModuleBasic;
    var DomElementScale         = LBGAlchemy.DomElementScale;

    if (!LBGAlchemy.ModuleBalance) {

        LBGAlchemy.ModuleBalance = function() {

            this.templateHolder = null;

            this.options = {
                'idName': 'balance-section',
                'className': 'balance mid-section'
            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleBalance.prototype = new ModuleBasic();
        var s = LBGAlchemy.ModuleBasic.prototype;

        p.Init = function() {

            this.template = [

                '    <ul class="list">',
                '      <li>List item 1</li>',
                '      <li>List item 2</li>',
                '      <li>List item 3</li>',
                '      <li>List item 4</li>',
                '      <li class="list-divider">List Divider</li>',
                '      <li>List item 3</li>',
                '    </ul>',

                ''

              ].join('');

        }



        //setting the list id and class
        p.Opened = function(tDiv) {
            var opt = this.options;
            if (!(tDiv && opt)) return;
            this.listDiv = tDiv;
            tDiv.id = opt.idName;
            tDiv.setAttribute('class', opt.className);
            //set size
            this.onresize();

            //scale tween
            this.sTween = new DomElementScale.createWithAnimation(tDiv, 1, 0, 1, 1, true, 250, TWEEN.Easing.Quadratic.Out, 0, null);
            console.log('testing');
        }

        //resize the list
        p.onresize = function() {
            var tDiv            = this.listDiv;
            var tDivHeight      = tDiv.clientHeight;
            var liList          = tDiv.getElementsByTagName('li');
            var liListLength    = liList.length;

            for(var i=0; i<liListLength; i++) {
                var padTop = parseInt(window.getStyle(liList[i], 'paddingTop').replace('px'));
                var padBot = parseInt(window.getStyle(liList[i], 'paddingBottom').replace('px'))
                liList[i].style.height = Math.floor((tDivHeight/liListLength) - padTop - padBot) + 'px';
            }

        }

    }


})(window);