(function(globalObj){

    var LBGAlchemy          = globalObj.namespace.LBGAlchemy;
    var ModuleBasic         = LBGAlchemy.ModuleBasic;
    var dataManager;

    if (!LBGAlchemy.ModuleFooter) {

        LBGAlchemy.ModuleFooter = function() {

            this.templateHolder = null;
            this.butOptions = [
                'foothome',
                'footprofile',
                'footmoney',
                'footatm',
                'footcontact'
            ];
            this.Init();
        };

        LBGAlchemy.ModuleFooter.MENUTAP = 'ModuleFooter.MENUTAP';
        var p = LBGAlchemy.ModuleFooter.prototype = new ModuleBasic();

        p.Init = function() {

            var listOpt = this.listCreator();
            console.log('ModuleFooter init: ');
            this.template = [

                '<div class="inner">',
                '    <ul id="footermenu" class="footermenu">',
                        listOpt,
                '    </ul>',
                '</div>',
                ''

              ].join('');

        }


        p.Opened = function() {
            dataManager = LBGAlchemy.singletons.dataManager;

            this.initListTouchEvent();
        }

        p.listCreator = function() {
            var list    = this.butOptions;
            listLength  = this.butOptions.length;
            var out = '';
            for(var i=0; i<listLength; i++) {
                out+='<li id="' + list[i] + '"" data-state="' + list[i].replace('foot', '') + '""></li>';
            }
            return out;
        }

        p.initListTouchEvent = function() {
            var list    = this.butOptions;
            listLength  = this.butOptions.length;
            var that = this;
            var tapHandlerBind = function(e) { that.tapHandler(e) };
            for(var i=0; i<listLength; i++) {
                var tmp = document.getElementById(list[i]);
                var hammertime = Hammer(tmp).on("tap", tapHandlerBind);
            }
        }

        p.tapHandler = function(e) {
            var tT = e.target.getAttribute('data-state');
            var opt = this.butOptions;
            var details = tT;
            this.dispatchCustomEvent(LBGAlchemy.ModuleFooter.MENUTAP, details);
        }

    }


})(window);