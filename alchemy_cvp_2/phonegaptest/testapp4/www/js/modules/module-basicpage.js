(function(globalObj){

    var LBGAlchemy              = globalObj.namespace.LBGAlchemy;
    var ModuleBasic             = LBGAlchemy.ModuleBasic;
    var DomElementPosition      = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleBasicPage) {

        LBGAlchemy.ModuleBasicPage = function() {

            this.templateHolder = null;
            this.options = {};
            this.Init();
        };

        var p = LBGAlchemy.ModuleBasicPage.prototype = new ModuleBasic();
        var s = LBGAlchemy.ModuleBasic.prototype;

        p.Init = function() {

            this.template = [

              ].join('');

        }

        p.Opened = function(tDiv, opt) {
            dataManager = LBGAlchemy.singletons.dataManager;
            this.tweenType   = dataManager.tweenOptions.verticalTweenEasingType;
            this.bHeight     = dataManager.browserOptions.browserHeight;
            this.tweenSpeed  = dataManager.tweenOptions.verticalTweenSpeed;
            console.log('hahahahahahahahah')
            this.setPageDiv(tDiv, opt);
            console.log('hahahahahahahahah')
            //set position to 0px if defailt
            console.log('dom testing', this.isDefault);
            // if (this.isDefault) {
                dataManager      = LBGAlchemy.singletons.dataManager;
                var tweenType   = dataManager.tweenOptions.verticalTweenEasingType;
                var bHeight     = dataManager.browserOptions.browserHeight;
                var tweenSpeed  = dataManager.tweenOptions.verticalTweenSpeed;
                console.log('hjaha wowowow', tDiv);
                var dD     = new DomElementPosition.createWithAnimation(tDiv, 0, 800, 0, 0, 500, null, 1000, null);
                console.log('hohohoh', dD)
            // } 
        }

        //set id and class for page div
        p.setPageDiv = function(tDiv, opt) {
            console.log(opt, tDiv)
            if (!(tDiv && opt)) return;
            this.listDiv = tDiv;
            if (opt.idName)     tDiv.id = opt.idName;
            if (opt.className)  tDiv.setAttribute('class', opt.className);
        }

        p.verTapOpen = function(ele) {
            this.sTween     = new DomElementPosition.createWithAnimation(ele, 0, -this.bHeight, 0, 0, this.tweenSpeed, this.tweenType, 1000, null);

        }

        p.verTapClose = function(ele) {

        }


        p.dragStartHandler = function(e) {
            console.log('test drag start');
        }
        //touch handler
        p.dragEndHandler = function(e) {
            var cW = Math.round(this._conWidth*2/3);
            var pX = this.prevX;
            var con = this.container;
            var time = this.options.time;
            if (pX>cW)  this.sTween = new DomElementPosition.createWithAnimation(con, pX-this._conWidth, 0, 0, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);
            else        this.sTween = new DomElementPosition.createWithAnimation(con, pX-this._conWidth, 0, -this._conWidth, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);
 
        }

        p.dragHandler = function(e) {
            var time = this.options.time;
            var cW = this._conWidth;
            var con = this.container;
            var pX = this.prevX;
            var cX = this.prevX = e.gesture.touches[0].pageX?e.gesture.touches[0].pageX:0;
            console.log(pX, cX, time);
            this.sTween = new DomElementPosition.createWithAnimation(con, pX-cW, 0, cX-cW, 0, time, TWEEN.Easing.Quadratic.Out, 150, null);

        }

    }


})(window);