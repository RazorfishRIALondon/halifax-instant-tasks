(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasic = LBGAlchemy.ModuleBasic;

    if (!LBGAlchemy.ModuleHeader) {

        LBGAlchemy.ModuleHeader = function() {

            this.templateHolder = null;
            this.Init();
        };

        var p = LBGAlchemy.ModuleHeader.prototype = new ModuleBasic();

        p.Init = function() {

            this.template = [

                // '<img class="logo"/>'
                '<h1 class="centered">Lisbon</h1>'

              ].join('');

        }

    }


})(window);