//window getStyle for inline CSS Style

if(window.getStyle == undefined) {

	//function to get inline CSS style
	window.getStyle = function(elem, name) {
	    // J/S Pro Techniques p136
	    if (elem.style[name]) {
	        return elem.style[name];
	    } else if (elem.currentStyle) {
	        return elem.currentStyle[name];
	    }
	    else if (document.defaultView && document.defaultView.getComputedStyle) {
	        name = name.replace(/([A-Z])/g, "-$1");
	        name = name.toLowerCase();
	        s = document.defaultView.getComputedStyle(elem, "");
	        return s && s.getPropertyValue(name);
	    } else {
	        return null;
	    }
	}

}


// Request animation frame polyfill

if(window.requestAnimFrame == undefined) {
	window.requestAnimFrame = (function(){
        return  window.requestAnimationFrame       || 
        window.webkitRequestAnimationFrame || 
        window.mozRequestAnimationFrame    || 
        window.oRequestAnimationFrame      || 
        window.msRequestAnimationFrame     || 
        function( callback ){
        window.setTimeout(callback, 1000 / 60);
        };
    })();
}

// Console grouping polyfill

if(window.console == undefined) {
        var console = new Object();
        window.console = console;
        console.dir = function(){};     
        console.debug = function(){};
        console.info = function(){};
        console.warn = function(){};
        console.log = function() {};
        console.trace = function(){};
        console.group = function(){};
        console.groupCollapsed = function(){};
        console.timeStamp = function() {};
        console.profile = function() {};
        console.profileEnd = function() {};


        if (document.location.href.indexOf("debug=true") != -1) {

            var logElement = document.createElement("div");
            
            logElement.style.position = "absolute";
            logElement.style.width = "400px";
            logElement.style.height = "200px";
            logElement.style.color = "#FFFFFF";
            logElement.style.background = "#232323";
            logElement.style.top = "60px";
            logElement.style.left = "20px";
            logElement.style.overflow = "scroll";
            logElement.style.zIndex = "100000";
            logElement.style.fontSize = "10px";
            logElement.style.borderWidth = "1px";
            logElement.style.borderColor = "#888888";

        
            // we stack up lines to add to the fake console so it doens't overload the DOM
            var consoleLineStack = [];

            document.body.appendChild(logElement);
            console.log = function(aMessage){

                var argumentsString = "";
                for (var i = 0; i < arguments.length; i++)
                    argumentsString = argumentsString + " "  + arguments[i].toString();

                consoleLineStack.push(argumentsString);
            };

            console.error = function(aMessage){
                var argumentsString = "";
                for (var i = 0; i < arguments.length; i++)
                    argumentsString = argumentsString + " "  + arguments[i].toString();

                consoleLineStack.push("<span style='color:#FF0000'>" + argumentsString + "</span>");        
            };

            // take the last line from the stack and append it to the fake console
            function __updateDummyConsole() {
                if (consoleLineStack.length > 0)
                    logElement.innerHTML = logElement.innerHTML + "<br/>" + consoleLineStack.shift();
            };

            setInterval(__updateDummyConsole, 100);

        }
    
}
