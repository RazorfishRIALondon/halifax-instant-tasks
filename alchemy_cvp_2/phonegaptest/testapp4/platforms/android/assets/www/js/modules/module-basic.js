
(function(globalObj){

    var LBGAlchemy      = globalObj.namespace.LBGAlchemy;
    var EventDispatcher = LBGAlchemy.EventDispatcher;

    if (!LBGAlchemy.ModuleBasic) {

        LBGAlchemy.ModuleBasic = function() {

            this.templateHolder = null;
            this.Init();
        };

        LBGAlchemy.ModuleBasic.INTERNAL_LINK = "LBGAlchemy.ModuleBasic.INTERNAL_LINK";
        LBGAlchemy.ModuleBasic.EXTERNAL_LINK = "LBGAlchemy.ModuleBasic.EXTERNAL_LINK";


        var p = LBGAlchemy.ModuleBasic.prototype = new EventDispatcher();

        p.Init = function() {
            //Call when instantiated
        }

        p.Open = function(container, divBefore) {


            if (container) {
                this.InjectTemplate(container);
                if (divBefore) {
                    var tBefore = document.getElementById(divBefore);
                    var tDiv = this.appendParsedTemplate(tBefore);
                }
                else {
                    var tDiv = this.setParsedTemplate();
                }
            }


            //extend functions
            this.Opened(tDiv);
            // var that = this;
            //onresize function
            // globalObj.onresize = function(){ that.onResize() };
        }


        p.Opened = function(tDiv) {            
        }

        p.InjectTemplate = function(tp) {
            this.templateHolder = tp;
        }

        p.setParsedTemplate = function() {

            if (this.templateHolder) {
                var tT          = document.createElement('div');
                tT.innerHTML    = this.template;
                var tH          = this.templateHolder;
                tH.innerHTML    = tT.innerHTML;

                var links           = tH.getElementsByTagName('a');
                var linksLength     = links.length;
                var that            = this;

                for(var i=0; i<linksLength; i++) {
                    var lD  = links[i].getAttribute('data-link');
                    if (lD) {
                        var lDIsHttp = lD.toLowerCase().slice(0,4);
                        var linkHandler = null;
                        if (lDIsHttp != 'http')     linkHandler = function(eE) { that.internalLinkHandler(eE) };
                        else                        linkHandler = function(eE) { that.externalLinkHandler(eE) };

                        links[i].addEventListener("click", linkHandler);
                    }
                }

                return tT;
            }
        }

        p.appendParsedTemplate = function(tBefore) {

            if (!tBefore) return;

            var tT          = document.createElement('div');
            tT.innerHTML    = this.template;
            if (tBefore.nextSibling) {
                tBefore.parentNode.insertBefore(tT, tBefore.nextSibling);
            }
            else {
                tBefore.parentNode.appendChild(tT);
            }

            var links           = tBefore.getElementsByTagName('a');
            var linksLength     = links.length;
            var that            = this;

            for(var i=0; i<linksLength; i++) {
                var lD  = links[i].getAttribute('data-link');
                if (lD) {
                    var lDIsHttp = lD.toLowerCase().slice(0,4);
                    var linkHandler = null;
                    if (lDIsHttp != 'http')     linkHandler = function(eE) { that.internalLinkHandler(eE) };
                    else                        linkHandler = function(eE) { that.externalLinkHandler(eE) };

                    links[i].addEventListener("click", linkHandler);
                }
            }     

            return tT;    
        }

        p.internalLinkHandler = function(eE) {
            console.log('me clicked internal');
            this.internalLinkFunc(eE);
            this.dispatchCustomEvent(LBGAlchemy.ModuleBasic.INTERNAL_LINK);
        }

        p.internalLinkFunc = function(eE){

        }

        p.externalLinkHandler = function(eE) {
            console.log('me clicked external');
            this.externalLinkFunc(eE);
            this.dispatchCustomEvent(LBGAlchemy.ModuleBasic.EXTERNAL_LINK);
        }

        p.externalLinkFunc = function(eE) {

        }

        p.onresize = function() {

        }

        // -----------------------------------------------------------------------------
        //
        // FUNCTIONS for calculating and scaling divisions accordingly
        //
        // -----------------------------------------------------------------------------

        //work out width according to the number of digits
        p.scaleAutoByDigits = function(scalefactor, numDigits) {
            var scaleAmount = scalefactor/numDigits;
            return scaleAmount;
        }

        //calculate the scale according to the width of a division and its width
        p.scaleCalc = function(percentage, scaleFactor, el) {
            var sz = 0;
            sz = Math.round(el.clientWidth * scaleFactor * percentage);
            return sz;
        }

        //style a group of elements
        p.setGroupStyle = function(ti, options, tagname) {
            tiLength = ti.length;
            for(var i=0; i<tiLength; i++) {
                var tmp         = ti[i].getElementsByTagName(tagname);
                var tmpLength   = tmp.length;
                for(var j=0; j<tmpLength; j++) {
                    this.setStyle(tmp[j], options);
                }
            }       
        }

        //style a single element
        p.setStyle = function(el, options) {

            for(var i in options) {

                el.style[i] = options[i];
            }
        }

        //center div
        p.centerDiv = function(target, parent) {

            pWidth = parent.clientWidth;
            pHeight = parent.clientHeight;
            tWidth = target.clientWidth;
            tHeight = target.clientHeight;

            target.style.position = 'absolute';
            target.style.left = (pWidth - tWidth)/2 + 'px';
            target.style.top = (pHeight - tHeight)/2 + 'px';
        }

    }

})(window);