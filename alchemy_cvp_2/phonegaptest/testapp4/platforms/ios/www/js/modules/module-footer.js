(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasic = LBGAlchemy.ModuleBasic;

    if (!LBGAlchemy.ModuleFooter) {

        LBGAlchemy.ModuleFooter = function() {

            this.templateHolder = null;
            this.Init();
        };

        var p = LBGAlchemy.ModuleFooter.prototype = new ModuleBasic();

        p.Init = function() {

            this.template = [

                '<div class="inner">',
                '</div>',
                ''

              ].join('');

        }

        p.Open = function() {

            
        }

    }


})(window);