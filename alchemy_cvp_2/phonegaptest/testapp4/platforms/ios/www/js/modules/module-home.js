(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;

    if (!LBGAlchemy.ModuleHome) {

        LBGAlchemy.ModuleHome = function() {

            this.templateHolder = null;

            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleHome.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        p.Init = function() {

            this.options = {
                'time':1000,
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [

                '<div id="foreigncurrency-section" class="index large-section">',
                '   <div class="section-title">',
                '       <h3>Balance &amp; History</h3>',
                '   </div>',

                //balance section
                '   <div id="foreigncurrency" class="foreign-currency">',
                '       <h1>&euro;663.12</h1>',
                '   </div>',
                '   <div id="localcurrency" class="local-currency">',
                '       <h3>in pounds &pound;565.59</h3>',
                '   </div>',
                '</div>',

                //exchange rate section
                '<div id="exchangerate-section" class="index mid-section">',
                '   <div class="section-title">',
                '       <h3>Exchange Rate</h3>',
                '   </div>',

                '   <div id="exchangerate" class="foreign-currency">',
                '       <h1>&euro;1.7 - &pound;1.0</h1>',
                '   </div>',

                '   <div id="localcurrency" class="local-currency">',
                '       <h3>Euro - British Pound</h3>',
                '   </div>',
                '</div>',

                //information section
                '<div class="index bottommid-section">',
                '    <div id="atmdisplay" class="grid2-left">',
                '   <div class="section-title">',
                '       <h3>Local ATMs</h3>',
                '   </div>',
                '     <div class="inner">',
                '           <h5>&nbsp;</h5>',
                '           <h5>Atelier Bank<br/>Largo Santos 1<br/>1200-808<br/>Lisboa</h5>',
                '     </div>',
                '    </div>',
                '    <div id="insurancedisplay" class="grid2-right">',
                '   <div class="section-title">',
                '       <h3>Insurance</h3>',
                '   </div>',
                '     <div class="inner">',
                '           <h5>&nbsp;</h5>',
                '           <h5>From</h5>',
                '           <h4>&pound;99</h4>',
                '           <h5>per day</h5>',
                '     </div>',               
                '    </div>',
                '</div>',


              ].join('');

        }


        p.Opened = function() {
            
            this.onresize();
            var tCon = this.container;
            var hammer = this.hammer = Hammer(tCon, {
                drag_block_vertical: true
            });

            var that = this;
            var dragStartHandlerBind        = function(e) {that.dragStartHandler(e)};
            var dragEndHandlerBind          = function(e) {that.dragEndHandler(e)};
            var dragHandlerBind             = function(e) {that.dragHandler(e)};
            hammer.on("drag", dragHandlerBind);
            hammer.on("dragstart", dragStartHandlerBind);
            hammer.on("dragend", dragEndHandlerBind);
            hammer.on("tap", dragEndHandlerBind);
        }

        p.rescaleContent = function() {
            var ti                        = document.getElementsByClassName('section-title');
            var lC                        = document.getElementsByClassName('local-currency');
            var fC                        = document.getElementById('foreigncurrency');
            var eX                        = document.getElementById('exchangerate');
            var atmParent                 = document.getElementById('atmdisplay')
            var atm                       = atmParent.getElementsByClassName('inner');
            var insParent                 = document.getElementById('insurancedisplay');
            var ins                       = insParent.getElementsByClassName('inner');
            
            var eXH = this.exCurr         = eX.getElementsByTagName('h1')[0];
            var fCH = this.foreignCurr    = fC.getElementsByTagName('h1')[0];
            var lCH = this.localCurr      = document.getElementById('localcurrency').getElementsByTagName('h3')[0];
            

            //scale sections
            var fchScale = this.scaleCalc(0.98, this.scaleAutoByDigits( 0.12*6, fCH.innerHTML.length ), fC);
            var exScale  = this.scaleCalc(0.98, this.scaleAutoByDigits( 0.09*9, eXH.innerHTML.length ), eX);
            var lchScale = this.scaleCalc(0.1, 0.4, fC);
            var tiScale  = this.scaleCalc(0.1, 0.4, fC);
            var h4Scale  = this.scaleCalc(0.2, 0.7, insParent);
            var h5Scale  = this.scaleCalc(0.1, 0.7, atmParent);


            var tiOpt = {
                'font-size': tiScale+'px'
            }
            var fchOpt = {
                'font-size': fchScale+'px'
            };
            var lchOpt = {
                'font-size': lchScale+'px'
            }
            var exOpt = {
                'font-size': exScale+'px'
            }

            var midh4Opt = {
                'font-size': h4Scale+'px'
            }

            var midh5Opt = {
                'font-size': h5Scale+'px'
            }

            this.setGroupStyle(lC, lchOpt, 'h3');
            this.setGroupStyle(ti, tiOpt, 'h3');
            this.setStyle(fC, fchOpt);
            this.setStyle(eX, exOpt);

            this.setGroupStyle(atm, midh5Opt, 'h5');
            this.setGroupStyle(ins, midh4Opt, 'h4');
            this.setGroupStyle(ins, midh5Opt, 'h5');

            //calculate position
            this.centerDiv(atm[0], atmParent);
            this.centerDiv(ins[0], insParent);
        }

        p.onresize = function() {
            this.rescaleContent();
        }


    }


})(window);