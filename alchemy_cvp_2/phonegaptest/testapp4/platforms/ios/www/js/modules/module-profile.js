(function(globalObj){

    var LBGAlchemy              = globalObj.namespace.LBGAlchemy;
    var ModuleBasic             = LBGAlchemy.ModuleBasic;
    var DomElementPosition      = LBGAlchemy.DomElementPosition;

    if (!LBGAlchemy.ModuleProfile) {

        LBGAlchemy.ModuleProfile = function() {

            this.templateHolder = null;

            this.options = {
                'idName': 'profile-section',
                'className': 'profile mid-section',
            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleProfile.prototype = new ModuleBasic();
        var s = LBGAlchemy.ModuleBasic.prototype;

        p.Init = function() {

            this.template = [


                ''

              ].join('');

        }



        //setting the list id and class
        p.Opened = function() {
            var tCon = this.container;
            // set size
            this.onresize();
            console.log(tCon);
            // scale tween
            this.sTween = new DomElementPosition.createWithAnimation(tCon, 0, 0, 0, 760, 800, TWEEN.Easing.Elastic.Out, 1000, null);
            //setthe hammer

        }

        //resize the list
        p.onresize = function() {
            this._conWidth = this.container.clientWidth;
            this._conHeight = this.container.clientHeight;

        }

        p.dragStartHandler = function(e) {
            console.log('test drag start');
        }
        //touch handler
        p.dragEndHandler = function(e) {
            var cW = Math.round(this._conWidth*2/3);
            var pX = this.prevX;
            var con = this.container;
            var time = this.options.time;
            if (pX>cW)  this.sTween = new DomElementPosition.createWithAnimation(con, pX-this._conWidth, 0, 0, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);
            else        this.sTween = new DomElementPosition.createWithAnimation(con, pX-this._conWidth, 0, -this._conWidth, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);
 
        }

        p.dragHandler = function(e) {
            var time = this.options.time;
            var cW = this._conWidth;
            var con = this.container;
            var pX = this.prevX;
            var cX = this.prevX = e.gesture.touches[0].pageX?e.gesture.touches[0].pageX:0;
            this.sTween = new DomElementPosition.createWithAnimation(con, pX-cW, 0, cX-cW, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);

        }

    }


})(window);