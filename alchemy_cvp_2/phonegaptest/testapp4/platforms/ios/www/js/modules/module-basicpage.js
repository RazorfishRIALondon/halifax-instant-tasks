(function(globalObj){

    var LBGAlchemy              = globalObj.namespace.LBGAlchemy;
    var ModuleBasic             = LBGAlchemy.ModuleBasic;
    var DomElementPosition      = LBGAlchemy.DomElementPosition;

    if (!LBGAlchemy.ModuleBasicPage) {

        LBGAlchemy.ModuleBasicPage = function() {

            this.templateHolder = null;
            this.options = {};
            this.Init();
        };

        var p = LBGAlchemy.ModuleBasicPage.prototype = new ModuleBasic();

        p.Init = function() {

            this.options = {
                'time':1000,
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [

              ].join('');

        }


        p.dragStartHandler = function(e) {
            console.log('test drag start');
        }
        //touch handler
        p.dragEndHandler = function(e) {
            var cW = Math.round(this._conWidth*2/3);
            var pX = this.prevX;
            var con = this.container;
            var time = this.options.time;
            if (pX>cW)  this.sTween = new DomElementPosition.createWithAnimation(con, pX-this._conWidth, 0, 0, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);
            else        this.sTween = new DomElementPosition.createWithAnimation(con, pX-this._conWidth, 0, -this._conWidth, 0, time, TWEEN.Easing.Quadratic.Out, 150, this.dragCompleteHandlerBind);
 
        }

        p.dragHandler = function(e) {
            var time = this.options.time;
            var cW = this._conWidth;
            var con = this.container;
            var pX = this.prevX;
            var cX = this.prevX = e.gesture.touches[0].pageX?e.gesture.touches[0].pageX:0;
            console.log(pX, cX, time);
            this.sTween = new DomElementPosition.createWithAnimation(con, pX-cW, 0, cX-cW, 0, time, TWEEN.Easing.Quadratic.Out, 150, null);

        }

    }


})(window);