(function(globalObj){

  if (!globalObj.namespace) {
    globalObj.namespace = {};
  }
  var ns = globalObj.namespace;

  if (!ns.LBGAlchemy) {
    ns.LBGAlchemy = {};
  }

  var LBGAlchemy    = ns.LBGAlchemy;

  if (!ns.LBGAlchemy.App) {

    LBGAlchemy.App   = function () {

      this.Init();
    };

    var p = LBGAlchemy.App.prototype;

    p.Init = function() {
      console.log('LGBAlchemy.func.Init called.');

      var that = this;
      //load all libraries modules
      $LAB
      //external libraries
      .script("js/tween.js")
      .script("js/hammer.js")
      .script("js/canvasloader.js")
      .wait()
      //internal libraries
      .script("js/utils/polyfill.js")
      .script("js/events/EventDispatcher.js")
      .script("js/animation/AnimationManager.js")
      .script("js/animation/DomElementScale.js")
      .script("js/animation/DomElementPosition.js")
      .wait()
      .script("js/modules/module-basic.js")
      .script("js/modules/module-basicpage.js")
      .wait()
      //modules with pages
      .script("js/modules/module-loader.js")
      .wait(function(){that.InitLoaderComplete()})
      .script("js/modules/module-header.js")
      .script("js/modules/module-footer.js")
      .script("js/modules/module-balance.js")
      .script("js/modules/module-profile.js")
      .script("js/modules/module-home.js")
      .wait(function(){that.InitComplete()});
    }


    p.InitLoaderComplete = function() {
      var nN = new LBGAlchemy.ModuleLoader();
      var lD = document.getElementById('loader');
      nN.Open(lD);

      this.loader = nN;
      //loading disable temporary
    }

    p.InitComplete = function() {
      console.log('LGBAlchemy.func.InitComplete called.');

      this.loader.Close();

      //begin the Tween Animation
      this.animationManager = LBGAlchemy.AnimationManager.create();

      // prepare header and footer
      var mM = this.mM = new LBGAlchemy.ModuleHeader();
      var hD = document.getElementById('head');
      mM.Open(hD);

      var oO = this.oO = new LBGAlchemy.ModuleFooter();
      var fD = document.getElementById('foot');
      oO.Open(fD);

      //prepare content
      var tT = this.tT = new LBGAlchemy.ModuleHome();
      var dD = document.getElementById('content');
      tT.Open(dD);


      //prepare content
      var pF = this.pF = new LBGAlchemy.ModuleProfile();
      var eE = document.getElementById('profile');
      pF.Open(eE);

      //balance 
      var bB = this.bB = new LBGAlchemy.ModuleBalance();
      var bD = document.getElementById('content');
      bB.Open(bD, 'foreigncurrency-section');

      //extend functions

      var that = this;
      window.onresize = function(){ that.onresizeFunc(); };

    }


    p.onresizeFunc = function() {

      this.mM.onresize();
      this.oO.onresize();
      this.tT.onresize();
      this.bB.onresize();
    }

  }


})(window);


var lbgalchemyApp = new window.namespace.LBGAlchemy.App();

