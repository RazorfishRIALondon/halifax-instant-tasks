LBGAlchemy.modules.loginModule = function () {

	console.log("LBGAlchemy.views.loginModule()");

	var ro = {};
	ro.title = "Login";
	ro.id = "login";
	ro.viewClass = "content-"+ro.id;
	ro.view = "";

	ro.view += '<div class="'+ro.viewClass+'">';

		ro.view += '<form class="input-group">';
		ro.view += '<input type="email" placeholder="Email">';
		ro.view += '<input type="text" placeholder="Password">';
		ro.view += '</form>';

		ro.view += '<a class="button button-block" href="#home">LOGIN</a>';
		ro.view += '<a class="button button-block" href="#registration">REGISTRATION</a>';

	ro.view += '</div>';

	ro.init = function ( body, complete ) {

		// init module

		body.find(".content").append(ro.view);

		console.log("LBGAlchemy.views.loginModule().init()");

		complete();

	};

	return ro;

};