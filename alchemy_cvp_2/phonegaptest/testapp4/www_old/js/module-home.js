LBGAlchemy.modules.homeModule = function () {

	console.log("LBGAlchemy.views.homeModule()");

	var ro = {};
	ro.title = "Home";
	ro.id = "home";
	ro.viewClass = "content-"+ro.id;
	ro.view = "";


	ro.view += '<div class="'+ro.viewClass+'">';

		ro.view += '<ul class="list">';
		
		for (var i = 0; i < 20; i++) {
		
			ro.view += '<li>';
			ro.view += '<a href="#xxxx">';
			ro.view += 'List item '+(i+1);
			//ro.view += '<span class="chevron"></span>';
			ro.view += '<span class="count">0</span>';
			ro.view += '</a>';
			ro.view += '</li>';

		};

		ro.view += '</ul>';

	ro.view += '</div>';

	ro.init = function ( body, complete ) {

		// init module

		body.find(".content").append(ro.view);

		console.log("LBGAlchemy.views.homeModule().init()");

		complete();

	};


	return ro;

};