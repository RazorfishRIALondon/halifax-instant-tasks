LBGAlchemy.modules.registrationModule = function () {

	console.log("LBGAlchemy.views.registrationModule()");

	var ro = {};
	ro.title = "Registration";
	ro.id = "registration";
	ro.viewClass = "content-"+ro.id;
	ro.view = "";

	ro.view += '<div class="'+ro.viewClass+'">';

		ro.view += '<form class="input-group">';
		ro.view += '<input type="text" placeholder="Name">';
		ro.view += '<input type="email" placeholder="Email">';
		ro.view += '<input type="password" placeholder="Password">';
		ro.view += '</form>';

		ro.view += '<a class="button button-block" href="#login">SIGN UP</a>';

	ro.view += '</div>';

	ro.init = function ( body, complete ) {

		// init module

		body.find(".content").append(ro.view);

		console.log("LBGAlchemy.views.registrationModule().init()");

		complete();

	};

	return ro;

};