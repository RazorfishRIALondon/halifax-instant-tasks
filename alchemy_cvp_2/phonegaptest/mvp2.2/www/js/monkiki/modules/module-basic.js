
(function(){

    var namespace           = monkikiNS.getNamespace("generic");
    var ns                  = monkikiNS.getNamespace("generic.modules");
    var EventDispatcher     = namespace.events.EventDispatcher;
    var DomElementPosition  = namespace.animation.DomElementPosition;
    var DomElementScale     = namespace.animation.DomElementScale;
    var dataManager;

    if (!ns.ModuleBasic) {

        ns.ModuleBasic = function() {

            this.templateHolder = null;
            this.Init();
            this.container = null;
            this.pageId = '';
            this.pageClass = '';
            this.isDefault = false;
        };

        ns.ModuleBasic.INTERNAL_LINK = "ModuleBasic.INTERNAL_LINK";
        ns.ModuleBasic.EXTERNAL_LINK = "ModuleBasic.EXTERNAL_LINK";


        var p = ns.ModuleBasic.prototype = new EventDispatcher();

        p.Init = function() {
            //Call when instantiated
        }

        p.setIsDefault = function(isDefault) {
            this.isDefault = isDefault;
        }

        p.Open = function(container, pageId, pageClass, divBefore) {


            if (container) {
                this.InjectTemplate(container);
                this.container = container;
                if (divBefore) {
                    var tBefore = document.getElementById(divBefore);
                    var tDiv = this.appendParsedTemplate(tBefore, pageId, pageClass);
                }
                else {
                    var tDiv = this.setParsedTemplate(pageId, pageClass);
                }
            }
            //extend functions
            this.Opened(tDiv);



        }

        p.Opened = function(tDiv) {  
           
        }

        p.InjectTemplate = function(tp) {
            this.templateHolder = tp;
        }

        p.setParsedTemplate = function() {

            if (this.templateHolder) {
                var tT          = document.createElement('div');
                var tOut        = null;
                if (this.pageId)        tT.id = this.pageId;
                if (this.pageClass)     tT.setAttribute('class', this.pageClass);
                tT.innerHTML    = this.template;
                var tH          = this.templateHolder;
                //append div if it has an id, otherwise out content into the container
                if (this.pageId)    {
                    tOut = tH.appendChild(tT);
                }
                else {
                    tH.innerHTML = tT.innerHTML;
                    tOut = tH;
                }       

                var links           = tH.getElementsByTagName('a');
                var linksLength     = links.length;
                var that            = this;

                for(var i=0; i<linksLength; i++) {
                    var lD  = links[i].getAttribute('data-link');
                    if (lD) {
                        var lDIsHttp = lD.toLowerCase().slice(0,4);
                        var linkHandler = null;
                        if (lDIsHttp != 'http')     linkHandler = function(eE) { that.internalLinkHandler(eE) };
                        else                        linkHandler = function(eE) { that.externalLinkHandler(eE) };

                        links[i].addEventListener("click", linkHandler);
                    }
                }

                return tOut;
            }
        }

        p.appendParsedTemplate = function(tBefore) {

            if (!tBefore || !this.pageId) {
                console.log('this.pageId is not defined');
                return;
            }
            var tParent     = tBefore.parentNode;
            var tT          = document.createElement('div');
            tT.id           = this.pageId;
            if (pageClass)     tT.setAttribute('class', this.pageClass);
            tT.innerHTML    = this.template;
            if (tBefore.nextSibling) {
                tO = tParent.insertBefore(tT, tBefore.nextSibling);
            }
            else {
                tO = tParent.appendChild(tT);
            }

            var links           = tBefore.getElementsByTagName('a');
            var linksLength     = links.length;
            var that            = this;

            for(var i=0; i<linksLength; i++) {
                var lD  = links[i].getAttribute('data-link');
                if (lD) {
                    var lDIsHttp = lD.toLowerCase().slice(0,4);
                    var linkHandler = null;
                    if (lDIsHttp != 'http')     linkHandler = function(eE) { that.internalLinkHandler(eE) };
                    else                        linkHandler = function(eE) { that.externalLinkHandler(eE) };

                    links[i].addEventListener("click", linkHandler);
                }
            }     

            return tO;    
        }

        p.internalLinkHandler = function(eE) {
            // console.log('me clicked internal');
            this.internalLinkFunc(eE);
            this.dispatchCustomEvent(LBGAlchemy.ModuleBasic.INTERNAL_LINK);
        }

        p.internalLinkFunc = function(eE){

        }

        p.externalLinkHandler = function(eE) {
            // console.log('me clicked external');
            this.externalLinkFunc(eE);
            this.dispatchCustomEvent(LBGAlchemy.ModuleBasic.EXTERNAL_LINK);
        }

        p.externalLinkFunc = function(eE) {

        }

        p.onresize = function() {

        }

        // -----------------------------------------------------------------------------
        //
        // FUNCTIONS for calculating and scaling divisions accordingly
        //
        // -----------------------------------------------------------------------------

        //work out width according to the number of digits
        p.scaleAutoByDigits = function(scalefactor, numDigits) {
            var scaleAmount = scalefactor/numDigits;
            return scaleAmount;
        }

        //calculate the scale according to the width of a division and its width
        p.scaleCalc = function(percentage, scaleFactor, el) {
            var sz = 0;
            sz = Math.round(el.clientWidth * scaleFactor * percentage);
            return sz;
        }

        //style a group of elements
        p.setGroupStyle = function(ti, options, tagname) {
            tiLength = ti.length;
            for(var i=0; i<tiLength; i++) {
                var tmp         = ti[i].getElementsByTagName(tagname);
                var tmpLength   = tmp.length;
                for(var j=0; j<tmpLength; j++) {
                    this.setStyle(tmp[j], options);
                }
            }       
        }

        //style a single element
        p.setStyle = function(el, options) {

            for(var i in options) {

                el.style[i] = options[i];
            }
        }

        //center div
        p.centerDiv = function(target, parent) {

            pWidth = parent.clientWidth;
            pHeight = parent.clientHeight;
            tWidth = target.clientWidth;
            tHeight = target.clientHeight;

            target.style.position = 'absolute';
            target.style.left = (pWidth - tWidth)/2 + 'px';
            target.style.top = (pHeight - tHeight)/2 + 'px';
        }

    }

})();