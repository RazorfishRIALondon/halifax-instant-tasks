(function(globalObj){


    var namespace           = monkikiNS.getNamespace("generic");
    var ns                  = monkikiNS.getNamespace("generic.modules");
    var ModuleBasicPage     = ns.ModuleBasicPage;
    var ModuleBasic         = ns.ModuleBasic;
    var DomElementOpacity   = monkikiNS.getNamespace("generic.animation").DomElementOpacity;
    var dataManager;

    if (!ns.ModuleAlert) {

        ns.ModuleAlert = function(type) {

            this.templateHolder = null;
            this.isOpened       = null;
            this.options = {

            };

            this.Init(type);
        };

        var p = ns.ModuleAlert.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;


        p.Init = function(type) {
            this.pageId = 'alert';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   

                '<div id="alert-bg"></div>',
                '<div id="alert-messagebox">',
                this.setType(type),
                '   <div id="alert-message"><h3>Lloyds Bank</h3><p>Welcome to Lisbon, we’ve made sure your account works throughout Portugal.</p></div>',
                '</div>'
            ].join('');       
        }

        p.OpenedAfter = function(tDiv) {
            dataManager         = namespace.singletons.dataManager;
            this.isOpened       = false;
        }

        p.tapToggle = function() {
            var that = this;
            if (this.isOpened) {
                console.log('tapOpen from alert')
                var dD = DomElementOpacity.createWithAnimation(this.innerContainer, 1, 0, dataManager.tweenOptions.opacityTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, function(){ that.isOpened=false });

            }
            else {
                console.log('tapOpen open from alert')
                var dD = DomElementOpacity.createWithAnimation(this.innerContainer, 0, 1, dataManager.tweenOptions.opacityTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, function(){ that.isOpened=true });

            }

            return !this.isOpened;
        }

        p.tapClose = function() {
             if (this.isOpened) {
                var that = this;
                console.log('tapClose from alert')
                var dD = DomElementOpacity.createWithAnimation(this.innerContainer, 1, 0, dataManager.tweenOptions.opacityTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0, function(){ that.isOpened=false });

            }           
        }

        //set the type of alert
        p.setType = function(type) {

            var tT = type?type:'right';
            var out;
            switch(tT) {
                case 'left':
                    out = '   <div class="alert-left-triangle"></div>';
                    break;
                case 'mid':
                    out = '   <div class="alert-mid-triangle"></div>';
                    break;
                case 'right':
                    out = '   <div class="alert-right-triangle"></div>';
                    break;
            }

            return out;
        }


    }

})(window);