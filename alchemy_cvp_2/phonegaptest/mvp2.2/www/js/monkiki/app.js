(function(globalObj){


    if (globalObj.monkikiNS) {
        console.warn("monkikiNS global object has already been constructed.");
        return;
    }
    var monkikiNS = {};
    globalObj.monkikiNS = monkikiNS;
    monkikiNS.projectName = "lbsMvp";

    monkikiNS.getNamespace = function(aPackagePath) {

        var currentObject = this;

        var currentArray = aPackagePath.split(".");
        var currentArrayLength = currentArray.length;
        for(var i = 0; i < currentArrayLength; i++) {
          var currentName = currentArray[i];
          if(currentObject[currentName] === undefined) {
            currentObject[currentName] = {};
          }
          currentObject = currentObject[currentName];
        }

        return currentObject;
    };

    monkikiNS.getClass = function(aClassPath) {

        var lastSplitPosition = aClassPath.lastIndexOf(".");
        var packagePath = aClassPath.substring(0, lastSplitPosition);
        var className = aClassPath.substring(lastSplitPosition+1, aClassPath.length);
        
        var packageObject = this.getNamespace(packagePath);
        if(packageObject[className] === undefined) {
          console.error("Class " + aClassPath + " doesn't exist.");
          return null;
        }
        return packageObject[className];
    };

    monkikiNS.singletons = new Object();

    // if (!ns.LBGAlchemy) {
    //     ns.LBGAlchemy = {};
    // }

    // var dataManager;
    // var LBGAlchemy    = ns.LBGAlchemy;

    // if (!ns.LBGAlchemy.AppBasic) {

    //     LBGAlchemy.AppBasic   = function () {

    //         this.Init();
    //     };

    //     var p = LBGAlchemy.AppBasic.prototype;

    //     p.Init = function() {
    //       console.log('LGBAlchemy.func.Init called.');

    //       var that = this;
    //       //load all libraries modules
    //       $LAB
    //       //external libraries
    //       .script("js/libs/tween.js")
    //       .script("js/libs/hammer.js")
    //       .script("js/libs/canvasloader.js")
    //       .wait(function(){that.InitLoaderComplete()})

    //       //internal libraries
    //       .script("js/utils/polyfill.js")
    //       .script("js/events/EventDispatcher.js")
    //       .script("js/data/data-manager.js")
    //       .script("js/animation/AnimationManager.js")
    //       .script("js/animation/DomElementScale.js")
    //       .script("js/animation/DomElementPosition.js")
    //       .script("js/animation/DomElementOpacity.js")
    //       .script("js/animation/DomElementRotate.js")
    //       .script("js/animation/DomElementBG.js")
    //       .wait()
    //       //modules with pages
    //       .script("js/modules/module-basic.js")
    //       .script("js/modules/module-basicpage.js")  
    //       .script("js/modules/module-loader.js") 
    //       .wait()
    //       //commons
    //       .script("js/common/common-footer.js")
    //       .script("js/common/common-header.js")      
    //       .wait(function(){that.InitComplete()});
    //     }


    //     p.InitLoaderComplete = function() {
    //         console.log('LGBAlchemy.func.InitLoaderComplete called.');
    //         var nN = new LBGAlchemy.ModuleLoader();
    //         var lD = document.getElementById('loader');
    //         nN.Open(lD);
    //         this.loader = nN;
    //         //loading disable temporary
    //     }

    //     p.InitComplete = function() {
    //       console.log('LGBAlchemy.func.InitComplete called.');

    //       // this.loader.Close();


    //       dataManager = LBGAlchemy.DataManager.createSingleton();
    //       var bData = dataManager.setBrowserData({
    //           browserWidth: document.body.clientWidth,
    //           browserHeight: document.body.clientHeight,
    //       });

         

    //       //window resize
    //       this.onresizeFunc();
    //       var that = this;
    //       window.onresize = function(){ that.onresizeFunc(); };
    //     }

    //     //resize all pages
    //     p.onresizeFunc = function() {

    //     }
    // }

})(window);


