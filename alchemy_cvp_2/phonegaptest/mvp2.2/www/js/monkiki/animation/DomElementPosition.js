(function(){
	
	var namespace = monkikiNS.getNamespace("generic.animation");

	if(!namespace.DomElementPosition) {

		var DomElementPosition = function DomElementPosition() {

			this._init();
		};

		namespace.DomElementPosition = DomElementPosition;

		var p = DomElementPosition.prototype;

		p._init = function() {
			console.log('LGBAlchemy.DomElementPosition Initialised.')
			this._element 			= null;
			this._x 				= 0;
			this._y 				= 0;
			var that 				= this;
			this._updateCallBack = function() { that._update() };

			this._tween = new TWEEN.Tween(this);
			this._tween.onUpdate(this._updateCallBack);
		}

		p.setElement = function(ele) {
			this._element = ele;
			return this;
		}

		p.getElement = function() {
			return this._element;
		}

		p.getXPosition = function() {
			return this._x;
		};
		
		p.getYPosition = function() {
			return this._y;
		};

		p.setStartPosition = function(aX, aY) {
			this._x = aX;
			this._y = aY;
			return this;
		};

		p.setCallback = function(aCallback) {
			this._tween.onComplete(aCallback);
		}


		p.animateTo = function(aHor, aVer, aTime, aEasing, aDelay) {

			this._tween.to({"_x": aHor, "_y": aVer}, aTime).easing(aEasing).delay(aDelay);
			this._tween.start();

			return this;
		}

		p.update = function() {
			this._update();
		}

		p._update = function() {

			var elementTransform = "translate(" + this._x + "px, " + this._y + "px)";

			if(this._element !== null) {
				this._element.style.setProperty("-khtml-transform", elementTransform, "");
				this._element.style.setProperty("-moz-transform", elementTransform, "");
				this._element.style.setProperty("-ms-transform", elementTransform, "");
				this._element.style.setProperty("-webkit-transform", elementTransform, "");
				this._element.style.setProperty("-o-transform", elementTransform, "");
				this._element.style.setProperty("transform", elementTransform, "");
				// console.log(elementTransform);
			}	
		}

		p.destroy = function() {
			this._element = null;
			this._updateCallback = null;
			this._tween = null;
		};

		DomElementPosition.create = function(aElement, aStartX, aStartY) {
			var newDomElementPosition = new DomElementPosition();
			newDomElementPosition.setElement(aElement);
			newDomElementPosition.setStartPosition(aStartX, aStartY);
			return newDomElementPosition;
		}

		DomElementPosition.createWithAnimation = function(aElement, aStartX, aStartY, aX, aY, aTime, aEasing, aDelay, aCallback) {
			var newDomElementPosition = new DomElementPosition();
			newDomElementPosition.setElement(aElement);
			// console.log('callback', aCallback);
			if(typeof aCallback == 'function') newDomElementPosition.setCallback(aCallback);
			newDomElementPosition.setStartPosition(aStartX, aStartY);
			newDomElementPosition.animateTo(aX, aY, aTime, aEasing, aDelay);
			return newDomElementPosition;			
		}



	}

})();