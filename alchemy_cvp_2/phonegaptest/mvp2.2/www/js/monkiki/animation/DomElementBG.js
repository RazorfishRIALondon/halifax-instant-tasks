(function(){
	
	var namespace = monkikiNS.getNamespace("generic.animation");

	if(!namespace.DomElementBG) {

		var DomElementBG = function DomElementBG() {

			this._init();
		};

		namespace.DomElementBG = DomElementBG;

		var p = DomElementBG.prototype;

		p._init = function() {
			console.log('LGBAlchemy.DomElementBG Initialised.')
			this._element 			= null;
			this._x 				= 0;
			this._y 				= 0;
			var that 				= this;
			this._updateCallBack = function() { that._update() };

			this._tween = new TWEEN.Tween(this);
			this._tween.onUpdate(this._updateCallBack);
		}

		p.setElement = function(ele) {
			this._element = ele;
			return this;
		}

		p.getElement = function() {
			return this._element;
		}

		p.getBG = function() {
			return this._bg;
		};

		p.setStartBG = function(aX) {
			this._bg = aX;
			return this;
		};

		p.setCallback = function(aCallback) {
			this._tween.onComplete(aCallback);
		}


		p.animateTo = function(aHor, aTime, aEasing, aDelay) {

			this._tween.to({"_bg": aHor}, aTime).easing(aEasing).delay(aDelay);
			this._tween.start();

			return this;
		}

		p.update = function() {
			this._update();
		}

		p._update = function() {

			var elementTrans 		= this._bg;

			if(this._element !== null) {
				this._element.style.setProperty("background-color", elementTrans, "");
				console.log(elementTrans);
			}	
		}

		p.destroy = function() {
			this._element = null;
			this._updateCallback = null;
			this._tween = null;
		};

		DomElementBG.create = function(aElement, aStartX) {
			var newDomElementBG = new DomElementBG();
			newDomElementBG.setElement(aElement);
			newDomElementBG.setStartBG(aStartX, aStartY);
			return newDomElementBG;
		}

		DomElementBG.createWithAnimation = function(aElement, aStartX, aX, aTime, aEasing, aDelay, aCallback) {
			var newDomElementBG = new DomElementBG();
			newDomElementBG.setElement(aElement);
			if(typeof aCallback == 'function') newDomElementBG.setCallback(aCallback);
			newDomElementBG.setStartBG(aStartX);
			newDomElementBG.animateTo(aX, aTime, aEasing, aDelay);
			return newDomElementBG;			
		}



	}

})();