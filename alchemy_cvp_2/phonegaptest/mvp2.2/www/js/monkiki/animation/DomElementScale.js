(function(){
	
	var namespace = monkikiNS.getNamespace("generic.animation");

	if(!namespace.DomElementScale) {

		var DomElementScale = function DomElementScale() {

			this._init();
		};

		namespace.DomElementScale = DomElementScale;

		var p = DomElementScale.prototype;

		p._init = function() {
			console.log('LGBAlchemy.DomElementScale Initialised.')
			this._element 			= null;
			this._horizontalScale 	= 1;
			this._verticalScale 	= 1;	
			var that 				= this;
			this._updateCallBack = function() { that._update() };

			this._tween = new TWEEN.Tween(this);
			this._tween.onUpdate(this._updateCallBack);

		}

		p.setElement = function(ele) {
			this._element = ele;
			this._originalWidth = this._element.clientWidth;
			this._originalHeight = this._element.clientHeight;
			return this;
		}

		p.setScaleInner = function(isInner) {
			var out = isInner?isInner:false;
			this.scaleInner = out;
			return out;
		}

		p.getElement = function() {
			return this._element;
		}

		p.setCallback = function(aCallback) {
			this._tween.onComplete(aCallback);
		}


		p.getHorizontalScale = function() {
			return this._horizontalScale;
		}

		p.getVerticalScale = function() {
			return this._verticalScale;
		}

		p.setStartScale = function(aHor, aVer, aHorOrigin, aVerOrigin) {

			this._horizontalOrigin = aHorOrigin?aHorOrigin:0.5;
			this._verticalOrigin = aVerOrigin?aVerOrigin:0;
			this._horizontalScale = aHor;
			this._verticalScale = aVer;

			return this;
		}

		p.animateTo = function(aHor, aVer, aTime, aEasing, aDelay) {

			this._tween.to({"_horizontalScale": aHor, "_verticalScale": aVer}, aTime).easing(aEasing).delay(aDelay);

			this._tween.start();

			return this;
		}

		p.getTransform = function() {
			return "scale(" + this._horizontalScale.toFixed(3) + ", " + this._verticalScale.toFixed(3) + ")";
		}

		p.getTransformOrigin = function() {
			return this._horizontalOrigin*100 + "% " + this._verticalOrigin*100 + "%";
		}

		p.setTransformOrigin = function() {
			var ele = this._element;
			var elementTransformOrigin 	= this.getTransformOrigin();
			ele.style.setProperty("-khtml-transform-origin", elementTransformOrigin, "");
			ele.style.setProperty("-moz-transform-origin", elementTransformOrigin, "");
			ele.style.setProperty("-ms-transform-origin", elementTransformOrigin, "");
			ele.style.setProperty("-webkit-transform-origin", elementTransformOrigin, "");
			ele.style.setProperty("-o-transform-origin", elementTransformOrigin, "");
			ele.style.setProperty("transform-origin", elementTransformOrigin, "");
			console.log('kokojo');
		}

		p.update = function() {
			this._update();
		}

		p._update = function() {

			if (this._element === null) return;

			var sInner 					= this.scaleInner;
			var elementTransform 		= this.getTransform();	
			var ele 					= this._element;

			if (sInner) {
				ele.style.setProperty("-khtml-transform", elementTransform, "");
				ele.style.setProperty("-moz-transform", elementTransform, "");
				ele.style.setProperty("-ms-transform", elementTransform, "");
				ele.style.setProperty("-webkit-transform", elementTransform, "");
				ele.style.setProperty("-o-transform", elementTransform, "");
				ele.style.setProperty("transform", elementTransform, "");	
				console.log(elementTransform)

			}
			else {
				ele.style.setProperty("display", "block");
				ele.style.setProperty("min-width", '0px', "");
				ele.style.setProperty("min-height", '0px', "");

				ele.style.setProperty("width", this._originalWidth*this._horizontalScale+'px', "");
				ele.style.setProperty("height", this._originalHeight*this._verticalScale+'px', "");
			}
	
		}


		p.destroy = function() {

			this._element = null;
			this._updateCallBack = null;
			if (this._tween !== null) this._tween.stop();
			this._tween = null;

		}

		DomElementScale.create = function(aElement, aStartHorizontalScale, aStartVerticalScale, scaleInner) {
			var newDomElementScale = new DomElementScale();
			newDomElementScale.setElement(aElement);
			newDomElementScale.setScaleInner(scaleInner);
			newDomElementScale.setStartScale(aStartHorizontalScale, aStartVerticalScale);
			return newDomElementScale;
		}

		DomElementScale.createWithAnimation = function(aElement, aStartHorizontalScale, aStartVerticalScale, aHorizontalScale, aVerticalScale, scaleInner, aTime, aEasing, aDelay, aCallback) {
			
			var aEase = aEasing?aEasing:TWEEN.Easing.Quadratic.InOut;
			var newDomElementScale = new DomElementScale();
			newDomElementScale.setElement(aElement);
			newDomElementScale.setScaleInner(scaleInner);
			if(typeof aCallback == 'function') newDomElementScale.setCallback(aCallback);
			newDomElementScale.setStartScale(aStartHorizontalScale, aStartVerticalScale);
			newDomElementScale.setTransformOrigin();
			newDomElementScale.animateTo(aHorizontalScale, aVerticalScale, aTime, aEase, aDelay);
			return newDomElementScale;
		}



	}

})();