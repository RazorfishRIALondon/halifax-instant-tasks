(function(){

    var namespace           = monkikiNS.getNamespace("generic.common");
    var ModuleBasic         = monkikiNS.getNamespace("generic.modules").ModuleBasic;
    var dataManager;

    if (!namespace.CommonFooter) {

        namespace.CommonFooter = function() {

            this.templateHolder = null;
            this.butOptions = [

            ];
            this.triangleId = "triangle";
            this.Init();
        };

        namespace.CommonFooter.MENUTAP = 'CommonFooter.MENUTAP';
        var p = namespace.CommonFooter.prototype = new ModuleBasic();

        p.Init = function() {

            dataManager = monkikiNS.getNamespace("generic.singletons").dataManager;
            var listOpt = this.listCreator();

            console.log('CommonFooter init: ');
            this.template = [

                '<div class="inner">',
                '    <ul id="footermenu" class="footermenu">',
                        listOpt,
                '    </ul>',
                '</div>',
                ''

              ].join('');

        }


        p.Opened = function() {
            this.initListTouchEvent();
        }

        p.showTriangle = function(canvas, tWidth, tHeight, gradStart, gradEnd) {

            // var ctx             = canvas.getContext('2d');
            // canvas.style.width  = canvas.parentNode.clientWidth;
            // canvas.style.height = canvas.parentNode.clientHeight;
            // //grad
            // var lingrad = ctx.createLinearGradient(0,0,0,300);
            // lingrad.addColorStop(1, 'rgba(0,0,0,1)');
            // lingrad.addColorStop(0.5, 'rgba(0,255,0,1)');
            // lingrad.addColorStop(0, 'rgba(0,0,255,1)'); 

            // ctx.fillStyle       = lingrad;
            // ctx.beginPath();
            // ctx.moveTo(0, 0);
            // ctx.lineTo((wW-tWidth)/2, 0);
            // ctx.lineTo(wW/2, tHeight);
            // ctx.lineTo((wW+tWidth)/2, 0);
            // ctx.lineTo(wW, 0);
            // ctx.lineTo(wW, hH);
            // ctx.lineTo(0, hH);
            // ctx.fill();       

            // return canvas;
        }

        p.hideTriangle = function() {

        }

        p.listCreator = function() {
            console.log('list creator is me');
            var list    = dataManager.getFooterMenuData();
            console.log('list creator is me', list);
            listLength  = list.length;
            var out = '';
            for(var i=0; i<listLength; i++) {
                out+='<li id="' + list[i] + '"" data-state="' + list[i].replace('foot', '') + '""><div id="icon'+list[i].replace('foot', '')+'"></div><h6>'+list[i].replace('foot', '')+'</h6></li>';
            }
            return out;
        }

        p.moveTriangle = function(num) {
            el          = this.container;
            listLength  = dataManager.getFooterMenuData().length;
            var tI = document.getElementById(this.triangleId);
            tI.style.left = num * Math.round(el.clientWidth/listLength) + 'pt';
        }

        p.initListTouchEvent = function() {
            var list    = dataManager.getFooterMenuData();
            listLength  = dataManager.getFooterMenuData().length;
            var that = this;
            var tapHandlerBind = function(e) { that.tapHandler(e) };
            for(var i=0; i<listLength; i++) {
                var tmp = document.getElementById(list[i]);
                var hammertime = Hammer(tmp).on("tap", tapHandlerBind);
            }
        }

        p.tapHandler = function(e) {
            //make sure the child click get the same attention as well
            var sSrc = (e.target.nodeName==="LI")?e.target:e.target.parentNode;
            var tT = sSrc.getAttribute('data-state');

            var opt = this.butOptions;
            var details = {
                name: tT,
                id: sSrc.id,
            };
            console.log('jaja');
            this.dispatchCustomEvent(namespace.CommonFooter.MENUTAP, details);
        }

        p.s = function(no) {
            if (no==null) return;
            var list    = this.butOptions;
            for(var i=0; i<listLength; i++) {
                var tmp = document.getElementById(list[i]);
                if (no==i)      { tmp.style.opacity = 1; }
                else            tmp.style.opacity = 0.5;
            }
        }

    }


})();