(function(globalObj){

    var namespace   = monkikiNS.getNamespace("generic");
    var ns          = monkikiNS.getNamespace("generic.common");
    var ModuleBasic = namespace.modules.ModuleBasic;

    if (!ns.CommonHeader) {

        var CommonHeader = function CommonHeader() {

            this.templateHolder = null;
            this.Init();
        };

        CommonHeader.ALERTTAP = 'CommonHeader.ALERTTAP';
        CommonHeader.LOCATIONTAP = 'CommonHeader.LOCATIONTAP';
        CommonHeader.MENUTAP = 'CommonHeader.MENUTAP';
        var p = CommonHeader.prototype = new ModuleBasic();
        ns.CommonHeader = CommonHeader;

        p.Init = function() {

            this.template = [

                // '<img class="logo"/>'
                '<h1 class="centered">Lisbon</h1>',
                '<div id="listbut" class="listBut"></div>',
                '<div id="alertbut" class="alertbut"></div>'

              ].join('');

        }

        p.Opened = function() {
            this.initListTouchEvent();
        }

        p.Show = function() {
            this.container.setAttribute('class', 'bar-title main-title');
        }

        p.Hide = function() {
            this.container.setAttribute('class', '');
        }


        p.initListTouchEvent = function(e) {

            var tmp     = document.getElementById('alertbut');
            var tmp2    = document.getElementById('listbut');
            var that    = this;
            var tapHandlerBind = function(e) { that.tapHandler(e) };
            var hammertime = Hammer(this.container).on("tap", tapHandlerBind);
        }

        p.tapHandler = function(e) {
            var xDis = e.gesture.touches[0].pageX;
            if (xDis>280) {
               this.dispatchCustomEvent(CommonHeader.ALERTTAP); 
            }
            else if (xDis<280 && xDis>50) {
                this.dispatchCustomEvent(CommonHeader.LOCATIONTAP); 
            }
            else if (xDis<50) {
                this.dispatchCustomEvent(CommonHeader.MENUTAP);
            }
        }

    }


})(window);