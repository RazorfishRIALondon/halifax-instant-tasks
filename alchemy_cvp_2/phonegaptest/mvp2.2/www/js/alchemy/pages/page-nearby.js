(function(){

	var namespace = 		monkikiNS.getNamespace("generic");
	var pages = 			monkikiNS.getNamespace("generic.pages");
	var ModuleBasicPage =	monkikiNS.getNamespace("generic.modules").ModuleBasicPage;

	if (!pages.PageNearby) {

		var PageNearby = function PageNearby() {


			this.Init();
		}

		pages.PageNearby = PageNearby;
		var p = PageNearby.prototype = new ModuleBasicPage();

		p.Init = function() {

			this.pageId 	= 'nearby';
			this.pageClass 	= 'page-container';

			this.template = [
				'<img class="pullRefresh" />',
				'<img class="dummyimg" src="img/dummy/nearby-dummy@2x.png" />'
			].join('');
		}
	}

})();