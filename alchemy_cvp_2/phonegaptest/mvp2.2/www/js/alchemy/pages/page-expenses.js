(function(){

	var namespace = 		monkikiNS.getNamespace("generic");
	var pages = 			monkikiNS.getNamespace("generic.pages");
	var ModuleBasicPage =	monkikiNS.getNamespace("generic.modules").ModuleBasicPage;

	if (!pages.PageExpenses) {

		var PageExpenses = function PageExpenses() {


			this.Init();
		}

		pages.PageExpenses = PageExpenses;
		var p = PageExpenses.prototype = new ModuleBasicPage();

		p.Init = function() {

			this.pageId 	= 'expenses';
			this.pageClass 	= 'page-container';

			this.template = [
				'<img class="pullRefresh" />',
				'<img class="dummyimg" src="img/dummy/expenses-dummy@2x.png" />'
			].join('');
		}
	}

})();