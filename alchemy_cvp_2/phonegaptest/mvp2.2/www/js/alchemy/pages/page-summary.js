(function(){

	var namespace = 		monkikiNS.getNamespace("generic");
	var pages = 			monkikiNS.getNamespace("generic.pages");
	var ModuleBasicPage =	monkikiNS.getNamespace("generic.modules").ModuleBasicPage;

	if (!pages.PageSummary) {

		var PageSummary = function PageSummary() {

			this.Init();
		}

		pages.PageSummary = PageSummary;
		var p = PageSummary.prototype = new ModuleBasicPage();

		p.Init = function() {

			this.pageId 	= 'summary';
			this.pageClass 	= 'page-container';

			this.template = [
				'<img class="pullRefresh" />',
				'<img class="dummyimg" src="img/dummy/summary-dummy@2x.png" />'	
			].join('');
		}
	}

})();