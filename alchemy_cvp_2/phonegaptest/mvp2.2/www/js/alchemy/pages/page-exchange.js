(function(){

	var namespace = 		monkikiNS.getNamespace("generic");
	var pages = 			monkikiNS.getNamespace("generic.pages");
	var ModuleBasicPage =	monkikiNS.getNamespace("generic.modules").ModuleBasicPage;

	if (!pages.PageExchange) {

		var PageExchange = function PageExchange() {


			this.Init();
		}

		pages.PageExchange = PageExchange;
		var p = PageExchange.prototype = new ModuleBasicPage();

		p.Init = function() {

			this.pageId 	= 'exchange';
			this.pageClass 	= 'page-container';

			this.template = [
				'<img class="pullRefresh" />',
				'<img class="dummyimg" src="img/dummy/exchange-dummy@2x.png" />'
			].join('');
		}
	}

})();