(function(){

    var namespace =         monkikiNS.getNamespace("generic");
    var pages =             monkikiNS.getNamespace("generic.pages");
    var ModuleBasicPage =   monkikiNS.getNamespace("generic.modules").ModuleBasicPage;
    var DomElementPosition = monkikiNS.getNamespace("generic.animation").DomElementPosition;

    if (!pages.PageLogin) {

        var PageLogin = function PageLogin() {

            this.templateHolder = null;
            this.options = {

            };

            this.Init();
        };

        var p = PageLogin.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        pages.PageLogin = PageLogin;
        // var u = ModuleBasic.prototype;

        PageLogin.LOGIN = 'PageLogin.LOGIN';
        PageLogin.BACK = 'PageLogin.BACK';
        PageLogin.LOGINSUCCESS = 'PageLogin.LOGINSUCCESS';

        p.Init = function() {
            this.pageId = 'login';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   

                '<div class="title-section"><div id="backtomain" class="backBut"></div><p>Banking Abroad</p></div>',
                '<div class="top-section" id="customerlogin">',
                '   <form>',
                '     <p>User ID</p>',
                '     <input type="text" placeholder="Full name">',
                '     <p>Password</p>',
                '     <input type="password" placeholder="Password">',
                '     <p>&nbsp;</p>',
                '     <a id="existinglogin" class="button button-block">Choose existing</a>',
                '   </form>',
                '</div>',
                '<div class="logo-section">',
                '   <div class="inner"></div>',
                '</div>'
            ].join('');       
        }

        p.OpenedAfter = function() {

            var dD = document.getElementById('backtomain');
            var lI = document.getElementById('existinglogin');
            var that = this;
            //back
            var tapBackHandlerBind = function(e) { that.tapBackHandler(e) };
            var hammertime = Hammer(dD).on("tap", tapBackHandlerBind);
            //login
            var tapLoginHandlerBind = function(e) { that.tapLoginHandler(e) };
            var hammertime = Hammer(lI).on("tap", tapLoginHandlerBind);

        }

        p.tapLoginHandler = function(e) {
            //make sure the child click get the same attention as well
            var sSrc = (e.target.nodeName==="A")?e.target:e.target.parentNode;
            // var tT = sSrc.getAttribute('data-link');

            // var opt = this.butOptions;
            var details = '';
            this.dispatchCustomEvent(PageLogin.LOGIN, details);
        }

        p.CloseAfter = function() {
            var details = '';
            this.dispatchCustomEvent(PageLogin.LOGINSUCCESS, details);
        }

        p.tapBackHandler = function(e) {
            //make sure the child click get the same attention as well
            var sSrc = (e.target.nodeName==="A")?e.target:e.target.parentNode;
            // var tT = sSrc.getAttribute('data-link');

            // var opt = this.butOptions;
            var details = '';
            this.dispatchCustomEvent(PageLogin.BACK, details);
        }

    }

})();