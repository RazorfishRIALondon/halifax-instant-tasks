(function(){

	var namespace 			= monkikiNS.getNamespace("generic");
	var modules 			= monkikiNS.getNamespace("generic.modules");
	var EventDispatcher 	= namespace.events.EventDispatcher;
	var CommonHeader 		= namespace.common.CommonHeader;
	var CommonFooter 		= namespace.common.CommonFooter;
	var AnimationManager 	= namespace.animation.AnimationManager;
	var pages 				= monkikiNS.getNamespace("generic.pages");
	var dataManager;



	if (!namespace.SiteManagerBasic) {

		var SiteManagerBasic = function SiteManagerBasic() {

			this.dataManager 	= null;
			this.pages 			= [];
			this.overlays 		= [];
			this.beginpages 	= [];
		}

		namespace.SiteManagerBasic = SiteManagerBasic;
		p = SiteManagerBasic.prototype = new EventDispatcher();


		p.Setup = function() {

			console.log('Setup Begins now');
			this.dataManager = dataManager = namespace.data.DataManager.createSingleton();
			dataManager.setBrowserData({
                browserWidth: 320,
                browserHeight: 568,
                footerHeight: 0,
                headerHeight: 0
            });
            
            var that = this;

            var dContent 	= this.contentDiv 	= document.getElementById('content');
            var dOverlay 	= this.overlayDiv 	= document.getElementById('overlay');
            var dBegin 		= this.beginDiv 	= document.getElementById('begin');

			this.loader = new modules.ModuleLoader();
            var lD = document.getElementById('loader');
            this.loader.Open(lD);

			//begin the Tween Animation
			this.animationManager = AnimationManager.create();         
			var that = this;

			var headDiv 	= this.headDiv = document.getElementById('head');
			var sHeader 	= new CommonHeader(); 
			sHeader.Open(headDiv);
			sHeader.addEventListener(CommonHeader.ALERTTAP, function(e){ that.alertTapHandler(e) });
			//swipe or tap top
			sHeader.addEventListener(CommonHeader.LOCATIONTAP, function(e){ that.locationTapHandler(e) });
			sHeader.addEventListener(CommonHeader.MENUTAP, function(e){ that.slideTapHandler(e) });
			var hammertime = Hammer(document.body).on("swipe", function(e) { that.slideSwipeHandler(e) });

			var footDiv 	= this.footDiv = document.getElementById('foot');
			var sFooter 	= new CommonFooter();
			sFooter.Open(footDiv);
          	sFooter.addEventListener(CommonFooter.MENUTAP, function(e){ that.menuTapHandler(e) });

          	var contentDiv 	= document.getElementById('content');
          	var pageList 	= dataManager.getMenuData();
          	this.pageFunc 	= this.SetupPages(contentDiv, pageList);

			//prepare ALERT
			var aL 			= new modules.ModuleAlert();
			this.initOverlay(aL, dOverlay);

          	//prepare LOGIN
			var lO 			= new pages.PageLogin();
			this.initBeginPage(lO, dBegin);
			lO.addEventListener(pages.PageLogin.LOGIN, function(e){ that.loginExistingHandler(e) });
			// lO.addEventListener(Pages.PageLogin.BACK, function(e){ that.loginBackHandler(e) });
			lO.addEventListener(pages.PageLogin.LOGINSUCCESS, function(e){ that.loginExistingSuccessHandler(e) });


			console.log('Setup Complete');

			this.SetupAfter();
		}

		//create the divisions
		p.SetupPages = function(contentDiv, pList) {

			var pListLength = pList.length;
			var oFunc 		= {};
			for(var i=0; i<pListLength; i++) {
				var oName 		= 'Page'+ this.capitaliseFirstLetter(pList[i]);
				var PageFunc 	= monkikiNS.getNamespace("generic.pages")[oName];
				var pFunc 		= new PageFunc();
				pFunc.Open(contentDiv);
				oFunc[pList[i]] = pFunc;
			}

			return oFunc;

		}

		p.SetupAfter = function() { }

		p.Load = function() {

			//internal loader
			this.LoadComplete();
		}

		p.LoadComplete = function() {
			this.loader.Close();
		}


        //init all pages
        p.initPage = function(pObj, el) {

          	pObj.Open(el);
          	this.pages.push(pObj);
        }

        //init all pages
        p.initOverlay = function(pObj, el) {

        	console.log('hi', el, pObj);
          	pObj.Open(el);
          	this.overlays.push(pObj);
          	console.log('hello');
        }

        p.initBeginPage = function(pObj, el) {
          pObj.Open(el);
          this.beginpages.push(pObj);          
        }

		p.menuTapHandler = function(e) { }

		p.alertTapHandler = function(e) { }

		p.locationTapHandler = function(e) {}

		p.slideTapHandler = function(e) {}

		p.slideSwipeHandler = function(e) {}

		//LOGIN PAGE HANDLER
        p.loginExistingHandler = function(e) {
            var pP = this.beginpages;
            pP[0].tapClose();       
        }

        p.loginExistingSuccessHandler = function(e) {
            // hide the login section
            this.beginDiv.style.display = 'none'; 
        }

        p.loginBackHandler = function(e) {
            var pP = this.beginpages;
            pP[1].tapOpen();      
        }

		//capitalize string
		p.capitaliseFirstLetter = function(string)
		{
		    return string.charAt(0).toUpperCase() + string.slice(1);
		}

	}


})();