(function(){

	var namespace 			= monkikiNS.getNamespace("generic");
	var modules 			= monkikiNS.getNamespace("generic.modules");
	var CommonHeader 		= namespace.common.CommonHeader;
	var CommonFooter 		= namespace.common.CommonFooter;
	var AnimationManager 	= namespace.animation.AnimationManager;
	var SiteManagerBasic 	= namespace.SiteManagerBasic;
	var DomElementPosition 	= monkikiNS.getNamespace("generic.animation").DomElementPosition;


	if (!namespace.SiteManager) {

		var SiteManager = function SiteManager() {

			this.loader 	= null;
			this.pages 		= [];
			this.overlays 	= [];
			this.currentPageName = null;
			this.isSlide = false;
			this.isAlertOpen = false;
		}

		namespace.SiteManager = SiteManager;
		p = SiteManager.prototype = new SiteManagerBasic();

		p.SetupAfter = function() {

			//hack for now, fix later!!!
			this.alertTapHandler();
		}


        p.resetZIndex = function(current) {
          var pP 		= this.pageFunc;
          var dM        = this.dataManager.getMenuData();
          var dMLength  = dM.length;
          console.log('current page', this.currentPage, current);
          for(var i=0; i<dMLength; i++) {

            //close tap of not current or previous page
            // if (pP[i]!=current && pP[i]!=this.currentPage)    pP[i].tapClose(); 

            //set the current and previous page to top and second to top then rest are at the bottom
            if (dM[i]===current) {
            	 pP[current].Show();
            	 pP[current].setZIndex(99);
            }                             
            // else if (dM[i]==this.currentPageName)             pP[dM[i]].setZIndex(98);
            else {
            	pP[dM[i]].Hide();
            	pP[dM[i]].setZIndex(i);
            }
          }
        }

        p.resetOverlayZIndex = function(current, isOpen) {
          var pP        = this.overlays;
          var pPLength  = this.overlays.length;
          for(var i=0; i<pPLength; i++) {

            if (isOpen) {
            	this.overlayDiv.style.display = 'block';
            	pP[i].setZIndex(i+100); //set to be higher than page
        	}
            else {
            	this.overlayDiv.style.display = 'none';
            	pP[i].setZIndex(i+10);  //set slightly higher page
        	}
          }                  
        }

		//footer menu setup
		p.menuTapHandler = function(e) {
			dM 			= this.dataManager.getFooterMenuData();
			dMLength 	= dM.length;
			console.log('hello',e.detail.id);
			var currentTween = TWEEN.getAll().length;
			if (currentTween>0 || e.detail==this.currentPageName ||  e.detail==null) return;

			for(var i=0; i<dMLength; i++) {

				var el = document.getElementById(dM[i]);
				if (e.detail.id == dM[i]) 	{ 
					el.style.opacity = 1; 
					this.resetZIndex(e.detail.name);
					this.currentPageName = e.detail.name;
				}
				else { 
					el.style.opacity = 0.4; 
				}
			}

			console.log('testing complete');
		}

		p.slideTapHandler = function(e) {
			if (this.isAlertOpen==true) return;
			if (this.isSlide == false) {
				var dD = DomElementPosition.createWithAnimation(this.contentDiv, 0, 0, 280, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.headDiv, 0, 0, 280, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.footDiv, 0, 0, 280, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				this.isSlide = true;
			}
			else {
				var dD = DomElementPosition.createWithAnimation(this.contentDiv, 280, 0, 0, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.headDiv, 280, 0, 0, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.footDiv, 280, 0, 0, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				this.isSlide = false;
			}
		}

		p.slideSwipeHandler = function(e) {

			if (this.isAlertOpen==true) return;
			e.gesture.preventDefault();
			if (this.isSlide == false && e.gesture.direction=='right') {
				var dD = DomElementPosition.createWithAnimation(this.contentDiv, 0, 0, 280, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.headDiv, 0, 0, 280, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.footDiv, 0, 0, 280, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				this.isSlide = true;
			}
			else if (e.gesture.direction=='left') {
				var dD = DomElementPosition.createWithAnimation(this.contentDiv, 280, 0, 0, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.headDiv, 280, 0, 0, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				var dD = DomElementPosition.createWithAnimation(this.footDiv, 280, 0, 0, 0, this.dataManager.tweenOptions.verticalTweenSpeed/2, this.dataManager.tweenOptions.verticalTweenEasingType, 0);
				this.isSlide = false;
			}			
		}

		p.locationTapHandler = function(e) {
			
		}

		p.alertTapHandler = function(e) { 
			console.log('alert Tap to outside');
			//check if something is tweening or it is current then return
			var currentTween = TWEEN.getAll().length;
			if (currentTween>0) return;

			var pP    	= this.overlays;
			var isOpen 	= this.isAlertOpen = pP[0].tapToggle();
			this.resetOverlayZIndex(pP[0], isOpen);			
		}

        //init singleton
		SiteManager.createSingleton = function() {
			if (!namespace.singletons) namespace.singletons = {};
			if (!namespace.singletons.siteManager)
			{
				namespace.singletons.siteManager = new SiteManager();
				namespace.singletons.siteManager.Setup();
			}	
			return namespace.singletons.siteManager;		
		};

	}


})();