(function(){

    // ==============================================
    // 
    //  BEGINS!!!
    // 
    // ==============================================
    function LibLoaded() {

        console.log('jaja')
        var SiteManager = monkikiNS.getNamespace("generic").SiteManager;
        var siteManager = SiteManager.createSingleton();
        siteManager.Load();
    }

    function deviceReady() {

          $LAB
          .script("js/libs/canvasloader.js")
          //external libraries
          .script("js/libs/tween.js")
          .script("js/libs/hammer.js")
          .script("js/utils/polyfill.js")
          .wait()

          //internal libraries
          .script("js/monkiki/app.js")
          .script("js/alchemy/data/data-manager.js")
          .wait()
          .script("js/monkiki/events/EventDispatcher.js")
          .script("js/monkiki/animation/AnimationManager.js")
          .script("js/monkiki/animation/DomElementScale.js")
          .script("js/monkiki/animation/DomElementPosition.js")
          .script("js/monkiki/animation/DomElementOpacity.js")
          .script("js/monkiki/animation/DomElementRotate.js")
          .script("js/monkiki/animation/DomElementBG.js")
          .wait()
          //modules with pages
          .script("js/monkiki/modules/module-basic.js")
          .script("js/monkiki/modules/module-basicpage.js")  
          .script("js/monkiki/modules/module-loader.js") 
          .script("js/monkiki/modules/module-alert.js") 
          .wait()
          //commons
          .script("js/alchemy/common/common-footer.js")
          .script("js/alchemy/common/common-header.js")

          //load the pages
          .script("js/alchemy/pages/page-summary.js")  
          .script("js/alchemy/pages/page-expenses.js")  
          .script("js/alchemy/pages/page-accounts.js")  
          .script("js/alchemy/pages/page-exchange.js")  
          .script("js/alchemy/pages/page-nearby.js")  
          .script("js/alchemy/pages/page-login.js")  
          .wait()
          //begin with siteManager
          .script("js/alchemy/SiteManager-basic.js")  
          .script("js/alchemy/SiteManager.js")      
          .wait(LibLoaded);

    }

    //Only loads through Device ready if they are the following devices
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
        document.addEventListener("deviceready", deviceReady, false);
    } else {
        deviceReady();
    }

})();