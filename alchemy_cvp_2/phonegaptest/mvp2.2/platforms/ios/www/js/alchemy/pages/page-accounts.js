(function(){

	var namespace = 		monkikiNS.getNamespace("generic");
	var pages = 			monkikiNS.getNamespace("generic.pages");
	var ModuleBasicPage =	monkikiNS.getNamespace("generic.modules").ModuleBasicPage;

	if (!pages.PageAccounts) {

		var PageAccounts = function PageAccounts() {


			this.Init();
		}

		pages.PageAccounts = PageAccounts;
		var p = PageAccounts.prototype = new ModuleBasicPage();

		p.Init = function() {

			this.pageId 	= 'accounts';
			this.pageClass 	= 'page-container';

			this.template = [
				'<img class="pullRefresh" />',
				'<img class="dummyimg" src="img/dummy/accounts-dummy@2x.png" />'	
			].join('');
		}
	}

})();