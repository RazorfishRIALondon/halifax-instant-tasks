(function(){

    var namespace       = monkikiNS.getNamespace("generic.data");
    var ns              = monkikiNS.getNamespace("generic");
    var EventDispatcher = monkikiNS.getNamespace("generic.events").EventDispatcher;

    if (!namespace.DataManager) {

 		namespace.DataManager = function() {

            //STATIC OPTIONS
            this.tweenOptions = {
                verticalTweenEasingType:    TWEEN.Easing.Quintic.InOut,
                verticalTweenSpeed:         500,
                horizontalTweenEasingType:  TWEEN.Easing.Elastic.InOut,
                opacityTweenSpeed:          250
            };

 			this.browserOptions = {
                browserWidth: 0,
                browserHeight: 0,
                footerHeight: 0,
                headerHeight: 0


            };

            this._footerMenuOptions = [
                'footsummary',
                'footexpenses',
                'footaccounts',
                'footexchange',
                'footnearby'

            ];
        };

        var p = namespace.DataManager.prototype;  

        p.Setup = function() {
        	console.log('DataManager init');
        } 	

        p.setFooterMenuData = function(eE) {
            this._footerMenuOptions = eE;
        }

        p.getFooterMenuData = function() {
            return this._footerMenuOptions;
        }

        p.getMenuData = function() {
            var _footerMenuOptionsLength = this._footerMenuOptions.length;
            var out = [];
            for(var i=0; i<_footerMenuOptionsLength; i++) {
                out[i] = this._footerMenuOptions[i].replace('foot', '');
            }

            return out;
        }

        p._initRemoteServerData = function() {

        }

        p._initLocalServerData = function() {

        }

        p.setBrowserData = function(options) {
        	var opt = window.mergeObjects(this.browserOptions, options);
        	this.browserOptions = opt;
        	return opt;
        }

        p.getBrowserData = function() {
            return this.browserOptions;
        }

		namespace.DataManager.createSingleton = function() {
			if (!ns.singletons) ns.singletons = {};
			if (!ns.singletons.dataManager)
			{
				ns.singletons.dataManager = new namespace.DataManager();
				ns.singletons.dataManager.Setup();
			}	
			return ns.singletons.dataManager;		
		};

    }


})();