(function(globalObj){

    var namespace   = monkikiNS.getNamespace("generic.modules");
    var ns          = monkikiNS.getNamespace("generic");
    var ModuleBasic = namespace.ModuleBasic;

    if (!namespace.ModuleLoader) {

        namespace.ModuleLoader = function() {

            this.templateHolder = null;
            this.Init();
        };

        namespace.ModuleLoader.CloseComplete = "LBGAlchemy.ModuleLoader.CloseComplete";

        var p = namespace.ModuleLoader.prototype = new ModuleBasic();

        p.Init = function() {

                console.log('loader init');
                this.template = [

                    '<div id="main-loader-logo">',
                    '<img src="img/header-logo.png" width="250" height="51" />',
                    '</div>',
                    '<div id="main-loader-spinner">',
                    '</div>'

                ].join('');

        }

        p.Opened = function() {
                var cl = new CanvasLoader("main-loader-spinner");
                cl.setColor('#ffffff'); // default is '#000000'
                cl.setShape('spiral'); // default is 'oval'
                cl.setDiameter(60); // default is 40
                cl.setDensity(14); // default is 40
                cl.setSpeed(1); // default is 2
                cl.setFPS(30); // default is 24
                cl.show(); // Hidden by default
                this.cl = cl;
        }

        p.Close = function() {

            //temp
            this.CloseComplete();
        }

        p.CloseComplete = function() {
            this.templateHolder.style.display = 'none';
            this.cl.hide();
            this.dispatchEvent(LBGAlchemy.ModuleLoader.CloseComplete);
        }
    }


})(window);