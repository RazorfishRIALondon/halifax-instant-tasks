(function(globalObj){


    if (globalObj.monkikiNS) {
        console.warn("monkikiNS global object has already been constructed.");
        return;
    }
    var monkikiNS = {};
    globalObj.monkikiNS = monkikiNS;
    monkikiNS.projectName = "lbsMvp";

    monkikiNS.getNamespace = function(aPackagePath) {

        var currentObject = this;

        var currentArray = aPackagePath.split(".");
        var currentArrayLength = currentArray.length;
        for(var i = 0; i < currentArrayLength; i++) {
          var currentName = currentArray[i];
          if(currentObject[currentName] === undefined) {
            currentObject[currentName] = {};
          }
          currentObject = currentObject[currentName];
        }

        return currentObject;
    };

    monkikiNS.getClass = function(aClassPath) {

        var lastSplitPosition = aClassPath.lastIndexOf(".");
        var packagePath = aClassPath.substring(0, lastSplitPosition);
        var className = aClassPath.substring(lastSplitPosition+1, aClassPath.length);
        
        var packageObject = this.getNamespace(packagePath);
        if(packageObject[className] === undefined) {
          console.error("Class " + aClassPath + " doesn't exist.");
          return null;
        }
        return packageObject[className];
    };

    monkikiNS.singletons = new Object();

})(window);


