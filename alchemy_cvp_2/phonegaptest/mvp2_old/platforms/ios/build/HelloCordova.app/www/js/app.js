(function(globalObj){


    if (!globalObj.namespace) {
        globalObj.namespace = {};
    }
    var ns = globalObj.namespace;

    if (!ns.LBGAlchemy) {
        ns.LBGAlchemy = {};
    }

    var dataManager;
    var LBGAlchemy    = ns.LBGAlchemy;

    if (!ns.LBGAlchemy.App) {

            LBGAlchemy.App   = function () {

                this.Init();
                this.pages = [];
            };

        var p = LBGAlchemy.App.prototype;

        p.Init = function() {
          console.log('LGBAlchemy.func.Init called.');

          var that = this;
          //load all libraries modules
          $LAB
          //external libraries
          .script("js/tween.js")
          .script("js/hammer.js")
          .script("js/canvasloader.js")
          .wait(function(){that.InitLoaderComplete()})

          //internal libraries
          .script("js/utils/polyfill.js")
          .script("js/events/EventDispatcher.js")
          .script("js/data/data-manager.js")
          .script("js/animation/AnimationManager.js")
          .script("js/animation/DomElementScale.js")
          .script("js/animation/DomElementPosition.js")
          .wait()
          //modules with pages
          .script("js/modules/module-basic.js")
          .script("js/modules/module-basicpage.js")
          .wait()
          .script("js/modules/module-loader.js")
          .script("js/modules/module-header.js")
          .script("js/modules/module-footer.js")
          .script("js/modules/module-home.js")
          .script("js/modules/module-profile.js")
          .script("js/modules/module-money.js")
          .script("js/modules/module-atm.js")
          .script("js/modules/module-contact.js")
          .wait(function(){that.InitLoaderComplete()})
          .wait(function(){that.InitComplete()});
        }


        p.InitLoaderComplete = function() {
            console.log('LGBAlchemy.func.InitLoaderComplete called.');
            var nN = new LBGAlchemy.ModuleLoader();
            var lD = document.getElementById('loader');
            nN.Open(lD);
            console.log(nN);
            this.loader = nN;
            //loading disable temporary
        }

        p.InitComplete = function() {
          console.log('LGBAlchemy.func.InitComplete called.');

          this.loader.Close();


          dataManager = LBGAlchemy.DataManager.createSingleton();
            var bData = dataManager.setBrowserData({
                browserWidth: document.body.clientWidth,
                browserHeight: document.body.clientHeight,

            });

            var fData = dataManager.setFooterMenuData([

                'profile',
                'abroad',
                'money',
                'atm',
                'contact'
            ]);

          //begin the Tween Animation
          this.animationManager = LBGAlchemy.AnimationManager.create();         
          // prepare header and footer
          var mM = this.mM = new LBGAlchemy.ModuleHeader();
          var hD = document.getElementById('head');
          mM.Open(hD);

          var oO = this.oO = new LBGAlchemy.ModuleFooter();
          var fD = document.getElementById('foot');
          oO.Open(fD);
          oO.addEventListener(LBGAlchemy.ModuleFooter.MENUTAP, function(e){ that.menuTapHandler(e) });


          var dContent = document.getElementById('content');
          //prepare HOME
          var hO = new LBGAlchemy.ModuleHome();
          hO.setIsDefault(true);
          this.initPage(hO, dContent);

          //prepare PROFILE
          var pR = new LBGAlchemy.ModuleProfile();
          this.initPage(pR, dContent);

          //prepare MONEY
          var mO = new LBGAlchemy.ModuleMoney();
          this.initPage(mO, dContent);

          //prepare ATM
          var aT = new LBGAlchemy.ModuleAtm();
          this.initPage(aT, dContent);

          // //prepare PROFILE
          var cO = new LBGAlchemy.ModuleContact();
          this.initPage(cO, dContent);

          //window resize
          this.onresizeFunc();
          var that = this;
          window.onresize = function(){ that.onresizeFunc(); };
        }

        //resize all pages
        p.onresizeFunc = function() {

          var pP        = this.pages;
          var pPLength  = this.pages.length;
          for(var i=0; i<pPLength; i++) {
            console.log()
            if (typeof pP[i].onresize ==='function') pP[i].onresize();
          }
        }

        //init all pages
        p.initPage = function(pObj, el) {

          pObj.Open(el);
          this.pages.push(pObj);
        }

        //menu handler
        p.menuTapHandler = function(e) {

          //check if something is tweening or it is current then return
          var currentTween = TWEEN.getAll().length;
          if (currentTween>0 || e.detail==this.currentPageName) return;

          var opt = dataManager.getFooterMenuData();
          var pP = this.pages;

          if (e.detail)
          switch(e.detail) {
            case opt[0]:
              //home
              this.resetZIndex(pP[0]);
              pP[0].tapOpen();
              this.currentPage = pP[0]; 
              this.oO.moveTriangle(0);
              break;
            case opt[1]:
              //profile
              this.resetZIndex(pP[1]);
              pP[1].tapOpen();
              this.currentPage = pP[1]; 
              this.oO.moveTriangle(1);
              break;
            case opt[2]:
              //money
              this.resetZIndex(pP[2]);
              pP[2].tapOpen();
              this.currentPage = pP[2]; 
              this.oO.moveTriangle(2);
              break;
            case opt[3]:
              //atm
              this.resetZIndex(pP[3]);
              pP[3].tapOpen();
              this.currentPage = pP[3]; 
              this.oO.moveTriangle(3);
              break;
            case opt[4]:
              //contact
              this.resetZIndex(pP[4]);
              pP[4].tapOpen();
              this.currentPage = pP[4]; 
              this.oO.moveTriangle(4);
              break;
          }

          this.currentPageName = e.detail;
        }

        p.resetZIndex = function(current) {
          var pP        = this.pages;
          var pPLength  = this.pages.length;
          for(var i=0; i<pPLength; i++) {

            if (pP[i]!=current || pP[i]!=this.currentPage)    pP[i].tapClose();

            if (pP[i]===current)                              current.setZIndex(99);
            else if (pP[i]===this.currentPage)                pP[i].setZIndex(98);
            else                                              pP[i].setZIndex(i);
          }
        }
    }


})(window);

var lbgalchemyApp = new window.namespace.LBGAlchemy.App();