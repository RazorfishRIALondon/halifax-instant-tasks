(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleContact) {

        LBGAlchemy.ModuleContact = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleContact.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        p.Init = function() {
            this.pageId = 'contact';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   
            ];       
        }


    }

})(window);