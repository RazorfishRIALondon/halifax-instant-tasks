(function(globalObj){

    var LBGAlchemy      = globalObj.namespace.LBGAlchemy;
    var EventDispatcher = LBGAlchemy.EventDispatcher;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var DomElementScale = LBGAlchemy.DomElementScale;
    var ModuleBasic     = LBGAlchemy.ModuleBasic;
    var dataManager;

    if (!LBGAlchemy.ModuleBasicPage) {

        LBGAlchemy.ModuleBasicPage = function() {

            this.templateHolder = null;
            this.Init();
            this.innerContainer = null;
            this.isDefault = false;
        };

        var p = LBGAlchemy.ModuleBasicPage.prototype = new ModuleBasic();
        var s = LBGAlchemy.ModuleBasic.prototype;


        p.Init = function() {

        }

        p.Opened = function(tDiv) {
            console.log('innercontainer', tDiv);
            this.innerContainer = tDiv;
            console.log('innercontainer as', this.innerContainer);
            dataManager = LBGAlchemy.singletons.dataManager;

            this.OpenedAfter(tDiv);
        }

        p.OpenedAfter = function(tDiv) {

        }


        //open and close from footer menu
        p.tapOpen = function() {
            console.log('tapOpen', this.innerContainer);
            var dD = DomElementPosition.createWithAnimation(this.innerContainer, 0, dataManager.browserOptions.browserHeight, 0, 0, dataManager.tweenOptions.verticalTweenSpeed, dataManager.tweenOptions.verticalTweenEasingType, 0);
        }

        p.tapClose = function() {
            var dD = DomElementPosition.createWithAnimation(this.innerContainer, 0, 0, 0, dataManager.browserOptions.browserHeight, 0, dataManager.tweenOptions.verticalTweenEasingType, 0);
            console.log(dD);
        }     

        p.getZIndex = function() {
            return this.container.style.zIndex;
        }

        p.setZIndex = function(z) {
            this.innerContainer.style.zIndex = z;
        } 


    }


})(window);