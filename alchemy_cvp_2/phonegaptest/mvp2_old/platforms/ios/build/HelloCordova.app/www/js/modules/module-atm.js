(function(globalObj){

    var LBGAlchemy = globalObj.namespace.LBGAlchemy;
    var ModuleBasicPage = LBGAlchemy.ModuleBasicPage;
    var ModuleBasic = LBGAlchemy.ModuleBasic;
    var DomElementPosition = LBGAlchemy.DomElementPosition;
    var dataManager;

    if (!LBGAlchemy.ModuleAtm) {

        LBGAlchemy.ModuleAtm = function() {

            this.templateHolder = null;
            this.options = {

            };

            this.Init();
        };

        var p = LBGAlchemy.ModuleAtm.prototype = new ModuleBasicPage();
        var s = ModuleBasicPage.prototype;
        var u = ModuleBasic.prototype;

        p.Init = function() {
            this.pageId = 'atm';
            this.pageClass = 'page-container';
            this.options = {
                'hidePosition':0,
                'showPosition':0
            };

            this.template = [   
            ];       
        }


    }

})(window);