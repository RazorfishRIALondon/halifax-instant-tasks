3.2.10 (Media Mark)
69a371280247703b6826a506d3f305faebdca55f
o:Sass::Tree::RootNode
:@template"�// Tab icons used with permission from Drew Wilson
// http://pictos.drewwilson.com/
// Pictos icons are (c) 2010 Drew Wilson

@import '../global';

/**
 * @class Ext.tab.Bar
 */

/**
 * @var {boolean} $include-tabbar-uis Optionally disable separate tabbar UIs (light and dark).
 */
$include-tabbar-uis: $include-default-uis !default;

/**
 * @var {boolean} $include-top-tabs
 * Optionally exclude top tab styles by setting to false.
 */
$include-top-tabs: true !default;

/**
 * @var {boolean} $include-bottom-tabs
 * Optionally exclude bottom tab styles by setting to false.
 */
$include-bottom-tabs: true !default;

/**
 * @var {color} $tabs-light
 * Base color for "light" UI tabs.
 */
$tabs-light: desaturate($base-color, 10%) !default;

/**
 * @var {color} $tabs-light-active
 * Active color for "light" UI tabs.
 */
$tabs-light-active: lighten(saturate($active-color, 20%), 20%) !default;

/**
 * @var {color} $tabs-dark
 * Base color for "dark" UI tabs.
 */
$tabs-dark: darken($base-color, 20%) !default;

/**
 * @var {color} $tabs-dark-active
 * Active color for "dark" UI tabs.
 */
$tabs-dark-active-color: saturate(lighten($active-color, 30%), 70%) !default;

/**
 * @var {string} $tabs-bar-gradient
 * Background gradient style for tab bars.
 */
$tabs-bar-gradient: $base-gradient !default;

/**
 * @class Ext.tab.Tab
 */

/**
 * @var {string} $tabs-bottom-radius
 * Border-radius for bottom tabs.
 */
$tabs-bottom-radius: .25em !default;

/**
 * @var {string} $tabs-bottom-icon-size
 * Icon size for bottom tabs
 */
$tabs-bottom-icon-size: 1.65em !default;

/**
 * @var {string} $tabs-bottom-active-gradient
 * Background gradient style for active bottom tabs.
 */
$tabs-bottom-active-gradient: $base-gradient !default;

/**
 * @var {boolean} $include-tab-highlights
 * Optionally disable all gradients, text-shadows, and box-shadows. Useful for CSS debugging,
 * non-performant browsers, or minimalist designs.
 */
$include-tab-highlights: $include-highlights !default;

// Private

$tabs-top-height: $global-row-height - .8em;

$tabs-top-icon-size: $tabs-top-height - .6em;

/**
 * Includes default tab styles.
 *
 * @member Ext.tab.Bar
 */
@mixin sencha-tabs {
  @if $include-top-tabs {
    @include sencha-top-tabs;
  }
  @if $include-bottom-tabs {
    @include sencha-bottom-tabs;
  }

  @if $include-tabbar-uis {
    @include sencha-tabbar-ui('light', $tabs-light, $tabs-bar-gradient, $tabs-light-active);
    @include sencha-tabbar-ui('dark', $tabs-dark, $tabs-bar-gradient, $tabs-dark-active-color);
    @include sencha-tabbar-ui('neutral', $neutral-color, $tabs-bar-gradient, darken($neutral-color, 40));
  }

  // Rules for all tabs
  .x-tab.x-item-disabled span.x-button-label, .x-tab.x-item-disabled .x-button-icon {
    @include opacity(.5);
  }
  .x-tab.x-draggable {
    @include opacity(.7);
  }

  .x-tab {
    z-index: 1;
    -webkit-user-select: none;
    overflow: visible !important;

    .x-button-label {
      overflow: visible !important;
    }
  }
}


@mixin sencha-top-tabs {
  .x-tabbar.x-docked-top {
    border-bottom-width: .1em;
    border-bottom-style: solid;
    height: $global-row-height;
    padding: 0 .8em;

    .x-tab {
      position: relative;
      padding: (($tabs-top-height - 1em) / 2) .8em;
      height: $tabs-top-height;
      min-width : 3.3em;
      @if $include-border-radius { @include border-radius($tabs-top-height / 2); }

      .x-button-label {
        font-size              : .8em;
        line-height            : 1.2em;
        text-rendering         : optimizeLegibility;
        -webkit-font-smoothing : antialiased;
      }

      .x-badge {
        font-size : .6em !important;
        top       : -0.5em;
      }

      &.x-tab-icon {
        padding : (($tabs-top-height - 1em) / 2) - .1em .8em;

        .x-button-icon {
          -webkit-mask-size : $tabs-top-icon-size;
          width             : $tabs-top-icon-size;
          height            : $tabs-top-icon-size;
          display           : inline-block;
          margin            : 0 auto;
          position          : relative;
        }

        .x-button-label {
          margin      : 0;
          margin-left : .3em;
          padding     : .1em 0 .2em 0;
          display     : inline-block;
          position    : relative;
          top         : -.4em;
        }
      }
    }
  }
}


@mixin sencha-bottom-tabs {
  .x-tabbar.x-docked-bottom {
    border-top-width: .1em;
    border-top-style: solid;
    height: 3em;
    padding: 0;

    .x-tab {
      @if $include-border-radius { @include border-radius($tabs-bottom-radius); }
      min-width: 3.3em;
      position: relative;
      padding-top: .2em;
      @include box-orient(vertical);

      .x-button-icon {
        -webkit-mask-size: $tabs-bottom-icon-size;
        width: $tabs-bottom-icon-size;
        height: $tabs-bottom-icon-size;
        display: block;
        margin: 0 auto;
        position: relative;
      }

      .x-button-label {
        margin: 0;
        padding: .1em 0 .2em 0;
        font-size: 9px;
        line-height: 12px;
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
      }
    }
  }

  @if $include-default-icons {
    @include pictos-iconmask('bookmarks');
    @include pictos-iconmask('download');
    @include pictos-iconmask('favorites');
    @include pictos-iconmask('info');
    @include pictos-iconmask('more');
    @include pictos-iconmask('time');
    @include pictos-iconmask('user');
    @include pictos-iconmask('team');
  }
}

/**
 * Creates a theme UI for tabbar/tab components.
 *
 *     // SCSS
 *     @include sencha-button-ui('pink', #333, 'matte', #AE537A);
 *
 *     // JS
 *     var tabs = new Ext.tab.Panel({
 *        tabBar: {
 *          ui: 'pink',
 *          dock: 'bottom',
 *          layout: { pack: 'center' }
 *        },
 *        ...
 *     });
 *
 * @param {string} $ui-label The name of the UI being created.
 *   Can not included spaces or special punctuation (used in class names)
 * @param {color} $bar-color Base color for the tab bar.
 * @param {string} $bar-gradient Background gradient style for the tab bar.
 * @param {color} $tab-active-color Background-color for active tab icons.
 *
 * @member Ext.tab.Bar
 */
@mixin sencha-tabbar-ui($ui-label, $bar-color, $bar-gradient, $tab-active-color) {
  .x-tabbar-#{$ui-label} {
    @include background-gradient($bar-color, $bar-gradient);
    border-top-color: darken($bar-color, 5%);
    border-bottom-color: darken($bar-color, 15%);

    .x-tab {
      @include color-by-background($bar-color, 40%);
      border-bottom: 1px solid transparent;
    }

    .x-tab-active {
      @include color-by-background($bar-color, 90%);
      border-bottom-color: lighten($bar-color, 3%);
    }

    .x-tab-pressed {
      @include color-by-background($bar-color, 100%);
    }
  }

  @if $include-bottom-tabs {
    .x-tabbar-#{$ui-label}.x-docked-bottom {
      .x-tab {
        @include bevel-by-background($bar-color);
        .x-button-icon {
          @include mask-by-background($bar-color, 20%, $tabs-bar-gradient);
        }
      }

      .x-tab-active {
        @include background-gradient(darken($bar-color, 5%), recessed);
        @include bevel-by-background(lighten($bar-color, 10%));

        @if ($include-tab-highlights) {
          @include box-shadow(darken($bar-color, 10%) 0 0 .25em inset);
        }

        .x-button-icon {
          @include background-gradient($tab-active-color, $tabs-bottom-active-gradient);
        }
      }
    }
  }

  @if $include-top-tabs {
    .x-tabbar-#{$ui-label}.x-docked-top {
      .x-tab-active {
        @include background-gradient(darken($bar-color, 5%), 'recessed');
        @include color-by-background(darken($bar-color, 5%));
      }
      .x-tab {
        .x-button-icon {
          @include mask-by-background($bar-color, 20%, $tabs-bar-gradient);
        }
      }
    }
  }
}
:
@linei:@options{ :@has_childrenT:@children[*o:Sass::Tree::CommentNode
;i;@;
[ :
@type:silent:@value["/* Tab icons used with permission from Drew Wilson
 * http://pictos.drewwilson.com/
 * Pictos icons are (c) 2010 Drew Wilson */o:Sass::Tree::ImportNode;0;i
;@:@imported_file0;
[ :@imported_filename"../globalo;
;i;@;
[ ;:normal;[""/**
 * @class Ext.tab.Bar
 */o;
;i;@;
[ ;;;["k/**
 * @var {boolean} $include-tabbar-uis Optionally disable separate tabbar UIs (light and dark).
 */o:Sass::Tree::VariableNode;i:@guarded"!default;@:
@name"include-tabbar-uis:
@expro:Sass::Script::Variable	;i;@;"include-default-uis:@underscored_name"include_default_uis;
[ o;
;i;@;
[ ;;;["j/**
 * @var {boolean} $include-top-tabs
 * Optionally exclude top tab styles by setting to false.
 */o;;i;"!default;@;"include-top-tabs;o:Sass::Script::Bool;i;@;T;
[ o;
;i;@;
[ ;;;["p/**
 * @var {boolean} $include-bottom-tabs
 * Optionally exclude bottom tab styles by setting to false.
 */o;;i;"!default;@;"include-bottom-tabs;o;;i;@;T;
[ o;
;i!;@;
[ ;;;["K/**
 * @var {color} $tabs-light
 * Base color for "light" UI tabs.
 */o;;i%;"!default;@;"tabs-light;o:Sass::Script::Funcall:@keywords{ ;i%;@;"desaturate:@splat0:
@args[o;	;i%;@;"base-color;"base_coloro:Sass::Script::Number;i%;@:@denominator_units[ :@numerator_units["%:@original"10%;i;
[ o;
;i';@;
[ ;;;["T/**
 * @var {color} $tabs-light-active
 * Active color for "light" UI tabs.
 */o;;i+;"!default;@;"tabs-light-active;o;;{ ;i+;@;"lighten;0;[o;;{ ;i+;@;"saturate;0;[o;	;i+;@;"active-color;"active_coloro;;i+;@;[ ; ["%;!"20%;io;;i+;@;[ ; ["%;!"20%;i;
[ o;
;i-;@;
[ ;;;["I/**
 * @var {color} $tabs-dark
 * Base color for "dark" UI tabs.
 */o;;i1;"!default;@;"tabs-dark;o;;{ ;i1;@;"darken;0;[o;	;i1;@;"base-color;"base_coloro;;i1;@;[ ; ["%;!"20%;i;
[ o;
;i3;@;
[ ;;;["R/**
 * @var {color} $tabs-dark-active
 * Active color for "dark" UI tabs.
 */o;;i7;"!default;@;"tabs-dark-active-color;o;;{ ;i7;@;"saturate;0;[o;;{ ;i7;@;"lighten;0;[o;	;i7;@;"active-color;"active_coloro;;i7;@;[ ; ["%;!"30%;i#o;;i7;@;[ ; ["%;!"70%;iK;
[ o;
;i9;@;
[ ;;;["[/**
 * @var {string} $tabs-bar-gradient
 * Background gradient style for tab bars.
 */o;;i=;"!default;@;"tabs-bar-gradient;o;	;i=;@;"base-gradient;"base_gradient;
[ o;
;i?;@;
[ ;;;[""/**
 * @class Ext.tab.Tab
 */o;
;iC;@;
[ ;;;["S/**
 * @var {string} $tabs-bottom-radius
 * Border-radius for bottom tabs.
 */o;;iG;"!default;@;"tabs-bottom-radius;o;;iG;@;[ ; ["em;!"0.25em;f	0.25;
[ o;
;iI;@;
[ ;;;["Q/**
 * @var {string} $tabs-bottom-icon-size
 * Icon size for bottom tabs
 */o;;iM;"!default;@;"tabs-bottom-icon-size;o;;iM;@;[ ; ["em;!"1.65em;f1.6499999999999999 ff;
[ o;
;iO;@;
[ ;;;["o/**
 * @var {string} $tabs-bottom-active-gradient
 * Background gradient style for active bottom tabs.
 */o;;iS;"!default;@;" tabs-bottom-active-gradient;o;	;iS;@;"base-gradient;"base_gradient;
[ o;
;iU;@;
[ ;;;["�/**
 * @var {boolean} $include-tab-highlights
 * Optionally disable all gradients, text-shadows, and box-shadows. Useful for CSS debugging,
 * non-performant browsers, or minimalist designs.
 */o;;iZ;"!default;@;"include-tab-highlights;o;	;iZ;@;"include-highlights;"include_highlights;
[ o;
;i\;@;
[ ;;;["/* Private */o;;i^;0;@;"tabs-top-height;o:Sass::Script::Operation
;i^:@operand1o;	;i^;@;"global-row-height;"global_row_height;@:@operator:
minus:@operand2o;;i^;@;[ ; ["em;!"
0.8em;f0.80000000000000004 ��;
[ o;;i`;0;@;"tabs-top-icon-size;o;"
;i`;#o;	;i`;@;"tabs-top-height;"tabs_top_height;@;$;%;&o;;i`;@;[ ; ["em;!"
0.6em;f0.59999999999999998 33;
[ o;
;ib;@;
[ ;;;["F/**
 * Includes default tab styles.
 *
 * @member Ext.tab.Bar
 */o:Sass::Tree::MixinDefNode;ig;@;"sencha-tabs;	T;
[u:Sass::Tree::IfNode�[o:Sass::Script::Variable	:
@lineih:@options{ :
@name"include-top-tabs:@underscored_name"include_top_tabs0[o:Sass::Tree::MixinNode:@keywords{ ;ii;@;"sencha-top-tabs:@children[ :@splat0:
@args[ u;(�[o:Sass::Script::Variable	:
@lineik:@options{ :
@name"include-bottom-tabs:@underscored_name"include_bottom_tabs0[o:Sass::Tree::MixinNode:@keywords{ ;il;@;"sencha-bottom-tabs:@children[ :@splat0:
@args[ u;(�[o:Sass::Script::Variable	:
@lineio:@options{ :
@name"include-tabbar-uis:@underscored_name"include_tabbar_uis0[o:Sass::Tree::MixinNode:@keywords{ ;ip;@;"sencha-tabbar-ui:@children[ :@splat0:
@args[	o:Sass::Script::String	;ip;@:
@type:string:@value"
lighto; 	;ip;@;"tabs-light;	"tabs_lighto; 	;ip;@;"tabs-bar-gradient;	"tabs_bar_gradiento; 	;ip;@;"tabs-light-active;	"tabs_light_activeo;
;{ ;iq;@;"sencha-tabbar-ui;[ ;0;[	o;	;iq;@;;;"	darko; 	;iq;@;"tabs-dark;	"tabs_darko; 	;iq;@;"tabs-bar-gradient;	"tabs_bar_gradiento; 	;iq;@;"tabs-dark-active-color;	"tabs_dark_active_coloro;
;{ ;ir;@;"sencha-tabbar-ui;[ ;0;[	o;	;ir;@;;;"neutralo; 	;ir;@;"neutral-color;	"neutral_coloro; 	;ir;@;"tabs-bar-gradient;	"tabs_bar_gradiento:Sass::Script::Funcall;{ ;ir;@;"darken;0;[o; 	;ir;@;"neutral-color;	"neutral_coloro:Sass::Script::Number;ir;@:@denominator_units[ :@numerator_units[ :@original"40;i-o;
;iu;@;
[ ;;;["/* Rules for all tabs */o:Sass::Tree::RuleNode:
@rule["V.x-tab.x-item-disabled span.x-button-label, .x-tab.x-item-disabled .x-button-icon;iv;@:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;iv:@members[o:Sass::Selector::Sequence;.[o:#Sass::Selector::SimpleSequence
;-@�;iv;.[o:Sass::Selector::Class;-@�;iv;["
x-tabo;1;-@�;iv;["x-item-disabled:@sourceso:Set:
@hash{ :@subject0o;0
;-@�;iv;.[o:Sass::Selector::Element	;-@�:@namespace0;iv;["	spano;1;-@�;iv;["x-button-label;2o;3;4{ ;50o;/;.[o;0
;-@�;iv;.[o;1;-@�;iv;["
x-tabo;1;-@�;iv;["x-item-disabled;2o;3;4{ ;50o;0
;-@�;iv;.[o;1;-@�;iv;["x-button-icon;2o;3;4{ ;50;	T;
[o:Sass::Tree::MixinNode;{ ;iw;@;"opacity;
[ ;0;[o;;iw;@;[ ; [ ;!"0.5;f0.5:
@tabsi o;);*[".x-tab.x-draggable;iy;@;+o;,;-" ;iy;.[o;/;.[o;0
;-@9;iy;.[o;1;-@9;iy;["
x-tabo;1;-@9;iy;["x-draggable;2o;3;4{ ;50;	T;
[o;8;{ ;iz;@;"opacity;
[ ;0;[o;;iz;@;@1; [ ;!"0.7;f0.69999999999999996 ff;9i o;);*[".x-tab;i};@;+o;,;-" ;i};.[o;/;.[o;0
;-@U;i};.[o;1;-@U;i};["
x-tab;2o;3;4{ ;50;	T;
[	o:Sass::Tree::PropNode;i~;@;["z-index:@prop_syntax:new;
[ ;9i ;o:Sass::Script::String;@;:identifier;"1o;:;i;@;["-webkit-user-select;;;<;
[ ;9i ;o;=;@;;>;"	noneo;:;i{;@;["overflow;;;<;
[ ;9i ;o;=;@;;>;"visible !importanto;);*[".x-button-label;i};@;+o;,;-" ;i};.[o;/;.[o;0
;-@w;i};.[o;1;-@w;i};["x-button-label;2o;3;4{ ;50;	T;
[o;:;i~;@;["overflow;;;<;
[ ;9i ;o;=;@;;>;"visible !important;9i ;9i ;0;[ o;';i�;@;"sencha-top-tabs;	T;
[o;);*[".x-tabbar.x-docked-top;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@�;i�;.[o;1;-@�;i�;["x-tabbaro;1;-@�;i�;["x-docked-top;2o;3;4{ ;50;	T;
[
o;:;i�;@;["border-bottom-width;;;<;
[ ;9i ;o;=;@;;>;"	.1emo;:;i�;@;["border-bottom-style;;;<;
[ ;9i ;o;=;@;;>;"
solido;:;i�;@;["height;;;<;
[ ;9i ;o;	;i�;@;"global-row-height;"global_row_heighto;:;i�;@;["padding;;;<;
[ ;9i ;o;=;@;;>;"0 .8emo;);*[".x-tab;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@�;i�;.[o;1;-@�;i�;["
x-tab;2o;3;4{ ;50;	T;
[o;:;i�;@;["position;;;<;
[ ;9i ;o;=;@;;>;"relativeo;:;i�;@;["padding;;;<;
[ ;9i ;o:Sass::Script::List	:@separator:
space;i�;@;[o;"
;i�;#o;"
;i�;#o;	;i�;@;"tabs-top-height;"tabs_top_height;@;$;%;&o;;i�;@;[ ; ["em;!"1em;i;@;$:div;&o;
;i�;@;@1; [ ;io;;i�;@;[ ; ["em;!"
0.8em;f0.80000000000000004 ��o;:;i�;@;["height;;;<;
[ ;9i ;o;	;i�;@;"tabs-top-height;"tabs_top_heighto;:;i�;@;["min-width;;;<;
[ ;9i ;o;=;@;;>;"
3.3emu;(�[o:Sass::Script::Variable	:
@linei�:@options{ :
@name"include-border-radius:@underscored_name"include_border_radius0[o:Sass::Tree::MixinNode:@keywords{ ;i�;@;"border-radius:@children[ :@splat0:
@args[o:Sass::Script::Operation
;i�:@operand1o; 	;i�;@;"tabs-top-height;	"tabs_top_height;@:@operator:div:@operand2o:Sass::Script::Number;i�;@:@denominator_units[ :@numerator_units[ :@original"2:@valueio;);*[".x-button-label;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@�;i�;.[o;1;-@�;i�;["x-button-label;2o;3;4{ ;50;	T;
[	o;:;i�;@;["font-size;;;<;
[ ;9i ;o;=;@;;>;"	.8emo;:;i�;@;["line-height;;;<;
[ ;9i ;o;=;@;;>;"
1.2emo;:;i�;@;["text-rendering;;;<;
[ ;9i ;o;=;@;;>;"optimizeLegibilityo;:;i�;@;["-webkit-font-smoothing;;;<;
[ ;9i ;o;=;@;;>;"antialiased;9i o;);*[".x-badge;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@!;i�;.[o;1;-@!;i�;["x-badge;2o;3;4{ ;50;	T;
[o;:;i�;@;["font-size;;;<;
[ ;9i ;o;=;@;;>;".6em !importanto;:;i�;@;["top;;;<;
[ ;9i ;o;=;@;;>;"-0.5em;9i o;);*["&.x-tab-icon;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@=;i�;.[o:Sass::Selector::Parent;-@=;i�o;1;-@=;i�;["x-tab-icon;2o;3;4{ ;50;	T;
[o;:;i�;@;["padding;;;<;
[ ;9i ;o;?	;@;A;i�;@;[o;"
;i�;#o;"
;i�;#o;"
;i�;#o;	;i�;@;"tabs-top-height;"tabs_top_height;@;$;%;&o;;i�;@;[ ; ["em;!"1em;i;@;$;B;&o;
;i�;@;@1; [ ;i;@;$;%;&o;;i�;@;[ ; ["em;!"
0.1em;f0.10000000000000001 ��o;;i�;@;[ ; ["em;!"
0.8em;f0.80000000000000004 ��o;);*[".x-button-icon;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@m;i�;.[o;1;-@m;i�;["x-button-icon;2o;3;4{ ;50;	T;
[o;:;i�;@;["-webkit-mask-size;;;<;
[ ;9i ;o;	;i�;@;"tabs-top-icon-size;"tabs_top_icon_sizeo;:;i�;@;["
width;;;<;
[ ;9i ;o;	;i�;@;"tabs-top-icon-size;"tabs_top_icon_sizeo;:;i�;@;["height;;;<;
[ ;9i ;o;	;i�;@;"tabs-top-icon-size;"tabs_top_icon_sizeo;:;i�;@;["display;;;<;
[ ;9i ;o;=;@;;>;"inline-blocko;:;i�;@;["margin;;;<;
[ ;9i ;o;=;@;;>;"0 autoo;:;i�;@;["position;;;<;
[ ;9i ;o;=;@;;>;"relative;9i o;);*[".x-button-label;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@�;i�;.[o;1;-@�;i�;["x-button-label;2o;3;4{ ;50;	T;
[o;:;i�;@;["margin;;;<;
[ ;9i ;o;=;@;;>;"0o;:;i�;@;["margin-left;;;<;
[ ;9i ;o;=;@;;>;"	.3emo;:;i�;@;["padding;;;<;
[ ;9i ;o;=;@;;>;".1em 0 .2em 0o;:;i�;@;["display;;;<;
[ ;9i ;o;=;@;;>;"inline-blocko;:;i�;@;["position;;;<;
[ ;9i ;o;=;@;;>;"relativeo;:;i�;@;["top;;;<;
[ ;9i ;o;=;@;;>;"
-.4em;9i ;9i ;9i ;9i ;0;[ o;';i�;@;"sencha-bottom-tabs;	T;
[o;);*[".x-tabbar.x-docked-bottom;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@�;i�;.[o;1;-@�;i�;["x-tabbaro;1;-@�;i�;["x-docked-bottom;2o;3;4{ ;50;	T;
[
o;:;i�;@;["border-top-width;;;<;
[ ;9i ;o;=;@;;>;"	.1emo;:;i�;@;["border-top-style;;;<;
[ ;9i ;o;=;@;;>;"
solido;:;i�;@;["height;;;<;
[ ;9i ;o;=;@;;>;"3emo;:;i�;@;["padding;;;<;
[ ;9i ;o;=;@;;>;"0o;);*[".x-tab;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@;i�;.[o;1;-@;i�;["
x-tab;2o;3;4{ ;50;	T;
[u;([o:Sass::Script::Variable	:
@linei�:@options{ :
@name"include-border-radius:@underscored_name"include_border_radius0[o:Sass::Tree::MixinNode:@keywords{ ;i�;@;"border-radius:@children[ :@splat0:
@args[o; 	;i�;@;"tabs-bottom-radius;	"tabs_bottom_radiuso;:;i�;@;["min-width;;;<;
[ ;9i ;o;=;@;;>;"
3.3emo;:;i�;@;["position;;;<;
[ ;9i ;o;=;@;;>;"relativeo;:;i�;@;["padding-top;;;<;
[ ;9i ;o;=;@;;>;"	.2emo;8;{ ;i�;@;"box-orient;
[ ;0;[o;=	;i�;@;;>;"verticalo;);*[".x-button-icon;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@1;i�;.[o;1;-@1;i�;["x-button-icon;2o;3;4{ ;50;	T;
[o;:;i�;@;["-webkit-mask-size;;;<;
[ ;9i ;o;	;i�;@;"tabs-bottom-icon-size;"tabs_bottom_icon_sizeo;:;i�;@;["
width;;;<;
[ ;9i ;o;	;i�;@;"tabs-bottom-icon-size;"tabs_bottom_icon_sizeo;:;i�;@;["height;;;<;
[ ;9i ;o;	;i�;@;"tabs-bottom-icon-size;"tabs_bottom_icon_sizeo;:;i�;@;["display;;;<;
[ ;9i ;o;=;@;;>;"
blocko;:;i�;@;["margin;;;<;
[ ;9i ;o;=;@;;>;"0 autoo;:;i�;@;["position;;;<;
[ ;9i ;o;=;@;;>;"relative;9i o;);*[".x-button-label;i�;@;+o;,;-" ;i�;.[o;/;.[o;0
;-@h;i�;.[o;1;-@h;i�;["x-button-label;2o;3;4{ ;50;	T;
[o;:;i�;@;["margin;;;<;
[ ;9i ;o;=;@;;>;"0o;:;i�;@;["padding;;;<;
[ ;9i ;o;=;@;;>;".1em 0 .2em 0o;:;i�;@;["font-size;;;<;
[ ;9i ;o;=;@;;>;"9pxo;:;i�;@;["line-height;;;<;
[ ;9i ;o;=;@;;>;"	12pxo;:;i�;@;["text-rendering;;;<;
[ ;9i ;o;=;@;;>;"optimizeLegibilityo;:;i�;@;["-webkit-font-smoothing;;;<;
[ ;9i ;o;=;@;;>;"antialiased;9i ;9i ;9i u;($[o:Sass::Script::Variable	:
@linei�:@options{ :
@name"include-default-icons:@underscored_name"include_default_icons0[o:Sass::Tree::MixinNode:@keywords{ ;i�;@;"pictos-iconmask:@children[ :@splat0:
@args[o:Sass::Script::String	;i�;@:
@type:string:@value"bookmarkso;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"downloado;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"favoriteso;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"	infoo;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"	moreo;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"	timeo;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"	usero;
;{ ;i�;@;"pictos-iconmask;[ ;0;[o;	;i�;@;;;"	team;0;[ o;
;i�;@;
[ ;;;["�/**
 * Creates a theme UI for tabbar/tab components.
 *
 *     // SCSS
 *     @include sencha-button-ui('pink', #333, 'matte', #AE537A);
 *
 *     // JS
 *     var tabs = new Ext.tab.Panel({
 *        tabBar: {
 *          ui: 'pink',
 *          dock: 'bottom',
 *          layout: { pack: 'center' }
 *        },
 *        ...
 *     });
 *
 * @param {string} $ui-label The name of the UI being created.
 *   Can not included spaces or special punctuation (used in class names)
 * @param {color} $bar-color Base color for the tab bar.
 * @param {string} $bar-gradient Background gradient style for the tab bar.
 * @param {color} $tab-active-color Background-color for active tab icons.
 *
 * @member Ext.tab.Bar
 */o;';i�;@;"sencha-tabbar-ui;	T;
[o;);*[".x-tabbar-o;	;i�;@;"ui-label;"ui_label;i�;@;	T;
[o;8;{ ;i ;@;"background-gradient;
[ ;0;[o;	;i ;@;"bar-color;"bar_coloro;	;i ;@;"bar-gradient;"bar_gradiento;:;i;@;["border-top-color;;;<;
[ ;9i ;o;;{ ;i;@;"darken;0;[o;	;i;@;"bar-color;"bar_coloro;;i;@;[ ; ["%;!"5%;i
o;:;i;@;["border-bottom-color;;;<;
[ ;9i ;o;;{ ;i;@;"darken;0;[o;	;i;@;"bar-color;"bar_coloro;;i;@;[ ; ["%;!"15%;io;);*[".x-tab;i;@;+o;,;-" ;i;.[o;/;.[o;0
;-@�;i;.[o;1;-@�;i;["
x-tab;2o;3;4{ ;50;	T;
[o;8;{ ;i;@;"color-by-background;
[ ;0;[o;	;i;@;"bar-color;"bar_coloro;;i;@;[ ; ["%;!"40%;i-o;:;i;@;["border-bottom;;;<;
[ ;9i ;o;=;@;;>;"1px solid transparent;9i o;);*[".x-tab-active;i	;@;+o;,;-" ;i	;.[o;/;.[o;0
;-@�;i	;.[o;1;-@�;i	;["x-tab-active;2o;3;4{ ;50;	T;
[o;8;{ ;i
;@;"color-by-background;
[ ;0;[o;	;i
;@;"bar-color;"bar_coloro;;i
;@;[ ; ["%;!"90%;i_o;:;i;@;["border-bottom-color;;;<;
[ ;9i ;o;;{ ;i;@;"lighten;0;[o;	;i;@;"bar-color;"bar_coloro;;i;@;[ ; ["%;!"3%;i;9i o;);*[".x-tab-pressed;i;@;+o;,;-" ;i;.[o;/;.[o;0
;-@';i;.[o;1;-@';i;["x-tab-pressed;2o;3;4{ ;50;	T;
[o;8;{ ;i;@;"color-by-background;
[ ;0;[o;	;i;@;"bar-color;"bar_coloro;;i;@;[ ; ["%;!"	100%;ii;9i ;9i u;(5
[o:Sass::Script::Variable	:
@linei:@options{ :
@name"include-bottom-tabs:@underscored_name"include_bottom_tabs0[o:Sass::Tree::RuleNode:
@rule[".x-tabbar-o; 	;i;@;"ui-label;	"ui_label".x-docked-bottom;i;@:@has_childrenT:@children[o;
;[".x-tab;i;@:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
;@;i;[o:Sass::Selector::Class;@;i;["
x-tab:@sourceso:Set:
@hash{ :@subject0;T;[o:Sass::Tree::MixinNode:@keywords{ ;i;@;"bevel-by-background;[ :@splat0:
@args[o; 	;i;@;"bar-color;	"bar_coloro;
;[".x-button-icon;i;@;o;;" ;i;[o;;[o;
;@/;i;[o;;@/;i;["x-button-icon;o;;{ ;0;T;[o;;{ ;i;@;"mask-by-background;[ ;0;[o; 	;i;@;"bar-color;	"bar_coloro:Sass::Script::Number;i;@:@denominator_units[ :@numerator_units["%:@original"20%:@valueio; 	;i;@;"tabs-bar-gradient;	"tabs_bar_gradient:
@tabsi ;"i o;
;[".x-tab-active;i;@;o;;" ;i;[o;;[o;
;@O;i;[o;;@O;i;["x-tab-active;o;;{ ;0;T;[	o;;{ ;i;@;"background-gradient;[ ;0;[o:Sass::Script::Funcall;{ ;i;@;"darken;0;[o; 	;i;@;"bar-color;	"bar_coloro;;i;@;[ ;["%; "5%;!i
o:Sass::Script::String	;i;@:
@type:identifier;!"recessedo;;{ ;i;@;"bevel-by-background;[ ;0;[o;#;{ ;i;@;"lighten;0;[o; 	;i;@;"bar-color;	"bar_coloro;;i;@;[ ;["%; "10%;!iu:Sass::Tree::IfNode�[o:Sass::Script::Variable	:
@linei :@options{ :
@name"include-tab-highlights:@underscored_name"include_tab_highlights0[o:Sass::Tree::MixinNode:@keywords{ ;i!;@;"box-shadow:@children[ :@splat0:
@args[o:Sass::Script::List	:@separator:
space;i!;@:@value[
o:Sass::Script::Funcall;{ ;i!;@;"darken;0;[o; 	;i!;@;"bar-color;	"bar_coloro:Sass::Script::Number;i!;@:@denominator_units[ :@numerator_units["%:@original"10%;io;;i!;@;[ ;[ ;"0;i o;;i!;@;@;[ ;"0;i o;;i!;@;[ ;["em;"0.25em;f	0.25o:Sass::Script::String	;i!;@:
@type:identifier;"
inseto;
;[".x-button-icon;i$;@;o;;" ;i$;[o;;[o;
;@;i$;[o;;@;i$;["x-button-icon;o;;{ ;0;T;[o;;{ ;i%;@;"background-gradient;[ ;0;[o; 	;i%;@;"tab-active-color;	"tab_active_coloro; 	;i%;@;" tabs-bottom-active-gradient;	" tabs_bottom_active_gradient;"i ;"i ;"i u;(�[o:Sass::Script::Variable	:
@linei+:@options{ :
@name"include-top-tabs:@underscored_name"include_top_tabs0[o:Sass::Tree::RuleNode:
@rule[".x-tabbar-o; 	;i,;@;"ui-label;	"ui_label".x-docked-top;i,;@:@has_childrenT:@children[o;
;[".x-tab-active;i-;@:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i-:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
;@;i-;[o:Sass::Selector::Class;@;i-;["x-tab-active:@sourceso:Set:
@hash{ :@subject0;T;[o:Sass::Tree::MixinNode:@keywords{ ;i.;@;"background-gradient;[ :@splat0:
@args[o:Sass::Script::Funcall;{ ;i.;@;"darken;0;[o; 	;i.;@;"bar-color;	"bar_coloro:Sass::Script::Number;i.;@:@denominator_units[ :@numerator_units["%:@original"5%:@valuei
o:Sass::Script::String	;i.;@:
@type:string;""recessedo;;{ ;i/;@;"color-by-background;[ ;0;[o;;{ ;i/;@;"darken;0;[o; 	;i/;@;"bar-color;	"bar_coloro;;i/;@;[ ; ["%;!"5%;"i
:
@tabsi o;
;[".x-tab;i1;@;o;;" ;i1;[o;;[o;
;@K;i1;[o;;@K;i1;["
x-tab;o;;{ ;0;T;[o;
;[".x-button-icon;i2;@;o;;" ;i2;[o;;[o;
;@[;i2;[o;;@[;i2;["x-button-icon;o;;{ ;0;T;[o;;{ ;i3;@;"mask-by-background;[ ;0;[o; 	;i3;@;"bar-color;	"bar_coloro;;i3;@;[ ; ["%;!"20%;"io; 	;i3;@;"tabs-bar-gradient;	"tabs_bar_gradient;&i ;&i ;&i ;0;[	[o;;@;"ui-label;"ui_label0[o;;@;"bar-color;"bar_color0[o;;@;"bar-gradient;"bar_gradient0[o;;@;"tab-active-color;"tab_active_color0